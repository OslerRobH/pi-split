################################################
# Cyclic Voltametry Worker
# deya.sanchez@gmail.com
################################################
import sys        # reading stdout from subprocess calls
import subprocess 
import gui_defs as ui    # our GUI definitions
import time       # so we can sleep
import os         # so we can rename the results file
import re         # regular expression to strip out numbers
import datetime   # so we can tack on a timestamp to the result file
import socket     # so we can identify the hostname to tack to the result file
import config_sequence as cfg # workers and order of execution
import common as cms    # project-wide flags

from threading import Thread

from kivy.clock import Clock    # to schedule event

# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc

fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

Ps4_Max_Retries = 3 # number of times we try and connect after comms fail

dbg_no_prompt = True

# parameter files: 
# 1. for peak detection: cv_hwpot_params.txt 
# 2. amplification(stabilisation) cv_amp_params.txt

#########################################
# This is our special source thread in 
# charge of the PS4 interface and the
# task that slurps in the measurement data
#########################################
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False 
        self.enable_plot_mode = cms.ReleaseFlag.enable_plot
        self.gpio = hal.octet_gpio
        if cms.gui_geek_mode:
            self.ourcfg = cfg.geek_dict_sys_state_sequence
        else:
            self.ourcfg = cfg.dict_sys_state_sequence
        self.init_Once()
        self.shut_down = False
        print("%s MainWorkerThread(Thread) called" % __file__)


    ##########################
    # variables that we only 
    # set-up once per lifetime
    ##########################
    def init_Once(self):
        self.task_state = MeasStates.initialising   # state machine tracker
        wk_print("state machine -> initialising")
        self.pkcount = 0
        self.NeedPeaks = True   # we only need to calculate peaks for the first measurement in the series
        self.TotalCount = 0     # total number of CV measurements we do before we restart
        # figure out how many times we need to run CV per test
        for ix in range ( len(self.ourcfg) ):
            if self.ourcfg[ix] == 'CV':
                self.TotalCount += 1
        assert ((self.TotalCount % 2) == 0), "Total number of CVs must be even"
        self.StageCnt_I  = self.TotalCount/2 #5    # electrodes:(6,7,8,9,10)number of measurements for the first parameter file, anything above we use the second parameter file
        self.msCount = 0        # keep a count of the number of measurements we do before we re-start this state machine
        self.maxPt = []         # maximum peak data pair
        self.minPt = []         # minimum peak data pair
        self.midPt = 0          # mid point between peaks
        return

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        if self.msCount == self.TotalCount:
            self.init_Once()
        self.msplot_coords = []  # initialise measurement coordinates of our plot
        self.cv_data = []
        self.stop = True        # we start with everything quiet

        self.task_state = MeasStates.initialising   # state machine tracker
        wk_print("state machine -> initialising")
        self.gpio.set_ps4(True)
        self.UserReply = UsrPromptReply.unknown
        self.mono_done = True    # true if our child process has finished
        self.Error_active = False
        self.pw_counter = 0     # please wait counter to show something on the status message
        self.Ps4_retry_count = 0
        # deprecated self.have_version = False

        #plot parameters
        self.MinX = 0.0
        self.MaxX = 0.1
        self.MinY = -0.1
        self.MaxY = 0.1 

        self.msplot_Xmin = self.MinX
        self.msplot_Xmax = self.MaxX
        self.msplot_Ymin = self.MinY
        self.msplot_Ymax = self.MaxY

        self.please_wait = True
        return


    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled:
            self.task_state = MeasStates.exiting
            wk_print("state machine [exiting]")
            return True
        else:
            return False

    ####################################
    # Called to initialise this state,
    # machine, reset it or recover from
    # an error
    ####################################
    def State_Reset(self):
        self.init_vars()
        # make sure there are no wild chimps lurking before we start
        self.KillTheMonkeys(True)
        return

    #########################################
    # state machine that deals with kicking
    # off the measurement
    #########################################
    def run(self):

        # keep running until the application exits
        while not self.cancelled:

            # Initialise this state machine
            #################################
            if self.task_state == MeasStates.initialising:
                self.State_Reset()
                self.task_state = MeasStates.idle
                wk_print("initialising -> idle")

            # Wait for a measurement request
            #################################
            elif self.task_state == MeasStates.idle:
                if not self.stop:
                    self.task_state = MeasStates.rdInParams
                    wk_print("idle -> rdInParams")
                else:    
                    time.sleep(1)


            # Measurement Sequence
            #######################

            # Read in the CV parameters for the measurement
            ###############################################
            # index: 1  2   3      4   5     6   7     8
            #      t_eq 2 E_beg -0.45 vtx1 -0.5 vtx2 -0.5 
            # index:  9     10     11       12     13    14 
            #       E_step 0.314 scan_rate 0.25 num_scans 2 
            elif self.task_state == MeasStates.rdInParams:
                self.cv_param_string = self.RdParameters()
                #debug_tag wk_print(("file params: %s" % self.cv_param_string))
                # move to the next state
                self.task_state = MeasStates.getVersion
                wk_print("rdInParams -> getVersion")

            # Invoke the chimp, and get the version number
            ##############################################
            elif self.task_state == MeasStates.getVersion:
                #debug_tag wk_print(("file contents: %s" % self.cv_param_string))
                #debug_tag wk_print("calling the chimp...")
                chimp_cmd = 'sudo /usr/bin/mono ' + os.getcwd() + '/PSoslerCV.exe ' + self.cv_param_string
                #debug_tag wk_print(chimp_cmd)
                child_retval = None
                self.start_time = datetime.datetime.utcnow()
                try:
                    #debug_tag wk_print("subprocess")
                    self.child = subprocess.Popen([chimp_cmd], 
                                            shell=True, 
                                            stdout=subprocess.PIPE, 
                                            stderr=subprocess.STDOUT,
                                            stdin=subprocess.PIPE,
                                            bufsize=1,
                                            universal_newlines=True)
                    #debug_tag wk_print("opened")
                    self.mono_done = False
                    while self.ChimpRunning():
                        if self.stop:
                            wk_print("stop on getVersion")
                            break
                        if self.monitor_cancel():
                            wk_print("getVersion -> cancel")
                            break
                        rx_data = self.child.stdout.readline()
                        child_retval = self.child.poll()
                        if child_retval is not None:
                            wk_print("exit on getVersion")
                            break
                        if rx_data != '\0':
                            #debug_tag wk_print(rx_data)
                            if rx_data.startswith("CV Measurement Version"):
                                self.chimpVersion = rx_data.rstrip()
                                # deprecated self.have_version = True
                                wk_print(self.chimpVersion)
                                self.task_state = MeasStates.gotVersion
                                wk_print("getVersion -> gotVersion")
                                break
                            if self.detectAssert(rx_data):
                                wk_print("getVersion -> assert")
                                break
                    #while

                except subprocess.CalledProcessError as err:
                    wk_print(err.output)
                    child_retval = ChimpReturns.MeasurementFailed
                except OSError:
                    wk_print("OS error")
                    child_retval = ChimpReturns.Bug
                except ValueError:
                    wk_print("Value error")
                    child_retval = ChimpReturns.Bug

                self.ParseChimpReply(child_retval)

            # Wait for the measurement to start
            ##############################################
            elif self.task_state == MeasStates.gotVersion:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on getVersion")
                        break
                    if self.monitor_cancel():
                        wk_print("gotVersion -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on gotVersion")
                        break
                    if rx_data != '\0':            # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("Measurement started"):
                            self.task_state = MeasStates.ms_CV
                            wk_print("gotVersion -> ms_CV")
                            break
                        if self.detectAssert(rx_data):
                            wk_print("gotVersion -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # CV measurement
            ################################################
            elif self.task_state == MeasStates.ms_CV:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on ms_CV")
                        break
                    if self.monitor_cancel():
                        wk_print("ms_CV -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on ms_CV")
                        break
                    if rx_data != '\0':           # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("index, V, "):
                            self.task_state = MeasStates.start_plotting
                            wk_print("ms_CV -> start_plotting")
                            break
                        if self.detectAssert(rx_data):
                            wk_print("ms_CV -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Plot
            ################################################
            elif self.task_state == MeasStates.start_plotting:
                self.cv_data = []
                data_is_sane = True
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on start_plotting")
                        break
                    if self.monitor_cancel():
                        wk_print("start_plotting -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on start_plotting")
                        break
                    if rx_data != '\0':           # null character
                        #debug_tag wk_print(rx_data)
                        # debug_tag print('.,', end='')
                        if rx_data.startswith("Curve finished") :
                            self.task_state = MeasStates.wait_for_done
                            wk_print("start_plotting -> wait_for_done")
                            break
                        else:
                            pdata_list = self.findNumInByteList(rx_data)

                            if len(pdata_list) == 3:
                                self.please_wait = False
                                # we are expecting the data to arrive in this order
                                # index, V, I,
                                #  0     1  2
                                self.Voltage = float(pdata_list[1])
                                self.Current = float(pdata_list[2])
                                
                                # TODO: check if the data is sane
                                # current < 0.5 then bad
                                # current > 5 then bad
                                # voltage < 0.1 bad
                                # voltage > 0.3 bad

                                # make sure the limits of our plot is within bounds of the arriving data
                                # so long as the data is sane
                                if data_is_sane:
                                    #debug_tag wk_print(("V = %f" % self.Voltage))
                                    #debug_tag wk_print(("I = %f" % self.Current))
                                    self.cv_data.append((self.Voltage,self.Current))

                                    if self.enable_plot_mode:
                                        if self.Voltage > self.MaxX:
                                            self.MaxX = self.Voltage
                                        if self.Voltage < self.MinX:
                                            self.MinX = self.Voltage
                                        if self.Current > self.MaxY:
                                            self.MaxY = self.Current
                                        if self.Current < self.MinY:
                                            self.MinY = self.Current

                                        #stash it where everyone can access it
                                        self.msplot_coords=self.cv_data
                                        self.msplot_Xmin = self.MinX
                                        self.msplot_Xmax = self.MaxX
                                        self.msplot_Ymin = self.MinY
                                        self.msplot_Ymax = self.MaxY

                                    self.have_data = True
                                else:
                                    self.have_data = False
                                    # at this point we may as well bail out there
                                    # is a bug in the system we need to fix
                                    self.task_state = MeasStates.error_Ps4
                                    wk_print("start_plotting -> error_Ps4")
                                    self.KillTheMonkeys(True)
                                    break
                        if self.detectAssert(rx_data):
                            wk_print("start_plotting -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Wait for done
            ################################################
            elif self.task_state == MeasStates.wait_for_done:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on start_plotting")
                        break
                    if self.monitor_cancel():
                        wk_print("wait_for_done -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on wait_for_done")
                        break
                    if rx_data != '\0':           # null character
                        # debug_tag wk_print(rx_data)
                        if self.detectAssert(rx_data):
                            wk_print("wait_for_done -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Find the peaks from our data
            ################################################
            elif self.task_state == MeasStates.get_peaks:
                # maximum peak                
                self.maxPt = max(self.cv_data, key=lambda x:x[1])
                #debug_tag wk_print(("maximum peak  %f" % self.maxPt[1]))
                #debug_tag wk_print(("equivalent voltage %f" % self.maxPt[0]))
                
                # minimum peak
                self.minPt = min(self.cv_data, key=lambda x:x[1])
                #debug_tag wk_print(("minimum peak  %f" % self.minPt[1]))
                #debug_tag wk_print(("equivalent voltage %f" % self.minPt[0]))

                self.midPt = self.calc_midpoint(self.maxPt[0], self.minPt[0])
                self.ourPk_str = ('Os: Max={:.4f}'.format(self.maxPt[0]) + 
                        'V  Min={:.4f}'.format(self.minPt[0]) + 
                        'V Mid point={:.4f}'.format(self.midPt) + 'V')

                if self.StageCnt_I > 0:
                    self.NeedPeaks = True  # we only calculate peaks for the very first electrode measurement
                    self.StageCnt_I -= 1
                else:
                    self.NeedPeaks = False
                self.task_state = MeasStates.upload
                wk_print("get_peaks -> upload")

            # save results to the cloud        
            ####################################################
            elif self.task_state == MeasStates.upload:
                self.Results_to_Dropb()
                if dbg_no_prompt:
                    if 0.19 < self.midPt < 0.31:
                        self.Save_PkFile(True)
                    else:
                        self.Save_PkFile(False)
                    self.task_state = MeasStates.done
                else:
                    # now ask them if they are happy with this measurement
                    self.task_state = MeasStates.askYesorNo
                    wk_print("upload -> askYesorNo")

            # Ask Y/N and Wait for user input
            ################################################
            elif self.task_state == MeasStates.askYesorNo:
                if self.UserReply != UsrPromptReply.unknown:
                    self.task_state = MeasStates.gotReply
                    wk_print("askYesorNo -> gotReply")
                elif self.monitor_cancel():
                    wk_print("askYesorNo -> cancel")
                else:
                    time.sleep(1)

            # User reply arrived, process it
            ################################################
            elif self.task_state == MeasStates.gotReply:
                # Yes
                if self.UserReply == UsrPromptReply.yes:
                    wk_print("received touch yes")
                    self.Save_PkFile(True)
                # No    
                elif self.UserReply == UsrPromptReply.no:
                    wk_print("received touch no")
                    self.Save_PkFile(False)
                self.task_state = MeasStates.done
                wk_print("gotReply -> done")


            # All done!
            ####################################################
            elif self.task_state == MeasStates.done:
                time.sleep(1)

            # Attempt to recover from an error and start again
            ####################################################
            elif self.task_state == MeasStates.recover:
                prev_stop = self.stop
                self.State_Reset()  # reset our state
                # the reset has stomped on the stop value so restore it
                self.stop = prev_stop
                self.task_state = MeasStates.rdInParams # and go try again
                wk_print("recover -> rdInParams")


            # Error states
            ################################################
            elif self.task_state == MeasStates.failed:
# TODO: bodge alert                if not self.Error_active:
#                    self.task_state = MeasStates.recover
#                    wk_print("failed -> recover")
#                else:
#                    time.sleep(1)
                if self.StageCnt_I > 0:
                    self.NeedPeaks = True  # we only calculate peaks for the very first electrode measurement
                    self.StageCnt_I -= 1
                else:
                    self.NeedPeaks = False
                self.task_state = MeasStates.done


            elif self.task_state == MeasStates.error_comms:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("comms error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_hw:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("hw error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_params:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("param error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.chimp_bug:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("chimp bug -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.user_aborted:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("user aborted -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.got_assert:
                # we don't budge from this state unless we are quiting
                self.monitor_cancel()

            elif self.task_state == MeasStates.error_EEPROM:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("EEPROM error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_Ps4:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("NaN error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_cvUnstable:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("unstable -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_ecable:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("error_cable -> recover")
                else:
                    time.sleep(1)

            else:
                # check if the state has changed along the way 
                if self.task_state >= MeasStates.max_state:
                    wk_print("Bug not sure what error state we are in")
                    wk_print(self.task_state)
                    while True:
                        if self.stop or self.monitor_cancel():
                            break


            self.monitor_cancel()

            # Exiting, tidy up
            #############################    
            if self.task_state == MeasStates.exiting:
                break

        #################################################################### 
        # End of while not cancelled loop
        ####################################################################

        if not self.mono_done:
            if self.child.poll() is None:
                ret_value = self.child.stdin.write('Q\n')
                self.child.stdin.flush()
                wk_print(ret_value)
                # schedule a clock that waits for the application to finish
                # if still live after 5 seconds then yank the plug
                killchimp_event = Clock.schedule_once(self.SchKillTheMonkeys, 5)
                while self.child.poll() is not None:
                    if self.stop:
                        rx_data = self.child.stdout.readline()
                        sys.stdout.write(rx_data)
                # if the chimp went away then no need to kill it
                if self.child.poll is not None:
                    Clock.unschedule(killchimp_event)
        
        # wait here until we have tidied up
        if not self.mono_done:
            while self.child.poll() is not None:
                print(',', end='')

        self.shut_down = True
        sys.stdout.flush()
        return

    ################################################################
    # Helpers
    ################################################################
    def RoundUp(self, in_number,  multiple):
        test_num = in_number % multiple
        return in_number if test_num == 0 else in_number + multiple - test_num

           
    def RoundDown(self, in_number, multiple):
        test_num = in_number % multiple
        return (in_number - multiple) if test_num == 0 else in_number - multiple - test_num


    #####################################################
    #calculate the half way point between two values
    #####################################################
    def calc_midpoint(self, i_point_1, i_point_2):
        # shove the values into an array
        list_points = []
        list_points.append(i_point_1)
        list_points.append(i_point_2)

        # now do the maths:
        #           mid_point = { py + [ (px -py)/2 ] }
        # where: px needs to be greater than py
        mid_point = min(list_points) + ( (max(list_points) - min(list_points))/2.0)

        return mid_point


    #####################################################
    # Helper method that saves the calculated mid point 
    # state EDC value which will be used by the EIS 
    # measurement
    #####################################################
    def Save_PkFile(self, i_bUseValue):
        # agreed filename
        # TODO: move this assignement out to a seperate file so 
        # EIS and CV are talking to the same thing when they 
        # import it
        peakfile = ("./EdcPeak%d.txt" % self.pkcount)
        self.pkcount += 1
        strToWrite = '{:.6f}'.format(self.midPt) + '\n'

        # start by deleting any pre-existing files
        # If file exists, delete it ##
        if os.path.isfile(peakfile):
            os.remove(peakfile)

        # now save the file with the Good/Bad flag and the value
        with open(peakfile, "w") as pkf:
            if i_bUseValue:
                pkf.write("good %s" % strToWrite)
            else:
                pkf.write("bad %s" % strToWrite)
            pkf.flush()
            pkf.close()


#        if i_bUseValue:
#            # now update
#            #  the eis-params.txt file, we are only updating the Edc value
#            src_eis_filename = './eis-params.defaults.txt'
#            target_eis_filename = './eis-params.txt'
#            # frist try and open our source file
#            try:
#                src_param_file = open(src_eis_filename)
#            except IOError:
#                # throw a wobboly
#                self.Error_active
#                self.task_state = MeasStates.our_bug # this is our own bug here
#                wk_print("BUG: you forgot the eis parameter files!!")
#            else:
#                with src_param_file:
#                    src_param_str = []
#                    src_param_str = src_param_file.read()
#                    src_paramlist = src_param_str.split()
#                    # now update the EDC value with our peak value
#                    # index 0  1  2   3  4   5
#                    #     t_eq 2 scan 2 E_dc 0.25 ....
#                    src_paramlist[5] = '{:.6f}'.format(self.midPt)
#                    # finally save it to the target file
#                    if os.path.isfile(target_eis_filename):
#                        os.remove(target_eis_filename)
#                    with open(target_eis_filename, "w") as teis:
#                        teis.write(' '.join(src_paramlist))
#                        teis.flush()
#                        teis.close()
#        #input("check eis file then press any key...")

                    
        return

    #####################################################
    # Helper method, detects asserts and puts us in the 
    # correct state
    #####################################################
    def detectAssert(self, TestString):
        if TestString.startswith("Assert Failed"):
            wk_print(TestString)
            #extract the line number
            self.assert_LineNo = self.findNumInByteList(TestString)
            self.task_state = MeasStates.got_assert
            return True
        else:
            return False

    #####################################################
    # Helper method, strips out all the numbers in a
    # string represented by a list of bytes
    #####################################################
    def findNumInByteList(self, TestString):
        return re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", (TestString) )

    #####################################################
    ## Kill any mono stray processes
    #####################################################
    def KillTheMonkeys(self, bforce):
        # check if there are any wild monkeys and kill them all
        if bforce:
            os.system("killall -9 mono")
            self.mono_done = True
        elif not self.mono_done:
            os.system("killall -9 mono")
            self.mono_done = True
        return

    #####################################################
    # Schedulable Forced Kill The Monkeys
    # Note this will only run once
    #####################################################
    def SchKillTheMonkeys(self, dt):
        self.KillTheMonkeys(True)
        return False


    #####################################################
    ## Stop any mono child process
    #####################################################
    def StopTheMonkey(self):
        wk_print("attempt to stop the chimp")
        if not self.mono_done:
            wk_print("chimp not done")
            if self.child.poll() is None:
                wk_print("child not None")
                ret_value = self.child.stdin.write('Q\n')
                self.child.stdin.flush()
                wk_print("Q signal sent")
                wk_print(ret_value)
                while self.child.poll() is not None:
                    rx_data = self.child.stdout.readline()
                    sys.stdout.write(rx_data)
                wk_print("chimp stopped")
                self.mono_done = True
        return


    #####################################################
    ## Test whether we still need to service the subprocess
    #####################################################
    def ChimpRunning(self):
        if not self.stop and not self.mono_done:
            return True
        else:
            return False

    #####################################################
    ## Update the state depending on what the chimp said
    #####################################################
    def ParseChimpReply(self, reply):
        # don't bother parsing when we are about to stop or 
        # when we are going to cancel
        if self.stop or self.cancelled:
            return

        print("CV: ParseChimReply", reply)
        if reply is not None:
            if reply == ChimpReturns.Success:
                wk_print("chimp Success")
                if self.have_data and self.NeedPeaks:
                    self.task_state = MeasStates.get_peaks
                    wk_print("Success -> get_peaks")
                else:
                    self.msCount += 1
                    self.task_state = MeasStates.done
                    wk_print("Success -> initialising")
            elif reply == ChimpReturns.MeasurementFailed:
                wk_print("measurement failed -> failed")
# TODO: bodge alert                 #self.Error_active = True
                self.task_state = MeasStates.failed
            elif reply == ChimpReturns.FailedToConnect:
                wk_print("FATAL ERROR: failed to connect -> error_comms")
                wk_print("going straight to Fail don't pass Go")
                wk_print("######################### ERROR: PS4")
                self.Error_active = True
                self.task_state = MeasStates.error_comms
            elif reply == ChimpReturns.DeviceNotFound:
                wk_print("device not found -> error_hw")
                wk_print("FATAL ERROR: is not connected or it has spannered its license")
                wk_print("going straight to Fail don't pass Go")
                self.Error_active = True
                self.task_state = MeasStates.error_hw
                wk_print("######################### ERROR: PS4")
            elif reply == ChimpReturns.InvalidParameters:
                wk_print(" Invalid parameters -> error_params")
                self.Error_active = True
                self.task_state = MeasStates.error_params
            elif reply == ChimpReturns.UserAborted:
                wk_print(" user aborted -> initialising")
                self.task_state = MeasStates.initialising
            elif reply == ChimpReturns.EEPROM_lost:
                wk_print(" PS4 brains corrupt -> error_EEPROM")
                self.Error_active = True
                self.task_state = MeasStates.error_EEPROM
            elif reply == ChimpReturns.CV_Unstable:
                wk_print(" Unstable -- not sure how we got here -> error_cvUnstable")
                self.Error_active = True
                self.task_state = MeasStates.error_cvUnstable
            elif reply == ChimpReturns.Electrode_fault:
                wk_print(" electrode cable fault -> error_ecable")
                self.Error_active = True
                self.task_state = MeasStates.error_ecable
            elif reply == ChimpReturns.HaveHitAssert:
                wk_print(" have hit assert -> chimp_bug")
                self.Error_active = True
                self.task_state = MeasStates.chimp_bug
            elif reply == ChimpReturns.UnknownError: # 255
                wk_print(" Unknown Error -> our_bug")
                self.Error_active = True
                self.task_state = MeasStates.our_bug
            else:
                wk_print(" Unhandled Error! Bug! -> our_bug chimp reply[%d]" %reply)
                self.Error_active = True
                self.task_state = MeasStates.our_bug
            
            self.mono_done = True

            wk_print(" mono done")
            wk_print (self.child.stdout.read())
            return
        else:
            if not self.mono_done:
                wk_print(" still going...")
            return


    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        return self.msplot_coords
    
    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return self.msplot_Xmin
    def get_PlotMaxX(self):
        return self.msplot_Xmax
    def get_PlotMinY(self):
        return self.msplot_Ymin
    def get_PlotMaxY(self):
        return self.msplot_Ymax

    ###############################
    # returns True if we have hit a 
    # measurement error
    ###############################
    def get_IsError(self):
        return self.Error_active

    ###############################
    # Returns True if we need the
    # user to select Yes or No option
    ###############################
    def get_PromptActive(self):
        if self.task_state == MeasStates.askYesorNo:
            return True
        else:
            return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        assert (self.task_state == MeasStates.askYesorNo), "no active prompt here"
        return ui.index_gui_dict.ixgui_yesno

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    ###############################
    # Returns true if we are idle
    ###############################
    def get_IsPaused(self):
        if ( (self.task_state == MeasStates.idle) and (self.stop) ) or (self.task_state == MeasStates.done):
            return True
        else:
            return False

    ###############################
    # DEPRECATED Returns .net app version
    ###############################
#    def get_Version(self):
#        if self.have_version:
#            verslist = self.chimpVersion.split()
#            return verslist[3]
#        else:
#            return ' '

    ###############################
    # Returns a string reflecting
    # the current measurement state
    ###############################
    def get_StatusMsg(self):
        
        if   self.task_state == MeasStates.initialising :      
            return 'Initialising'
        elif self.task_state == MeasStates.idle         :      
            return 'Idle'
        elif self.task_state == MeasStates.rdInParams   :      
            return 'reading parameters'
        elif self.task_state == MeasStates.getVersion   : 
            return 'reading version'
        elif self.task_state == MeasStates.gotVersion   :
            return self.chimpVersion + '\n                 Connecting...'
        elif self.task_state == MeasStates.ms_CV       :
            return 'Starting CV'
        elif self.task_state == MeasStates.failed       :
            return 'measurement failed'
        elif self.task_state == MeasStates.chimp_bug    :
            return 'have hit assert'
        elif self.task_state == MeasStates.done         :
            if self.msCount == 1:
                return 'CV measurement complete\n' + self.ourPk_str
            else:
                return 'CV measurement complete'
        elif self.task_state == MeasStates.error_comms  :
            return 'failed to connect'
        elif self.task_state == MeasStates.error_hw     :
            return '     HW ERROR!:\nUSB-C PS4/PS4 PSU'
        elif self.task_state == MeasStates.error_params :
            return 'invalid parameters'
        elif self.task_state == MeasStates.our_bug      :
            return 'Unknown Error'
        elif self.task_state == MeasStates.askYesorNo   :
            return self.ourPk_str + '\nWould you like to use this value for EIS?'
        elif self.task_state == MeasStates.gotReply     :
            if self.UserReply == UsrPromptReply.no:
                return 'EIS will use preset value'
            else:
                return 'Saving value for EIS'
        elif self.task_state == MeasStates.user_aborted :
            return 'measurement aborted'
        elif self.task_state == MeasStates.exiting      :
            return 'shutting down'
        elif self.task_state == MeasStates.start_plotting:
            if self.please_wait:
                if self.pw_counter >= 40:
                    self.pw_counter = 0
                else:
                    self.pw_counter += 1
                if self.pw_counter < 10:
                    return 'CV measurement starting\n         Please wait. |'
                elif 10 <= self.pw_counter < 20:
                    return 'CV measurement starting\n         Please wait.  /'
                elif 20 <= self.pw_counter < 30:
                    return 'CV measurement starting\n         Please wait. -- '
                elif 30 <= self.pw_counter <= 40:
                    return 'CV measurement starting\n         Please wait.  \\'
            else:
                return 'V = ' + '{:.4f}'.format(self.Voltage) + ' V\nI = ' + '{:4f}'.format(self.Current) + ' uA'
        elif self.task_state == MeasStates.wait_for_done:
            return 'CV measurement complete'
        elif self.task_state == MeasStates.got_assert:
            return 'Internall error:\n' + self.assert_LineNo[0]
        elif self.task_state == MeasStates.get_peaks:
            return 'Calculating...'
        elif self.task_state == MeasStates.upload:
            #return self.ps4_str + '\n' + self.ourPk_str
            return self.ourPk_str
        elif self.task_state == MeasStates.error_EEPROM:
            self.Error_active = True
            return 'PS EEPROM Error!'
        elif self.task_state == MeasStates.error_Ps4:
            self.Error_active = True
            return 'Error corrupt data!'
        elif self.task_state == MeasStates.error_ecable:
            self.Error_active = True
            return      '      HW ERROR!:\nElectrode connection'
        elif self.task_state == MeasStates.error_cvUnstable:
            self.Error_active = True
            return 'Error CV Unstable!'
        elif self.task_state == MeasStates.recover:
            return 'Resseting'
        else:
            wk_print(" Unhandled state %d" % self.task_state)
            return 'Bug! - get_StatusMsg -'

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        if bReply:
            self.UserReply = UsrPromptReply.yes
            wk_print(" reply set to yes")
        else:
            self.UserReply = UsrPromptReply.no
            wk_print(" reply set to no")
        return

    #############################
    # Clear the current error 
    # to get us back to retry
    #############################
    def set_ClearError(self):
        wk_print(" clearing error")
        self.Error_active = False
        self.stop = False
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        wk_print("continuing...")
        if self.task_state == MeasStates.idle or self.task_state == MeasStates.initialising:
            self.stop = False
        elif self.task_state == MeasStates.done:
            self.task_state = MeasStates.idle
            self.stop = False
        else:
            wk_print("cannot continue in current state %d" %self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        wk_print("cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        wk_print("Stop called state")
        if not self.stop:
            self.stop = True
            self.StopTheMonkey()
            self.init_Once()
            self.task_state = MeasStates.initialising
            wk_print("stop -> initialising")
        else:
            wk_print("stop already in progress")
        return

    ################################################################
    # Read CV parameters
    # -check if we have a parameter file in our current location
    # -otherwise check for a default parameter file in our current 
    # location
    # - if neither of them found then leave the app to set the
    # parameters
    ################################################################
    def RdParameters(self):
        peak_file = ("%s%s" % (cms.CmsFilenames.cv_param_path, cms.CmsFilenames.cv_param_peak))
        stability_file = ("%s%s" % (cms.CmsFilenames.cv_param_path, cms.CmsFilenames.cv_param_stab))

        # check for new parameters
        try:
            # check to se if we are at the peak or stabilisation stage
            if cms.CvDefs.same_param_pk_stab:
                param_file = open(peak_file)
            else:
                if self.NeedPeaks:
                    param_file = open (peak_file)
                else:
                    param_file = open (stability_file)

        except IOError:
            # throw a wobboly
            self.Error_active = True
            self.task_state = MeasStates.our_bug # this is our own bug here
            wk_print("BUG: you forgot the parameter files!!")
            
            # no file found so check for a defaults file
            try:
                param_file = open (stability_file)
            except IOError:
                # no defaults file either so let the chimp set the values
                wk_print("WARNING: CV parameter file not found")
                return ' '  
            # let the chimp figure it out
            else:
                with param_file:
                    # read in the default parameter file
                    return param_file.read()    
        else:
            with param_file:
                wk_print("parameter file found")
                return param_file.read()    

    #############################################################
    # Creates <measurement ID>_<hostname>_<timestamp> string
    # this gives us a unique filename
    #############################################################
    def Make_unique_str(self, i_mId, i_timestamp):
        res_ts = "{:%Y%m%d%H%M}".format(i_timestamp)  # grab a timestamp
        res_fts = res_ts.replace("/", '')   # remove the forward slashes from the string
        res_host = socket.gethostname()       #grab the hostname
        return i_mId + '_' + res_host + '_' + res_fts #create the unique string 


    #############################################################
    # method that deals with stashing the data to the cloud
    # grab the results file rename it with host and timestamp
    # then shove it up to dropbox
    #############################################################
    def Results_to_Dropb(self):
        # update this version number every time we mess with this file
        txtFileVersion = cms.get_vers_str()
        # rename the results file
        end_time = datetime.datetime.utcnow()
        end_time_tag = self.Make_unique_str(cms.CvDropBox.prefix, end_time)
        newfilename = cms.CvDropBox.path + end_time_tag + cms.CvDropBox.ext
        wk_print("%s -> %s" %(cms.CvDropBox.src_name, newfilename))
        os.rename(cms.CvDropBox.src_name, newfilename)
        # we will append the name, start time and version to the begining of the file
        # start_time_tag = self.Make_unique_str('CV', self.start_time)
        start_time_tag = self.Make_unique_str(cms.CvDropBox.prefix, self.start_time)
        header_tag = cms.RdExpId() + 'Measurement ID: ' + txtFileVersion + ' ' + start_time_tag + '\n'
        
        # append the results if we have them
        with open(newfilename, "a") as resultfile:
            resultfile.write( '\n' + self.ourPk_str )
            resultfile.write('\nMeasurement started: ' + "{:%Y%m%d%H%M}".format(self.start_time) )
            resultfile.write('\nMeasurement ended:   ' + "{:%Y%m%d%H%M}".format(end_time) )
            resultfile.flush()
            resultfile.close()
        # prepend the Measurement ID string
        with open(newfilename, "r+") as resultfile:
            contents = resultfile.read()
            resultfile.seek(0,0)
            resultfile.write(header_tag + contents)
            resultfile.flush()
            resultfile.close()

        return



###################################
# Debug print with worker id
###################################
def wk_print(dbg_string):
    print("CV: %s" % dbg_string)

#########################################
# Background Task States
#########################################
class MeasStates():
    initialising     = 0     # initialising our variables
    idle             = 1     # waiting for a request to start measuring
    rdInParams       = 2     # we have been requested to start
    getVersion       = 3     # read in the version of the .Net subprocess
    gotVersion       = 4     # version obtained
    ms_CV            = 5     # measureing CV
    failed           = 6     # measurement failed
    chimp_bug        = 7
    done             = 8     # task completed
    error_comms      = 9 
    error_hw         = 10
    error_params     = 11
    our_bug          = 12
    askYesorNo       = 13    # ask the user what they want to do now
    gotReply         = 14    # user pressed a button, we have a reply
    user_aborted     = 15
    exiting          = 16    # start shuting down this thread
    start_plotting   = 17
    wait_for_done    = 18 
    got_assert       = 19
    get_peaks        = 20
    upload           = 21
    error_EEPROM     = 22
    error_Ps4        = 23
    error_ecable     = 24     # dodgy electrode cable
    error_cvUnstable = 25
    recover          = 26    # recover from an error and try again

    max_state        = 27

###################################
# Error codes returned by the chimp
# Reference: debugDialogs.cs
# public enum ExitCode : int
###################################
class ChimpReturns:
    Success           = 0
    MeasurementFailed = 1
    FailedToConnect   = 2
    DeviceNotFound    = 3
    InvalidParameters = 4
    UserAborted       = 5
    EEPROM_lost       = 6
    CV_Unstable       = 7
    Electrode_fault   = 8
    HaveHitAssert     = 170   # 0xAA
    UnknownError      = 255   # 0xff
    Bug               = 300

###################################
# User prompt reply
###################################
class UsrPromptReply():
    yes     = 0
    no      = 1
    unknown = 2

# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.gpio.bioc_init()     # initialise the hardware in stand alone mode
    worker.start()

    print("[p] to print status")
    print("[g] to start/continue")
    print("[s] to stop")
    print("[q] to quit")
    print("[e] to clear error")
    print("[y] yes")
    print("[n] no")
    print("[w] to switch electrodes")
    print("[?/h] for help")

    print("selecting dummy electrode")
    worker.gpio.switch_we('ST')
    electrode = 1

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':                  # print status
            print(worker.get_StatusMsg())
            print("Working electrode %d" % electrode)
        elif cmd == 'g':                # go
            worker.set_Continue()
        elif cmd == 'e':                # clear the error
            worker.set_ClearError()
        elif cmd == 'q':                # quit
            worker.set_Cancel()
            break
        elif cmd == 's':                # stop
            worker.Stop()
        elif cmd == 'y':                # yes
            worker.set_Reply(True)
        elif cmd == 'n':                # no
            worker.set_Reply(False)
        elif cmd == 'w':
            if electrode == 1:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE1')
                electrode += 1

            elif electrode == 2:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE2')
                electrode += 1

            elif electrode == 3:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE3')
                electrode += 1

            elif electrode == 4:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE4')
                electrode += 1

            elif electrode == 5:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE5')
                electrode = 9

            elif electrode == 9:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE9')
                electrode += 1

            elif electrode == 10:
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE10')
                electrode += 1

            elif electrode == 11:
                print("selecting DUMMY electrode")
                worker.gpio.switch_we('ST')
                electrode = 1
        elif cmd == '?' or cmd == 'h':
            print("[p] to print status")
            print("[g] to start/continue")
            print("[s] to stop")
            print("[q] to quit")
            print("[e] to clear error")
            print("[y] yes")
            print("[n] no")
            print("[w] to switch electrodes")
            print("[?/h] for help")


if __name__ == "__main__":

    main()

    sys.exit()
