""" Main entry to the API

 Sets up the GUI and manages the overall control of the system.
 Relies on workers/modules implementing the agreed interfaces:

 https://oslerdiagnostics.planio.com/projects/platform/wiki/RPi_API_Interfaces

---
 Debugging Notes;
 To run with the debugger open VC from the command line as follows:
 sudo code --user-data-dir /tmp

 deya.sanchez@gmail.com
"""
import config_sequence as cfgseq  # workers and order of execution
import gui_defs as cgui           # GUI definitions
import sys                        # reading stdout from subprocess calls
import subprocess                 # for the shutdown command on the Pi
import socket                     # so we can identify the hostname to tack to the result file
import common as cms    # for on_dev_pc
import time

from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.garden.graph import Graph, MeshLinePlot
from kivy.clock import Clock

on_dev_pc = cms.DebugFlags.on_dev_pc


class WorkerSm(BoxLayout):

    def __init__(self,):
        super(WorkerSm, self).__init__()
        self.plot = MeshLinePlot(color=[1, 0, 0, 1])
        self.current_state = PsCtrlState.Init   # our current state
        self.enable_plot_mode = cms.ReleaseFlag.enable_plot
        self.init_vars()

        print("GUI: WorkerSm init called")
        return

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.current_state = PsCtrlState.Init   # our current state
        self.sequence_started = False           # True once the Go/Test button gets pressed
        self.worker_to_run = 0                  # the worker count
        self.prev_worker = 0                    # previous worker, keeps track of when we change workers
        self.first_worker = 0                   # first valid worker (i.e. we have code for it)
        self.replied = False
        self.HaveStartedOnce = False         # ensures we only initialise once
        self.shuttingDown = False            # ensures we only go through shut down motions once
        self.maxWorkers = 0
        self.WaitForStop = False
        self.ui_update_event = 0    # scheduled event to update the screen
        self.shutdown_event  = 0    # scheduled event to start shutting down
        self.deadwait_event  = 0    # scheduled event to wait for the workers to die for shutdown
        self.pulplug_event   = 0    # scheduled event that yanks the plug
        self.cancel_pressed  = False
        if cms.gui_geek_mode:
            self.ourcfg = cfgseq.geek_dict_sys_state_sequence
        else:
            self.ourcfg = cfgseq.dict_sys_state_sequence

        # initialise our system state machine sequence
        self.get_SysStateStats()

        return

    ################################################################
    # Update the GUI 
    ################################################################
    def UpdateUiStatus(self, dt):
        # check for the quit event
        if (self.current_state == PsCtrlState.EndGame) and (self.WaitForStop == True) and (self.Get_IsWorkerPaused(self.worker_to_run) == True):
            self.WaitForStop = False
            self.BailOut()
            return True            
        # don't do anything until the user has pressed the Go button
        if self.Get_IsWorkerRunning(self.worker_to_run) == True and self.sequence_started == True: 
            
            # look out for the abort state
            if ( self.Get_IsWorkerRunning(self.worker_to_run) == True ) and ( self.Get_AreAllCancelled() == True ):
                print("GUI: not convinced we ever hit this condition") 
                self.StartShutDown() 
                return True
            else:
                if self.enable_plot_mode:
                    # plot
                    #-----------------
                    graph_limits = self.ids['graph']
                    graph_limits.xmax= self.our_worker.get_PlotMaxX()
                    graph_limits.xmin= self.our_worker.get_PlotMinX()
                    graph_limits.ymin= self.our_worker.get_PlotMinY()
                    graph_limits.ymax= self.our_worker.get_PlotMaxY()
                    graph_limits.x_ticks_major = self.our_worker.get_PlotMaxX()/2.0
                    graph_limits.y_ticks_major = self.our_worker.get_PlotMaxY()/2.0
                    self.plot.points = self.our_worker.get_PlotData()

                    # plot axis labels
                    #-----------------
                    if ( self.prev_worker != self.worker_to_run ):
                        # update the labels
                        graph_limits.xlabel = cms.dict_plotWorker_labels[self.WorkerLUT[self.worker_to_run][Lix.id]][0]
                        graph_limits.ylabel = cms.dict_plotWorker_labels[self.WorkerLUT[self.worker_to_run][Lix.id]][1]
                        self.prev_worker = self.worker_to_run
                        # update step number too
                        self.ids.spare_label.text = str(self.worker_to_run) + '/' + str(self.maxWorkers)

                # Mux labels
                #-----------------
                if self.Get_WkStrId(self.worker_to_run) == 'MUX':
                    self.ids.elec_label.text = self.our_worker.get_StatusMsg()

                # The main status label                
                #----------------------
                if (self.cancel_pressed == True) and (self.WaitForStop == True):
                    self.ids.status_label.text = 'Stopping...'
                else:
                    # don't update the status message if we are running the pause worker, 
                    # this is only for development
                    # look out for the prompt state so we update our buttons accordingly
                    if self.our_worker.get_PromptActive():
                        if not self.replied:
                            self.StateUpdate(PsCtrlState.AskYN)
                    elif self.replied:
                        self.replied = False
                    # also look out for the error state
                    if self.our_worker.get_IsError():
                        self.StateUpdate(PsCtrlState.Error)
                    if self.current_state == PsCtrlState.EndGame: 
                        self.ids.status_label.text = 'Shutting down'
                    # don't update the status message when the pause worker is running
                    elif self.Get_WkStrId(self.worker_to_run) != 'PAUSE':
                        self.ids.status_label.text = self.our_worker.get_StatusMsg()

                # Keep this state machine ticking
                #--------------------------------
                # check if our worker has finished, if it has then start the next one
                if (self.shuttingDown == False) and (self.cancel_pressed == False):
                    self.Check_NextWorker()
                #--------------------------------
        if (self.WaitForStop) and (self.Get_AreWorkersPaused()):
            print("GUI: All Workers paused")
            self.WaitForStop = False
        return True


    ################################################################
    # Start the very first worker in the sequence
    # and set-up a scheduler for the screen update
    ################################################################
    def StartSequence(self):
        # don't do anything until the worker has actually stopped
        if self.WaitForStop:
            return
        self.sequence_started = True
        self.replied = False
        self.StateUpdate(PsCtrlState.Started)     # update our state
        # make sure we only set the scheduler once per boot
        if not self.HaveStartedOnce:
            self.ids.graph.add_plot(self.plot)

        # clear any previous cancel presses
        self.cancel_pressed = False

        # get the measurement going
        print("GUI: StartSequence")
        self.Set_WkRunning(self.worker_to_run) 
        if not self.HaveStartedOnce:
            self.ui_update_event = Clock.schedule_interval(self.UpdateUiStatus, 0.01)
            self.HaveStartedOnce = True
        return



    ########################################################################
    #
    #           GUI Helpers anad housekeeping
    #
    ########################################################################
    # LUT value       Label for Button 1,  Label for Button 2         State
    # Lab development
    #  0 :      (  'START'           ,   'QUIT'         ), 
    #  1 :      (  'INSERT CARTRIDGE',   'QUIT'         ), 
    #  2 :      (  'CONFIRM'         ,   'QUIT'         ), 
    #  3 :      (  'TEST'            ,   'QUIT'         ),
    #  4 :      (  'CANCEL'          ,   'QUIT'         ),
    #  5 :      (  'RESET'           ,   'QUIT'         ),
    #  6 :      (  'YES'             ,   'NO'           ),
    #  7 :      (  'good'            ,   'bye'          ),
    # October demo
    # 0  :      (  'START'            ), # splash screen, initialising
    # 1  :      (  'INSERT CARTRIDGE' ), # after initialisation is complete
    # 2  :      (  'CONFIRM'          ), # marries up with status messages
    # 3  :      (  'TEST'             ), # once blood sample is ready
    # 4  :      (  'CANCEL'           ), # to abort at any stage
    # 5  :      (  'RESET'            ), # in case of system error
    # 6  :      (  'bug 6'            ), # you shouldn't be calling these
    # 7  :      (  'bug 7'            ), # you shouldn't be calling these


    ################################################################
    # Go Button press handler
    # callback function that we use when the button gets pressed
    ################################################################
    def GoPressed(self):
        #debug_tag
        print("GUI: button 1 pressed")  # test
        sys.stdout.flush()
        if self.current_state   == PsCtrlState.Init:     # Start
            self.StartSequence()
            print("GUI: START pressed")

        elif ( (self.current_state == PsCtrlState.Started) or  # Stop
               (self.current_state == PsCtrlState.Stop) ):     # Stop
            self.cancel_pressed = True
            self.StopNreset()
            if cms.DebugFlags.flight_mode:
                quit()
            print("GUI: STOP pressed")

        elif self.current_state == PsCtrlState.AskYN:    # Yes
            print("GUI: YES pressed")
            self.our_worker.set_Reply(True)
            self.replied = True
            self.StateUpdate(PsCtrlState.Stop)

        elif self.current_state == PsCtrlState.Error :   # Reset
            self.cancel_pressed = False
            self.our_worker.set_ClearError()
            self.StateUpdate(PsCtrlState.Started) 
            print("GUI: reset pressed")

        elif self.current_state == PsCtrlState.EndGame: # button disabled
            print("GUI: Btn1 bye state")
        else:
            print("GUI: Bug! GoPressed")
        sys.stdout.flush()
        return

    ################################################################
    # Stop Button press handler
    # callback function that we use when the button gets pressed
    ################################################################
    def StopPressed(self):
        #debug_tag
        print("GUI: button 2 pressed")  # test
        sys.stdout.flush()
        if ( (self.current_state == PsCtrlState.Init)    or # Quit   
             (self.current_state == PsCtrlState.Started) or # Quit  
             (self.current_state == PsCtrlState.Stop)    or # Quit  
             (self.current_state == PsCtrlState.Error) ):   # Quit
            print("GUI: QUIT pressed")
            if not self.cancel_pressed:
                self.cancel_pressed = True
                self.StopNreset()
            self.StartShutDown() 

        elif self.current_state == PsCtrlState.AskYN   :  # 2 No
            print("GUI: NO pressed")
            self.our_worker.set_Reply(False)
            self.replied = True
            self.StateUpdate(PsCtrlState.Init)

        elif self.current_state == PsCtrlState.EndGame:  # 5 Bye
            print("GUI: quit bye")
        else:
            print("GUI: Bug! StopPressed")
        sys.stdout.flush()
        return

    ################################################################
    # Update GUI related stuff depending on our new state
    ################################################################
    def StateUpdate(self, new_state):
        if ( new_state != self.current_state ):
            print("GUI: Gui [%s] ->[%s]" % (dict_strState[self.current_state], dict_strState[new_state]) )
            self.current_state = new_state
            if new_state == PsCtrlState.AskYN:
                # get the flavour of prompt from the worker
                index = self.our_worker.get_PromptType()
                self.ids.Go_button.text   = cgui.dict_gui_usr_prompt_lab[index][0]
                self.ids.Stop_button.text = cgui.dict_gui_usr_prompt_lab[index][1]
            else: 
                if self.cancel_pressed:
                    self.ids.Go_button.text = 'Continue'
                else:               
                    self.ids.Go_button.text   = dict_Btns[self.current_state][0]
                self.ids.Stop_button.text = dict_Btns[self.current_state][1]
        return




    #################################################################################
    # Shut Down Sequence Helpers
    #################################################################################

    #########################################
    # Call this first
    # Method to start shutting down
    #########################################
    def StartShutDown(self):
        if not self.shuttingDown:
            self.shuttingDown = True 
            self.StateUpdate(PsCtrlState.EndGame)       # no more key presses allowed
            #if we never actually did anything then shortcut to exit
            if not self.HaveStartedOnce:
                self.PullThePlug(0)
            else:
                self.StopAllWorkers()
                # in case our current worker does not stop schedule switch
                # off regardless
                self.shutdown_event = Clock.schedule_once(self.ShuttingDown, 15)
        return

    #########################################
    # Scheduled event that executes in case
    # our current worker fails to stop. 
    # This is scheduled by the StartShutDown 
    # method
    #########################################
    def ShuttingDown(self, dt):
        self.BailOut()
        return False    # we should only need to do this once

    #########################################
    # Called once or worker has stopped.
    # May also be called by a shceduled 
    # event ShuttingDown() if the worker 
    # failed to stop
    # Method to kill all our workers 
    # and schedule power off
    #########################################
    def BailOut(self):
        # if there is a schedule event to call us then cancel that now
        if (self.shutdown_event != 0 ):
            self.shutdown_event.cancel()
        self.Set_WkCancelAll()              # tell all our workers to Harakiri
        print("GUI: cancelling screen updates") 
        self.ui_update_event.cancel()
        # schedule a clock that shuts off regardless just in case 
        # a worker hangs while trying to shutdown
        print("GUI: Pulling the plug in 15 seconds..")
        self.pulplug_event = Clock.schedule_once(self.PullThePlug, 15)
        # now schedule shutting down nicely
        print("GUI: wait for the death of workers")
        self.deadwait_event = Clock.schedule_interval(self.WaitForDeath, 0.5)
        return


    #########################################
    # Method that waits for all our workers
    # to die before yanking the plug
    #########################################
    def WaitForDeath(self, dt):
        workers_alive = 0
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.Get_WkExists(ix)):
                the_worker = self.Get_WkInstance(ix)
                if the_worker.is_alive() :
                    workers_alive += 1
        if workers_alive == 0:
            print("GUI: All workers are dead")
            # cancel the back up event
            self.pulplug_event.cancel()
            self.PullThePlug(0)
            return False    # this unschedules the event
        return True

    #########################################
    # Method that actually yanks the plug
    #########################################
    def PullThePlug(self, dt):
        print("GUI: Pulling the plug")
        working_on_dev_pc = on_dev_pc
        # so we don't accidentally switch off the PC while developing
        if socket.gethostname() == 'obeeone':   # Deya's dev pc hostname
            print("GUI: may the force be with you")
            working_on_dev_pc = True
        if not working_on_dev_pc:          # anyone else will be powered off
            print("shutting down...")
            sys.stdout.flush()
            bailoutcmd = "sudo /sbin/shutdown -h now"
            #bailoutcmd = "sudo /sbin/shutdown"
        if not working_on_dev_pc:
            subprocess.Popen(bailoutcmd.split())
        # shouldn't get here
        quit()
        return False    # just to be tidy

    #################################################################################
    # Current Worker Helpers
    #################################################################################


    #########################################
    # Method to stop and reset all our workers
    #########################################
    def StopNreset(self):
        print("GUI: StopNreset")
        self.StopAllWorkers()
        # we then go back to the start so we can re-start
        print("GUI: go back to the start")
        self.StateUpdate(PsCtrlState.Init)
        return

    #################################################################################
    # System Control Helpers
    #################################################################################

    #####################################
    # Get our system state control stats
    # TODO: refactor so we don't have to 
    # keep track of duplicates
    #####################################
    def get_SysStateStats(self):
        # determine the number of workers including duplicates
        self.maxWorkers = len(self.ourcfg)
        print("GUI: num of workers in our sequence %d" % self.maxWorkers)

        # now create a matrix to keep track of live workers
        # worker index, instance, exists?, started?, running? 
        self.WorkerLUT = {}
        # by default everything should be off until invoked
        # hence nothing is currently running
        wrunning = False
        # and nothing has been cancelled
        wcancelled = False
        # and everything is paused
        wpaused = True
        # scan through our System State Machine Worker Sequence to populate our LUT
        # entries will be populated as follows:
        #
        # index     = string ID of the worker
        # instance  = instantiation of this thread
        # exists    = true if it has been coded up
        # running   = true if it has been started
        # paused    = true if it has been started and currently paused
        # cancelled = true if it was started and then cancelled
        #
        #  ix   [index,       instance, wexists, wrunning, wpaused, wcancelled]
        # {0:   ['HwInit',    0,        False,   False,    True,    False     ], 
        #  1:   ['Cartridge', 0,        False,   False,    True,    False     ],
        # etc...
        for ix in range(self.maxWorkers):
            index = self.ourcfg[ix]
            #debug_tag print("GUI: index = %s\n" % index)
            # check if the worker has been cooked, otherwise don't attempt to run it
            if ( cfgseq.dict_funcp_ssWorkers[index] == 'not_yet' ):
                wexists = False
            else:
                wexists = True
            self.WorkerLUT[ix]=[index, 0, wexists, wrunning, wpaused, wcancelled]
        # now do a second pass to save instantiations
        for ix in range(self.maxWorkers):
            if (self.Get_WkExists(ix)) and (self.Get_WkInstance(ix) == 0):
                wk_instance = cfgseq.dict_funcp_ssWorkers[self.WorkerLUT[ix][Lix.id]].MainWorkerThread()
                self.Set_Instance(ix,wk_instance)
        #debug_tag print(self.WorkerLUT)
        # Now set our first worker to the first existing one in our LUT 
        for ix in range(self.maxWorkers):
            if ( self.Get_WkExists(ix)):
                self.worker_to_run = ix
                self.first_worker = ix # back up this value so we can restart
                print("GUI: starting worker set to %s" % self.Get_WkStrId(ix) )
                break
        # --- debug section
        # scan through each worker print the function pointer
        #debug_tag print("GUI: here\n")
        #debug_tag print( cfgseq.dict_funcp_ssWorkers[self.WorkerLUT[22][0]] )
        #debug_tag print("GUI: \nhere\n")
        return

    #########################################
    # Method to stop the current worker
    #########################################
    def StopAllWorkers(self):
        print("GUI: Stopping workers...")
        # this will take a while to trickle through the state machines
        # and reflect its state, this is no bad thing as the gui should
        # be able to show progress of stopping the active worker
        # until then block the user from pressing the start button again
        self.WaitForStop = True

        unique_wk_instances = []    # so we only stop each unique instance once

        # go through our list of workers and stop all the ones that
        # are running
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)
                if worker not in unique_wk_instances:
                    print("GUI: Stopping %s" % self.Get_WkStrId(ix))
                    unique_wk_instances.append(worker)
                    worker.Stop()

        return 

    ################################################################
    # Set the instance on all occurrances of this worker
    ################################################################
    def Set_Instance(self, i_workerId, i_instance):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker 
        # as started and running
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # only do this once for each unique worker
                if self.Get_WkInstance(ix) == 0:
                    # we have a match so save the instance value
                    self.WorkerLUT[ix][Lix.instance] = i_instance
        return

    ################################################################
    # Start the worker thread running
    # if it has already started and is currently paused then
    # make it continue
    ################################################################
    def Set_WkRunning(self, i_workerId):
        # make sure the worker exists
        assert (self.Get_WkExists(i_workerId)), ("this worker does not exist %d %s" % i_workerId % self.Get_WkStrId(i_workerId))
        # check if this worker has already been started
        if not self.WorkerLUT[i_workerId][Lix.running]:
            #debug_tag print("GUI: worker not running")
            # get it running
            self.our_worker = self.Get_WkInstance(i_workerId)  # invokes worker method MainWorkerThread
            self.our_worker.start()                                     # invokes worker method run()
            # save the new state of this worker in our LUT
            self.Set_RunNFlagWorker(i_workerId, True)
            # now kick it and tell it to get on with it
            # give it a chance to initialise
            time.sleep(1)
            self.our_worker.set_Continue()
        elif ( (self.Get_IsWorkerRunning(i_workerId)) and (self.Get_IsWorkerPaused(i_workerId)) ):
            # ensure our worker is up to date
            if self.our_worker != self.Get_WkInstance(i_workerId):
                self.our_worker = self.Get_WkInstance(i_workerId)
            # wake up this worker
            self.our_worker.set_Continue()
            self.Set_RunNFlagWorker(i_workerId, True)
        # update the worker that has focus
        self.worker_to_run = i_workerId
        print("GUI worker running %s" % self.Get_WkStrId(i_workerId))
        return

    
    ################################################################
    # Get all our workers to shutdown nicely because we are just 
    # about to power off
    ################################################################
    def Set_WkCancelAll(self):
        # go through our list of workers and cancel all the ones that
        # are running

        unique_wk_instances = []    # so we only stop each unique instance once

        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)
                if worker not in unique_wk_instances:
                    unique_wk_instances.append(worker)
                    worker.set_Cancel()
                self.Set_CancelFlagWorker(ix)


    ################################################################
    # Set the cancel flag for all occurances of this worker
    # in our state machine
    ################################################################
    def Set_CancelFlagWorker(self, i_workerId):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker 
        # as cancelled
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so set the flags
                self.WorkerLUT[ix][Lix.cancelled] = True
                #debug_tag print("GUI: ", self.Get_WkStrId(ix), " [", ix, "] cancelled")
        return

    ################################################################
    # Set/clear the running flag for all occurances of this worker
    # in our state machine
    ################################################################
    def Set_RunNFlagWorker(self, i_workerId, i_bRunState):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker 
        # as started and running also clear the paused flag
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so set the flags
                self.WorkerLUT[ix][Lix.running] = i_bRunState
                if i_bRunState:
                    self.WorkerLUT[ix][Lix.paused] = False
                else:
                    self.WorkerLUT[ix][Lix.paused] = True
                #debug_tag print("GUI: ", self.Get_WkStrId(ix), " [", ix, "] is running")
        return

    ################################################################
    # Check if our worker is running
    # returns true if the worker is running, false if it has not started
    # or if it does not exist
    #  
    # ***Note*** that a worker may be running but may be paused.
    #  
    ################################################################
    def Get_IsWorkerRunning(self, i_workerId):
        if not self.Get_WkExists(i_workerId):
            return False
        return self.WorkerLUT[i_workerId][Lix.running]


    ################################################################
    # Check if our worker is paused
    # returns true if the worer is paused (or does not exist), 
    # false if active
    ################################################################
    def Get_IsWorkerPaused(self, i_workerId):
        # first check if it exists and is running 
        if (self.Get_WkExists(i_workerId)) and (self.WorkerLUT[i_workerId][Lix.running]):
            # grab the instance 
            the_worker = self.Get_WkInstance(i_workerId)
            current_state = the_worker.get_IsPaused()
        else:
            print("GUI: worker not available [ %s ]" % self.Get_WkStrId(i_workerId))
            return True
        worker_Id = self.Get_WkStrId(i_workerId)
        # scan through our LUT and update the state of this worker
        for ix in range(self.maxWorkers):
            if worker_Id == self.Get_WkStrId(ix):
                # we have a match so check the flags
                self.WorkerLUT[ix][Lix.paused] = current_state
        return current_state

    ################################################################
    # Check if all our workers are paused
    ################################################################
    def Get_AreWorkersPaused(self):
        # hope for the best
        status = True
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)              # get the worker instance
                wkPauseState = worker.get_IsPaused()          # get the current state of pause
                self.WorkerLUT[ix][Lix.paused] = wkPauseState # update our flag
                if not wkPauseState:
                    status = False
        return status
    
    ################################################################
    # Test whether a worker that was once running has now been 
    # cancelled
    ################################################################
    def Get_IsWorkerCancelled(self, i_workerId):
        # first check whether it is or was once running
        if (self.WorkerLUT[i_workerId][Lix.running]) and (self.Get_WkExists(i_workerId)):
            the_worker = self.Get_WkInstance(i_workerId)
            current_state  =  the_worker.get_Cancelled()
        else:
            return False
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and update the state of this worker
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so check the flags
                self.WorkerLUT[ix][Lix.cancelled] = current_state
        return current_state

    ################################################################
    # Test whether all the workers have been told to cancel
    ################################################################
    def Get_AreAllCancelled(self):
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.Get_WkExists(ix)):
                the_worker = self.Get_WkInstance(ix)
                if not the_worker.get_Cancelled() :
                    return False
        return True

    ################################################################
    # Get the worker instance
    ################################################################
    def Get_WkInstance(self, i_workerId):
        #assert (self.Get_WkExists(i_workerId), ("this worker does not exist %d %s" % i_workerId % self.Get_WkStrId(i_workerId))
        return self.WorkerLUT[i_workerId][Lix.instance]

    ################################################################
    # Get the worker string id
    ################################################################
    def Get_WkStrId(self, i_workerId):
        return self.WorkerLUT[i_workerId][Lix.id]

    ################################################################
    # Test if the worker exists
    ################################################################
    def Get_WkExists(self, i_workerId):
        return self.WorkerLUT[i_workerId][Lix.exists]

    ################################################################
    # Get the next worker
    ################################################################
    def Get_NextWorker(self, i_workerId):
        #debug_tag print("GUI: Entry: Get_NextWorker(%d)" % i_workerId)
        # if we are at the last worker then go back to the start
        next_worker = i_workerId + 1

        if next_worker == self.maxWorkers:
            print("GUI: max workers = %d" % self.maxWorkers)
            return self.first_worker

        for ix in range(next_worker, self.maxWorkers):
            if self.Get_WkExists(ix):
                #debug_tag print("GUI: Next worker is %s" % self.Get_WkStrId(ix))
                return ix
            else:
                print("GUI: skip [%s]" % self.Get_WkStrId(ix))
        # if we are here it means that we got to the end of the list and the 
        # worker didn't exist so back to the start
        return self.first_worker

    ################################################################
    # Test whether our current worker has done its job and the next
    # one needs to be started. If so then kick that off.
    ################################################################
    def Check_NextWorker(self):
        #debug_tag print("GUI: Entry: Check_NextWorker()")
        # don't run this method if we are about to shut down
        if not self.shuttingDown:
            if self.Get_IsWorkerPaused(self.worker_to_run):
                print("GUI: %s is done" % self.Get_WkStrId(self.worker_to_run) )
                # our current worker has nothing more to do so get the next one going
                next_worker = self.Get_NextWorker(self.worker_to_run)
                #debug_tag print("GUI: %s next worker" % self.Get_WkStrId(next_worker))
                # if we've gone back round the loop then reset the gui state machine
                # but don't start the worker until the user presses the button
                if next_worker == self.first_worker:
                    self.sequence_started = False
                    print("GUI: back round to %s worker" % self.first_worker)
                    self.worker_to_run = next_worker
                    # tell everyone to go back to the initial state
                    self.StopAllWorkers()
                    self.StateUpdate(PsCtrlState.Init)
                    return
                else:
                    print("GUI: kicking off next worker [%s]" % self.Get_WkStrId(next_worker))
                # Update the worker to run and kick it off
                    self.Set_WkRunning(next_worker)
        return


########################################################################
# Helpers
########################################################################
###################################
# Our time constants for the sleep
# method call
# units are micro seconds
###################################
class cTime():
    milliS = 1000

###################################
# Worker LUT indeces
# self.WorkerLUT[ix]=[index, wexists, wrunning, wpaused]
###################################
class Lix():
    id        = 0   # string that identifies the worker
    instance  = 1   # instantiation pointer
    exists    = 2   # has this worker been coded
    running   = 3   # has this worker been started
    paused    = 4   # has this worker paused
    cancelled = 5   # has this worker died

###################################
# Our WorkerSm states
###################################
class PsCtrlState():
    Init   =  0 # nothing has been pressed
    Started=  1 # Start has been pressed
    AskYN  =  2 # waiting for user input
    Stop   =  3 # stop everythig
    Error  =  4 # something has gone wrong
    EndGame = 5 # buttons disabled, we are shutting down

###################################
# Debug string of Our WorkerSm states
###################################
dict_strState = {
    0 : ('Init   '),   # nothing has been pressed
    1 : ('Started'),   # Start has been pressed
    2 : ('AskYN  '),   # waiting for user input
    3 : ('Stop   '),   # stop everythig
    4 : ('Error  '),   # something has gone wrong
    5 : ('EndGame'),   # buttons disabled, we are shutting down
    
}

###############################################
# buttons state string look up table
##############################################
dict_Btns = {
# PsCtrlState value       Label for Button 1,                                              Label for Button 2                                                      State
    0 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_start][0]   ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_start][1]     ), # PsCtrlState.Init
    1 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_cancel][0]  ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_cancel][1]    ), # PsCtrlState.Started
    2 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_yesno][0]   ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_yesno][1]     ), # PsCtrlState.AskYN
    3 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_cancel][0]  ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_cancel][1]    ), # PsCtrlState.Stop
    4 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_reset][0]   ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_reset][1]     ), # PsCtrlState.Error
    5 :         (  cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_bye][0]     ,   cgui.dict_gui_usr_prompt_lab[cgui.index_gui_dict.ixgui_bye][1]       ), # PsCtrlState.EndGame
}

########################################
# Kivy workhorse
########################################
class Ps4IfaceCtrl(App):
    def build(self):
        print("GUI: Ps4IfaceCtrl(App) called")
        return Builder.load_file("geek.kv")

    def GetVersion(self):
        ##
        # version string wrapper
        #
        return cms.get_vers_str()


#########################################
# Main Entry to this module
# initialise variables
# set-up our plotting data thread
# start up the gui
#########################################
if __name__ == "__main__":

    Ps4IfaceCtrl().run()
    
    print('main Exit')
    sys.stdout.flush()


# EOF
