##
# @package config_sequence
#
# Worker Configuration for the System Controller
# Defines our workers and order of execution
#
#
# deya.sanchez@gmail.com
#
import common
import cartridge_motor # DC motor
import hw_init         # put the hardware in a safe state
import fluidics        # like it says on the tin
import eis_measure     # Electrochemical Impedance Spectroscopy worker
import cv_measure      # Cyclic Voltametry worker
import ps_mux          # multiplexer worker
import pause_worker    # for development integration


##
# Define what test we want to run
#
class DevelConfigs():
    dev_setup_quick      = 0
    dev_setup_full       = 1
    dev_setup_9_to_10    = 2
    dev_setup_1_to_4     = 3
    dev_setup_1_to_5     = 4


if common.calibrate_fluidics or common.do_quick_test: # for debug, development and testing
    dev_local_config = DevelConfigs.dev_setup_quick
else:
    dev_local_config = DevelConfigs.dev_setup_1_to_5  # use this one for demos

##
# Dictionary that lists the sequence of events/workers
# sequence based on state machine
# https://oslerdiagnostics.planio.com/projects/platform/wiki/System_State_Machine
#
#
# place holders for workers that have not been cooked yet
# comment out the ones that are ready, example:
# hw_init     = 'not_yet' # initialise the hardware to a safe state, flag up any ADC detected problems
#
#
# Dictionary of function pointers for our workers in our system state machine
# would be invoked like so:
#
# self.our_worker_thread = dict_funcp_ssWorkers['OCP].MainWorkerThread()
#
dict_funcp_ssWorkers = {
    # worker ID   thread function/class
    'HWinit'    : hw_init        , # initialise the hardware
    'Cartridge' : cartridge_motor, # waits for a cartridge and prepares it for fluidics, also ejects it
    'Fluidics'  : fluidics       , # Fluidics
    'CV'        : cv_measure     , # Cyclic Voltametry measurement
    'EIS'       : eis_measure    , # Electrochemical Impedance Spectroscopy
    'MUX'       : ps_mux         , # switch to another electrode
    'PAUSE'     : pause_worker   , # for general pausing during integration development
}

##
# Electrodes 9 & 10 with quick fluidics
#
if dev_local_config == DevelConfigs.dev_setup_quick:

    # sequence of measurements
    # -------------------------
    dict_sys_state_sequence = {
        # Sequence
        # Counter        worker ID
        0        :     'HWinit'   ,  # initialise the hardware
        1        :     'Cartridge',
        2        :     'Fluidics' ,  # first mix
        3        :     'MUX'      ,  # switch to electrode
        4        :     'CV'       ,
        5        :     'MUX'      ,  # switch to electrode
        6        :     'CV'       ,
        7        :     'Fluidics' ,  # second mix
        8        :     'MUX'      ,  # switch to electrode
        9        :     'CV'       ,
        10       :     'EIS'      ,
        11       :     'MUX'      ,  # switch to electrode
        12       :     'CV'       ,
        13       :     'EIS'      ,
        14       :     'Fluidics' , # release pressure
        15       :     'Cartridge', # open chamber
    }

    # Multiplexer - electrodes
    # ------------------------
    dict_mux_sequence = {
        # sequence
        # index     name
        0  :    'ST'  ,
        1  :    'WE3' ,
    }

    # Fluidics
    fluidcfg = 'v15'

    # eta
    test_time = (9 * 60) + 27   # 9.27 minutes


##
# The full monty electrodes 1-10
# slow fluidics
#
elif dev_local_config == DevelConfigs.dev_setup_full:

    dict_sys_state_sequence = {
        
        # Sequence
        # Counter        worker ID
        0        :      'HWinit'    ,  # initialise the hardware
        1        :      'Cartridge' ,  # prepare the cartridge
        2        :      'Fluidics'  ,  # first mix
        3        :      'MUX'       ,  # electrode  1
        4        :      'CV'        ,
        5        :      'MUX'       ,  # electrode  2
        6        :      'CV'        ,
        7        :      'MUX'       ,  # electrode  3
        8        :      'CV'        ,
        9        :      'MUX'       ,  # electrode  4
        10        :      'CV'        ,
        11        :      'MUX'       ,  # electrode  5
        12        :      'CV'        ,
        13        :      'MUX'       ,  # electrode  6
        14        :      'CV'        ,
        15        :      'MUX'       ,  # electrode  7
        16        :      'CV'        ,
        17        :      'MUX'       ,  # electrode  8
        18       :      'CV'        ,
        19       :      'MUX'       ,  # electrode  9
        20       :      'CV'        ,
        21       :      'MUX'       ,  # electrode  10
        22       :      'CV'        ,
        23       :      'Fluidics'  ,  # second mix
        24       :      'MUX'       ,  # electrode  1
        25       :      'CV'        ,
        26       :      'EIS'       ,
        27       :      'MUX'       ,   # electrode 2
        28       :      'CV'        ,
        29       :      'EIS'       ,
        30       :      'MUX'       ,   # electrode 3
        31       :      'CV'        ,
        32       :      'EIS'       ,
        33       :      'MUX'       ,   # electrode 4
        34       :      'CV'        ,
        35       :      'EIS'       ,
        36       :      'MUX'       ,   # electrode 5
        37       :      'CV'        ,
        38       :      'EIS'       ,
        39       :      'MUX'       ,   # electrode 6
        40       :      'CV'        ,
        41       :      'EIS'       ,
        42       :      'MUX'       ,   # electrode 7
        43       :      'CV'        ,
        44       :      'EIS'       ,
        45       :      'MUX'       ,   # electrode 8
        46       :      'CV'        ,
        47       :      'EIS'       ,
        48       :      'MUX'       ,   # electrode 9
        49       :      'CV'        ,
        50       :      'EIS'       ,
        51       :      'MUX'       ,   # electrode 10
        52       :      'CV'        ,
        53       :      'EIS'       ,
        54       :      'Fluidics'  ,   # release pressure
        55       :      'Cartridge' ,   # open chamber
    }
    # Multiplexer - electrodes
    # ------------------------
    dict_mux_sequence = {
        # sequence
        # index     name
        0        : 'WE1' ,
        1        : 'WE2' ,
        2        : 'WE3' ,
        3        : 'WE4' ,
        4        : 'WE5' ,
        5        : 'WE6' ,
        6        : 'WE7' ,
        7        : 'WE8' ,
        8        : 'WE9' ,
        9        : 'WE10',
    }

    # Fluidics
    fluidcfg = 'october'

    # eta
    test_time = 28 * 60   # 28 minutes


##
# Electrodes 9 & 10 slow fluidics
# dev_setup_9_to_10
#
elif dev_local_config == DevelConfigs. dev_setup_9_to_10:

    # sequence of measurements
    # -------------------------
    dict_sys_state_sequence = {# Sequence
        # Counter        worker ID
        0       :       'HWinit'   ,  # initialise the hardware
        1       :       'Cartridge',
        2       :       'Fluidics' ,  # first mix
        3       :       'MUX'      ,  # switch to electrode 9  setting
        4       :       'CV'       ,
        5       :       'MUX'      ,  # switch to electrode 10  setting
        6       :       'CV'       ,
        7       :       'Fluidics' ,  # second mix
        8       :       'MUX'      ,  # switch to electrode 9  setting
        9       :       'CV'       ,
        10      :       'EIS'      ,
        11      :       'MUX'      ,  # switch to electrode 10  setting
        12      :       'CV'       ,
        13      :       'EIS'      ,
        14      :       'Fluidics' ,  # release pressure
        15      :       'Cartridge',  # open chamber
    }

    # Multiplexer - electrodes
    # ------------------------
    dict_mux_sequence = {
        # sequence
        # index     name
        0        : 'WE9' ,
        1        : 'WE10',
    }

    # Fluidics
    fluidcfg = 'october'

    # eta
    test_time = 15 * 60   # 15 minutes

elif dev_local_config == DevelConfigs.dev_setup_1_to_4:
####################################################
# Electrodes 1-5 slow fluidics
# dev_setup_1_to_5
####################################################
    dict_sys_state_sequence = {
        
        # Sequence
        # Counter        worker ID
        0        :      'HWinit'    ,  # initialise the hardware
        1        :      'Cartridge' ,  # prepare the cartridge
        2        :      'Fluidics'  ,  # first mix
        3        :      'MUX'       ,  # electrode  1
        4        :      'CV'        ,
        5        :      'MUX'       ,  # electrode  2
        6        :      'CV'        ,
        7        :      'MUX'       ,  # electrode  3
        8        :      'CV'        ,
        9        :      'MUX'       ,  # electrode  4
        10       :      'CV'        ,
        11       :      'Fluidics'  ,  # second mix
        12       :      'MUX'       ,  # electrode  1
        13       :      'CV'        ,
        14       :      'EIS'       ,
        15       :      'MUX'       ,   # electrode 2
        16       :      'CV'        ,
        17       :      'EIS'       ,
        18       :      'MUX'       ,   # electrode 3
        19       :      'CV'        ,
        20       :      'EIS'       ,
        21       :      'MUX'       ,   # electrode 4
        22       :      'CV'        ,
        23       :      'EIS'       ,
        24       :      'Fluidics'  ,   # release pressure
        25       :      'Cartridge' ,   # open chamber
    }
    # Multiplexer - electrodes
    # ------------------------
    dict_mux_sequence = {# sequence
        # index     name
        0        : 'WE1' ,
        1        : 'WE2' ,
        2        : 'WE3' ,
        3        : 'WE4' ,
    }

    # Fluidics
    fluidcfg = 'slow'


    # eta
    test_time = 27 * 60   # 27 minutes

else:  # elif dev_local_config == DevelConfigs.dev_setup_1_to_5:

    ##
    # Electrodes 1-5 slow fluidics
    # dev_setup_1_to_5
    #
    dict_sys_state_sequence = {
        
        # Sequence
        # Counter        worker ID
        0        :      'HWinit'    ,  # initialise the hardware
        1        :      'Cartridge' ,  # prepare the cartridge
        2        :      'Fluidics'  ,  # first mix
        3        :      'MUX'       ,  # electrode  1
        4        :      'CV'        ,
        5        :      'MUX'       ,  # electrode  2
        6        :      'CV'        ,
        7        :      'MUX'       ,  # electrode  3
        8        :      'CV'        ,
        9        :      'MUX'       ,  # electrode  4
        10       :      'CV'        ,
        11       :      'MUX'       ,  # electrode  5
        12       :      'CV'        ,
        13       :      'Fluidics'  ,  # second mix
        14       :      'MUX'       ,  # electrode  1
        15       :      'CV'        ,
        16       :      'EIS'       ,
        17       :      'MUX'       ,   # electrode 2
        18       :      'CV'        ,
        19       :      'EIS'       ,
        20       :      'MUX'       ,   # electrode 3
        21       :      'CV'        ,
        22       :      'EIS'       ,
        23       :      'MUX'       ,   # electrode 4
        24       :      'CV'        ,
        25       :      'EIS'       ,
        26       :      'MUX'       ,  # electrode 5
        27       :      'CV'        ,
        28       :      'EIS'       ,
        29       :      'Fluidics'  ,   # release pressure
        30       :      'Cartridge' ,   # open chamber
    }
    # Multiplexer - electrodes
    # ------------------------
    dict_mux_sequence = {
        # sequence
        # index     name
        0        : 'WE1' ,
        1        : 'WE2' ,
        2        : 'WE3' ,
        3        : 'WE4' ,
        4        : 'WE5' ,
    }

    # Fluidics
    fluidcfg = 'october'


    # eta
    test_time = 27 * 60   # 27 minutes


##
# Electrodes 1-5 no fluidics
# TODO: test that we haven't broken geek mode
geek_dict_sys_state_sequence = {

    # Sequence
    # Counter        worker ID
    0        :      'HWinit'    ,  # initialise the hardware
    1        :      'Cartridge' ,  # prepare the cartridge

    2        :      'MUX'       ,  # electrode  1
    3        :      'CV'        ,
    4        :      'MUX'       ,   # electrode 2
    5        :      'CV'        ,
    6        :      'MUX'       ,   # electrode 3
    7        :      'CV'        ,
    8        :      'MUX'       ,   # electrode 4
    9        :      'CV'        ,
    10       :      'MUX'       ,   # electrode 5
    11       :      'CV'        ,

    12       :      'MUX'       ,  # electrode  1
    13       :      'CV'        ,
    14       :      'EIS'       ,
    15       :      'MUX'       ,   # electrode 2
    16       :      'CV'        ,
    17       :      'EIS'       ,
    18       :      'MUX'       ,   # electrode 3
    19       :      'CV'        ,
    20       :      'EIS'       ,
    21       :      'MUX'       ,   # electrode 4
    22       :      'CV'        ,
    23       :      'EIS'       ,
    24       :      'MUX'       ,   # electrode 5
    25       :      'CV'        ,
    26       :      'EIS'       ,

    27       :      'Cartridge' ,   # open chamber
}

# Multiplexer - electrodes
# ------------------------
geek_dict_mux_sequence = {
    # sequence
    # index     name
    # 0        : 'ST'  ,
    0        : 'WE1' ,
    1        : 'WE2' ,
    2        : 'WE3' ,
    3        : 'WE4' ,
    4        : 'WE5' ,

}


# Slow table
###############
if fluidcfg == 'october':
    # Load reservoir - Repeat 200 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_reservoir_200 = {
        0: (50, False, False, False, False, False, True, False, False, False, False, False, False),
        1: (100, False, False, False, False, False, True, False, False, False, True, False, False),
        2: (50, False, False, False, False, False, False, False, False, False, True, False, False),
        3: (50, False, False, False, False, False, False, False, True, False, True, False, False),
        4: (100, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # Load plasma - Repeat 285 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_plasma = {
        0: (70, False, False, False, False, True, False, False, False, False, False, False, True),
        1: (70, False, False, False, False, False, False, False, False, False, False, False, True),
    }
    # Load buffer - Repeat 16 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_buffer = {
        0: (50, True, False, False, False, False, False, False, False, False, False, False, False),
        1: (500, True, False, False, False, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, True, False, False, True, False, False, False, False, False),
        4: (500, False, False, False, True, False, False, False, False, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M1 TO M1* - Repeat 14 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    mix_m1_to_m1s = {
        0: (50, False, False, False, True, False, False, False, False, False, False, False, False),
        1: (100, False, False, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, False, False, False, True, True, False, False, False, False),
        4: (100, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M1* TO M1 - Repeat 14 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    mix_m1_to_m1 = {
        0: (50, False, False, False, False, False, False, False, True, False, False, False, False),
        1: (100, False, False, False, False, False, False, True, True, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, True, False, False, True, False, False, False, False, False),
        4: (100, False, False, False, True, False, False, False, False, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M1 TO M1* - Repeat 16 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    mix_to_m1s_2 = {
        0: (50, False, False, False, True, False, False, False, False, False, False, False, False),
        1: (500, False, False, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, False, False, False, True, True, False, False, False, False),
        4: (500, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # Load 1/10 dilution - Repeat 35 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load1_10_dilution_35 = {
        0: (70, False, False, False, False, False, False, False, False, False, False, True, True),
        1: (70, False, False, False, False, False, False, False, False, False, False, False, True),
    }
    # Load buffer - Repeat 22 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_buffer_22 = {
        0: (50, False, False, False, False, False, False, False, False, True, False, False, False),
        1: (500, False, False, False, False, False, False, True, False, True, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, True, False, False, True, False, False, False, False, False),
        4: (500, False, False, False, True, False, False, False, False, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M2 TO M2* - Repeat 20 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Mix_m2_m2s_20 = {
        0: (50, False, False, False, True, False, False, False, False, False, False, False, False),
        1: (100, False, False, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, False, False, False, True, True, False, False, False, False),
        4: (100, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M2* TO M2 - Repeat 20 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Mix_m2s_m2_i20 = {
        0: (50, False, False, False, False, False, False, False, True, False, False, False, False),
        1: (100, False, False, False, False, False, False, True, True, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, True, False, False, True, False, False, False, False, False),
        4: (100, False, False, False, True, False, False, False, False, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M2 TO M2* - Repeat 20 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Mix_m2_m2s_20 = {
        0: (50, False, False, False, True, False, False, False, False, False, False, False, False),
        1: (100, False, False, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, False, False, False, True, True, False, False, False, False),
        4: (100, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M2* TO M2 - Repeat 20 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Mix_m2s_m2_ii_20 = {
        0: (50, False, False, False, False, False, False, False, True, False, False, False, False),
        1: (100, False, False, False, False, False, False, True, True, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, True, False, False, True, False, False, False, False, False),
        4: (100, False, False, False, True, False, False, False, False, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # MIX M2 TO M2* - Repeat 22 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Mix_m2_ms_22 = {
        0: (50, False, False, False, True, False, False, False, False, False, False, False, False),
        1: (500, False, False, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, False, False, False, False, True, True, False, False, False, False),
        4: (500, False, False, False, False, False, False, False, True, False, False, False, False),
        5: (50, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # Wash/agitate before loading into sensor through v11 - Repeat 4 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_agitate_blist_v11_4 = {
        0: (50, False, True, False, False, True, False, False, False, False, False, True, False),
        1: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        2: (500, False, True, False, False, True, False, False, False, False, False, True, False),
        3: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        4: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        5: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        6: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        7: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Wash before loading into sensor through v9 - Repeat 4 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_agitate_blist_v9_4 = {
        0: (50, False, True, False, False, True, False, False, False, True, False, False, False),
        1: (500, False, True, False, False, True, False, True, False, True, False, False, False),
        2: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Load sample into sensor through v11 - Repeat 5 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_samp_in_sens_v11_5 = {
        0: (50, True, False, False, False, True, False, False, False, False, False, True, False),
        1: (500, True, False, False, False, True, False, True, False, False, False, True, False),
        2: (50, True, False, False, False, True, False, True, False, False, False, False, False),
        3: (50, True, False, False, False, True, True, True, False, False, False, False, False),
        4: (500, True, False, False, False, True, True, False, False, False, False, False, False),
        5: (50, True, False, False, False, True, False, False, False, False, False, False, False),
    }
    # Load sample into sensor with agitation through v9 + incubate 30s - Repeat 5 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Load_samp_in_sens_v9_5 = {
        0: (50, True, False, False, False, True, False, False, False, True, False, False, False),
        1: (200, True, False, False, False, True, False, True, False, True, False, False, False),
        2: (200, True, False, False, False, True, False, False, False, True, False, False, False),
        3: (200, True, False, False, False, True, False, True, False, True, False, False, False),
        4: (200, True, False, False, False, True, False, False, False, True, False, False, False),
        5: (200, True, False, False, False, True, False, True, False, True, False, False, False),
        6: (50, True, False, False, False, True, False, True, False, False, False, False, False),
        7: (50, True, False, False, False, True, True, True, False, False, False, False, False),
        8: (500, True, False, False, False, True, True, False, False, False, False, False, False),
        9: (50, True, False, False, False, True, False, False, False, False, False, False, False),
        10: (30000, False, False, False, False, False, False, False, False, False, False, False, False),
    }
    # Incubation of sample - Run only one time - DO NOT repeat,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Inc_samp_1 = {
        0: (150000, False, False, False, False, False, False, False, False, False, False, False, False)
    }
    # Wash buffer through bypass - Repeat 4 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_th_by_4 = {
        0: (50, False, True, False, True, False, False, False, False, False, False, False, False),
        1: (500, False, True, False, True, False, False, True, False, False, False, False, False),
        2: (50, False, True, False, False, False, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, False, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, False, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, False, False, False, False, False, False, False, False),
    }
    # Wash buffer through v11 - Repeat 10 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_th_v11_10 = {
        0: (50, False, True, False, False, True, False, False, False, False, False, True, False),
        1: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        2: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Wash buffer through v9 - Repeat 10 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_th_v9_10 = {
        0: (50, False, True, False, False, True, False, False, False, True, False, False, False),
        1: (500, False, True, False, False, True, False, True, False, True, False, False, False),
        2: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Wash buffer w agitation through v10- Repeat 11 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_w_ag_v11_10 = {
        0: (50, False, True, False, False, True, False, False, False, False, False, True, False),
        1: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        2: (500, False, True, False, False, True, False, False, False, False, False, True, False),
        3: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        4: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        5: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        6: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        7: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    first_mix = {  # number of times to repeat, event table
        0: (200, Load_reservoir_200),
        1: (285, Load_plasma),
        2: (16, Load_buffer),
        3: (14, mix_m1_to_m1s),
        4: (14, mix_m1_to_m1),
        5: (16, mix_to_m1s_2),
        6: (35, Load1_10_dilution_35),
        7: (23, Load_buffer_22),
        8: (20, Mix_m2_m2s_20),
        9: (20, Mix_m2s_m2_i20),
        10: (20, Mix_m2_m2s_20),
        11: (20, Mix_m2s_m2_ii_20),
        12: (22, Mix_m2_ms_22),
        13: (4, Wash_agitate_blist_v11_4),
        14: (4, Wash_agitate_blist_v9_4),
        15: (5, Load_samp_in_sens_v11_5),
        16: (5, Load_samp_in_sens_v9_5),
        17: (1, Inc_samp_1),
        18: (4, Wash_buf_th_by_4),
        19: (10, Wash_buf_th_v11_10),
        20: (10, Wash_buf_th_v9_10),
        21: (10, Wash_buf_w_ag_v11_10),
    }
    # DAB through bypass - Repeat 4 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Dab_thru_bypass_4 = {
        0: (50, False, False, True, True, False, False, False, False, False, False, False, False),
        1: (500, False, False, True, True, False, False, True, False, False, False, False, False),
        2: (50, False, False, True, False, False, False, True, False, False, False, False, False),
        3: (50, False, False, True, False, False, True, True, False, False, False, False, False),
        4: (500, False, False, True, False, False, True, False, False, False, False, False, False),
        5: (50, False, False, True, False, False, False, False, False, False, False, False, False),
    }
    # DAB through v11 - Repeat 30 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Dab_thru_v11_30 = {
        0: (50, False, False, True, False, True, False, False, False, False, False, True, False),
        1: (500, False, False, True, False, True, False, True, False, False, False, True, False),
        2: (50, False, False, True, False, True, False, True, False, False, False, False, False),
        3: (50, False, False, True, False, True, True, True, False, False, False, False, False),
        4: (500, False, False, True, False, True, True, False, False, False, False, False, False),
        5: (50, False, False, True, False, True, False, False, False, False, False, False, False),
    }
    # Incubate DAB with agitation - Repeat 5 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Incub_Dab_agit_5 = {
        0: (35000, False, False, True, False, True, False, False, False, False, False, False, False),
        1: (200, False, False, True, False, True, False, True, False, False, False, True, False),
        2: (200, False, False, True, False, True, False, False, False, False, False, True, False),
        3: (200, False, False, True, False, True, False, True, False, False, False, True, False),
        4: (200, False, False, True, False, True, False, False, False, False, False, True, False),
        5: (200, False, False, True, False, True, False, True, False, False, False, True, False),
    }
    # Wash buffer through v11 - Repeat 10 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_bef_thru_V11_10 = {
        0: (50, False, True, False, False, True, False, False, False, False, False, True, False),
        1: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        2: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Wash buffer through v9 - Repeat 10 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_thru_v9_10 = {
        0: (50, False, True, False, False, True, False, False, False, True, False, False, False),
        1: (500, False, True, False, False, True, False, True, False, True, False, False, False),
        2: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        3: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        4: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        5: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Wash buffer w agitation through v11- Repeat 10 times,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    Wash_buf_w_ag_thru_v11_10 = {
        0: (50, False, True, False, False, True, False, False, False, False, False, True, False),
        1: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        2: (500, False, True, False, False, True, False, False, False, False, False, True, False),
        3: (500, False, True, False, False, True, False, True, False, False, False, True, False),
        4: (50, False, True, False, False, True, False, True, False, False, False, False, False),
        5: (50, False, True, False, False, True, True, True, False, False, False, False, False),
        6: (500, False, True, False, False, True, True, False, False, False, False, False, False),
        7: (50, False, True, False, False, True, False, False, False, False, False, False, False),
    }
    # Run second measurement,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12
    second_mix = {
        # second mix list goes here
        0: (4, Dab_thru_bypass_4),
        1: (30, Dab_thru_v11_30),
        2: (5, Incub_Dab_agit_5),
        3: (10, Wash_bef_thru_V11_10),
        4: (10, Wash_buf_thru_v9_10),
        5: (10, Wash_buf_w_ag_thru_v11_10),
    }

##
# V15 table -- Work in progress -- Oksana to finalise
#
else:
    # Load reservoir
    # step : ms,    v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11    v12
    Load_reservoir = { # move liquid from blister to buffer reservoirs
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
        0: (100,  False, False, False, False, False, True,  False, False, False, False, False, False),
        1: (100,  False, False, False, False, False, True,  False, False, False, True,  False, False),
        2: (100,  False, False, False, False, False, False, False, False, False, True,  False, False),
        3: (100,  False, False, False, False, False, False, False, True,  False, True,  False, False),
        4: (100,  False, False, False, False, False, False, False, True,  False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # Vacuum flickering 1st round
    # step : ms,    v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11    v12
    Vacuum_flickering_1st = { # first metering
        0: (100,   False, False, False, False, True,  False, False, False, False, False, False, True),
        1: (150,   False, False, False, False, True,  False, False, False, False, False, False, False),}


    # Load buffer 1st round
    # step : ms,    v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Load_buffer_1st = {
        0: (100,  True,  False, False, False, False, False, False, False, False, False, False, False),   
        1: (2000, True,  False, False, False, False, False, True,  False, False, False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        4: (2000, False, False, False, True,  False, False, False, False, False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # MIX M1 TO M1*
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    mix_10star_to_10star2 = {  # 10* -> 10**  1st
        0: (100,  False, False, False, True,  False, False, False, False, False, False, False, False),
        1: (200,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, False, False, False, True,  True,  False, False, False, False),
        4: (200,  False, False, False, False, False, False, False, True,  False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # MIX M1* TO M1
    # step : ms,   v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    mix_10star2_to_10star = {  # 10** -> 10*  1st
        0: (100,  False, False, False, False, False, False, False, True,  False, False, False, False),
        1: (200,  False, False, False, False, False, False, True,  True,  False, False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        4: (200,  False, False, False, True,  False, False, False, False, False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }


    # Second metering and mixing
    # Load 1/10 dilution - Repeat 35 times,,,,,,,,,,,,
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Vacuum_flickering_2nd = { # vacuum flickering of second metering
        0: (100,   False, False, False, False, False, False, False, False, False, False, True,  True),
        1: (150,   False, False, False, False, False, False, False, False, False, False, True,  False), }

    # Load buffer
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Load_buffer_2nd = {
        0: (100,  False, False, False, False, False, False, False, False, True,  False, False, False),
        1: (2000, False, False, False, False, False, False, True,  False, True,  False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        4: (2000, False, False, False, True,  False, False, False, False, False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # MIX 250* and 250**
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Mix_250s_to_250ss = { # 250* -> 250** 1st
        0: (100,  False, False, False, True,  False, False, False, False, False, False, False, False),
        1: (300,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, False, False, False, True,  True,  False, False, False, False),
        4: (300,  False, False, False, False, False, False, False, True,  False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # MIX M2* TO M2 - Repeat 20 times,,,,,,,,,,,,
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Mix_250ss_to_250s = { # 250** -> 250* 1st
        0: (100,  False, False, False, False, False, False, False, True,  False, False, False, False),
        1: (300,  False, False, False, False, False, False, True,  True,  False, False, False, False),
        2: (100,  False, False, False, False, False, False, True,  False, False, False, False, False),
        3: (100,  False, False, False, True,  False, False, True,  False, False, False, False, False),
        4: (300,  False, False, False, True,  False, False, False, False, False, False, False, False),
        5: (100,  False, False, False, False, False, False, False, False, False, False, False, False), }

    # Sample loading
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Sample_loading_washing_v11 = {
        0: (100,  False,  True, False, False, True,  False, False, False, False, False, True,  False),
        1: (500,  False,  True, False, False, True,  False, True,  False, False, False, True,  False),
        2: (500,  False,  True, False, False, True,  False, False, False, False, False, True,  False),
        3: (500,  False,  True, False, False, True,  False, True,  False, False, False, True,  False),
        4: (100,  False,  True, False, False, True,  False, True,  False, False, False, False, False),
        5: (100,  False,  True, False, False, True,  True,  True,  False, False, False, False, False),
        6: (500,  False,  True, False, False, True,  True,  False, False, False, False, False, False),
        7: (100,  False,  True, False, False, True,  False, False, False, False, False, False, False), }


    # Wash before loading into sensor through v9 - Repeat 4 times,,,,,,,,,,,,
    # step : ms,  v1      v2    v3      v4     v5     v6     v7     v8     v9     v10    v11     v12
    Sample_loading_washing_v9 = {
        0: (100, False, True, False, False, True, False, False, False, True,  False, False, False),
        1: (100, False, True, False, False, True, False, True,  False, True,  False, False, False),
        2: (100, False, True, False, False, True, False, True,  False, True,  False, False, False),
        3: (100, False, True, False, False, True, False, True,  False, True,  False, False, False),
        4: (100, False, True, False, False, True, False, True,  False, True,  False, False, False),
        5: (100, False, True, False, False, True, False, True,  False, True,  False, False, False),
        6: (100, False, True, False, False, True, False, True,  False, False, False, False, False),
        7: (100, False, True, False, False, True, True,  True,  False, False, False, False, False),
        8: (100, False, True, False, False, True, True,  False, False, False, False, False, False),
        9: (100, False, True, False, False, True, True,  False, False, False, False, False, False),
        10:(100, False, True, False, False, True, True,  False, False, False, False, False, False),
        11:(100, False, True, False, False, True, True,  False, False, False, False, False, False),
        12:(100, False, True, False, False, True, True,  False, False, False, False, False, False),
        13:(100, False, True, False, False, True, False, False, False, False, False, False, False), }

    # Load sample into sensor through v11 - Repeat 5 times,,,,,,,,,,,,
    # step : ms,  v1      v2    v3      v4    v5    v6     v7     v8     v9     v10    v11     v12
    Sample_loading_v11 = {
        0: (100, True, False, False, False, True, False, False, False, False, False, True,  False),
        1: (500, True, False, False, False, True, False, True,  False, False, False, True,  False),
        2: (100, True, False, False, False, True, False, True,  False, False, False, False, False),
        3: (100, True, False, False, False, True, True,  True,  False, False, False, False, False),
        4: (500, True, False, False, False, True, True,  False, False, False, False, False, False),
        5: (100, True, False, False, False, True, False, False, False, False, False, False, False), }

    # Load sample into sensor with agitation through v9 + incubate 30s - Repeat 5 times,,,,,,,,,,,,
    # step : ms,   v1      v2    v3      v4    v5      v6     v7     v8     v9     v10    v11     v12
    Sample_loading_v9_inc = {
        0: (100,   True,  False, False, False, True,  False, False, False, True,  False, False, False),
        1: (200,   True,  False, False, False, True,  False, True,  False, True,  False, False, False),
        2: (200,   True,  False, False, False, True,  False, False, False, True,  False, False, False),
        3: (200,   True,  False, False, False, True,  False, True,  False, True,  False, False, False),
        4: (200,   True,  False, False, False, True,  False, False, False, True,  False, False, False),
        5: (200,   True,  False, False, False, True,  False, True,  False, True,  False, False, False),
        6: (100,   True,  False, False, False, True,  False, True,  False, False, False, False, False),
        7: (100,   True,  False, False, False, True,  True,  True,  False, False, False, False, False),
        8: (500,   True,  False, False, False, True,  True,  False, False, False, False, False, False),
        9: (100,   True,  False, False, False, True,  False, False, False, False, False, False, False),
        10:(30000, False, False, False, False, False, False, False, False, False, False, False, False), }

    # Wash buffer through bypass - Repeat 4 times,,,,,,,,,,,,
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Sensor_washing_bypass = {
        0: (100, False, True, False, True,  False, False, False, True, False, False, False, False),
        1: (500, False, True, False, True,  False, False, True,  True, False, False, False, False),
        2: (100, False, True, False, False, False, False, True,  True, False, False, False, False),
        3: (100, False, True, False, False, False, True,  True,  True, False, False, False, False),
        4: (600, False, True, False, False, False, True,  False, True, False, False, False, False), }


    # Wash buffer through v11 - Repeat 10 times,,,,,,,,,,,,
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Washing_v11_1st = {
        0: (100, False, True, False, False, True, False, False, True, False, False, True,  False),
        1: (500, False, True, False, False, True, False, True,  True, False, False, True,  False),
        2: (100, False, True, False, False, True, False, True,  True, False, False, False, False),
        3: (100, False, True, False, False, True, True,  True,  True, False, False, False, False),
        4: (500, False, True, False, False, True, True,  False, True, False, False, False, False),
        5: (100, False, True, False, False, True, False, False, True, False, False, False, False), }

    # Wash buffer through v9 - Repeat 10 times,,,,,,,,,,,,
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Washing_v9 = {
        0: (100, False, True, False, False, True, False, False, False, True,  False, False, False),
        1: (500, False, True, False, False, True, False, True,  False, True,  False, False, False),
        2: (100, False, True, False, False, True, False, True,  False, False, False, False, False),
        3: (100, False, True, False, False, True, True,  True,  False, False, False, False, False),
        4: (500, False, True, False, False, True, True,  False, False, False, False, False, False),
        5: (100, False, True, False, False, True, False, False, False, False, False, False, False), }

    # Wash buffer w agitation through v10- Repeat 11 times,,,,,,,,,,,, REMOVED TO SPEED THE DEMO
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Washing_v11_2nd = {
        0: (100, False, True, False, False, True, False, False, False, False, False, True,  False),
        1: (500, False, True, False, False, True, False, True,  False, False, False, True,  False),
        2: (500, False, True, False, False, True, False, False, False, False, False, True,  False),
        3: (500, False, True, False, False, True, False, True,  False, False, False, True,  False),
        4: (100, False, True, False, False, True, False, True,  False, False, False, False, False),
        5: (100, False, True, False, False, True, True,  True,  False, False, False, False, False),
        6: (500, False, True, False, False, True, True,  False, False, False, False, False, False),
        7: (100, False, True, False, False, True, False, False, False, False, False, False, False), }

    first_mix = {  # number of times to repeat, event table

        0:  (300, Load_reservoir),
        1:  (50, Vacuum_flickering_1st),
        2:  (22, Load_buffer_1st),
        3:  (13, mix_10star_to_10star2),
        4:  (9,  mix_10star2_to_10star),
        5:  (13, mix_10star_to_10star2),
        6:  (9,  mix_10star2_to_10star),
        7:  (18, mix_10star_to_10star2),
        8:  (25, Vacuum_flickering_2nd ),
        9:  (25, Load_buffer_2nd),
        10: (20, Mix_250s_to_250ss),
        11: (20, Mix_250ss_to_250s),
        12: (20, Mix_250s_to_250ss),
        13: (20, Mix_250ss_to_250s),
        14: (23, Mix_250s_to_250ss),
        15: ( 4, Sample_loading_washing_v11),
        16: ( 4, Sample_loading_washing_v9),
        17: ( 5, Sample_loading_v11),
        18: ( 5, Sample_loading_v9_inc),
        19: ( 4, Sensor_washing_bypass),
        20: (11, Washing_v11_1st),
        21: (11, Washing_v9),
        22: (11, Washing_v11_2nd),
    }

    # DAB through bypass - Repeat 4 times,,,,,,,,,,,,
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Dab_bypass = {
        0: (100, False, False, True, True,  False, False, False, True, False, False, False, False),
        1: (500, False, False, True, True,  False, False, True,  True, False, False, False, False),
        2: (100, False, False, True, False, False, False, True,  True, False, False, False, False),
        3: (100, False, False, True, False, False, True,  True,  True, False, False, False, False),
        4: (500, False, False, True, False, False, True,  False, True, False, False, False, False),
        5: (100, False, False, True, False, False, False, False, True, False, False, False, False), }

    # DAB through v11 - Repeat 30 times,,,,,,,,,,,,
    # step : ms,   v1     v2    v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Dab_v11 = {
        0: (100, False, False, True, False, True, False, False, False, False, False, True,  False),
        1: (500, False, False, True, False, True, False, True,  False, False, False, True,  False),
        2: (100, False, False, True, False, True, False, True,  False, False, False, False, False),
        3: (100, False, False, True, False, True, True,  True,  False, False, False, False, False),
        4: (500, False, False, True, False, True, True,  False, False, False, False, False, False),
        5: (100, False, False, True, False, True, False, False, False, False, False, False, False), }

    # Incubate DAB with agitation - Repeat 5 times,,,,,,,,,,,,
    # step : ms,   v1     v2      v3     v4    v5      v6     v7    v8     v9     v10    v11    v12
    Dab_incubation = {
        0: (35000, False, False, False, False, True, False, False, False, False, False, False, False),
        1: (200,   False, False, True,  False, True, False, True,  False, False, False, True,  False),
        2: (200,   False, False, True,  False, True, False, False, False, False, False, True,  False),
        3: (200,   False, False, True,  False, True, False, True,  False, False, False, True,  False),
        4: (200,   False, False, True,  False, True, False, False, False, False, False, True,  False),
        5: (200,   False, False, True,  False, True, False, True,  False, False, False, True,  False), }

    # Run second measurement,,,,,,,,,,,,
    # step time,1,2,3,4,5,6,7,8,9,10,11,12

    second_mix = {

        # second mix list goes here
        0: (4, Dab_bypass),
        1: (30, Dab_v11),
        2: (5, Dab_incubation),
        3: (11, Washing_v11_1st),
        4: (11, Washing_v9),
        5: (11, Washing_v11_2nd),
    }



