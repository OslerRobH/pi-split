import time
from i2c_master import i2c_master

START_STATE = 1
LIMIT_STATE = 7

def get_timestamp():
    ts = time.gmtime()
    return time.strftime("%Y-%m-%d %H:%M:%S", ts)

newState = START_STATE
i2c = i2c_master()
first = False
while True:
    status_byte = 0
   
    if newState != START_STATE:
        first = True 
    while status_byte != newState:
        # send to GUI (Slave)
        result = i2c.send_ChangeState(newState)
        print("%s send_ChangeState sending command result = %.2X" 
            % (get_timestamp(), result))
        if result < 0:
            print("Prolem sending command exiting...")
            exit(1)
        else:
            if not first:
                print ("%s 2 second delay" % (get_timestamp()))
                time.sleep(2)
            else:
                print ("%s 20 second delay" % (get_timestamp()))
                time.sleep(20)
                first = False

            state_byte = i2c.get_state()
            print ("%s [%.2X] state_byte = %.2X" 
                % (get_timestamp(), newState, state_byte))

#            status_byte = i2c.get_status()
#            print ("%s [%.2X] status_byte = %.2X" 
#                % (get_timestamp(), newState, status_byte))

    newState = newState + 1
    if newState == LIMIT_STATE:
        newState = START_STATE

    print ("%s [%.2X] 3 second delay" % (get_timestamp(), newState))
    time.sleep(3)
    
