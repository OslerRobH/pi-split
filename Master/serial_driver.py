# import seriial library
import serial

# declare serial_driver class that uses the serial library
class serial_driver(object):

    # @brief the constructor or initialiser
    # @param self the this reference
    # @return None
    def init(self):
        print("serial_driver:: init()")
        self.serial = serial.Serial('/dev/serial0', 9600)
        self.b = ''
    
    # @brief a simple self test
    # @note the serial port is opened by Raspbian at boot up
    # @param self the this reference
    # @return read string in self.b
    def test(self):
        print("serial_driver:: test() write")
        self.write(0x20)
        print("serial_driver:: test() read")
        self.read()
        return self.b
   
 
    # @brief read data from serial port
    # @param self the this reference
    # @return read string in self.b
    def read(self):
        print("serial_driver:: read()")
        self.b = self.serial.read(self.serial.in_waiting or 1)
        # if a character was received print it
        if len(self.b) > 1:
            print("serial_driver:: read() found = [%s]" % (self.b))
        else:
            print("serial_driver:: read() nothing received")
        return self.b
    
    # @brief write supplied string to serial port
    # @param self the this reference
    # @param str the string to send
    # @return result of send
    def write(self, str):
        print("serial_driver:: write()")
        result = self.serial.write(str)
        return result
