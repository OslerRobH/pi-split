#!/usr/bin/env python3
# encoding: utf-8
"""
tst_hw_manual.py
Created by Alan Austin on 17 August 2018
HW Test code - manually invoked tests  
Rewritten with a proper command dispatcher and using HAL


Copyright (c) Osler Diagnostics Ltd 2018. All rights reserved.
"""
    
import sys
import time

fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

"""
NOTES
"""

class OctetHWTest():

    def __init__(self):
        # matches hw_test commands to functions
        self.cmd_dispatch_table = {
            'help'        : self.help,
            'h'           : self.help,
            'setpin'      : self.setpin,
            'getpin'      : self.getpin,
            'adcread'     : self.adcread,
            'adc'         : self.adcread,
            'I2Cscan'     : self.i2cscan,
            'I2Cping'     : self.i2cping,
            'ps4'         : self.ps4,     
            'pwm'         : self.pwm,     
            'pinmap'      : self.pinmap,
            'clearall'    : self.clearall,
            'x'           : self.clearall,
            'pospressure' : self.pospressure,
            '+'           : self.pospressure,
            'negpressure' : self.negpressure,
            '-'           : self.negpressure,
            'draw'        : self.draw,
            'test'        : self.test,
            'spin'        : self.spin,
            'beep'        : self.beep,
            'quit'        : self.quit,
            'q'           : self.quit,
            'p+'          : self.pos_pump,
            'p-'          : self.neg_pump,
            'psi': self.psi,
        }

        self.gpio = hal.octet_gpio
        self.exit = False

    def _cmd(self):
        while not self.exit:
            cmdstr = input('> ')
            if len(cmdstr) != 0:
                arg_list = cmdstr.split()

                # first item in list is command. 
                cmd = arg_list[0]

                # Call function from dispatcher,removing first item from arg_list (the command itself)
                if cmd in self.cmd_dispatch_table:
                    self.cmd_dispatch_table[cmd](arg_list[1:])
                else:
                    print("Command not recognised")

    def help(self, arg_list):
        """ [h] to show help """
        #OctetHWTest._info(OctetHWTest)
        print("Manual Tests:")
        print("pinmap                           - list pin-names")
        print("setpin {pin-name} {0|1}          - set GPIO pin")
        print("getpin {pin-name}                - get GPIO pin state")
        print("adc {pin-name}                   - read adc voltage")
        print("spin {destination}               - spin to home|pogos|burster")
        print("pwm {DCMOTOR|VARVALVE} {0..100}  - pwm test")
        print("ps4                              - palmsens4 status")
        print("ps4 {on|off}                     - palmsens4 on/off")
        print("+                                - test positive pressure")
        print("-                                - test negative pressure")
        print("draw {0..100}                    - test variable valve")
        print("beep                             - beep")
        print("h                                - show help")
        print("q                                - quit manual tests")
        print("p+                               - turn the positive pump on")
        print("p-                               - turn the negative pump on")
        print("psi                              - read pressures")

    def spin(self, arg_list):
        """ [spin {destination}]  to spin to next|home|optos|burster """

        # set motor target opto
        if len(arg_list) == 0:
            end = 'next'
        elif arg_list[0].lower() in ('home', 'pogos', 'burster'):
            end = arg_list[0].lower()
        else:
            print("Invalid command")
            return()

        timeout_count = 500
        self.gpio.set_motor(75)
        self.gpio.set_pin('OPTLED', True)
        while timeout_count != 0:

            if end == 'home' and self.gpio.get_pin('OPTOPOS1') == 0:
                self.gpio.set_motor(0)
                self.gpio.set_pin('OPTLED', False)
                print("Home")
                return()

            elif end == 'pogos' and self.gpio.get_pin('OPTOPOS2') == 0:
                self.gpio.set_motor(0)
                self.gpio.set_pin('OPTLED', False)
                print("Pogo Pins Down")
                return()

            elif end == 'burster' and self.gpio.get_pin('OPTOPOS3') == 0:
                self.gpio.set_motor(0)
                self.gpio.set_pin('OPTLED', False)
                print("Burster Down")
                return()

            elif end == 'next':  
                if self.gpio.get_pin('OPTOPOS1') == 0:
                    self.gpio.set_motor(0)
                    self.gpio.set_pin('OPTLED', False)
                    print("Home")
                    return()
                if self.gpio.get_pin('OPTOPOS2') == 0:
                    self.gpio.set_motor(0)
                    self.gpio.set_pin('OPTLED', False)
                    print("Pogo Pins Down")
                    return()
                if self.gpio.get_pin('OPTOPOS3') == 0:
                    self.gpio.set_motor(0)
                    self.gpio.set_pin('OPTLED', False)
                    print("Burster Down")
                    return()

            timeout_count -=1
            time.sleep(0.01)

        self.gpio.set_motor(0)
        self.gpio.set_pin('OPTLED', False)
        print("Opto not detected")


    def clearall(self, arg_list):
        """ [x] to deactivate all valves and pumps """
        self.gpio.set_port("valves", 0)

    def pospressure(self, arg_list):
        """ [+] to test positive pressure """

        print("Positive pressure test")
        # Activate all valves 
        print("All valves ON")
        for valve in ('CV1', 'CV2', 'CV3', 'CV4', 'CV5', 'CV6', 'CV7', 'CV8', 'CV9', 'CV10', 'CV11', 'CV12'):
            self.gpio.set_pin(valve, True)
            time.sleep(0.2)

        # turn on positive pressure pump
        print("Pump ON")
        self.gpio.set_pin('PUMP_POS', True)

        # run monitoring pressure
        for i in range(15):
            print("%3.2f" % (self.gpio.psi('PS+')), end='\r')

        # turn off positive pressure pump
        print("Pump OFF")
        self.gpio.set_pin('PUMP_POS', False)

        print("\nCheck decay...")
        # monitor pressure decay
        for i in range(15):
            print("%3.2f" % (self.gpio.psi('PS+')), end='\r')
        print("\ndone")

    def pos_pump(self, arg_list):
        """[p+] to switch on positive pump on"""
        print("Positive pump ON")
        # Activate all valves
        print("All valves ON")
        for valve in ('CV1', 'CV2', 'CV3', 'CV4', 'CV5', 'CV6', 'CV7', 'CV8', 'CV9', 'CV10', 'CV11', 'CV12'):
            self.gpio.set_pin(valve, True)
            time.sleep(0.2)

        # turn on positive pressure pump
        print("Pump ON - press any key to switch it off")
        self.gpio.set_pin('PUMP_POS', True)

        # run monitoring pressure
        for i in range(30):
            print("[%3d] %3.2f" % (i, self.gpio.psi('PS+')), end='\r')
            time.sleep(0.25)

        # turn off positive pressure pump
        print("\nPump OFF")
        self.gpio.set_pin('PUMP_POS', False)

        print("Check decay...")
        # monitor pressure decay
        for i in range(50):
            print("[%3d] %3.2f" % (i, self.gpio.psi('PS+')), end='\r')
            time.sleep(0.25)
        print("\ndone")

    def negpressure(self, arg_list):
        """ [-] to test negative pressure """

        # De-activate all valves 
        print("Negative pressure test")
        print("All valves OFF")
        for valve in ('CV1', 'CV2', 'CV3', 'CV4', 'CV5', 'CV6', 'CV7', 'CV8', 'CV9', 'CV10', 'CV11', 'CV12'):
            self.gpio.set_pin(valve, False)
            time.sleep(0.3)

        # turn on negative pressure pump
        print("Pump ON")
        self.gpio.set_pin('PUMP_NEG', True)

        # run monitoring pressure
        for i in range(15):
            print("%3.2f" % (self.gpio.psi('PS-')), end='\r')

        # turn off negative pressure pump
        print("\nPump OFF")
        self.gpio.set_pin('PUMP_NEG', False)

        # monitor pressure decay
        print("Check decay...")

        for i in range(15):
            print("%3.2f" % (self.gpio.psi('PS-')), end='\r')
            time.sleep(0.3)
        print("\ndone")

    def neg_pump(self, arg_list):
        """ [-] to test negative pressure """

        # De-activate all valves
        print("Negative pressure test")
        print("All valves OFF")
        for valve in ('CV1', 'CV2', 'CV3', 'CV4', 'CV5', 'CV6', 'CV7', 'CV8', 'CV9', 'CV10', 'CV11', 'CV12'):
            self.gpio.set_pin(valve, False)
            time.sleep(0.3)

        # turn on negative pressure pump
        print("Pump ON - press any key to switch it off")
        self.gpio.set_pin('PUMP_NEG', True)

        # run monitoring pressure
        for i in range(30):
            print("[%2d] %3.2f" % (i, self.gpio.psi('PS-')), end='\r')
            time.sleep(0.5)

        # turn off negative pressure pump
        print("\nPump OFF")
        self.gpio.set_pin('PUMP_NEG', False)

        # monitor pressure decay
        print("Check decay...")
        for i in range(30):
            print("[%2d] %3.2f" % (i, self.gpio.psi('PS-')), end='\r')
            time.sleep(0.5)
        print("\ndone")

    def draw(self, arg_list):
        """ test draw pressure for a given variable valve duty cycle """
        if len(arg_list) < 1:
            print("Too few arguments")
            return
        dc = float(arg_list[0])

        if len(arg_list) == 2:
            duration = int(arg_list[1])
        else:
            duration = 20

        # De-activate all valves 
        print("Draw pressure test")
        print("All valves OFF")
        print("Variable valve = %7.4f" % (dc))
        for valve in ('CV1', 'CV2', 'CV3', 'CV4', 'CV5', 'CV6', 'CV7', 'CV8', 'CV9', 'CV10', 'CV11', 'CV12'):
            self.gpio.set_pin(valve, False)
            time.sleep(0.3)

        self.gpio.set_pin('PUMP_NEG', True)

        self.gpio.set_pin('DUMP', True)
        time.sleep(2.0)
        self.gpio.set_pin('DUMP', False)

        self.gpio.set_v_valve(dc)

        for i in range(duration):
            psi = self.gpio.psi('PS-')
            if psi < -5.0:
                self.gpio.set_pin('PUMP_NEG', False)
            else:
                self.gpio.set_pin('PUMP_NEG', True)
            time.sleep(0.25)
            psi_var = self.gpio.psi('PSv')
            print("-ve: %3.2f PSI, var: %3.2f PSI" % (psi, psi_var))
            time.sleep(0.25)

        self.gpio.set_pin('PUMP_NEG', False)
        self.gpio.set_v_valve(0)


    def setpin(self, arg_list):
        """ [setpin {pin-name} {0|1}] to set GPIO pin """
        if len(arg_list) < 2:
            print("Too few arguments")
            return
        try: # catch if hal throws an assert 
            self.gpio.set_pin(arg_list[0], True if (arg_list[1]=='1') else False)
        except:
            print("Invalid command")


    def getpin(self, arg_list):
        """ [getpin {pin-name}] to get GPIO pin state"""
        if len(arg_list) < 1:
            print("Too few arguments")
            return
        try: # catch if hal throws an assert 
            ret = self.gpio.get_pin(arg_list[0])
        except:
            print("Invalid command")
        else:
            print("%d" % (ret))

    def adcread(self, arg_list):
        """ [adc CTEMP|PS1|PS2|PS3|VBATT|5VQ|3V3|5V] to read adc voltage """

        scaling = {
        'CTEMP': 2.0, 
        'PSv': 2.0,
        'PS+': 2.0,
        'PS-': 2.0,
        '5V': 2.0, 
        '5VQ': 2.0, 
        'VBATT': 7.67, 
        '3V3': 2.0 
        }

        if len(arg_list) < 1:
            print("Too few arguments")
            return

        pin_name = arg_list[0]
        try: # catch if hal throws an assert
            #scaling fixed by resistive potential divider on ADC inputs
            volts = scaling[pin_name] * self.gpio.adc_pinread(pin_name)
        except:
            print("Invalid command")
        else:    
            print("Voltage: %3.2f" % (volts))

    def i2cscan(self, arg_list):
        """ Not implemented """
        print("Not implemented")

    def i2cping(self, arg_list):
        """ Not implemented """
        print("Not implemented")

    def pwm(self, arg_list):
        """ Set pwm value for variable valve or cartridge motor """

        if len(arg_list) < 2:
            print("Too few arguments")
            return

        pin_name = arg_list[0]
        # duty cycle
        dc = float(arg_list[1])
        if dc < 0 or dc >100.0:
            print('Value out of range')
            return

        if pin_name == 'VARVALVE':
            self.gpio.set_v_valve(dc)
        elif pin_name == 'DCMOTOR':
            self.gpio.set_motor(dc)
        else:
            print('Invalid pin name')

    def ps4(self, arg_list):
        """ palmsens4 power on/off """

        self.gpio.get_ps4_on()
        if len(arg_list) < 1:
            return

        if arg_list[0].lower() == 'on':
            ps4_on = True
        elif arg_list[0].lower() == 'off':
            ps4_on = False
        else:
            print("Error: invalid parameters")
            return

        self.gpio.set_ps4(ps4_on)


    def beep(self, arg_list):
        """ [beep] guess what this does """
        self.gpio.set_pin('BUZZER', 1)
        time.sleep(0.1)
        self.gpio.set_pin('BUZZER', 0)


    def pinmap(self, arg_list):
        """ [pinmap] to list pin-names """
        
        print('%-10s %-10s %-10s' % ('ID', 'Schematic', 'IN/OUT'))

        for key in hal.hal_pin_map:
            print('%-10s %-10s %-10s' % (hal.hal_pin_map[key][0], hal.hal_pin_map[key][1], hal.hal_pin_map[key][4]))

    def psi(self, arg_list):
        """ dump pressure readings """
        for i in range(30):
            pos_psi = self.gpio.psi('PS+')
            neg_psi = self.gpio.psi('PS-')
            var_psi = self.gpio.psi('PSv')
            print("(+) % 2.2fpsi (-) % 2.2fps (v) % 2.2fpsi" % (pos_psi, neg_psi, var_psi))


    def test(self, arg_list):
        """ - temporary test  """
        print("Pump on")
        self.gpio.set_pin('PUMP_POS', True)
        time.sleep(2)
        print("Pump off")
        self.gpio.set_pin('PUMP_POS', False)

    def info(object, spacing=10, collapse=1):              
        """ Generate help info. Print doc strings for all methods not starting with underscore. 

        Takes module, class, list, dictionary, or string.""" 
        print("Manual Tests:")
        methodList = [method for method in dir(object) if callable(getattr(object, method)) and method[0] != '_']
        processFunc = collapse and (lambda s: " ".join(s.split())) or (lambda s: s) 
        print ("\n".join(["%s" % 
                          (processFunc(str(getattr(object, method).__doc__))) 
                          for method in methodList]) )

    def quit(self, arg_list):
        """ [q] to  """
        self.exit = True

def main():
    """ """

    hal.octet_gpio.bioc_init()
    HWTest = OctetHWTest()
    HWTest._cmd()



if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Keyboard Interrupt")
        sys.exit()
