# @file states.py used to define states used by measurements
#

from enum import Enum

# @brief states - inherited states
#    0 : ('Init   '),   # nothing has been pressed
#    1 : ('Started'),   # Start has been pressed
#    2 : ('AskYN  '),   # waiting for user input
#    3 : ('Stop   '),   # stop everythig
#    4 : ('Error  '),   # something has gone wrong
#    5 : ('EndGame'),   # buttons disabled, we are shutting down
# @return None
class states(Enum):
     MEASURE_INIT = 0
     MEASURE_START = 1
     MEASURE_ASK_YN = 2
     MEASURE_STOP = 3
     MEASURE_ERROR = 4
     MEASURE_ENDGAME = 5

