################################################
# A class for curve fitting ISFET signal data
# Author: Zahid Anasari
################################################
import inspect
import numpy as np
from scipy.optimize import minimize
from collections import OrderedDict

class ComplexCurveFit:
    '''
    A class for curve fitting ISFET signal data
    '''

    def __init__(self,
                 model: callable,
                 xdata: np.array,
                 ydata: np.array,
                 guess_dict: dict,
                 bounds_dict: dict,
                 maxiter: int=2000):
        '''
        Initiliase the fit funtion bounds and initial conditions
        '''
        self.model = model
        self.model_name = model.__name__
        self.x = xdata
        self.y = ydata
        self.model_args = list(inspect.signature(model).
                               parameters.keys())[1:]
        self.guess = [guess_dict[x] for x in self.model_args]
        self.bounds = [bounds_dict[x] for x in self.model_args]
        try:
            self.fit_curve(maxiter)
            for k, v in guess_dict.items():
                gkey = '{}_g'.format(k)
                self.fit_results[gkey] = v
        except Exception as e:
            print('WARNING: Error during curve fitting ({})'.format(e))
            self.fit_results = None

    def residuals(self, params, x, y):
        expect = self.model(x, *params)
        errs = np.abs(y - expect) / np.abs(expect)
        return np.sum(errs)

    def fit_curve(self, maxiter):
        x = self.x
        y = self.y
        guess = self.guess
        method = 'Nelder-Mead'
        res = minimize(
            self.residuals,
            args=(x, y),
            x0=guess,
            method=method,
            options={
                'maxiter': maxiter,
                'ftol': 1e-6
            }
        )
        params = res.x
        self.fit_results = OrderedDict(zip(self.model_args, params))
        self.fit_results['N iter'] = res.nit
        self.fit_results['N Pts'] = x.size
