#!/usr/bin/env python3
# encoding: utf-8
"""
Test our hardware
"""

import sys
import time
import os

from threading import Thread

import common as cms    # for on_dev_pc
import fluidics         # pressure and valve tests
import cartridge_motor  # motor assembly
import eis_measure      # impedance
import tst_hw_manual as manual


# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc
dbg_skip_fluidics = cms.DebugFlags.skip_fluidics

fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

####################
# Fluidics Mapping
####################
# mechanical map
#   unit side
#  CV1         CV7
#  CV2         CV8
#  CV3         CV9
#  CV4         CV10
#  CV5         CV11
#  CV6         CV12 (draw)
#
#  outside world side
#

# Cartridge map
#  unit side
#  V11          V5
#  V7           V6
#  V4           V10
#  V3           V1
#  V2           V8
#  V9           V12

# pressure = valve OFF
# vacuum = valve ON (energised)

# Cartridge
##############
Motor_Stop = 0

class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.gpio = hal.octet_gpio  # instance to hardware abstraction layer
        self.gpio.bioc_init()
        self.shut_down = False
        self.fluid = fluidics.MainWorkerThread()
        self.cart = cartridge_motor.MainWorkerThread()
        self.monkey = eis_measure.MainWorkerThread()
        self.chamber = ChamberState.Unknown
        self.init_Once()
        print("%s MainWorkerThread(Thread) called" % __file__)

    ##########################
    # variables that we only
    # set-up once per lifetime
    ##########################
    def init_Once(self):
        self.chamber = ChamberState.Unknown
        self.task_state = WorkerStates.initialising
        # worker constants
        # -----------------

        # Fluidics
        #############
        self.fluid.init_vars()
        self.fluid.stop_pumpsNvalves()  # valves, and pumps off

        # Mech
        ###############
        self.cart.init_Once()
        self.cart.init_vars()
        self.cart.start()

        # Impedance
        ###############
        self.monkey.init_Once()
        self.monkey.init_vars()
        self.monkey.start()
        return

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.task_state = WorkerStates.initialising
        self.stop = True  # we start with everything quiet

        self.psi_pos = 0
        self.psi_neg = 0
        self.psi_var = 0

        idtagfile = ("%s%s" % (cms.CmsFilenames.id_tag_path, cms.CmsFilenames.id_tag))

        # If file already exists, delete it ##
        if os.path.isfile(idtagfile):
            os.remove(idtagfile)

        # now save the file with the tag
        with open(idtagfile, "w") as idf:
            idf.write("Experiment ID: HW Test")
            idf.flush()
            idf.close()

        self.UserReply = UsrPromptReply.unknown


    ###########################
    # put the hardware into a safe state
    # before we exit
    ###########################
    def SafeState(self):
        # switch the motor off
        self.speed = Motor_Stop
        self.gpio.set_motor(Motor_Stop)
        # switch the optos off
        self.gpio.set_pin("OPTLED", False)
        self.fluid.stop_pumpsNvalves()
        return


    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled:
            self.fluid.stop_pumpsNvalves()
            self.SafeState()
            self.task_state = WorkerStates.exiting
            print("TST: state machine [exiting]")
            return True
        else:
            return False

    ####################################
    # Main state machine
    ####################################
    def run(self):

        while not self.cancelled:

            self.monitor_cancel()

            # Initialise this state machine
            #################################
            if self.task_state == WorkerStates.initialising:
                self.init_vars()
                self.task_state = WorkerStates.idle
                print(">")

            # Wait for a measurement request
            #################################
            elif self.task_state == WorkerStates.idle:
                if not self.stop:
                    self.task_state = WorkerStates.test_io
                else:
                    time.sleep(5)

            # Test IO
            ######################
            elif self.task_state == WorkerStates.test_io:

                self.gpio.dump_input_state()
                self.sc_wait(3)

                if not self.stop and not self.cancelled:
                    print("######################################### IO TEST DONE - check values")
                    self.task_state = WorkerStates.test_mech
                    print("Testing the motor and optos")

            # Test the mechanism
            ######################
            elif self.task_state == WorkerStates.test_mech:
                self.cart.set_Continue()    # ask the mech to go home
                self.sc_wait(2)
                print(self.cart.get_StatusMsg())
                if not self.stop and not self.cancelled:
                    while not self.cart.get_PromptActive(): # wait for it to get home
                        self.sc_wait(0.5)
                        if self.stop or self.cancelled:
                            break

                    if not self.stop and not self.cancelled:
                        self.task_state = WorkerStates.home
                        self.chamber = ChamberState.Open
                        print("*******Please insert cartridge, when ready press [y]*********")

            # Chamber open (home)
            #####################
            elif self.task_state == WorkerStates.home:
                if not self.stop and not self.cancelled:
                    if self.UserReply == UsrPromptReply.yes:
                        self.task_state = WorkerStates.clamp
                        self.chamber = ChamberState.Closed
                        self.UserReply = UsrPromptReply.unknown
                        self.cart.set_Reply(True)   # tell it to clamp the cartridge
                        self.sc_wait(3)
                    else:
                        time.sleep(1)

            # Clamp down the cartridge
            ##########################
            elif self.task_state == WorkerStates.clamp:
                have_error = False
                while not self.cart.get_PromptActive():     # wait for it to finish clamping
                    if self.stop or self.cancelled:
                        break
                    self.sc_wait(1)
                    # if we get an error probably means the loop is not making
                    # clear the error so the chamber can be opened and try again
                    if self.cart.get_IsError():
                            have_error = True
                            self.cart.set_ClearError()
                            self.task_state = WorkerStates.test_mech
                            print("\nTry again")
                            break
                if not have_error and not self.stop and not self.cancelled:
                    self.task_state = WorkerStates.chk_valves
                    print("**************** Valves Test ****************")
                    print("To continue to next test press [n]")


            # Test the valves work
            ##########################
            elif self.task_state == WorkerStates.chk_valves:
                # turn valves ON - delays make sequence audible
                self.fluid.all_valves_energised(True)
                if not self.stop and not self.cancelled:
                    self.fluid.all_valves_energised(False)

                    if self.UserReply == UsrPromptReply.next_test:
                        self.task_state = WorkerStates.p_pressure
                        self.UserReply = UsrPromptReply.unknown
                        print("Opening (energising) Valves")
                        print("Positive Pressure test. To continue to next test press [n]")
                        self.fluid.all_valves_energised(True)
                    else:
                        time.sleep(1)


            # Test Positive pressure
            ##########################
            elif self.task_state == WorkerStates.p_pressure:
                print("**************** Positive Pressure Test ****************")
                print("[n] To continue to next test press")
                self.psi_pos = self.gpio.psi('PS+')
                print("+ve %3.2f PSI" % self.psi_pos)
                self.fluid.have_feedback = True
                self.fluid.pos_pump_on = True
                self.fluid.apply_settings()
                for i in range(60):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_pos = self.gpio.psi('PS+')
                    print("(+) %3.2f PSI" % self.psi_pos)

                    if self.psi_pos > fluidics.Psi.pos_target:
                        break

                self.fluid.pos_pump_on = False
                self.fluid.have_feedback = True
                self.fluid.apply_settings()
                print("Decay")
                for i in range(60):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_pos = self.gpio.psi('PS+')
                    print("(+) %5.2f PSI" % self.psi_pos)
                print("Positive pump off")

                if self.psi_pos > fluidics.Psi.pos_target:
                    print("######################################### Positive Pressure [PASSED]")
                    print("Check Decay")
                    for i in range(25):
                        if self.stop or self.cancelled:
                            print("bailing out")
                            break

                        prev_pos = self.psi_pos
                        self.psi_pos = self.gpio.psi('PS+')  # 33~44ms

                        print("%d DBG DECAY:[(+) %2.2fpsi D(%2.2f)]" % (
                            i, self.psi_pos, (prev_pos - self.psi_pos)))
                    self.task_state = WorkerStates.n_pressure
                    self.UserReply = UsrPromptReply.unknown
                    self.fluid.all_valves_energised(False)
                else:
                    print("######################################### Positive Pressure [FAILED]")

                if self.UserReply == UsrPromptReply.next_test:
                    self.task_state = WorkerStates.n_pressure
                    self.UserReply = UsrPromptReply.unknown
                    self.fluid.all_valves_energised(False)
                else:
                    time.sleep(1)

            # Test Negative pressure
            ##########################
            elif self.task_state == WorkerStates.n_pressure:
                print(" **************** Negative Pressure Test ****************")
                # -ve pressure test and variable valve
                self.fluid.set_dump(False)
                self.psi_neg = self.gpio.psi('PS-')
                self.psi_var = self.gpio.psi('PSv')
                print("(-) %5.2f PSI, var %5.2f PSI" % (self.psi_neg, self.psi_var))
                pass_count = 0
                self.fluid.var_val = 5.0
                self.fluid.neg_pump_on = True
                self.fluid.have_feedback = True
                self.fluid.apply_settings()
                delta = 5.0
                maxv = 100
                minv = 0.1

                for i in range(40):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    print("(-) %5.2f PSI, var %5.2f PSI vvs[%f]" % (self.psi_neg, self.psi_var, self.fluid.var_val))
                    self.fluid.have_feedback = True
                    if self.psi_var < -1.8:
                        self.fluid.var_val = 0.1
                    else:
                        self.fluid.var_val += delta

                    # check end stops
                    if self.fluid.var_val < minv:
                        self.fluid.var_val = minv
                    if self.fluid.var_val > maxv:
                        self.fluid.var_val = maxv

                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()
                    if self.psi_neg < fluidics.Psi.neg_target:
                        break

                self.fluid.neg_pump_on = False
                self.fluid.have_feedback = True
                self.fluid.apply_settings()
                print("Negative pump off")
                if self.psi_neg < fluidics.Psi.neg_target:
                    print("######################################### Negative Pressure [PASSED]")
                    pass_count += 1
                else:
                    print("######################################### Negative Pressure [FAILED]")

                if self.psi_var < -1.2:
                    print("################################### Variable valve Pressure [PASSED]")
                    pass_count += 1
                else:
                    print("####################################Variable valve Pressure [FAILED]")

                print("Check Decay")
                self.fluid.set_dump(False)
                print("Dump valve is OFF")
                self.fluid.var_val = 0
                self.fluid.have_feedback = True
                self.fluid.apply_settings()
                print("Variable Valve OFF")
                for i in range(25):
                    if self.stop or self.cancelled:
                        print("bailing out")
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break

                    prev_var = self.psi_var
                    self.psi_var = self.gpio.psi('PSv')  # 33~44ms

                    prev_neg = self.psi_neg
                    self.psi_neg = self.gpio.psi('PS-')  # 33~44ms typically 33ms

                    print("%d DBG DECAY:[(-) %2.2fpsi D(%2.2f)][(v) %2.2fpsi vvs[%f] D(%2.2f)]" % (
                            i, self.psi_neg, (prev_neg - self.psi_neg), self.psi_var, self.fluid.var_val, (prev_var - self.psi_var)))

                if pass_count == 2:
                    # top up the pressure for the next test
                    self.fluid.var_val = 10.0
                    self.fluid.neg_pump_on = True
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()

                    for i in range(40):
                        if self.stop or self.cancelled:
                            break
                        if self.UserReply == UsrPromptReply.next_test:
                            break
                        self.psi_neg = self.gpio.psi('PS-')
                        self.psi_var = self.gpio.psi('PSv')
                        print("(-) %3.2f PSI, var %3.2f PSI vvs[%f]" % (self.psi_neg, self.psi_var, self.fluid.var_val))
                        if self.psi_neg < fluidics.Psi.neg_target:
                            break
                        if self.psi_var < -1.8:
                            self.fluid.var_val = 0.1
                        elif self.psi_var > -1.0:
                            self.fluid.var_val += delta

                        # check end stops
                        if self.fluid.var_val < minv:
                            self.fluid.var_val = minv
                        if self.fluid.var_val > maxv:
                            self.fluid.var_val = maxv
                        self.fluid.have_feedback = True
                        self.fluid.apply_settings()

                    self.fluid.neg_pump_on = False
                    print("Negative pump off")
                    self.fluid.var_val = 0.1
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()
                    print("Dump valve test")
                    print("(-) Pump OFF, Dump valve ON")
                    for i in range(40):
                        if self.stop or self.cancelled:
                            break
                        if self.UserReply == UsrPromptReply.next_test:
                            break
                        self.psi_pos = self.gpio.psi('PS+')
                        self.psi_neg = self.gpio.psi('PS-')
                        self.psi_var = self.gpio.psi('PSv')
                        self.fluid.set_dump(True)
                        time.sleep(0.001)
                        self.fluid.set_dump(False)
                        print("%d(+) %3.2f PSI (-) %3.2f PSI, var %3.2f PSI vv[%2.1f]" % (i, self.psi_pos, self.psi_neg, self.psi_var, self.fluid.var_val))
                    time.sleep(0.2)
                    self.fluid.var_val = 1
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()
                    print("Dump valve off")
                    self.psi_pos = self.gpio.psi('PS+')
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    self.sc_wait(1)
                    print("%d(+) %5.2f PSI (-) %5.2f PSI, var %5.2f PSI vv[%2.1f]" % (
                    i, self.psi_pos, self.psi_neg, self.psi_var, self.fluid.var_val))

                    if self.psi_var > -1.50:
                        print("######################################### Dump valve [PASSED]")
                        pass_count += 1
                    else:
                        print("######################################### Dump valve [FAILED]")

                if pass_count == 3:
                    print("######################################### Test [PASSED]")
                    self.task_state = WorkerStates.pressurise

                # move to the next state based on our status
                if not self.stop and not self.cancelled:
                    if self.UserReply == UsrPromptReply.next_test:
                        self.task_state = WorkerStates.manifold
                        self.UserReply = UsrPromptReply.unknown
                        self.fluid.all_valves_energised(False)
                    else:
                        time.sleep(1)
                else:
                    self.fluid.stop_pumpsNvalves()

            # Manifold leak test
            ####################
            elif self.task_state == WorkerStates.manifold:
                print(" **************** Manifold Seal Test ****************")
                self.fluid.set_dump(False)
                print("Dump valve is OFF")
                self.fluid.all_valves_energised(False)
                print("All solenoids de-energised")
                self.fluid.pos_pump_on = True
                print("(+) PUMP ON")
                self.fluid.neg_pump_on = True
                print("(-) PUMP ON")
                self.fluid.var_val = 5.0
                print("(v) valve to %2.1f %%" % self.fluid.var_val)
                self.fluid.have_feedback = True
                self.fluid.apply_settings()

                for i in range(40):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_pos = self.gpio.psi('PS+')
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    print("%d(+) %5.2f PSI (-) %5.2f PSI, var %5.2f PSI vv[%2.1f]" % (
                    i, self.psi_pos, self.psi_neg, self.psi_var, self.fluid.var_val))
                    if self.psi_pos > 7.0:
                        self.fluid.pos_pump_on = False
                        print("(+) PUMP OFF")
                    if self.psi_neg < -7.0:
                        self.fluid.neg_pump_on = False
                        print("(-) PUMP OFF")
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()
                    if self.psi_var < -7.0:
                        break

                self.fluid.pos_pump_on = False
                print("(+) PUMP OFF")
                self.fluid.neg_pump_on = False
                print("(-) PUMP OFF")
                self.fluid.have_feedback = True
                self.fluid.apply_settings()

                print("Check decay")

                for i in range(20):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_pos = self.gpio.psi('PS+')
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    print("%d(+) %5.2f PSI (-) %5.2f PSI, var %5.2f PSI vv[%2.1f]" % (
                    i, self.psi_pos, self.psi_neg, self.psi_var, self.fluid.var_val))
                    time.sleep(1)

                print("Opening the dump valve")
                for i in range(40):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.psi_pos = self.gpio.psi('PS+')
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    self.fluid.set_dump(True)
                    time.sleep(0.5)
                    self.fluid.set_dump(False)
                    print("%d(+) %5.2f PSI (-) %5.2f PSI, var %5.2f PSI vv[%2.1f]" % (
                    i, self.psi_pos, self.psi_neg, self.psi_var, self.fluid.var_val))

                self.fluid.set_dump(False)

                # move to the next state based on our status
                if not self.stop and not self.cancelled:
                    if self.UserReply == UsrPromptReply.next_test:
                        self.task_state = WorkerStates.pressurise
                        self.UserReply = UsrPromptReply.unknown
                        self.fluid.all_valves_energised(False)
                    else:
                        time.sleep(1)
                else:
                    self.fluid.stop_pumpsNvalves()

            # Pressurise system
            ###################
            elif self.task_state == WorkerStates.pressurise:

                print(" **************** Pressurise System Test ****************")

                self.fluid.start_sys_pressure(True)
                print(" **************** Pressure Maintenance Test ****************")
                ps_failed = False
                for i in range(10):
                    if self.stop or self.cancelled:
                        break
                    if self.UserReply == UsrPromptReply.next_test:
                        break
                    self.fluid.hold_pressure(True)
                    print("STATUS: pos %5.2f PSI, neg %5.2f PSI, var %5.2f PSI" % (self.psi_pos, self.psi_neg, self.psi_var))
                    self.psi_pos = self.gpio.psi('PS+')
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')

                    # keep an eye on the pressures and make sure nothing is gone crazy
                    if self.psi_var < -7.0:
                        print("############################################# Pressure Test [FAILED]")
                        print("ERROR: check the variable valve and dump valve are functional")
                        ps_failed = True
                        break
                    elif self.psi_pos > 8.5:
                        print("############################################# Pressure Test [FAILED]")
                        print("ERROR: check the positive pump is functional")
                        ps_failed = True
                        break
                    elif self.psi_pos < 4.5:
                        print("############################################# Pressure Test [FAILED]")
                        print("ERROR: check for a leaky or malfunctioning valve")
                        ps_failed = True
                        break
                    elif self.psi_neg > -4.5:
                        print("############################################# Pressure Test [FAILED]")
                        print("ERROR: check negative pump is functional")
                        ps_failed = True
                        break

                # unit may be uncalibrated so allow the variable valve to be out
                if ( (-2.0 < self.psi_var < -1.0) and
                     (-6.0 < self.psi_neg < -4.0) and
                     ( 6.0 > self.psi_pos > 4.0) ):
                    print("############################################# Pressure Test [GOOD]")
                elif not ps_failed:
                    print("############################################# Pressure Test [PASSED]")

                self.fluid.stop_pumpsNvalves()

                # move to the next state based on our status
                if not self.stop and not self.cancelled:
                   self.task_state = WorkerStates.open_chamber
                   print("Opening chamber")
                else:
                    self.fluid.stop_pumpsNvalves()


            # Open chamber
            ################
            elif self.task_state == WorkerStates.open_chamber:
                print(" **************** Opening Chamber ****************")
                self.chamber = ChamberState.Open
                self.cart.set_Reply(True)
                self.sc_wait(1)
                if not self.stop and not self.cancelled:
                    self.cart.set_Continue()
                    print(self.cart.get_StatusMsg())
                    self.sc_wait(1)
                    self.task_state = WorkerStates.imp

            # Impedance test
            #############################
            elif self.task_state == WorkerStates.imp:
                # don't start this test until the previous worker has paused
                if self.cart.get_IsPaused():
                    print(" *********************** Impedance Test ***********************")
                    if not self.gpio.get_ps4_on():
                        self.gpio.set_ps4(True)
                        # wait for the ps4 to boot
                        print("PS4 booting")
                        for i in range(5):
                            self.sc_wait(1)
                            print(".", end='')
                    print("\nStarting test")
                    self.monkey.set_Continue()
                    self.task_state = WorkerStates.wait_imp
                    print(" *** Please Wait for Impedance Test to Complete ***")
                else:
                    time.sleep(1)

            # Impedance test
            #############################
            elif self.task_state == WorkerStates.wait_imp:
                if self.monkey.get_IsPaused():
                    print("Test finished, check the result")
                    self.task_state = WorkerStates.done
                elif self.monkey.get_IsError():
                    print(self.monkey.get_StatusMsg())
                    print("############################################ Impedance Test [FAILED]")
                    self.task_state = WorkerStates.done
                else:
                    time.sleep(1)

            # Done
            #############################
            elif self.task_state == WorkerStates.done:
                print("**************** AUTOMATED TEST COMPLETE ****************")
                print(self.get_StatusMsg())
                self.task_state = WorkerStates.initialising

            # Flush the variable valve
            # #############################
            elif self.task_state == WorkerStates.flush_vvalve:
                if self.UserReply == UsrPromptReply.yes :
                    self.UserReply = UsrPromptReply.unknown
                    self.fluid.all_valves_energised(False)
                    self.psi_neg = self.gpio.psi('PS-')
                    self.psi_var = self.gpio.psi('PSv')
                    print("-ve %5.2f PSI, var %5.2f PSI" % (self.psi_neg, self.psi_var))
                    self.fluid.neg_pump_on = False
                    self.fluid.var_val = 100
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()

                    for i in range(100):
                        if self.stop or self.cancelled:
                            break
                        if self.UserReply == UsrPromptReply.next_test:
                            break
                        self.psi_neg = self.gpio.psi('PS-')
                        self.psi_var = self.gpio.psi('PSv')
                        print("-ve %5.2f PSI, var %5.2f PSI" % (self.psi_neg, self.psi_var))
                        time.sleep(0.1)
                        if self.psi_neg < -9.0:
                            break

                    self.fluid.neg_pump_on = False
                    print("Negative pump off")
                    self.fluid.var_val = 0
                    self.fluid.have_feedback = True
                    self.fluid.apply_settings()

                    print("************** Variable valve flushed **************")
                    self.task_state = WorkerStates.initialising

                else:
                    time.sleep(1)

            # Skip to the cartridge test
            #############################
            elif self.task_state == WorkerStates.skip_2motor:
                # wait for stuff to stop
                while (not self.cart.get_IsPaused() and
                        not self.monkey.get_IsPaused()):
                    time.sleep(0.3)
                self.task_state = WorkerStates.home

            # Skip to the impedance test
            #############################
            elif self.task_state == WorkerStates.skip_2imp:
                while (not self.cart.get_IsPaused() and
                        not self.monkey.get_IsPaused()):
                    time.sleep(0.3)
                self.task_state = WorkerStates.imp


            # Exiting, tidy up
            #############################
            if self.task_state == WorkerStates.exiting:
                print("adios amigos")
                # make sure pumps and valves are off before we bail out
                self.fluid.stop_pumpsNvalves()
                self.SafeState()
                break


        # while

        # tidy up and exit

        self.fluid.stop_pumpsNvalves()
        self.SafeState()
        self.shut_down = True
        sys.stdout.flush()
        return

    # wait for time to elapse but bail
    # out early on a cancel or a stop
    #################################
    def sc_wait(self, wait_time):
        wait_count = 0
        wait_eh = 1
        wait_check = wait_time
        if wait_time > 1:
            wait_eh = 10
            wait_check = wait_time/10.0
        elif wait_time > 0.5:
            wait_eh = 2
            wait_check = wait_time/2.0
        while not self.stop and not self.cancelled:
            time.sleep(wait_check)
            wait_count += 1
            if wait_count == wait_eh:
                break

    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        msplot_coords = []
        return msplot_coords

    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return 0
    def get_PlotMaxX(self):
        return 1
    def get_PlotMinY(self):
        return 0
    def get_PlotMaxY(self):
        return 1

    ###############################
    # returns True if there is an
    # error
    ###############################
    def get_IsError(self):
        return False

    ###############################
    # Always return False, no user
    # interaction required by mux
    ###############################
    def get_PromptActive(self):
        return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        return 0

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    def get_IsPaused(self):
        if self.task_state == WorkerStates.idle  and self.stop == True:
           return True
        else:
            return False

    ###############################
    # Returns a string reflecting
    # the current mux state
    ###############################
    def get_StatusMsg(self):
        return self.task_state

    ################################################################
    # Setters
    ################################################################

    def set_Reply(self, bReply):
        print("HWTST: got reply")
        if bReply:
            self.UserReply = UsrPromptReply.yes
        else:
            self.UserReply = UsrPromptReply.next_test
        return

    #############################
    # Clear the current error
    # to get us back to retry
    #############################
    def set_ClearError(self):
        # ask Barry et. all what we doing here
        print("HWTST: Cannot clear irrecoverable error")
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        print("HWTST: continuing...")
        if self.task_state == WorkerStates.idle or self.task_state == WorkerStates.initialising:
            self.stop = False
        elif self.task_state == WorkerStates.done:
            self.task_state = WorkerStates.test_io
        else:
            print("HWTST: cannot continue in current state %s" % self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        print("HWTST: cancelling...")
        self.fluid.stop_pumpsNvalves()
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        print("HWTST: stopping...")
        self.stop = True
        # wait a wee bit for this to trickle down the state machine
        time.sleep(1)
        # stop all our workers
        print("HWTST: stopping fluidics")
        self.fluid.Stop()
        print("HWTST: stopping the motor")
        self.cart.Stop()
        print("HWTST: stopping impedance")
        self.monkey.Stop()
        print("HWTST: waiting for everything to stop")
        escape_hatch = 0
        while not self.monkey.get_IsPaused() and not self.cart.get_IsPaused():
            time.sleep(0.5)
            print(".", end='')
            escape_hatch += 1
            if escape_hatch > 10:
                print("bailing out")
        self.task_state = WorkerStates.idle

###################################
# User prompt reply
###################################
class UsrPromptReply():
    yes       = 0
    next_test = 1
    unknown   = 2

#####################################
# map opto numbers to meaningful
# position names
#####################################
class OptoPos():
    home    = 'OPTOPOS1'
    engage  = 'OPTOPOS2'
    burster = 'OPTOPOS3'

###################################
# Debug print
###################################
def dbg_print(dbg_string):
    print(dbg_string)
    return

# Pressure regulation select
############################
class PressMon():
    negative = 0
    positive = 1
    variable = 2

# Error conditions
########################
class ErrorStates():
    all_good = 'Passed'
    err_pos_pressure = 'Positive pressure fault'
    err_neg_pressure = 'Negative pressure fault'
    err_draw_pressure = 'Draw pressure fault'


# Worker machine states
########################
class WorkerStates():
    initialising = 'HWTST: initialising'
    idle = 'HWTST: idle'
    test_io = 'HWTST: testing IO'
    test_mech = 'HWTST: motor-cartridge test'
    home = 'HWTST: waiting for cartridge, press [y]'
    clamp = 'HWTST: clamping and loop test'
    chk_valves = 'HWTST: valves test, press [N] for next test'
    p_pressure = 'HWTST: positive pressure test, press [N] for next test'
    n_pressure = 'HWTST: negative pressure test, press [N] for next test'
    pressurise = 'HWTST: pressurising system test'
    manifold = 'HWTST: manifold leak test'
    open_chamber = 'HWTST: opening chamber'
    imp = 'Impedance test on dummy cell'
    wait_imp = 'Waiting for impedance test to complete'
    done = 'HWTST: Done, press [g] to restart'
    flush_vvalve = 'HWTST: Flushing variable valve'
    exiting = 'HWTST: adios amigos'
    skip_2motor = 'HWTST: skipping to cartridge test'
    skip_2imp = 'HWTST: skipping to impedance test'

# mechanism state
##################
class ChamberState():
    Unknown = 0
    Open = 1
    Closed = 2

# help string
##################
def helpstr():
    print("Automated Test Version 2.4")
    print("[p] to print status")
    print("[g] to start")
    print("[s] to stop")
    print("[q] to quit")
    print("[h] for help ")
    print("[N] for next test")
    print("[f] to flush the variable valve")
    print("[w] to switch electrodes")
    print("[i] to impedance")
    print("[o] to run the motor test")
    print("[m] to run Manual Tests")
    print("[?/h] for help")

# Test code
####################################
def main():

    print("Automated Test Initialising\n\n")
    worker = MainWorkerThread()

    # Start the thread
    worker.start()

    while not worker.get_IsPaused():
        time.sleep(5)

    helpstr()
    print("Impedance Initialising...")
    while not worker.monkey.get_IsPaused():
        time.sleep(0.25)

    if worker.monkey.get_IsPaused():
        print(worker.monkey.get_StatusMsg())
        print("selecting Dummy electrode")
        worker.gpio.switch_we('ST')
        electrode = 11

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':  # print status
            print(worker.get_StatusMsg())
            print(worker.cart.get_StatusMsg())
            print(worker.fluid.get_StatusMsg())
            print(worker.monkey.get_StatusMsg())
            if electrode == 11:
                print("Working electrode [Dummy]")
            else:
                print("Working electrode [WE%d]" % electrode)
        elif cmd == 'g':  # go
            worker.set_Continue()
        elif cmd == 'e':  # clear the error
            worker.set_ClearError()
        elif cmd == 'q':  # quit
            worker.set_Cancel()
            worker.gpio.set_ps4(False)
            break
        elif cmd == 's':  # stop
            worker.Stop()
        elif cmd == 'N':   # next test
            worker.set_Reply(False)
        elif cmd == 'y':                # yes
            worker.set_Reply(True)
        elif cmd == 'n':                # no
            worker.set_Reply(False)
        elif cmd == 'f':
            if worker.chamber == ChamberState.Open:
                print("######################### Flushing variable valve ####################")
                print("Ensure there is no cartridge in the chamber press [y] when ready")
                worker.task_state = WorkerStates.flush_vvalve
            else:
                print("can't flush the variable valve until the chamber is open")
        elif cmd == 'o':
            worker.Stop()
            print(worker.get_StatusMsg())
            print(worker.cart.get_StatusMsg())
            print(worker.fluid.get_StatusMsg())
            print(worker.monkey.get_StatusMsg())
            if electrode == 11:
                print("Working electrode [Dummy]")
            else:
                print("Working electrode [WE%d]" % electrode)
            print("starting motor test")
            worker.task_state = WorkerStates.skip_2motor

        elif cmd == 'i':
            worker.Stop()
            print(worker.get_StatusMsg())
            print(worker.cart.get_StatusMsg())
            print(worker.fluid.get_StatusMsg())
            print(worker.monkey.get_StatusMsg())
            if electrode == 11:
                print("Working electrode [Dummy]")
            else:
                print("Working electrode [WE%d]" % electrode)
            print("starting impedance test")
            worker.task_state = WorkerStates.skip_2imp

        elif cmd == 'w':
            if worker.task_state != WorkerStates.idle:
                print("ERROR: please wait for the current test to complete")
            else:
                if electrode == 11:
                    electrode = 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE1')

                elif electrode == 1:
                    electrode += 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE2')

                elif electrode == 2:
                    electrode += 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE3')

                elif electrode == 3:
                    electrode += 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE4')

                elif electrode == 4:
                    electrode += 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE5')

                elif electrode == 5:
                    electrode = 9
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE9')

                elif electrode == 9:
                    electrode += 1
                    print("selecting electrode WE%d" % electrode)
                    worker.gpio.switch_we('WE10')

                elif electrode == 10:
                    electrode += 1
                    print("selecting DUMMY electrode")
                    worker.gpio.switch_we('ST')

        elif cmd == 'm':
            HWTest = manual.OctetHWTest()
            HWTest.help([])
            HWTest._cmd()
            helpstr()

        elif cmd == 'h':
            helpstr()
            
        elif cmd == '?':
            helpstr()


if __name__ == "__main__":

    main()

    sys.exit()
