#!/usr/bin/env python3
# encoding: utf-8

import sys
import time
from threading import Thread
import common as cms    # for on_dev_pc
import gui_defs as ui    # our GUI definitions
import bioc

# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc

# TODO make fake hal for developing on PC
fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

Motor_Stop = bioc.MotSpeed.stop
Motor_Go = bioc.MotSpeed.go

class MainWorkerThread(Thread): # threaded version
    """ Class definitions for cartridge assembly control
    comprising: motor + opto sensors + cartridge detect switch + electrical loop detect switch 
    """
    __current_state = 'UNDETERMINED'

    def __init__(self):

        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.gpio = hal.octet_gpio
        print("hal ", self.gpio)

        self.init_Once()
        self.shut_down = False
        print("%s MainWorkerThread(Thread) called" % __file__)

    ##########################
    # variables that we only 
    # set-up once per lifetime
    ##########################
    def init_Once(self):
        self.state = States.boot
        self.__current_state = States.initialise
        return

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.stop= True             # we don't do anything until told to start
        self.Error_active = False   # no errors so far
        self.__current_state = States.initialise
        self.askToRemove = False
        self.home_sensor = False
        self.burst_sensor = False
        self.engage_sensor = False
        self.gpio.set_motor(Motor_Stop)
        self.gpio.set_pin("OPTLED", True)  # we start with the optos on
        time.sleep(0.1)
        self.get_optos()    # sample the state of the optos

        self.speed = Motor_Go
        self.UserReply = UsrPromptReply.unknown

        return

    ###########################
    # kick it off
    ###########################
    def run(self):
        # keep running until the application exits
        while not self.cancelled:
            self.advance_state()
        # if we are here we have been cancelled so tidy up
        self.SafeState()
        return

    ###########################
    # put the hardware into a safe state 
    # before we exit
    ###########################
    def SafeState(self):
        # switch the motor off
        self.speed = Motor_Stop
        self.gpio.set_motor(Motor_Stop)
        self.gpio.set_pin("OPTLED", False)  # switch the optos off
        return

    ######################
    # Worker State Machine
    ######################
    def advance_state(self):
        """ 
        Go to next state. State machine implemented here 
        """
        
        # for debug
        if self.state != self.__current_state:
            wk_print(self.state + " -> " + self.__current_state)
            self.state = self.__current_state

        # Initialise this state machine
        ###############################
        if self.state == States.initialise:
            self.init_vars()
            self.__current_state = States.idle

        # Wait for request to start
        ###########################
        elif self.state == States.idle:
            if not self.stop:
                # switch the PS4 ON here when we are running geek mode
                if cms.gui_geek_mode:
                    self.gpio.set_ps4(True)

                self.gpio.set_pin("OPTLED", True)              # switch the optos on now
                time.sleep(0.1)
                self.__current_state = States.check_cartridge
                self.get_optos()  # sample the state of the optos
            else:
                time.sleep(0.5)

        # Check for cartridge inserted at Boot
        ######################################
        # before we get the motor going, check if there is a cartridge in there
        elif self.state == States.check_cartridge:
            self.askToRemove = False        # assume they haven't left a cartridge in there
# the cartridge detect switch is unreliable so disabling this functionality
# ---------------------------------------------------------------------------
#            wk_print("checking for cartridge..")
#            if self.get_cartridge_detect():
#                wk_print("WARNING: Cartridge detected!")
#                self.askToRemove = True     # go home then ask them to get that outa there
#            # ok now get this puppy home at which point either get them to put a fresh
#            # one or remove whatever is in there at the moment
            wk_print("going home at speed %d" % self.speed)
            self.__current_state = States.travel_home   # now go wait till we get home

        # Take me home
        ##################
        elif self.state == States.travel_home:
            click_threshold = 47
            # only go home if we are not there yet
            self.get_optos()    # sample the optos
            wk_print("are we home? %r" % self.home_sensor)
            click_count = 0
            if not self.home_sensor:
                self.gpio.set_motor(self.speed)
            self.get_optos()    # sample the optos
            while not self.home_sensor:
                click_count += 1
                self.get_optos()    # keep sampling
                # we are blocking so check if we need to abort
                if self.stop or self.cancelled:
                    break
            self.gpio.set_motor(Motor_Stop)
            wk_print("travel home->stop motor [%d]" % click_count) # from jelly position 47
            if click_count > click_threshold:
                print(" WARNING: HOME OPTOS detected at click count [%d] expected [%d], check mechanism" % (click_count, click_threshold))
            if self.askToRemove:
                self.__current_state = States.remove
            else:
                self.__current_state = States.home
                wk_print("insert cartridge -> press button to continue")

        # Remove old cartridge
        ######################
        elif self.state == States.remove:
            # we don't budge from this state until they whip it out
            if not self.get_cartridge_detect():
                # hold for 2 seconds and check again make sure it has been removed
                for ix in range(40):
                    if self.stop or self.cancelled:
                        break
                    time.sleep(0.1)
                if not self.stop and not self.cancelled:
                    if not self.get_cartridge_detect():
                        wk_print("cartridge removed")
                        self.__current_state = States.home

        # Insert new cartridge
        #######################
        elif self.state == States.home:
            # now wait until they've shoved a cartridge in
# cartridge detect switch is unreliable so disabling this functionality
#----------------------------------------------------------------------
#            if self.get_cartridge_detect():
#                wk_print("Cartridge detected")
#                # give them some wiggle time
#                for ix in range(10):
#                    if self.stop or self.cancelled:
#                        break
#                    time.sleep(0.1)
#
#                if self.stop == False and self.cancelled == False:
#                    # make sure it is still in there
#                    if self.get_cartridge_detect():
#                        self.__current_state = States.clamp
# ----------------------------------------------------------------------
            if self.UserReply == UsrPromptReply.yes:        # REPLACEMENT CODE FOR WONKY SWITCH
                self.__current_state = States.clamp         # REPLACEMENT CODE FOR WONKY SWITCH
                wk_print("clamping the cartridge")
                self.UserReply = UsrPromptReply.unknown
            else:
                time.sleep(1)

        # Clamp down the cartridge
        ##########################
        elif self.state == States.clamp:
            click_threshold = 16
            click_count = 0
            self.gpio.set_motor(self.speed)
            self.get_optos()
            # now clamp it down
            while not self.engage_sensor:
                click_count += 1
                self.get_optos()
                if self.stop or self.cancelled:
                    break

            self.gpio.set_motor(Motor_Stop)
            if click_count > click_threshold:
                print("WARNING: CLAMP OPTOS detected at click count [%d] expected [%d], check mechanism" % (click_count, click_threshold))

            if not self.stop and not self.cancelled:
                wk_print("Cartridge clamped [%d]" % click_count)   # 16
                self.__current_state = States.check_pogo

        # Check to make sure we have contact
        ####################################
        elif self.state == States.check_pogo:
            if not self.get_loop_detect():

                # TODO hook here to reverse the motor and save the cartridge when electronics are available
                if cms.DebugFlags.ignore_pogo_fault:
                    self.__current_state = States.punch_capsules
                else:
                    self.Error_active = True
                    self.__current_state = States.error_pogos
                wk_print("                                  Ooooooh! you have a bady!")
            else:
               self.__current_state = States.punch_capsules
            wk_print("Punching capsules")


        # Punch the capsules
        #####################
        elif self.state == States.punch_capsules:
            click_threshold = 6
            self.get_optos()
            click_count = 0
            self.gpio.set_motor(self.speed)
            while not self.burst_sensor:
                click_count += 1
                self.get_optos()
                if self.stop or self.cancelled:
                    break
            self.gpio.set_motor(Motor_Stop)
            self.gpio.set_pin("OPTLED", False)  # switch the optos OFF
            if click_count > click_threshold:
                print("WARNING: BURST OPTOS detected at click count [%d] expected [%d], check mechanism" % (click_count, click_threshold))

            if not self.stop and not self.cancelled:
                #wk_print("Releasing the jelly [%d]" % click_count)
                wk_print("waiting for sample")
                self.__current_state = States.wait_4sample

        # Wait for sample in the cartridge
        ##################################
        elif self.state == States.wait_4sample:
            if self.UserReply == UsrPromptReply.yes:
                self.__current_state = States.released_jelly
                self.UserReply = UsrPromptReply.unknown
            else:
                time.sleep(1)

        # JELLY
        ########
        elif self.state == States.released_jelly:
            # ok now at this point we are done until we
            # are told to release the cartridge, now the
            # fluidics worker needs to go do some mixing
            # and some measurements need to happen before we
            # need to do anything else
            time.sleep(1)


        # Release cartridge
        ####################
        # we enter this state when the parent API tells us
        # to continue during the released jelly state
        # go back home so they can whip out the cartridge
        elif self.state == States.release_cart:
            click_threshold = 48
            # switch the OPTOS back on as we are going to need them
            self.gpio.set_pin("OPTLED", True)
            time.sleep(0.02)
            self.get_optos()  # sample the optos
            click_count = 0
            if not self.home_sensor:
                self.gpio.set_motor(self.speed)
            self.get_optos()  # sample the optos
            while not self.home_sensor:
                click_count += 1
                self.get_optos()  # keep sampling
                # we are blocking so check if we need to abort
                if self.stop or self.cancelled:
                    break
            self.gpio.set_motor(Motor_Stop)
            wk_print("Released [%d]" % click_count)
            if click_count > click_threshold:
                print("WARNING: RELEASE OPTOS detected at click count [%d] expected [%d], check mechanism" % (click_count, click_threshold))
            # at this point our work is done and we can run another test
            # so go back to the start
            self.__current_state = States.initialise

        # Pogos no worky worky
        ################################
        elif self.state == States.error_pogos:
            # we stay here until we are told to budge
            if not self.Error_active:
                self.__current_state = States.recover_error
            else:
                time.sleep(1)

        # Error recovery
        #################
        elif self.state == States.recover_error:
            # not much we can do, just go back to the start
            prev_stop = self.stop   # back up the current state of the stop
            self.init_vars()        # the stop gets stomped on 
            self.stop = prev_stop   # restore the state
            self.__current_state = States.idle  # rewind so they can remove the cartridge

        else:
            wk_print("BUG UNHANDLED STATE: %s" % self.state)
            wk_print(self.state + " -> " + self.__current_state)
            mykey = input("you have a bug")

    ################################################################
    # Helpers
    ################################################################
       
    def get_cartridge_detect(self):
        """Test whether the cartridge is in the chamber"""
        # take 10 readings to account for any debounce or wiggle
        num_deb_rd = 10
        threshold = 10
        sample_time = 0.005
        detect = 0
        for ix in range(num_deb_rd):
            detect += self.gpio.get_pin('CARTDETIN')
            time.sleep(sample_time)
        wk_print("cartridge %d out of %d" % (detect, num_deb_rd))
        if detect >= threshold:
            return True
        else:
            return False


    def get_loop_detect(self):
        """Test whether the pogo pins are on the sensor"""
        # take 10 readings to account for any debounce or wiggle
        num_deb_rd = 20
        threshold = 12
        sample_time = 0.01
        detect = 0
        for ix in range(num_deb_rd):
            lprd = self.gpio.get_pin('LOOPDETIN')
            #wk_print(lprd)
            detect += lprd
            time.sleep(sample_time)
        wk_print("*****************************LOOP detect [%d]/%d" % (detect, num_deb_rd))
        if detect >= threshold:     # active low
            return True
        else:
            return False

    def get_optos(self):
        """Test whether we have hit an opto"""
        # read all the inputs in one go
        port_data = self.gpio.rd_port()

        # mask in the opto values
        # the optos are ACTIVE-LOW
        if port_data & bioc.InputMask.opto_home:
            self.home_sensor = False
        else:
            self.home_sensor = True

        if port_data & bioc.InputMask.opto_engage:
            self.engage_sensor = False
        else:
            self.engage_sensor = True

        if port_data & bioc.InputMask.opto_burst:
            self.burst_sensor = False
        else:
            self.burst_sensor = True

        #wk_print("home=%r engage=%r burst=%r" %(self.home_sensor, self.engage_sensor, self.burst_sensor))
        return

    def get_state(self):
        return self.__current_state


    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        msplot_coords = []
        return msplot_coords

    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return 0
    def get_PlotMaxX(self):
        return 1
    def get_PlotMinY(self):
        return 0
    def get_PlotMaxY(self):
        return 1

    ###############################
    # returns True if there is an
    # error
    ###############################
    def get_IsError(self):
        return self.Error_active

    
    ###############################
    # return true when we are in the
    # paused state
    ###############################
    def get_PromptActive(self):
        if (self.__current_state == States.home or          # to remove/insert the cartridge
                self.__current_state == States.wait_4sample):  # add the sample
            return True
        else:
            return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        return ui.index_gui_dict.ixgui_continue

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    def get_IsPaused(self):
        if ( (self.__current_state == States.idle) and (self.stop == True) ) or (self.__current_state == States.released_jelly):
            return True
        else:
            return False
    
    ###############################
    # Returns a string reflecting
    # the current mux state
    ###############################
    def get_StatusMsg(self):
        if self.__current_state == States.home:
            return 'Insert Cartridge'
        else:
            return self.__current_state

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        wk_print("got reply")
        if bReply:
            self.UserReply = UsrPromptReply.yes
        else:
            self.UserReply = UsrPromptReply.no
        return

    #############################
    # Clear the current error
    # to get us back to retry
    #############################
    def set_ClearError(self):
        wk_print("Clearing Error")
        self.Error_active = False
        return


    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        wk_print("continuing...")
        if self.__current_state == States.idle:
            wk_print("Starting up")
            self.stop = False
        elif self.__current_state == States.released_jelly:
            wk_print("Finishing up")
            self.__current_state = States.release_cart
        else:
            wk_print("cannot continue in current state %s" % self.__current_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        wk_print("cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        wk_print("Stop called state")
        if not self.stop:
            self.gpio.set_motor(Motor_Stop) # stop the motor first
            self.stop = True
            self.__current_state = States.initialise
            wk_print("stop -> initialising")
        else:
            wk_print("stop already in progress")
        return

###################################
# Debug print with worker id
###################################
def wk_print(dbg_string):
    print("CART: %s" % dbg_string)

###################################
# User prompt reply
###################################
class UsrPromptReply():
    yes     = 0
    no      = 1
    unknown = 2

#####################################
# map opto numbers to meaningful
# position names
#####################################
class OptoPos():
    home    = 'OPTOPOS1'
    engage  = 'OPTOPOS2'
    burster = 'OPTOPOS3'


# Worker machine states
########################
class States():
    boot            = 'boot'
    initialise      = 'initialise'
    idle            = 'idle'
    check_cartridge = 'check-cartridge'
    travel_home     = 'going-home'
    home            = 'home'
    clamp           = 'engaging cartridge'
    check_pogo      = 'checking cartridge'
    remove          = 'remove cartridge'
    punch_capsules  = 'releasing jelly'
    wait_4sample    = 'add sample'
    released_jelly  = 'mixing'
    release_cart    = 'releasing cartridge'
    error_pogos     = 'error:contact'
    recover_error   = 'resetting'


# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.gpio.bioc_init()     # initialise the hardware in stand alone mode
    worker.start()

    print("[p] to print status")
    print("[g] to start")
    print("[s] to stop")
    print("[q] to quit")
    print("[e] to clear error")
    print("[y] yes")
    print("[n] no")
    print("[?/h] for help")

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':                  # print status
            print(worker.get_StatusMsg())
        elif cmd == 'g':                # go
            worker.set_Continue()
            time.sleep(2)
            print(worker.get_StatusMsg())
        elif cmd == 'e':                # clear the error
            worker.set_ClearError()
        elif cmd == 'q':                # quit
            worker.set_Cancel()
            break
        elif cmd == 's':                # stop
            worker.Stop()
        elif cmd == 'y':                # yes
            worker.set_Reply(True)
        elif cmd == 'n':                # no
            worker.set_Reply(False)


if __name__ == "__main__":

    main()

    sys.exit()
