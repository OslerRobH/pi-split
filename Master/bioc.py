#!/usr/bin/env python3

# Basic Input and Output Configuration

class BiocI2cAddr():
    MCP23017_U1 = 0x20
    MCP23017_U2 = 0x21
    MCP23017_U3 = 0x22
    MCP23017_U4 = 0x23
    MCP3424_U8  = 0x68
    MCP3424_U9  = 0x69
    MCP3424_U13 = 0x6C


bioci2caddr = BiocI2cAddr

class ChipNum():
    gpio_exp = 23017
    adc      = 3424
    fuelg    = 1725

class ChipName():
    mux          = 'multiplexer'
    valves       = 'valves'
    dump_valve   = 'dump'
    chg_shutdown = 'dump'
    psi_sens     = 'pressure'
    rail_sens    = 'rails'
    fuelg        = 'fuel'
    inputs       = 'inputs'

# I2C devices
dict_i2c_devs = {
    # index :    part num                Function, I2C addr,               sw reference
    #              0                      1            2                       3
    0       :    ( ChipNum.gpio_exp ,   'out' ,  bioci2caddr.MCP23017_U1 , ChipName.mux       ),
    1       :    ( ChipNum.gpio_exp ,   'out' ,  bioci2caddr.MCP23017_U2 , ChipName.valves    ),
    2       :    ( ChipNum.gpio_exp ,   'out' ,  bioci2caddr.MCP23017_U3 , ChipName.dump_valve),
    3       :    ( ChipNum.gpio_exp ,   'in'  ,  bioci2caddr.MCP23017_U4 , ChipName.inputs    ),
    4       :    ( ChipNum.adc      ,   'adc' ,  bioci2caddr.MCP3424_U8  , ChipName.psi_sens  ),
    5       :    ( ChipNum.adc      ,   'adc' ,  bioci2caddr.MCP3424_U9  , ChipName.rail_sens ),
    6       :    ( ChipNum.fuelg    ,   'fg'  ,  bioci2caddr.MCP3424_U13 , ChipName.fuelg     ),
}

# hardware IO list
dict_octet_bioc = {
    # index  :    sw name,    schematic ID, OnRpi?, BCM, Function   I2C addr,    AB Port mask
    #                0           1           2       3      4          5             6
    #-------------------------------------------------------------------------------------------
    # Rpi GPIOs we only list the ones we use on this hardware
     0 :        ('SPI_MOSI' , 'MOSI'   ,  True,  10,  'in'  ,      0               ,       0),
     1 :        ('SPI_MISO' , 'MISO'   ,  True,   9,  'out' ,      0               ,       0),
     2 :        ('SPI_CLK'  , 'SCLK'   ,  True,  11,  'out' ,      0               ,       0),
     3 :        ('SPI_CS'   , 'CS'     ,  True,   8,  'out' ,      0               ,       0),
     4 :        ('SPI_INT'  , 'INT'    ,  True,  16,  'in'  ,      0               ,       0),
     5 :        ('DCMOTOR'  , 'PWMDCM' ,  True,  13,  'pwm' ,      0               ,       0),
     6 :        ('VARVALVE' , 'PWMVV'  ,  True,  12,  'pwm' ,      0               ,       0),
     7 :        ('GENRST'   , 'GENRST' ,  True,  26,  'out' ,      0               ,       0),
     8 :        ('POFF'     , 'POFF'   ,  True,  20,  'out' ,      0               ,       0),

    # GPIO expander U1
     9 :        ('A0'       , 'U1_GPA0',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0100 ),
    10 :        ('A1'       , 'U1_GPA1',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0200 ),
    11 :        ('A2'       , 'U1_GPA2',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0400 ),
    12 :        ('A3'       , 'U1_GPA3',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0800 ),
    13 :        ('A4'       , 'U1_GPA4',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x1000 ),
    14 :        ('TRI3'     , 'U1_GPA5',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x2000 ),
    15 :        ('TRI2'     , 'U1_GPA6',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x4000 ),
    16 :        ('TRI1'     , 'U1_GPA7',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x8000 ),
    17 :        ('OPTLED'   , 'U1_GPB4',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0010 ),
    18 :        ('BUZZER'   , 'U1_GPB5',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0020 ),
    19 :        ('PS4'      , 'U1_GPB6',  False,  0,  'out' , bioci2caddr.MCP23017_U1 ,  0x0040 ),

    # GPIO expander U2
    20 :        ('CV1'       , 'U2_GPA0' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0100 ),
    21 :        ('CV2'       , 'U2_GPA1' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0200 ),
    22 :        ('CV3'       , 'U2_GPA2' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0400 ),
    23 :        ('CV4'       , 'U2_GPA3' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0800 ),
    24 :        ('CV5'       , 'U2_GPA4' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x1000 ),
    25 :        ('CV6'       , 'U2_GPA5' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x2000 ),
    26 :        ('CV7'       , 'U2_GPA6' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x4000 ),
    27 :        ('CV8'       , 'U2_GPA7' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x8000 ),
    28 :        ('CV9'       , 'U2_GPB0' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0001 ),
    29 :        ('CV10'      , 'U2_GPB1' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0002 ),
    30 :        ('CV11'      , 'U2_GPB2' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0004 ),
    31 :        ('CV12'      , 'U2_GPB3' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0008 ),
    32 :        ('CV13'      , 'U2_GPB4' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0010 ),
    33 :        ('DRAW'      , 'U2_GPB5' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0020 ),
    34 :        ('PUMP_NEG'  , 'U2_GPB6' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0040 ),
    35 :        ('PUMP_POS'  , 'U2_GPB7' , False,  0,  'out' , bioci2caddr.MCP23017_U2 ,  0x0080 ),

    # GPIO expander U3
    36 :         ('DUMP'      , 'U3_GPA0' , False,  0,  'out' , bioci2caddr.MCP23017_U3 ,  0x0100 ),
    37 :         ('CHGSHDN'   , 'U3_GPA1' , False,  0,  'out' , bioci2caddr.MCP23017_U3 ,  0x0200 ),

    # GPIO expander U4
    38 :         ('CARTDETIN' , 'U4_GPA0' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0001 ),
    39 :         ('LOOPDETIN' , 'U4_GPA1' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0002 ),
    40 :         ('OPTOPOS1'  , 'U4_GPA2' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0004 ),
    41 :         ('OPTOPOS2'  , 'U4_GPA3' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0008 ),
    42 :         ('OPTOPOS3'  , 'U4_GPA4' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0010 ),
    43 :         ('IS_PS4_PWR', 'U4_GPA5' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0020 ),
    44 :         ('SWDET'     , 'U4_GPB0' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0100 ),
    45 :         ('EPD'       , 'U4_GPB1' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0200 ),
    46 :         ('CHGFULL'   , 'U4_GPB2' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0400 ),
    47 :         ('CHGFAST'   , 'U4_GPB3' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x0800 ),
    48 :         ('CHGERR'    , 'U4_GPB4' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x1000 ),
    49 :         ('ALLRT'     , 'U4_GPB5' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x2000 ),
    50 :         ('LOOPDETIN2', 'U4_GPB6' , False,  0,  'in'  , bioci2caddr.MCP23017_U4 ,  0x4000 ),

}

PIN_MAP_INPUTS_START_INDEX = 38

# ADC channels
dict_adc_channel = {
  # index   channel      i2c address         sw name
  0 :     ( 1 ,  bioci2caddr.MCP3424_U8,  'NC' ),
  1 :     ( 2 ,  bioci2caddr.MCP3424_U8,  'PSv'   ),
  2 :     ( 3 ,  bioci2caddr.MCP3424_U8,  'PS+'   ),
  3 :     ( 4 ,  bioci2caddr.MCP3424_U8,  'PS-'   ),
  4 :     ( 1 ,  bioci2caddr.MCP3424_U9,  'VBATT' ),
  5 :     ( 2 ,  bioci2caddr.MCP3424_U9,  '5VQ'   ),
  6 :     ( 3 ,  bioci2caddr.MCP3424_U9,  '-3V3'   ),
  7 :     ( 4 ,  bioci2caddr.MCP3424_U9,  '5V'    ),
}


# mechanical map
#   unit side
#  CV1         CV7
#  CV2         CV8
#  CV3         CV9
#  CV4         CV10
#  CV5         CV11
#  CV6         CV12 (draw)
#
#  outside world side
#

# Cartridge map
#  unit side
#  V11          V5
#  V7           V6
#  V4           V10
#  V3           V1
#  V2           V8
#  V9           V12

# pressure = valve OFF
# vacuum = valve ON (energised)

# dict_mech_placement = {
#   ON : 0x0100    # CV1   OFF  0xFEFF
#   ON : 0x0200    # CV2   OFF  0xFDFF
#   ON : 0x0400    # CV3   OFF  0xFBFF
#   ON : 0x0800    # CV4   OFF  0xF7FF
#   ON : 0x1000    # CV5   OFF  0xEFFF
#   ON : 0x2000    # CV6   OFF  0xDFFF
#   ON : 0x4000    # CV7   OFF  0xBFFF
#   ON : 0x8000    # CV8   OFF  0x7FFF
#   ON : 0x0001    # CV9   OFF  0xFFFE
#   ON : 0x0002    # CV10  OFF  0xFFFD
#   ON : 0x0004    # CV11  OFF  0xFFFB
#   ON : 0x0008    # CV12  OFF  0xFFF7
# }
class ValveBitMasks():

    # ON bitmasks
    V1_on  = 0x0002  # CV10
    V2_on  = 0x1000  # CV5
    V3_on  = 0x0800  # CV4
    V4_on  = 0x0400  # CV3
    V5_on  = 0x4000  # CV7
    V6_on  = 0x8000  # CV8
    V7_on  = 0x0200  # CV2
    V8_on  = 0x0004  # CV11
    V9_on  = 0x2000  # CV6
    V10_on = 0x0001  # CV9
    V11_on = 0x0100  # CV1
    V12_on = 0x0008  # cv12
    VallOn = 0xFF0F

    # OFF bitmasks
    V1_off  = 0xFFFD  # CV10
    V2_off  = 0xEFFF  # CV5
    V3_off  = 0xF7FF  # CV4
    V4_off  = 0xFBFF  # CV3
    V5_off  = 0xBFFF  # CV7
    V6_off  = 0x7FFF  # CV8
    V7_off  = 0xFDFF  # CV2
    V8_off  = 0xFFFB  # CV11
    V9_off  = 0xDFFF  # CV6
    V10_off = 0xFFFE  # CV9
    V11_off = 0xFEFF  # CV1
    V12_off = 0xFFF7  # cv12
    VallOff = 0x0000

    Pump_neg_on  = 0x0040
    Pump_neg_off = 0xFFBF
    Pump_pos_on  = 0x0080
    Pump_pos_off = 0xFF7F


class LEDs():
    red      = 'TRI1'
    green    = 'TRI2'
    blue     = 'TRI3'
    expander = ChipName().mux


class LEDsMask():
    red_on   = 0x8000
    green_on = 0x4000
    blue_on  = 0x2000

    red_off  = 0x7FFF
    green_off= 0xBFFF
    blue_off = 0xDFFF
    all_of   = 0x1FFF


##
# bit mask for the input expander when the value is set
class InputMask():
    cart_detect   = 0x0001
    loop_detect   = 0x0002
    opto_home     = 0x0004
    opto_engage   = 0x0008
    opto_burst    = 0x0010
    ps4_off       = 0x0020
    sw_detect     = 0x0100
    epd           = 0x0200
    battery_full  = 0x0400
    charge_fast   = 0x0800
    charge_error  = 0x1000
    charge_allert = 0x2000
    loop_detect2  = 0x4000


##
# Motor speeds
#
class MotSpeed():
    go = 100
    slow = 40
    stop = 0


##
# Pressure purged values
#
class PsiPurged():
    pos = 0.1
    neg = -0.1
    var = -0.1


