'''
i2c_master.py
This is a message sender it sends the I2C message to the Slave Address
usage:
    ... example data
    command = 0xE5
    value1 = INITIAL_COMMAND
    value2 = 0
    values = "%c%c%c" % (command, value1, value2)
    result = sendData(project_defaults.I2C_SLAVE_ADDRESS, values)
'''
import smbus
import time
import project_defaults

class i2c_master():
    sending = False
 
    # Open i2c bus 1 and for read  or write
    bus = smbus.SMBus(1)
    
    def send(self, slaveAddress, data):

        if self.sending == True:
            print ("i2c_master::send(...) Recursive call FAIL")
            return False

        if slaveAddress == 0:
            print ("i2c_master::send(...) slaveAddress invalid FAIL")
            return False

 
        intsOfData = list(map(ord, data))
        if intsOfData[1:] == 0:
            print ("i2c_master::send(...) data invalid FAIL")
            return False

        self.sending = True
        try:
            self.bus.write_i2c_block_data(slaveAddress, intsOfData[0], intsOfData[1:])
#            print("i2c_master::send(...) PASS")
            self.sending = False
            return True
    
        except:
            print("i2c_master::send(...) exception FAIL")
            self.sending = False
            return False


    def send_ChangeState(self, state):
        command = project_defaults.COMMAND_MESSAGE_TYPE
        value1 = state
        value2 = 0
        values = "%c%c%c" % (command, value1, value2)
        try:
            print ("sending...")
            return self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
        except:
            # added for [Errno 121] Remote I/O error
            print("i2c_master::send_ChangeState send reset to TLC59116 1")
            values = "%c%c%c" % (0x6B,0xA5,0x5A)
            self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
            return -1


    def send_status(self, status):
        command = project_defaults.STATUS_MESSAGE_TYPE
        value1 = status
        value2 = 0
        values = "%c%c%c" % (command, value1, value2)
        try:
            print ("sending...")
            return self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
        except:
            # added for [Errno 121] Remote I/O error
            print("i2c_master_send_status send reset to TLC59116 1")
            values = "%c%c%c" % (0x6B,0xA5,0x5A)
            self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
            return -1


    '''
    @brief get_status - get the status from the GUI slave
    @param self - this
    @return the read status byte
    '''
    def get_status(self):
        if self.sending == True:
            print ("i2c_master::get_status(...) Recursive call FAIL")
            time.sleep(0.1)
            return -1

        try:
            self.sending = True
            status = self.bus.read_byte_data(project_defaults.I2C_SLAVE_ADDRESS, 
                                           0x00)
            self.sending = False
            return status

        except:
#            if self.sending == True:
#                # added for [Errno 121] Remote I/O error
#                print("i2c_master::get_status Exception send reset to TLC59116 2")
#                time.sleep(0.25)
#                values = "%c%c%c" % (0x6B,0xA5,0x5A)
#                self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
#                time.sleep(0.25)
#                self.sending = False
#                return -1
#
#            else:
#                print("i2c_master::get_status Exception self.sending == %.2X sleep(0.25)"
#                    % (self.sending))
            time.sleep(0.25)
            self.sending = False
            return -1

    '''
    @brief get_state - get the state from the GUI slave
    @param self - this
    @return the read state byte
    '''
    def get_state(self):
        if self.sending == True:
            print ("i2c_master::get_state(...) Recursive call FAIL")
            time.sleep(0.1)
            return -1

        try:
            self.sending = True
            state = self.bus.read_byte_data(project_defaults.I2C_SLAVE_ADDRESS, 
                                           project_defaults.STATE_MESSAGE_TYPE)
            self.sending = False
            return state

        except:
#            if self.sending == True:
#                # added for [Errno 121] Remote I/O error
#                print("i2c_master::get_state Exception send reset to TLC59116 2")
#                time.sleep(0.25)
#                values = "%c%c%c" % (0x6B,0xA5,0x5A)
#                self.send(project_defaults.I2C_SLAVE_ADDRESS, values)
#                time.sleep(0.25)
#                self.sending = False
#                return -1
#
#            else:
#                print("i2c_master::get_state Exception self.sending == %.2X sleep(0.25)"
#                    % (self.sending))
            time.sleep(0.25)
            self.sending = False
            return -1

