# import serial_driver class
from serial_driver import serial_driver

class test_serial_driver(object):
     

    def init(self):
        print("test_serial_driver:: init()")
        self.sd = serial_driver()
        self.sd.init()

    def test_it(self):
        print("test_serial_driver:: test_it()")
        self.sd.test()

tsd = test_serial_driver()
tsd.init()
tsd.test_it()
