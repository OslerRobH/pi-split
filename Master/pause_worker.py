################################################
# Multiplexer Worker
# Author: Muhammad Shakeel, 
# refactored: deya.sanchez@gmail.com
################################################
import sys        # flushing stdout
import time       # so we can sleep
import common as cms    # for on_dev_pc 
import gui_defs as ui    # our GUI definitions


# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc

from threading import Thread



#########################################################
# This is the class to handle MUX worker operations
# This runs a thread that will switch to different MUX
# states as per mux_states.cfg in round robin fashion
#########################################################
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.shut_down = False
        self.paused_state = False
        self.init_vars()
        print("%s MainWorkerThread(Thread) called" % __file__)

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.stop = True         # we start with everything quiet
        self.task_state = WorkerStates.initialising   # state machine tracker
        self.paused_state = False
        self.UserReply = UsrPromptReply.unknown
        return

    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled :
            print("||: -> exiting")
            self.task_state = WorkerStates.exiting
            return True
        else:
            return False

    #########################################
    # state machine that deals with kicking
    # off the mux states
    #########################################
    def run(self):

        # keep running until the application exits
        while not self.cancelled:

            # Initialise this state machine
            #################################
            if self.task_state == WorkerStates.initialising:
                self.init_vars()
                self.task_state = WorkerStates.idle
                print("||: initialising -> idle")

            # Wait for a request to start the multiplexer
            #############################################
            elif self.task_state == WorkerStates.idle:
                if not self.stop:
                    self.task_state = WorkerStates.pausing
                    print("||: idle -> pausing")
                else:
                    time.sleep(5)
                    self.monitor_cancel()

            # Switch the multiplexer
            ########################
            elif self.task_state == WorkerStates.pausing:
                if self.UserReply == UsrPromptReply.yes:
                    print("||: paused -> done")
                    self.task_state = WorkerStates.done
                else:
                    time.sleep(1)


            # switch done!
            ####################################################
            elif self.task_state == WorkerStates.done:
                time.sleep(1)

            # bug!
            #######
            else:
                # check if the state has changed along the way 
                if self.task_state >= WorkerStates.max_state:
                    print("MUX: Bug not sure what error state we are in")
                    print(self.task_state)
                    while True:
                        pass

            self.monitor_cancel()            

            # Exiting, tidy up
            #############################    
            if self.task_state == WorkerStates.exiting:
                print("MUX thread exiting")
                self.shut_down = True
                break

        self.shut_down = True
        sys.stdout.flush()
        return

    ################################################################
    # Helpers
    ################################################################

    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        msplot_coords = []
        return msplot_coords

    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return 0
    def get_PlotMaxX(self):
        return 1
    def get_PlotMinY(self):
        return 0
    def get_PlotMaxY(self):
        return 1

    ###############################
    # returns True if there is an
    # mux error
    ###############################
    def get_IsError(self):
        return False

    ###############################
    # return true when we are in the
    # paused state
    ###############################
    def get_PromptActive(self):
        if self.task_state == WorkerStates.pausing:
            return True
        else:
            return False


    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        return ui.index_gui_dict.ixgui_continue

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    ###############################
    # Returns true if we are idle
    ###############################
    def get_IsPaused(self):
        if ( (self.task_state == WorkerStates.idle) and (self.stop ) ) or (self.task_state == WorkerStates.done):
            return True
        else:
            return False

    ###############################
    # Returns a string reflecting
    # the current mux state
    ###############################
    def get_StatusMsg(self):
        if self.task_state == WorkerStates.initialising:
            return 'initialising'
        elif self.task_state == WorkerStates.idle:
            return 'idle'
        elif self.task_state == WorkerStates.pausing:
            return 'paused'
        elif self.task_state == WorkerStates.done:
            return 'resumming'
        else:
            return 'pause bug'

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        print("||: got reply")
        if (bReply ):
            self.UserReply = UsrPromptReply.yes
        else:
            self.UserReply = UsrPromptReply.no
        return

    #############################
    # Clear the current error
    # to get us back to retry
    #############################
    def set_ClearError(self):
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        print("||: continuing...")
        if self.task_state == WorkerStates.idle:
            self.stop = False
        elif self.task_state == WorkerStates.done:
            self.init_vars()
            self.task_state = WorkerStates.idle
            self.stop = False
        else:
            print("||: cannot continue in current state %d" %self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        print("||: cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        print("||: Stop called")
        if not self.stop:
            self.stop = True
            self.task_state = WorkerStates.initialising
            print("||: stop -> initialising")
        else:
            print("||: stop already in progress")
        return

#########################################
# Task States
#########################################
class WorkerStates():
    initialising     = 0     # initialising our variables
    idle             = 1     # waiting for a request to change mux state
    pausing          = 2     # switch the mux
    done             = 3     # mux switched
    exiting          = 4     # shuting down this API

    max_state        = 5

###################################
# User prompt reply
###################################
class UsrPromptReply():
    yes     = 0
    no      = 1
    unknown = 2
