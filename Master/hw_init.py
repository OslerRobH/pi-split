################################################
# Hardware Initialisation worker
# deya.sanchez@gmail.com
################################################
import sys        # flushing stdout
import time       # so we can sleep
import common as cms    # for on_dev_pc 


on_dev_pc = cms.DebugFlags.on_dev_pc

from threading import Thread

# import gpio handling class here
fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

#########################################################
# This is the class to handle MUX worker operations
# This runs a thread that will switch to different MUX
# states as per mux_states.cfg in round robin fashion
#########################################################
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.hal_inited = False
        self.daemon = True
        self.cancelled = False
        self.shut_down = False
        self.gpio = hal.octet_gpio
        print("hal ", self.gpio)
        self.stop = True         # we start with everything quiet
        self.task_state = WorkerStates.initialising   # state machine tracker
        self.Error_active = False
        self.Error_code = HwErrorStates.Error_None
        print("%s MainWorkerThread(Thread) called" % __file__)

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.stop = True         # we start with everything quiet
        self.task_state = WorkerStates.initialising   # state machine tracker
        self.Error_active = False
        self.Error_code = HwErrorStates.Error_None

        return

    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled:
            print("HwIni: -> exiting")
            self.task_state = WorkerStates.exiting
            return True
        else:
            return False

    #########################################
    # state machine that deals with kicking
    # off the mux states
    #########################################
    def run(self):

        # keep running until the application exits
        while not self.cancelled:

            # Initialise this state machine
            #################################
            if self.task_state == WorkerStates.initialising:
                if not self.hal_inited:
                    self.gpio.bioc_init()
                    self.hal_inited = True
                self.init_vars()
                self.gpio.set_pin('BUZZER', 1)
                time.sleep(0.3)
                self.gpio.set_pin('BUZZER', 0)
                time.sleep(0.3)
                self.task_state = WorkerStates.idle
                print("HwIni: initialising -> idle")

            # Wait for a request to start the HW initialisation
            #############################################
            elif self.task_state == WorkerStates.idle:
                if not self.stop:
                    self.gpio.set_pin('BUZZER', 1)
                    time.sleep(0.3)
                    self.gpio.set_pin('BUZZER', 0)
                    time.sleep(0.3)
                    self.task_state = WorkerStates.done
                    print("HwIni: idle -> done")
                else:
                    time.sleep(1)

            # switch done!
            ####################################################
            elif self.task_state == WorkerStates.done:
                # monitor the charger
                time.sleep(10)

            # Error states
            ##############
            elif self.task_state == WorkerStates.failed:
                # our error is irrecoverable so we wait for death
                self.monitor_cancel()

            # bug!
            #######
            else:
                # check if the state has changed along the way 
                if self.task_state >= WorkerStates.max_state:
                    print("HwIni: Bug not sure what error state we are in")
                    print(self.task_state)
                    while True:
                        if self.stop == True or self.monitor_cancel() == True:
                            break
                        
            self.monitor_cancel()

            # Exiting, tidy up
            # ###################
            if self.task_state == WorkerStates.exiting:
                print("HWIni thread exiting")
                self.shut_down = True
                break

        self.shut_down = True
        sys.stdout.flush()
        return

    ################################################################
    # Helpers
    ################################################################


    #################################
    # return the error string
    #################################
    def Get_ErrorStr(self):
       return 'Bug!'

    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        msplot_coords = []
        return msplot_coords

    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return 0
    def get_PlotMaxX(self):
        return 1
    def get_PlotMinY(self):
        return 0
    def get_PlotMaxY(self):
        return 1

    ###############################
    # returns True if there is an
    # mux error
    ###############################
    def get_IsError(self):
        return self.Error_active

    ###############################
    # Always return false, no user
    # interaction required by mux
    ###############################
    def get_PromptActive(self):
            return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        return 0

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    ###############################
    # Returns true if we are idle
    ###############################
    def get_IsPaused(self):
        if ( (self.task_state == WorkerStates.idle) and self.stop or (self.task_state == WorkerStates.done) ):
            return True
        else:
            return False

    ###############################
    # Returns a string reflecting
    # the current mux state
    ###############################
    def get_StatusMsg(self):
        if   self.task_state == WorkerStates.initialising :
            return 'HW Initialising'
        elif self.task_state == WorkerStates.idle         :
            return 'HW Idle'
        elif self.task_state == WorkerStates.done   :
            return 'Ready'
        elif self.task_state == WorkerStates.exiting      :
            return 'HW shutting down'
        else:
            print("Unhandled state %d" % self.task_state)
            return 'Bug! - get_StatusMsg'

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        return

    #############################
    # Clear the current error
    # to get us back to retry
    #############################
    def set_ClearError(self):
        print("HwIni: Cannot clear irrecoverable error")
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        print("HwIni: continuing...")
        if self.task_state == WorkerStates.idle or self.task_state == WorkerStates.initialising:
            self.stop = False
        elif self.task_state == WorkerStates.done:
            self.task_state = WorkerStates.idle
            self.stop = False
        else:
            print("HwIni: Cannot continue in current state %d" %self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        print("HwIni: cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        print("HwIni: Stop called")
        if not self.stop:
            self.stop = True
            self.task_state = WorkerStates.initialising
            print("HwIni: stop -> initialising")
        else:
            print("HwIni: stop already in progress")
        return

#########################################
# Task States
#########################################
class WorkerStates():
    initialising     = 0     # initialising our variables
    idle             = 1     # waiting for a request to change mux state
    done             = 2     # PS4 is ON
    failed           = 3     # fudge, we have a bug
    exiting          = 4     # shuting down this API

    max_state        = 5

#########################################
# Error States
#########################################
class HwErrorStates():
    Error_None      = 0
    Error_Ps4Rst    = 1
    Error_Ps4Pon    = 2

# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.start()

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':                  # print status
            print(worker.get_StatusMsg())
        elif cmd == 'g':                # go
            worker.set_Continue()
        elif cmd == 'e':                # clear the error
            worker.set_ClearError()
        elif cmd == 'q':                # quit
            worker.set_Cancel()
            break
        elif cmd == 's':                # stop
            worker.Stop()


if __name__ == "__main__":

    main()

    sys.exit()
