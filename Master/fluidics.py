#!/usr/bin/env python3
# encoding: utf-8

##
# @package fluidics
#
# Module that controls pumps, solenoids and variable valve.
# monitors pressures and runs the cartridge recipe mixes.
#
#
# deya.sanchez@gmail.com
#
import gc
import sys
import time

from threading import Thread

import common as cms  # for on_dev_pc
import config_sequence as cfg
import socket                       # so we can identify the hostname
import bioc                 # so we know what hardware we need to talk to
fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

##
# Debugging flags
#
on_dev_pc = cms.DebugFlags.on_dev_pc
dbg_skip_fluidics = cms.DebugFlags.skip_fluidics


##
# set these to True to enable print debug on pump and valves
# writes to ports
#
dbg_pumpNvalves = False
dbg_dump_valve = False  # set to TRUE when tuning PID
dbg_windup_guard = False  # set to TRUE when tuning PID
dbg_sol_states = False

##
# calibration debug flags
#
if cms.calibrate_fluidics:
    ##
    # These are the ones you change
    #
    dbg_stop_at_psi_init = False
    dbg_print_decay = False
    dbg_stop_after_mix = True
else:
    ##
    # Don't change these
    #
    dbg_stop_at_psi_init = False
    dbg_print_decay = False
    dbg_stop_after_mix = False


##
# Fluidics  worker thread
#
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.gpio = hal.octet_gpio  # instance to hardware abstraction layer
        self.valve_Ic = bioc.ChipName.valves
        self.task_state = WorkerStates.initialising
        self.valves_state = 0  # all valves off
        self.shut_down = False
        self.warn_cnt = 0
        self.pid_warn_low = 0       # number of times our vv pressure is under threshold
        self.pid_warn_high = 0      # number of times our vv pressure is over threshold
        self.ctrl_entries = 0
        self.prev_entries = 0
        self.wcnt_lo_pos = 0
        self.wcnt_hi_pos = 0
        self.wcnt_lo_neg = 0
        self.wcnt_hi_neg = 0
        self.wcnt_time = 0
        self.cnt_time = 0
        self.prev_neg_pump_on = False     # how long have we had the neg pump on
        self.prev_pos_pump_on = False     # how long have we had the neg pump on
        self.var_max_ontime = 0
        self.prev_valve_port = -1.0        # previous value writen to the port valve
        self.print_status = True
        self.prev_var_val = -1.0
        self.have_feedback = False
        self.prev_had_feedback = False
        self.lost_fb_neg_ts = get_current_ms_time()     # time when we lost feedback (-)
        self.lost_fb_pos_ts = get_current_ms_time()     # time when we lost feedback (-)

        self.lost_fb_ts = 0
        self.topup = False

        # PID related
        self.PTerm = 0.0
        self.ITerm = 0.0
        self.DTerm = 0.0
        self.last_error = 0.0
        self.current_time = None
        self.last_time = None

        # Windup Guard
        self.int_error = 0.0

        self.output = 0.0

        # these get initialised twice at start-up in the init_vars method
        self.stop = True  # we start with everything quiet

        self.psi_pos = 0
        self.psi_neg = 0
        self.psi_var = 0
        self.Error_active = False
        self.Error_flavour = ErrorStates.all_good
        # self.pump_state = 0 # neg_pump = bit0, pos_pump = bit1
        self.pump_control_select = PressMon.negative # control loop (+) -> (v) -> (-) -> (+)

        self.valves_state = 0  # all off
        self.neg_pump_on = False
        self.pos_pump_on = False
        self.var_val = 0
        self.idle_check_pressure = True

        self.ps_status_cnt = 0    # count to print the pressure state

        print("%s MainWorkerThread(Thread) called" % __file__)

    def init_vars(self):
        ##
        # initialise our variables
        #
        self.pid_init()
        self.task_state = WorkerStates.initialising
        self.stop = True  # we start with everything quiet

        self.psi_pos = 0
        self.psi_neg = 0
        self.psi_var = 0
        self.Error_active = False
        self.Error_flavour = ErrorStates.all_good
        # self.pump_state = 0 # neg_pump = bit0, pos_pump = bit1
        self.pump_control_select = PressMon.negative

        self.valves_state = 0  # all off
        self.neg_pump_on = False
        self.pos_pump_on = False
        self.var_val = 0
        self.idle_check_pressure = True

        self.stop_pumpsNvalves()  # valves, and pumps off
        self.ps_status_cnt = 0    # count to print the pressure state

        # Master Sequential List of valve open/close events
        # created by parsing Adept script files
        #
        if dbg_skip_fluidics:
            wk_print("############################# Skip Fluidics")
        return

    def monitor_cancel(self):
        ##
        # Tests if we have received a cancel from our caller
        # call this regularly when blocking so we can bail
        # out nicely when told to do so
        #
        # @returns: TRUE if we have been told to cancel
        # the state machine
        if self.cancelled:
            self.stop_pumpsNvalves()
            self.task_state = WorkerStates.exiting
            wk_print("state machine [exiting]")
            return True
        else:
            return False

    def run(self):
        ##
        # Main state machine for this thread
        #
        while not self.cancelled:
            self.monitor_cancel()

            ##
            # Initialise this state machine
            #################################
            if self.task_state == WorkerStates.initialising:
                self.init_vars()
                wk_print("Switching PS4 Off")
                self.gpio.set_ps4(False)
                self.task_state = WorkerStates.idle
                wk_print("initialising -> idle")

            ##
            # Wait for a measurement request
            #################################
            elif self.task_state == WorkerStates.idle:
                if not self.stop:
                    # jump straight in
                    self.task_state = WorkerStates.stage_I
                    wk_print("idle -> stage I")
                else:
                    time.sleep(1)

            ##
            # Fluidics Stage I
            ###################
            elif self.task_state == WorkerStates.stage_I:

                if not self.stop and not self.cancelled:
                    if dbg_skip_fluidics:
                        wk_print("### Skipping Fluidics Stage I -> done stage I")
                        self.task_state = WorkerStates.done_stage_I
                    else:
                        # get our system up to pressure
                        self.start_sys_pressure(False)

                        self.warn_cnt = 0
                        self.pid_warn_low = 0  # number of times our vv pressure is under threshold
                        self.pid_warn_high = 0  # number of times our vv pressure is over threshold
                        self.ctrl_entries = 0
                        self.wcnt_lo_pos = 0
                        self.wcnt_hi_pos = 0
                        self.wcnt_lo_neg = 0
                        self.wcnt_hi_neg = 0
                        self.wcnt_time = 0
                        self.cnt_time = 0

                    if not self.stop and not self.cancelled:
                        if on_dev_pc:
                            self.recipe(no_mix)
                        else:
                            self.topup = True
                            self.recipe(cfg.first_mix)

                    if not self.stop and not self.cancelled:
                        wk_print("first mix recipe done")
                        self.print_pidstats()

                        self.topup = False
                        # all valves de-energised
                        self.idle_check_pressure = False
                        self.neg_pump_on = False
                        self.pos_pump_on = False
                        self.var_val = Vcal.s_OFF
                        self.apply_settings()

                        if dbg_stop_after_mix and cms.calibrate_fluidics:
                            go_ahead = input("continue? [y/n]")

                            if go_ahead == 'n':
                                self.Stop()

                        if not self.stop:
                            # switch the PS4 on now
                            wk_print("Switching PS4 ON...")
                            self.gpio.set_ps4(True)

                            # move to the next state based on our status
                            if not self.stop and not self.cancelled:
                                self.task_state = WorkerStates.done_stage_I
                                wk_print("Fluid stage I -> stage I done")

            ##
            # Stage I done
            ################
            elif self.task_state == WorkerStates.done_stage_I:
                if dbg_skip_fluidics:
                    time.sleep(2.5)
                elif not self.stop and not self.cancelled:
                    # we sit here until told to parse the second part of the config scripts
                    if self.idle_check_pressure:
                        self.hold_pressure(False)
                        self.idle_check_pressure = False
                    else:
                        time.sleep(Vcal.sample_phold_time)
                        self.idle_check_pressure = True

            ##
            # Stage II
            ###########
            elif self.task_state == WorkerStates.stage_II:
                if dbg_skip_fluidics:
                    wk_print("### Skipping Fluidics Stage II -> done")
                    self.task_state = WorkerStates.done
                else:
                    wk_print("checking pressure")
                    self.hold_pressure(False)
                    self.ps_status_cnt = 301

                    if self.stop or self.cancelled:
                        wk_print("bailing out of stage II")
                    else:
                        if on_dev_pc:
                            self.recipe(no_mix)
                        else:
                            self.topup = True
                            self.recipe(cfg.second_mix)
                            self.topup = False

                        # all valves de-energised
                        self.idle_check_pressure = True
                        self.neg_pump_on = False
                        self.pos_pump_on = False
                        self.apply_settings()

                        if not self.stop and not self.cancelled:
                            self.print_pidstats()
                            self.task_state = WorkerStates.done
                            wk_print("stage II -> done")

            ##
            # Work done
            #############
            elif self.task_state == WorkerStates.done:
                # all work done wait here until told to re-start and do it all again
                # we drop out of this state with a continue call from our parent API
                if dbg_skip_fluidics:
                    time.sleep(3)

                elif not self.stop and not self.cancelled:
                    # we sit here until told to parse the second part of the config scripts
                    if self.idle_check_pressure:
                        self.hold_pressure(False)
                        self.idle_check_pressure = False
                    else:
                        self.gpio.rd_port() # service our hardware
                        time.sleep(Vcal.sample_phold_time)
                        self.idle_check_pressure = True

            ##
            # Release pressure because the cartridge is about to come out
            #############################################################
            elif self.task_state == WorkerStates.release:
                # we drop into this state with a continue call from our parent API
                self.init_vars()
                wk_print("Switching PS4 Off")
                self.gpio.set_ps4(False)
                self.task_state = WorkerStates.idle  # go back to the start
                wk_print("release -> idle")

            ##
            # TODO: Error states
            #############################
            elif self.task_state == WorkerStates.error:
                if not self.Error_active:
                    self.task_state = WorkerStates.recover
                    wk_print("code this up")
                else:
                    time.sleep(1)

            ##
            # Attempt to recover from an error and start again
            ####################################################
            elif self.task_state == WorkerStates.recover:
                prev_stop = self.stop  # make a backup of this state because we are just about to stomp on it
                self.init_vars()  # reset our state, stomped
                self.stop = prev_stop  # now restore
                self.task_state = WorkerStates.stage_I  # and go try again
                wk_print("recover -> stage I")

            ##
            # Exiting, tidy up
            #############################
            if self.task_state == WorkerStates.exiting:
                # make sure pumps and valves are off before we bail out
                self.stop_pumpsNvalves()
                break

            ##
            # We've been asked to stop
            #############################
            if self.task_state == WorkerStates.stopping:
                wk_print("stop state entry")
                self.stop_pumpsNvalves()
                self.gpio.set_ps4(False)
                self.task_state = WorkerStates.initialising
                wk_print("stopping -> initialising")

        # while

        # tidy up and exit
        self.stop_pumpsNvalves()
        self.shut_down = True
        sys.stdout.flush()
        return
    # end of the state machine

    def all_valves_energised(self, enable):
        ##
        # Debug Helper, not intended for use within this module
        # typically called from the automated test.
        #
        # @param enable TRUE to activate the solenoids
        #
        assert (enable or not enable), ("Invalid parameter ", enable)
        dbg = cms.WkDbg(worker_name="FLD energise", spam_enabled=dbg_sol_states)
        if enable:
            wk_print("All valves ON")
        else:
            wk_print ("All valves OFF")
        # figure out if we need to do this fast or slow (for debug testing)
        if self.task_state == WorkerStates.stage_I:
            if enable:
                self.valves_state = self.valves_state | bioc.ValveBitMasks.VallOn
                dbg.w_print(("valve state 0x%x" % self.valves_state))
            else:
                self.valves_state = self.valves_state & bioc.ValveBitMasks.VallOff
                dbg.w_print(("valve state 0x%x" % self.valves_state))
        else:
            # turn valves ON - delays make sequence audible
            for valve in ('CV1', 'CV2', 'CV3'):
                time.sleep(0.2)
                self.valves_state = self.gpio.set_pin(valve, enable)
                dbg.w_print(("valve state 0x%x" % self.valves_state))
            time.sleep(0.4)
            for valve in ('CV4', 'CV5', 'CV6'):
                time.sleep(0.2)
                self.valves_state = self.gpio.set_pin(valve, enable)
                dbg.w_print(("valve state 0x%x" % self.valves_state))
            time.sleep(0.4)
            for valve in ('CV7', 'CV8', 'CV9'):
                time.sleep(0.2)
                self.valves_state = self.gpio.set_pin(valve, enable)
                dbg.w_print(("valve state 0x%x" % self.valves_state))
            time.sleep(0.4)
            for valve in ('CV10', 'CV11', 'CV12'):
                time.sleep(0.2)
                self.valves_state = self.gpio.set_pin(valve, enable)
                dbg.w_print(("valve state 0x%x" % self.valves_state))

    ################################################################
    # Module Helpers
    ################################################################
    def start_sys_pressure(self, i_debug_enabled):
        ##
        # Get a cold booted system up to the initial pressure
        # Attempts to get as close to the target pressures from
        # a cold unit before we attempt to control the pressure
        # during normal operation.
        #
        # @param i_debug_enabled - set to True when this
        # method is being called from outside the fluidic state
        # machine. Otherwise, any calls within this module
        # should call this method with i_debug_enabled set to
        # False
        #
        self.print_status = False

        # purge the system so we always start in a known state
        self.stop_pumpsNvalves()
        # read our starting pressures
        self.psi_neg = self.gpio.psi('PS-')
        self.psi_pos = self.gpio.psi('PS+')
        self.psi_var = self.gpio.psi('PSv')
        wk_print("PURGED:(+) %2.2fpsi (-) %2.2fpsi (v) %2.2fpsi (+) %s (-) %s (v) %3.2f" % (
            self.psi_pos, self.psi_neg, self.psi_var, ('ON' if self.pos_pump_on else 'OFF'),
            ('ON' if self.neg_pump_on else 'OFF'), self.var_val))
        # we start with all our valves closed
        self.valves_state = 0  # all valves off
        wk_print(
            "PID settings: Kp=%5.1f Ki=%5.1f sv=%5.1f min=%5.1f max=%5.1f wg=%5.1f s_dump=%5.1f t_dump=%5.1f" % (
                Vcal.Kp, Vcal.Ki, Vcal.s_start_val, Vcal.s_endstop_min, Vcal.s_endstop_max, Vcal.wg,
                Vcal.s_dump, Vcal.t_dump))

        self.neg_pump_on = False
        self.pos_pump_on = False
        self.valves_state = self.set_all_solenoids(False) # all solenoids including draw de-energised
        self.apply_settings()
        dv_print("Dump valve OFF")
        self.set_dump(False)
        # read our starting pressures
        self.psi_neg = self.gpio.psi('PS-')
        self.psi_pos = self.gpio.psi('PS+')
        self.psi_var = self.gpio.psi('PSv')
        wk_print("TARGETS: (+) %2.2fpsi (v) %2.2fpsi (-) %2.2fpsi" % (Psi.pos_target, Psi.var_target, Psi.neg_target))
        wk_print("STARTING:(+) %2.2fpsi (-) %2.2fpsi (v) %2.2fpsi (+) %s (-) %s (v) %2.2f" % (
            self.psi_pos, self.psi_neg, self.psi_var, ('ON' if self.pos_pump_on else 'OFF'),
            ('ON' if self.neg_pump_on else 'OFF'), self.var_val))
        wk_print("Switching all pumps ON")
        # set the variable valve to a good throughput
        self.var_val = Vcal.s_start_val
        # then switch both pumps on and monitor all the sensors
        self.pos_pump_on = True
        self.neg_pump_on = True
        self.apply_settings()  # typically 12ms
        wk_print("Starting thresholds (+) %2.2f < target < %2.2f (v) %2.2f > target > %2.2f (-) %2.2f > target > %2.2f "
              % (PsiWarn.start_pos_lo, PsiWarn.start_pos_hi,
                 PsiWarn.start_var_lo, PsiWarn.start_var_hi,
                 PsiWarn.start_neg_lo, PsiWarn.start_neg_hi))
        wk_print("------Pressurising------")
        for i in range(100):
            if not i_debug_enabled and (self.stop or self.cancelled):
                self.stop_pumpsNvalves()
                print("start_sys_pressure bailing out")
                break
            prev_pos = self.psi_pos
            self.psi_pos = self.gpio.psi('PS+')  # 33~44ms
            if ((PsiWarn.start_pos_lo < self.psi_pos < PsiWarn.start_pos_hi) or
                    (PsiWarn.start_pos_hi < self.psi_pos)):
                self.pos_pump_on = False
            self.apply_settings()  # typically 12ms

            prev_var = self.psi_var
            self.psi_var = self.gpio.psi('PSv')  # 33~44ms
            if ( (PsiWarn.start_var_lo > self.psi_var > PsiWarn.start_var_hi) or
                    (PsiWarn.start_var_hi > self.psi_var)):
                self.var_val = Vcal.s_OFF
            # correct for variable valve overshoots
            if self.release_var_val_psi():
                print("OVERSHOOT: (v) %2.2fpsi dump valve opened to release pressure" % self.psi_var)
                # too much of a drop in pressure?
                # tweak t_dump, s_dump and s_post_dump
                # still too much of a drop?
                # shrink the foam on the dump tubing
                prev_var = self.psi_var
                self.psi_var = self.gpio.psi('PSv')  # 33~44ms
            self.apply_settings()  # typically 12ms

            prev_neg = self.psi_neg
            self.psi_neg = self.gpio.psi('PS-')  # 33~44ms typically 33ms
            if ( (PsiWarn.start_neg_lo > self.psi_neg > PsiWarn.start_neg_hi) or
                    (PsiWarn.start_neg_hi > self.psi_neg)):
                self.neg_pump_on = False
            self.apply_settings()  # typically 12ms

            print("%3d STARTING:[(+) %2.2fpsi %s D(%2.2f)][(-) %2.2fpsi %s D(%2.2f)][(v) %2.2fpsi %3.2f D(%2.2f)]" % (
                i,
                self.psi_pos, ('ON ' if self.pos_pump_on else 'OFF'), (prev_pos - self.psi_pos),
                self.psi_neg, ('ON ' if self.neg_pump_on else 'OFF'), (prev_neg - self.psi_neg),
                self.psi_var, self.var_val, (prev_var - self.psi_var)))

            if not self.pos_pump_on and not self.neg_pump_on and self.var_val == Vcal.s_OFF:
                break

        self.neg_pump_on = False
        self.pos_pump_on = False
        self.var_val = Vcal.s_OFF
        self.have_feedback = True
        self.lost_fb_ts = get_current_ms_time() # prevent the feedback warning at initialisation
        self.apply_settings()
        print("")
        # if all our pressures are within tolerance threshold then say we are at pressure
        if ((PsiWarn.warn_neg_lo > self.psi_neg > PsiWarn.warn_neg_hi) and
                (PsiWarn.warn_pos_lo < self.psi_pos < PsiWarn.warn_pos_hi) and
                (PsiWarn.warn_var_lo > self.psi_var > PsiWarn.warn_var_hi)):
            print("System at pressure")
        else:
            print("CALIBRATION ERROR ---- SYSTEM NOT AT PRESSURE")
            if not (PsiWarn.warn_neg_lo > self.psi_neg > PsiWarn.warn_neg_hi):
                print("Negative pressure out of bounds")
            if not (PsiWarn.warn_pos_lo < self.psi_pos < PsiWarn.warn_pos_hi):
                print("Positive pressure out of bounds")
            if not (PsiWarn.warn_var_lo > self.psi_var > PsiWarn.warn_var_hi):
                print("Variable valve pressure out of bounds")

        # ------ for calibration debug --------
        # if you want the decay to print out then set dbg_print_decay
        if dbg_print_decay and cms.calibrate_fluidics:
            # print decay values
            for i in range(25):
                if not i_debug_enabled and (self.stop or self.cancelled):
                    print("start_sys_pressure bailing out")
                    break

                prev_pos = self.psi_pos
                self.psi_pos = self.gpio.psi('PS+')  # 33~44ms

                prev_var = self.psi_var
                self.psi_var = self.gpio.psi('PSv')  # 33~44ms

                prev_neg = self.psi_neg
                self.psi_neg = self.gpio.psi('PS-')  # 33~44ms typically 33ms

                print("%d DBG DECAY:[(+) %2.2fpsi %s D(%2.2f)][(-) %2.2fpsi %s D(%2.2f)][(v) %2.2fpsi %3.2f D(%2.2f)]" % (
                    i,
                    self.psi_pos, ('ON ' if self.pos_pump_on else 'OFF'), (prev_pos - self.psi_pos),
                    self.psi_neg, ('ON ' if self.neg_pump_on else 'OFF'), (prev_neg - self.psi_neg),
                    self.psi_var, self.var_val, (prev_var - self.psi_var)))

        if dbg_stop_at_psi_init and cms.calibrate_fluidics:
            go_ahead = input("continue? [y/n]")

            if go_ahead == 'n':
                self.Stop()
        # ------ end debug --------

        self.print_status = True

    def stop_pumpsNvalves(self):
        ##
        # stop everything
        #
        dv_print("[4]Dump valve ON")
        self.set_dump(True)
        self.gpio.set_v_valve(100)
        self.psi_neg = self.gpio.psi('PS-')
        self.psi_pos = self.gpio.psi('PS+')
        self.psi_var = self.gpio.psi('PSv')

        print("Purging the system ...")
        while (self.psi_var < Psi.var_purged or
                self.psi_neg < Psi.neg_purged):
            self.psi_neg = self.gpio.psi('PS-')
            self.psi_pos = self.gpio.psi('PS+')
            self.psi_var = self.gpio.psi('PSv')
            wk_print("PURGING: (+) %2.2fpsi (v) %2.2fpsi (-) %2.2fpsi" %
                     (self.psi_pos, self.psi_neg, self.psi_var))
        print("System purged")
        self.set_dump(False)
        dv_print("Dump valve OFF")
        self.var_val = Vcal.s_OFF
        port_value = 0
        self.neg_pump_on = False  # cache the state
        self.pos_pump_on = False  # cache the state
        self.gpio.set_port(self.valve_Ic, port_value)
        self.gpio.set_v_valve(self.var_val)

        return

    def set_dump(self, enable):
        ##
        # Dump valve helper
        #
        if enable:
            self.gpio.set_pin('DUMP', 1)

        else:
            self.gpio.set_pin('DUMP', 0)

    def apply_settings(self):
        ##
        # Apply pressure settings
        # relies on self.valve_state having
        # the bit map state of CV1-CV12
        #
        port_value = self.valves_state
        test_variable = True

        # ----- Variable valve time check ----
        # has it been on at full whack and is it still on?
        if test_variable and self.var_val == Vcal.s_endstop_max:
            if self.prev_var_val == Vcal.s_endstop_max:
                # for the variable pressure we expect a rise of 0.2psi for every visit 30ms
                if self.psi_var < -1.5:
                    self.var_val = Vcal.s_endstop_min
                    print("(v) OFF timeout")

        # ---------- Calculate the bit pattern for the hardware ------------------
        if self.neg_pump_on:
            port_value = port_value | bioc.ValveBitMasks.Pump_neg_on
        else:
            port_value = port_value & bioc.ValveBitMasks.Pump_neg_off
        if self.pos_pump_on:
            port_value = port_value | bioc.ValveBitMasks.Pump_pos_on
        else:
            port_value = port_value & bioc.ValveBitMasks.Pump_pos_off
        dbg_print(port_value)

        # only WRITE it to the HARDWARE if we are making a change
        # ------------------------------------------------------
        if self.prev_valve_port != port_value:
            self.gpio.set_port(self.valve_Ic, port_value) # solenoids/valves and pumps
        if self.prev_var_val != self.var_val:   # variable PITA valve
            self.gpio.set_v_valve(self.var_val)

        # save the state we've just written
        self.valves_state = port_value

        # keep copies for nex time
        # -------------------------
        self.prev_valve_port = port_value   # value written to hardware
        self.prev_var_val = self.var_val    # variable valve setting
        self.prev_pos_pump_on = self.pos_pump_on    # positive
        self.prev_neg_pump_on = self.neg_pump_on    # negative

        # check how long we have been prevented from reading
        # the sensors if it is more than 250ms then top up the
        # pumps
        if self.prev_had_feedback and not self.have_feedback:
            current_time = get_current_ms_time()
            self.lost_fb_ts = current_time
            self.lost_fb_neg_ts = current_time
            self.lost_fb_pos_ts = current_time

        # No feedback
        if not self.have_feedback:
            current_time = get_current_ms_time()
            delta = current_time - self.lost_fb_pos_ts
            if self.topup and delta > Pumps.t_pos_topup:
                print("bypass + top up")
                # self.pos_pump_on = True
                # now update our timestamp
                self.lost_fb_pos_ts = get_current_ms_time()
            delta = current_time - self.lost_fb_neg_ts
            if self.topup and delta > Pumps.t_neg_topup:
                print("bypass - top up")
                # self.neg_pump_on = True
                # now update our timestamp
                self.lost_fb_neg_ts = get_current_ms_time()

        # we have regained feedback
        elif not self.prev_had_feedback:
            delta = get_current_ms_time() - self.lost_fb_ts
            print("                                         WARNING:No feedback for %3d ms" % delta, end='\r')

        self.prev_had_feedback = self.have_feedback
        if self.have_feedback:
            # ####### print status #######
            self.print_psi_status()

        # if we haven't had any feedback for more than
        self.have_feedback = False
        return

    def pid_init(self):
        ##
        # variable valve calibration pid initialise
        #
        wk_print("Entry: calibrate_init")

        self.current_time = time.time()
        self.last_time = self.current_time
        self.pid_clear()

    def pid_clear(self):
        ##
        # variable valve clear PID coefficients
        #
        wk_print("Entry: calibrate_clear")
        """Clears PID computations and coefficients"""

        self.PTerm = 0.0
        self.ITerm = 0.0
        self.DTerm = 0.0
        self.last_error = 0.0

        # Windup Guard
        self.int_error = 0.0

        self.output = 0.0

    def run_pid(self):
        ##
        # Calculates PID value for given reference feedback
        #
        # .. math::
        #     u(t) = K_p e(t) + K_i \int_{0}^{t} e(t)dt + K_d {de}/{dt}
        #
        # .. figure:: images/pid_1.png
        #    :align:   center
        #
        #    Test PID with Kp=1.2, Ki=1, Kd=0.001 (test_pid.py)
        # 
        #

        # error = Set Point - feedback value
        error = Psi.var_target - self.psi_var

        # our set point is
        self.current_time = time.time()
        delta_time = self.current_time - self.last_time
        delta_error = error - self.last_error

        if delta_time >= Vcal.sample_time:
            self.PTerm = Vcal.Kp * error
            self.ITerm += error * delta_time

            if self.ITerm < -Vcal.wg:
                wg_print("(-) wg")
                self.ITerm = -Vcal.wg
            elif self.ITerm > Vcal.wg:
                wg_print("(+) wg")
                self.ITerm = Vcal.wg

            self.DTerm = 0.0
            if delta_time > 0:
                self.DTerm = delta_error / delta_time

            # Remember last time and last error for next calculation
            self.last_time = self.current_time
            self.last_error = error

            self.output = self.PTerm + (Vcal.Ki * self.ITerm) + (Vcal.Kd * self.DTerm)

            # print("error %f pid valve value = %f" % ( error, self.output))
            # cap values
            if self.output > Vcal.s_endstop_max:
                self.output = Vcal.s_endstop_max
            if self.output < Vcal.s_endstop_min:
                self.output = Vcal.s_endstop_min
        return self.output

    def regulate_negative(self, active):
        ##
        # Regulate Negative Pressure
        #
        # @param active - set to True when we should
        # read the ADC for the pressure. Set to False to
        # guesstimate the pressure
        #

        # read from the ADC only when we are active
        self.have_feedback = True
        if active:
            self.psi_neg = self.gpio.psi('PS-')  # read PS3, CH4 U8, negative pressure
        elif self.neg_pump_on:  # otherwise guesstimate
            self.psi_neg += Pumps.neg_inc_delta  # change when on
        else:
            self.psi_neg += Pumps.neg_dec_delta  # change when off

        if self.psi_neg > Pumps.t_neg:  # not enough vacuum
            self.neg_pump_on = True  # cache the state
        else:
            self.neg_pump_on = False  # too much vacuum
        return

    def regulate_positive(self, active):
        ##
        # Regulate Positive Pressure
        #
        # @param active - set to True when we should
        # read the ADC for the pressure. Set to False to
        # guesstimate the pressure
        #

        # read from the ADC only when we are active
        self.have_feedback = True
        if active:
            self.psi_pos = self.gpio.psi('PS+')  # read PS2, CH3 U8, positive pressure
        elif self.pos_pump_on:  # otherwise guesstimate
            self.psi_pos += Pumps.pos_inc_delta  # change when on
        else:
            self.psi_pos += Pumps.pos_dec_delta  # change when off

        if self.psi_pos < Pumps.t_pos:  # too little positive
            self.pos_pump_on = True
        else:
            self.pos_pump_on = False  # too much positive pressure
        return

    def release_var_val_psi(self):
        ##
        # Release pressure from the variable valve
        #
        # if the pressure is too high then release the pressure
        #
        # @param active - set to True when we should
        # read the ADC for the pressure. Set to False to
        # guesstimate the pressure
        #
        released = False
        if self.psi_var < Vcal.t_dump:
            # wk_print("excess")
            self.var_val = Vcal.s_dump
            self.gpio.set_v_valve(self.var_val)
            dv_print("Releasing pressure p%f s%f" % (self.psi_var, self.var_val))
            self.set_dump(True)
            time.sleep(0.002)
            self.set_dump(False)
            dv_print("Dump valve OFF")
            # we've released pressure so now close the valve
            self.var_val = Vcal.s_post_dump
            released = True
        return released

    def regulate_var_valve(self):
        ##
        # Regulate Variable valve Pressure
        #

        # read from the ADC
        self.psi_var = self.gpio.psi('PSv')  # read PS1, CH2, U8, variable valve
        self.have_feedback = True

        # if the pressure is within 5% close the valve and bail out
        if (self.psi_var > PsiWarn.close_var_hi) and (self.psi_var < PsiWarn.close_var_lo):
            self.var_val = Vcal.s_OFF
            return

        # if the pressure is too high then release the pressure and skip the pid
        if self.release_var_val_psi():
            return

        self.var_val = self.run_pid()  # self.run_pid(self.psi_var)
        return

    def pid_pressurise_system(self):
        ##
        # Try and calibrate the variable valve
        #

        self.ctrl_entries += 1      # counter for warnings
        self.ps_status_cnt += 1     # counter for status

        # advance the state
        # -----------------------------------------------
        # control loop (+) -> (v) -> (-) -> (+)
        if self.pump_control_select == PressMon.positive:
            self.pump_control_select = PressMon.variable
        elif self.pump_control_select == PressMon.variable:
            self.pump_control_select = PressMon.negative
        else:
            self.pump_control_select = PressMon.positive

        # Control the pressures
        # ####### Positive pressure check #######
        if self.pump_control_select == PressMon.positive:
            self.regulate_positive(True)
            self.regulate_negative(False)

        # ####### variable pressure check #######
        elif self.pump_control_select == PressMon.variable:
            self.regulate_var_valve()
            self.regulate_positive(False)
            self.regulate_negative(False)

        # ###### Negative pressure check #######
        elif self.pump_control_select == PressMon.negative:
            self.regulate_positive(False)
            self.regulate_negative(True)

        # if all our pressures are within warning thresholds then say we are at pressure
        if (PsiWarn.warn_neg_lo > self.psi_neg > PsiWarn.warn_neg_hi and
           PsiWarn.warn_pos_lo < self.psi_pos < PsiWarn.warn_pos_hi and
           PsiWarn.warn_var_lo > self.psi_var > PsiWarn.warn_var_hi):
            return True

        return False

    def print_pidstats(self):
        ##
        # Print our PID stats
        #
        print("============================================")
        print("           FLUIDIC CONTROL STATS")
        print("Total Time warnings %d out of %d [%3.1f%%]" % (
            self.wcnt_time, self.cnt_time, (100 * self.wcnt_time/self.cnt_time)))
        print("Warning tolerance at %s" % PsiWarn.tolerance)
        print("Var Pressure Warnings %d out of %d control entries [%3.1f%%]" % (
            (self.pid_warn_high + self.pid_warn_low), self.ctrl_entries,
            (100 * (self.pid_warn_high + self.pid_warn_low))/self.ctrl_entries))
        print("Over pressure warnings %d [%3.1f%%]" % (
            self.pid_warn_high, (100* self.pid_warn_high)/self.ctrl_entries))
        print("Under pressure warnings %d [%3.1f%%]" % (
            self.pid_warn_low, (100 * self.pid_warn_low)/self.ctrl_entries))
        print("- - - - - - - - - - - - - - - - - - - - - - ")
        print("(+) Pressure Warnings %d out of %d control entries [%3.1f%%]" % (
            (self.wcnt_hi_pos + self.wcnt_lo_pos), self.ctrl_entries,
            (100 * (self.wcnt_hi_pos + self.wcnt_lo_pos))/self.ctrl_entries))
        print("Over pressure warnings %d [%3.1f%%]" % (
            self.wcnt_hi_pos, (100 * self.wcnt_hi_pos)/self.ctrl_entries))
        print("Under pressure warnings %d [%3.1f%%]" % (
            self.wcnt_lo_pos, (100 * self.wcnt_lo_pos)/self.ctrl_entries))
        print("- - - - - - - - - - - - - - - - - - - - - - ")
        print("(-) Warnings %d out of %d control entries [%3.1f%%]" % (
            (100 * (self.wcnt_hi_neg + self.wcnt_lo_neg)), self.ctrl_entries,
            (100 * (self.wcnt_hi_neg + self.wcnt_lo_neg))/self.ctrl_entries))
        print("Over Pressure warnings %d [%3.1f%%]" % (
            self.wcnt_hi_neg, (100 * self.wcnt_hi_neg)/self.ctrl_entries))
        print("Under Pressure warnings %d [%3.1f%%]" % (
            self.wcnt_lo_neg, (100 * self.wcnt_lo_neg)/self.ctrl_entries))
        print("PID settings: Kp=%2.1f Ki=%2.1f sv=%2.1f min=%3.1f max=%3.1f wg=%2.1f s_dump=%2.1f t_dump=%2.1f" %
                (Vcal.Kp, Vcal.Ki, Vcal.s_start_val,
                 Vcal.s_endstop_min, Vcal.s_endstop_max, Vcal.wg,
                 Vcal.s_dump, Vcal.t_dump))
        print("t_pos=%5.2f t_neg=%5.2f" % (Pumps.t_pos, Pumps.t_neg))
        print("============================================")

    def print_psi_status(self):
        ##
        # Print pressure status
        #
        if not self.print_status:
            return

        have_warning = False
        if self.prev_entries != self.ctrl_entries:
            if self.psi_var > PsiWarn.warn_var_lo:
                self.pid_warn_low += 1
                have_warning = True
            if self.psi_var < PsiWarn.warn_var_hi:
                self.pid_warn_high += 1
                have_warning = True
            if self.psi_neg < PsiWarn.warn_neg_hi:
                self.wcnt_lo_neg += 1
                have_warning = True
            if self.psi_neg > PsiWarn.warn_neg_lo:
                self.wcnt_hi_neg += 1
                have_warning = True
            if self.psi_pos > PsiWarn.warn_pos_hi:
                self.wcnt_hi_pos += 1
                have_warning = True
            if self.psi_pos < PsiWarn.warn_pos_lo:
                self.wcnt_lo_pos += 1
                have_warning = True

        self.prev_entries = self.ctrl_entries

        # order of control positive -> variable -> negative
        if have_warning:
            self.warn_cnt += 1
            print("WARNING%s: [%s(+) %2.2f psi %s][%s(-) %2.2f psi %s][%s(v) %2.2f psi %3.1f]" % (
                    dbg_warn_count(self.warn_cnt,self.ctrl_entries),
                    ('*' if self.pump_control_select == PressMon.positive else ' '),
                    self.psi_pos, ('ON ' if self.pos_pump_on else 'OFF'),
                    ('*' if self.pump_control_select == PressMon.negative else ' '),
                    self.psi_neg, ('ON ' if self.neg_pump_on else 'OFF'),
                    ('*' if self.pump_control_select == PressMon.variable else ' '),
                    self.psi_var, self.var_val ))
        else:
            if self.ps_status_cnt > PsiWarn.status_report:
                print("STATUS: [%s(+) %2.2f psi %s][%s(-) %2.2f psi %s][%s(v) %2.2f psi %3.1f]" % (
                    ('*' if self.pump_control_select == PressMon.positive else ' '),
                    self.psi_pos, ('ON ' if self.pos_pump_on else 'OFF'),
                    ('*' if self.pump_control_select == PressMon.negative else ' '),
                    self.psi_neg, ('ON ' if self.neg_pump_on else 'OFF'),
                    ('*' if self.pump_control_select == PressMon.variable else ' '),
                    self.psi_var, self.var_val ))
                self.ps_status_cnt = 0

    def hold_pressure(self, debug_enable):
        ##
        # Keep an eye on the  pressure
        # and adjust if needed, typically
        # called when we are not running
        # a recipe
        #
        # @param debug_enable - True to run
        # stand alone mode. False for normal
        # operation
        #
        for i in range(15):
            if not debug_enable and (self.stop or self.cancelled):
                break
            if self.check_all_pressures(): # bail out as soon as we get to pressure
                break

        # system should be pressurised now so switch the pumps off
        self.neg_pump_on = False
        self.pos_pump_on = False
        self.var_val = Vcal.s_OFF
        self.apply_settings()
        return

    def check_all_pressures(self):
        ##
        # Control all pressures in one go
        #

        # give each sensor a go
        self.pid_pressurise_system()
        self.apply_settings()

        self.pid_pressurise_system()
        self.apply_settings()

        ps_achieved = self.pid_pressurise_system()
        self.apply_settings()
        return ps_achieved

    ################################################################
    # Getters
    ################################################################

    def get_PlotData(self):
        ###
        # Plot data
        #

        msplot_coords = []
        return msplot_coords

    def get_PlotMinX(self):
        ##
        # Plot coordinate values
        #
        return 0

    def get_PlotMaxX(self):
        ##
        # Plot coordinate values
        #
        return 1

    def get_PlotMinY(self):
        ##
        # Plot coordinate values
        #
        return 0

    def get_PlotMaxY(self):
        ##
        # Plot coordinate values
        #
        return 1

    def get_IsError(self):
        ##
        # returns True if there is an
        # error
        #
        return self.Error_active

    def get_PromptActive(self):
        ##
        # Always return False, no user
        # interaction required by mux
        #
        return False

    def get_PromptType(self):
        ##
        # Returns the flavour of prompt
        # we require
        #
        return 0

    def get_Cancelled(self):
        ##
        # Has this thread been cancelled?
        #
        return self.cancelled

    def get_IsPaused(self):
        ##
        # Test whether this state machine is paused
        #
        if ( ( self.task_state == WorkerStates.idle  and self.stop == True ) or
         self.task_state == WorkerStates.done_stage_I or # call 2 - done
         self.task_state == WorkerStates.done):          # all work done
           return True
        else:
            return False

    def get_StatusMsg(self):
        ##
        # Returns a string reflecting
        # the current mux state
        #

        if (self.task_state == WorkerStates.stage_I or
                self.task_state == WorkerStates.stage_II or
                self.task_state == WorkerStates.done_stage_I or
                self.task_state == WorkerStates.done):
            return "%s:(+)%2.2f (-)%2.2f (v)%3.2f [%f]" % (self.task_state, self.psi_pos, self.psi_neg, self.psi_var, self.var_val)
        if self.task_state != WorkerStates.error:
            return self.task_state
        else:
            return self.Error_flavour

    ################################################################
    # Setters
    ################################################################

    def set_Reply(self, bReply):
        return

    def set_ClearError(self):
        ##
        # Clear the current error
        # to get us back to retry
        #

        # ask Barry et. all what we doing here
        wk_print("Cannot clear irrecoverable error")
        return

    def set_Continue(self):
        ##
        # We need to continue
        #
        wk_print("continuing...")
        if self.task_state == WorkerStates.idle or self.task_state == WorkerStates.initialising:
            self.stop = False
        elif self.task_state == WorkerStates.done_stage_I:
            self.task_state = WorkerStates.stage_II
            wk_print("done_stage_I -> stage_II")
        elif self.task_state == WorkerStates.done:
            self.task_state = WorkerStates.release
            wk_print("done -> release")
        else:
            wk_print(("cannot continue in current state %s" % self.task_state))
        return

    def set_Cancel(self):
        ##
        # We need to kill this state machine
        #
        wk_print("cancelling...")
        self.stop_pumpsNvalves()
        self.cancelled = True
        return

    def Stop(self):
        ##
        # Abort current operation
        # and re-initialise
        #
        wk_print("Stopping...")
        self.stop = True
        self.task_state = WorkerStates.stopping

    def set_all_solenoids(self, energised):
        ##
        # Set all the solenoids to on or off
        #
        # @param energised - set to True to
        # activate the solenoid, set to False to
        # deactivate the solenoid
        #
        port_value = 0
        if energised:
            port_value = bioc.ValveBitMasks.VallOn
        return port_value

    def recipe(self, mix_list):
        ##
        # cook the fluidic dataset
        #
        # @param mix_list - the fluidic procedure we need to process
        #

        slow_it_down = 1.0
        self.cnt_time = 0
        adc_delta_ms = 50
        for ix in mix_list:  # master list
            if self.stop or self.cancelled:
                break
            times_to_repeat = mix_list[ix][0]  # load up script
            print("\nMix %d/%d\n" % (ix, len(mix_list)))

            for kx in range(times_to_repeat):  # repeat this script according to the master list
                print("Mix %d/%d rep %d/%d     " % (ix, len(mix_list), kx, times_to_repeat), end='\r')
                if self.stop or self.cancelled:
                    break
                ev_table = mix_list[ix][1]  # first event

                for jx in ev_table:

                    ev_time = ev_table[jx][0] * slow_it_down  # event time
                    port_value = 0
                    # set the solenoid states
                    if ev_table[jx][1]:  # event valves
                        port_value = port_value | bioc.ValveBitMasks.V1_on
                    if ev_table[jx][2]:
                        port_value = port_value | bioc.ValveBitMasks.V2_on
                    if ev_table[jx][3]:
                        port_value = port_value | bioc.ValveBitMasks.V3_on
                    if ev_table[jx][4]:
                        port_value = port_value | bioc.ValveBitMasks.V4_on
                    if ev_table[jx][5]:
                        port_value = port_value | bioc.ValveBitMasks.V5_on
                    if ev_table[jx][6]:
                        port_value = port_value | bioc.ValveBitMasks.V6_on
                    if ev_table[jx][7]:
                        port_value = port_value | bioc.ValveBitMasks.V7_on
                    if ev_table[jx][8]:
                        port_value = port_value | bioc.ValveBitMasks.V8_on
                    if ev_table[jx][9]:
                        port_value = port_value | bioc.ValveBitMasks.V9_on
                    if ev_table[jx][10]:
                        port_value = port_value | bioc.ValveBitMasks.V10_on
                    if ev_table[jx][11]:
                        port_value = port_value | bioc.ValveBitMasks.V11_on
                    if ev_table[jx][12]:
                        port_value = port_value | bioc.ValveBitMasks.V12_on

                    self.valves_state = port_value
                    # we have read one line of the event
                    # write the value to the hardware
                    # guesstimate new psi values
                    self.regulate_negative(False)
                    self.regulate_positive(False)
                    self.apply_settings()

                    # now calculate the wait time
                    start_ms = int(round(time.time() * 1000))  # snapshot for start time
                    end_time_ms = start_ms + ev_time
                    current_ms = start_ms

                    keep_reading_psi = False
                    if round((end_time_ms - start_ms) / adc_delta_ms) > 1:
                        keep_reading_psi = True

                    while current_ms < end_time_ms:
                        if self.stop or self.cancelled:
                            break

                        if keep_reading_psi:
                            self.pid_pressurise_system()  # 13ms penalty
                            self.apply_settings()

                        # update the current time
                        current_ms = int(round(time.time() * 1000))  # snapshot for start time
                        if (current_ms + adc_delta_ms) < end_time_ms:
                            keep_reading_psi = True
                        else:
                            keep_reading_psi = False

                    current_ms = int(round(time.time() * 1000))  # snapshot for start time
                    # lets reduce the spam here
                    self.cnt_time += 1
                    if ((end_time_ms - start_ms) + 1) < (current_ms - start_ms):
                        self.wcnt_time += 1
                        print("TIME WARNING [%d/%d]: st=%d et=%d ct=%d delta=%d actual delta=%d" % (
                            self.wcnt_time, self.cnt_time, start_ms, end_time_ms, current_ms, (end_time_ms - start_ms), (current_ms - start_ms)))

                # ev_table
                gc.collect()
            # times to repeat
            gc.collect()
        # mix_list
        gc.collect()


######################################################
# Get time in milliseconds
######################################################
def get_current_ms_time():
    return int(round(time.time() * 1000))


###################################
# Debug print with worker id
###################################
def wk_print(dbg_string):
    print("FLD: %s" % dbg_string)


###################################
# Debug print
###################################
def dbg_print(dbg_string):
    if dbg_pumpNvalves:
        print(dbg_string)
    return


def dv_print(dbg_string):
    if dbg_dump_valve:
        print("FLD: %s" % dbg_string)
    return


def wg_print(dbg_string):
    if dbg_windup_guard:
        print("FLD: %s" % dbg_string)


###################################
# Debug warning count
###################################
def dbg_warn_count(count, entries):
    return " [%d/%d]" % (count, entries)


# Pressure target settings
###########################
class Psi():
    ##
    # Pressures we are aiming to maintain
    #
    pos_target =  6.5
    neg_target = -6.5
    var_target = -1.7
    ##
    # Pressures we should start with,
    # to consider the system purged
    pos_purged = bioc.PsiPurged.pos
    neg_purged = bioc.PsiPurged.neg
    var_purged = bioc.PsiPurged.var


# Warning thresholds
########################
class PsiWarn():
    tolerance = '10%'
    warn_pcent = 10.0 / 100.0
    warn_low = 1.0 - warn_pcent
    warn_high = 1.0 + warn_pcent
    warn_var_lo = warn_low * Psi.var_target
    warn_var_hi = warn_high * Psi.var_target
    warn_neg_hi = warn_high * Psi.neg_target
    warn_neg_lo = warn_low * Psi.neg_target
    warn_pos_hi = warn_high * Psi.pos_target
    warn_pos_lo = warn_low * Psi.pos_target
    status_report = 300

    # Tolerance to get the system up to pressure
    # positive
    # ---------


    sp_pcent = 5.0 / 100.0
    sp_low = 1.0 - sp_pcent
    sp_high = 1.0 + sp_pcent
    start_pos_hi = sp_high * Psi.pos_target
    start_pos_lo = sp_low * Psi.pos_target


    # negative
    # ---------
    sn_pcent = 5.0/100.0
    sn_low = 1.0 - sn_pcent
    sn_high = 1.0 + sn_pcent
    start_neg_hi = sn_high * Psi.neg_target
    start_neg_lo = sn_low * Psi.neg_target

    # variable
    # ---------
    sv_pcent = 5.0/100.0
    sv_low = 1.0 - sv_pcent
    sv_high = 1.0 + sv_pcent
    start_var_lo = sv_low * Psi.var_target
    start_var_hi = sv_high * Psi.var_target

    # variable - close valve threshold
    svh_pcent = 5.0/100.0
    svh_low = 1.0 - sv_pcent
    svh_high = 1.0 + sv_pcent
    close_var_hi = svh_high * Psi.var_target
    close_var_lo = svh_low * Psi.var_target


# Pump calibration settings
#######################################
class Pumps():
    if socket.gethostname() == 'delphi':
        # positive pump
        pos_inc_delta = 0.8  # rate at which the (+) pressure increments when the pump is on
        pos_dec_delta = -0.01  # rate at which the (+) pressure decays when the pump is off
        t_pos = 6.2  # pressure at which we switch the pump ON

        # negative pump
        neg_inc_delta = -0.5  # rate at which the (-) pressure increments when the pump is on
        neg_dec_delta = 0.03  # rate at which the (-) pressure decays when the pump is off
        t_neg = -6.2  # pressure at which we switch the pump ON

        # how often do we top up the
        # positive pressure when we have no readings
        t_pos_topup = 2000.0
        # negative pump time
        t_neg_topup = 2000.0

    elif socket.gethostname() == 'ares':
        # positive pump
        pos_inc_delta = 0.9  # rate at which the (+) pressure increments when the pump is on
        pos_dec_delta = -0.03  # rate at which the (+) pressure decays when the pump is off
        t_pos = 6.0  # pressure at which we switch the pump ON

        # negative pump
        neg_inc_delta = -0.11  # rate at which the (-) pressure increments when the pump is on
        neg_dec_delta = 0.03  # rate at which the (-) pressure decays when the pump is off
        t_neg = -6.0  # pressure at which we switch the pump ON

        # how often do we top up the
        # positive pressure when we have no readings
        t_pos_topup = 3000.0
        # negative pump time
        t_neg_topup = 2000.0

    elif socket.gethostname() == 'apollo':
        # On apollo we have two manifolds, the integrated one and the
        # exposed one.

        if cms.DebugFlags.apollo_open:
            # positive pump
            pos_inc_delta = 0.9     # rate at which the (+) pressure increments when the pump is on
            pos_dec_delta = -0.1   # rate at which the (+) pressure decays when the pump is off
            t_pos = 5.5  # pressure at which we switch the pump ON

            # negative pump
            neg_inc_delta = -0.3     # rate at which the (-) pressure increments when the pump is on
            neg_dec_delta = 0.05   # rate at which the (-) pressure decays when the pump is off
            t_neg = -5.8  # pressure at which we switch the pump ON

            # how often do we top up the
            # positive pressure when we have no readings
            t_pos_topup = 3000.0
            # negative pump time
            t_neg_topup = 2000.0

        # the one on the actual instrument capable of measurement
        else:
            # positive pump
            pos_inc_delta = 0.9     # rate at which the (+) pressure increments when the pump is on
            pos_dec_delta = -0.3   # rate at which the (+) pressure decays when the pump is off
            t_pos = 6.2  # pressure at which we switch the pump ON

            # negative pump
            neg_inc_delta = -0.3     # rate at which the (-) pressure increments when the pump is on
            neg_dec_delta = 0.03   # rate at which the (-) pressure decays when the pump is off
            t_neg = -6.2  # pressure at which we switch the pump ON

            # how often do we top up the
            # positive pressure when we have no readings
            t_pos_topup = 3000.0

            # negative pump time
            t_neg_topup = 2000.0

    elif socket.gethostname() == 'athena':
        # positive pump
        pos_inc_delta = 0.6  # rate at which the (+) pressure increments when the pump is on
        pos_dec_delta = -0.2  # rate at which the (+) pressure decays when the pump is off
        t_pos = 6.2  # pressure at which we switch the pump ON

        # negative pump
        neg_inc_delta = -0.3  # rate at which the (-) pressure increments when the pump is on
        neg_dec_delta = 0.01  # rate at which the (-) pressure decays when the pump is off
        t_neg = -6.2  # pressure at which we switch the pump ON

        # how often do we top up the
        # positive pressure when we have no readings
        t_pos_topup = 2000.0
        # negative pump time
        t_neg_topup = 2000.0

    else:
        # positive pump
        pos_inc_delta = 0.9  # rate at which the (+) pressure increments when the pump is on
        pos_dec_delta = -0.1  # rate at which the (+) pressure decays when the pump is off
        t_pos = 5.5  # pressure at which we switch the pump ON

        # negative pump
        neg_inc_delta = -0.3  # rate at which the (-) pressure increments when the pump is on
        neg_dec_delta = 0.05  # rate at which the (-) pressure decays when the pump is off
        t_neg = -4.8  # pressure at which we switch the pump ON

        # how often do we top up the
        # positive pressure when we have no readings
        t_pos_topup = 3000.0
        # negative pump time
        t_neg_topup = 1000.0


#  Variable valve calibration settings
#######################################
class Vcal():

    s_OFF = 0
    # time in between pressure checks when holding pressure
    sample_phold_time = 1
    # minimum time between samples
    sample_time = 0.05

    if socket.gethostname() == 'delphi':
        t_dump = -1.8    # threshold when we use the dump valve
        # self.psi_var < Vcal.t_dump
        s_dump = 0.3
        # value to set the variable valve once we've dumped
        s_post_dump = 0.1

        s_endstop_max = 25
        s_endstop_min = 0.1
        s_start_val = 5.1   # starting value for the variable valve

        # PID coefficients
        Kp = -18.5
        Ki = 0.0
        Kd = 0.0
        wg = 20.0   # windup guard
    elif socket.gethostname() == 'ares':
        t_dump = -2.0    # threshold when we use the dump valve
        # self.psi_var < Vcal.t_dump
        s_dump = 0.3    # Amount/width we open the variable valve when we release pressure
        s_post_dump = 0.1

        s_endstop_max = 9.0   # Check these values if variable valve pumps very quickly.
        s_endstop_min = 0.1    # Limit values for variable valve.
        s_start_val = 5.0      # starting value for the variable valve

        # PID coefficients
        Kp = -32.0
        Ki = 0.0
        Kd = 0.0
        wg = 50.0   # windup guard

    elif socket.gethostname() == 'apollo':
        # On apollo we have two manifolds, the integrated one and the
        # exposed one which we use for inspecting the cartridge.

        if cms.DebugFlags.apollo_open:
            t_dump = -1.8    # threshold when we use the dump valve
            # self.psi_var < Vcal.t_dump
            s_dump = 0.1
            s_post_dump = 0.0

            s_endstop_max  = 25.0
            s_endstop_min = 0.1
            s_start_val = 5.4       # starting value for the variable valve

            # PID coefficients
            Kp = -19.0
            Ki = -1.0
            Kd = 0.0
            wg = 20.0   # windup guard
        else:
            t_dump = -2.0    # threshold when we use the dump valve
            # self.psi_var < Vcal.t_dump
            s_dump = 0.01
            s_post_dump = 0.1

            s_endstop_max = 0.1
            s_endstop_min = 0.01
            s_start_val = 0.03       # starting value for the variable valve

            # PID coefficients
            Kp = -0.5
            Ki = 0.0
            Kd = 0.0
            wg = 20.0   # windup guard

    elif socket.gethostname() == 'athena':
        t_dump = -1.8    # threshold when we use the dump valve
        # self.psi_var < Vcal.t_dump
        s_dump = 0.1
        s_post_dump = 0.0

        s_endstop_max = 10.0
        s_endstop_min = 0.1
        s_start_val = 5.8       # starting value for the variable valve

        # PID coefficients
        Kp = -41.0
        Ki = -0.0
        Kd = 0.0
        wg = 50.0   # windup guard

    else:
        t_dump = -2.0    # threshold when we use the dump valve
        # self.psi_var < Vcal.t_dump
        s_dump = 0.1
        s_post_dump = 0.0

        s_endstop_max = 100
        s_endstop_min = 0.0
        s_start_val = 1.4       # starting value for the variable valve

        # PID coefficients
        Kp = -20.0
        Ki = -60.0
        Kd = 0.0
        wg = 25.0   # windup guard

#class FluidBitMasks():
#
#    # ON bitmasks
#    V1_on = 0x0002  # CV10
#    V2_on = 0x1000  # CV5
#    V3_on = 0x0800  # CV4
#    V4_on = 0x0400  # CV3
#    V5_on = 0x4000  # CV7
#    V6_on = 0x8000  # CV8
#    V7_on = 0x0200  # CV2
#    V8_on = 0x0004  # CV11
#    V9_on = 0x2000  # CV6
#    V10_on = 0x0001  # CV9
#    V11_on = 0x0100  # CV1
#    V12_on = 0x0008  # cv12
#
#    # OFF bitmasks
#    V1_off = 0xFFFD  # CV10
#    V2_off = 0xEFFF  # CV5
#    V3_off = 0xF7FF  # CV4
#    V4_off = 0xFBFF  # CV3
#    V5_off = 0xBFFF  # CV7
#    V6_off = 0x7FFF  # CV8
#    V7_off = 0xFDFF  # CV2
#    V8_off = 0xFFFB  # CV11
#    V9_off = 0xDFFF  # CV6
#    V10_off = 0xFFFE  # CV9
#    V11_off = 0xFEFF  # CV1
#    V12_off = 0xFFF7  # cv12
#
#    Pump_neg_on = 0x0040
#    Pump_neg_off = 0xFFBF
#    Pump_pos_on = 0x0080
#    Pump_pos_off = 0xFF7F


# Pressure regulation select
############################
class PressMon():
    negative = 0
    positive = 1
    variable = 2

# Error conditions
########################
class ErrorStates():
    all_good = 'Passed'
    err_pos_pressure = 'Positive pressure fault'
    err_neg_pressure = 'Negative pressure fault'
    err_draw_pressure = 'Draw pressure fault'


# Worker machine states
########################
class WorkerStates():
    initialising = 'F: initialise'
    idle = 'F: idle'
    stage_I = 'F-I'
    done_stage_I = 'F idle I'
    stage_II = 'F-II'
    done = 'F idle II'
    exiting = 'F: exiting'
    error = 'F: error'
    recover = 'F: resetting'
    release = 'F: releasing pressure'
    stopping = 'F: stopping'


#  Repeat 230 times
#  50,   off,   off,   off,   off,   off,   on,   off,   off,   off,   off,   off,   off
#  100,   off,   off,   off,   off,   off,   on,   off,   off,   off,   on,   off,   off
#  50,   off,   off,   off,   off,   off,   off,   off,   off,   off,   on,   off,   off
#  50,   off,   off,   off,   off,   off,   off,   off,   on,   off,   on,   off,   off
#  100,   off,   off,   off,   off,   off,   off,   off,   on,   off,   off,   off,   off
#  50,   off,   off,   off,   off,   off,   off,   off,   off,   off,   off,   off,   off
# ---------------------------------------------------------------------------------------
#   time,  V1,    V2,    V3,    V4,    V5,    V6,   V7,    V8,   V9,    V10, V11,   V12
test_fluid = {
    0: (50, False, False, False, False, False, True, False, False, False, False, False, False),
    1: (100, False, False, False, False, False, True, False, False, False, True, False, False),
    2: (50, False, False, False, False, False, False, False, False, False, True, False, False),
    3: (50, False, False, False, False, False, False, False, True, False, True, False, False),
    4: (100, False, False, False, False, False, False, False, True, False, False, False, False),
    5: (50, False, False, False, False, False, False, False, False, False, False, False, False), }
# for debug/simulation
no_mix = {0: (1, test_fluid), }



# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.gpio.bioc_init()     # initialise the hardware in stand alone mode
    worker.start()

    print("[p] to print status")
    print("[g] to start/continue")
    print("[s] to stop")
    print("[q] to quit")
    print("[e] to clear error")
    print("[?/h] for help")

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':  # print status
            print(worker.get_StatusMsg())
        elif cmd == 'g':  # go
            worker.set_Continue()
        elif cmd == 'e':  # clear the error
            worker.set_ClearError()
        elif cmd == 'q':  # quit
            worker.set_Cancel()
            break
        elif cmd == 's':  # stop
            worker.Stop()

        elif cmd == '?' or cmd == 'h':
            print("[p] to print status")
            print("[g] to start/continue")
            print("[s] to stop")
            print("[q] to quit")
            print("[e] to clear error")
            print("[?/h] for help")


if __name__ == "__main__":

    main()

    sys.exit()
