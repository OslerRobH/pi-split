#!/bin/bash

sudo rm -rf __pycache__

# set gui_geek_mode to False in common.py
sed -i 's/gui_geek_mode *= *True/gui_geek_mode = False/' common.py

sudo python3 master.py
