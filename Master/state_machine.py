#!/usr/bin/env python3
## file state_machine.py
#
# defines the class state_machine

class state_machine():

    m_state = 0
    m_min_state = 0
    m_max_state = 0

    # @brief init - initialise the class
    # @param self - the this object
    # @return None
    def init(self):
        self.m_state = 0
        self.m_min_state = 0
        self.m_max_state = 0
        return


    # @brief get_state - get the current state
    # @param self - the this object
    # @return the current state
    def get_state(self):
        return self.m_state
    
    
    # @brief set_state - sets the current state of the state machine
    #    checks if the state is in bounds >= m_min_state && <= m_max_state
    # @param self - the this object
    # @param new_state - the desired new state
    # @return True == Success, False == Out of Bounds value
    def set_state(self, new_state):
        
        if new_state <= self.m_max_state:
            self.m_state = new_state
            return True
        else:
            return False
    
    
    # @brief set_bounds - sets the bounds of the state, used for validation
    # @param self - the this object
    # @param min - the minimum value
    # @param max - the maximum value
    # @return always True
    def set_bounds(self, min, max):

        self.m_min_state = min
        self.m_max_state = max
        return True

    #
    # @brief is_state - is the current state the same as theState supplied
    # @param self - the this object
    # @param theState - the state to compare to
    # @return True - theState is the current state False Not the current state
    def is_state(self, theState):
        
        if self.m_state == theState:
            return True
        else:
            return False
    
    
    # @brief increment - bump the state onto the next state    
    # @return True - if successful else False - Failed
    # @param self - the this object
    def increment(self):
    
        if self.m_state < self.m_max_state:
            self.m_state = self.m_state + 1
            return True
        else:
            return False
    
    
    # @brief decrement - reduce the state back to the previous state 
    # @param self - the this object
    # @return True - if successful else False - Failed
    def decrement(self):
    
        if self.m_state > self.m_min_state:
           self.m_state = self.m_state - 1
           return True
        else:
           return False

