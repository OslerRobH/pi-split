################################################
# Electrochemical Impedance Spectroscopy Worker
# deya.sanchez@gmail.com
################################################
import sys        # reading stdout from subprocess calls
import subprocess 
import gui_defs as ui    # our GUI definitions
import time       # so we can sleep
import os         # so we can rename the results file
import re         # regular expression to strip out numbers
import datetime   # so we can tack on a timestamp to the result file
import socket     # so we can identify the hostname to tack to the result file
import config_sequence as cfg # workers and order of execution

# randles circuit stuff
from numpy import array
from fit_randles import FitRandles
from eis_data import EISData

from threading import Thread

from kivy.clock import Clock    # to schedule event

import common as cms    # for on_dev_pc 
# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc
hold_on_eis = cms.DebugFlags.hold_on_eis

fake_rpi_gpio = cms.DebugFlags.on_dev_pc
if fake_rpi_gpio:
    pass
else:
    import hal

Ps4_Max_Retries = 3 # number of times we try and connect after comms fail

#########################################
# This is our special sauce thread in 
# charge of the PS4 interface and the
# task that slurps in the measurement data
#########################################
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.enable_plot_mode = cms.ReleaseFlag.enable_plot
        self.gpio = hal.octet_gpio
        if cms.gui_geek_mode:
            self.ourcfg = cfg.geek_dict_sys_state_sequence
        else:
            self.ourcfg = cfg.dict_sys_state_sequence
        self.init_Once()
        self.shut_down = False
        print("%s MainWorkerThread(Thread) called" % __file__)
    
    ##########################
    # variables that we only
    # set-up once per lifetime
    ##########################
    def init_Once(self):
        self.task_state = MeasStates.initialising   # state machine tracker
        wk_print("state machine -> initialising")
        self.TotalCount = 0     # total number of EIS measurements we do before we restart
        # figure out how many times we need to run EIS per test
        for ix in range ( len(self.ourcfg) ):
            if self.ourcfg[ix] == 'EIS' :
                self.TotalCount += 1
        wk_print("Total EIS %d" % self.TotalCount)
        self.msCount = 0  # keep a count of the number of measurements we do before we re-start this state machine
        self.result_ix = 0
        self.rct_results = []

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        if self.msCount == self.TotalCount:
            self.init_Once()
        self.msplot_coords = []  # initialise measurement coordinates of our plot
        self.stop = True         # we start with everything quiet

        self.Ocp = 0.0           # the measured ocp value
        self.task_state = MeasStates.initialising   # state machine tracker
        wk_print("state machine -> initialising")
        self.OcpState = OcpOutcome.unknown
        self.UserReply = UsrPromptReply.unknown
        self.mono_done = True    # true if our child process has finished
        self.Error_active = False
        self.pw_counter = 0     # please wait counter to show something on the status message
        self.Ps4_retry_count = 0
        #deprecated self.have_version = False


        #plot parameters
        self.MinX = 0
        self.MaxX = 200
        self.MinY = 0
        self.MaxY = 200 

        self.msplot_Xmin = self.MinX
        self.msplot_Xmax = self.MaxX
        self.msplot_Ymin = self.MinY
        self.msplot_Ymax = self.MaxY

        self.please_wait = True

        #randles parameters
        self.Eac = 0.0
        self.Zreal = 0.0
        self.Zim = 0.0
        self.Freq = []
        self.Z = []
        self.have_data = False
        # default curve fit settings
        self.maxFreq = 0.0        # maximum fit frequency
        self.dbF_pk  = 5.0        # decades below frequncy peak
        self.Peak_threshold = 2.0 # minimum ohms drop for Im Z peak detection
        self.Awguess = 5.0        # Aw guess fir fit
        self.down_samprat = 1.0   # downsample ratio
        self.maxiter = 500.0     # maximum iteration
        # latest version parameters
        self.deca_peak = 2.0
        self.bHaveResults = False
        self.results = Results.unknown
        return

    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled:
            self.task_state = MeasStates.exiting
            wk_print("state machine [exiting]")
            return True
        else:
            return False

    ####################################
    # Called to initialise this state,
    # machine, reset it or recover from
    # an error
    ####################################
    def State_Reset(self):
        self.init_vars()
        # make sure there are no wild chimps lurking before we start
        self.KillTheMonkeys(True)
        return

    #########################################
    # state machine that deals with kicking
    # off the measurement
    #########################################
    def run(self):

        # keep running until the application exits
        while not self.cancelled:

            # Initialise this state machine
            #################################
            if self.task_state == MeasStates.initialising:
                self.State_Reset()
                self.task_state = MeasStates.idle
                wk_print("initialising -> idle")

            # Wait for a measurement request
            #################################
            elif self.task_state == MeasStates.idle:
                if not self.stop:
                    self.task_state = MeasStates.rdInParams
                    wk_print("idle -> rdInParams")
                else:
                    time.sleep(1)


            # Measurement Sequence
            #######################

            # Grab the input parameters for the 
            # measurement
            ###################################
            #index:  0   1   2  3   4   5    6     7
            #      t_eq 30 scan 2 E_dc 0.0 E_ac 0.01 
            #index:  8     9   10   11  12    13    14  15   16     17
            #       F_type 1 n_freq 71 maxf 100000 minf 0.1 do_Ocp  1 
            #index:  18       19  20    21    22     23     24      25     26    27
            #       tmax_Ocp 120 stab 0.05 Edc_upper 0.4 Edc_lower 0.1 Edc_Ovrd 0.21
            elif self.task_state == MeasStates.rdInParams:
                # go grab the input parameters
                self.eis_param_string = self.RdParameters()
                #debug_tag wk_print("file params: %s" % self.eis_param_string)
                paramlist = self.eis_param_string.split()
                #debug_tag wk_print(paramlist)
                self.Eac = float(paramlist[7])
                #debug_tag wk_print(self.Eac)
                # Grab the input parameters for the curve fitting algorithm
                #index 0         1         2             3        4           5
                #      maxFreq dbF_pk Peak_threshold  Awguess  down_samprat maxiter
                # new parameter list
                # index    0    1     2     3     4      5    6  7  8  9  10  11   12     13
                #        maxf 5000 decb_pk 5.0 deca_pk 2.0 pkth 2 awg 5 dsamp 1 maxiter 500
                #numindex       0           1           2       3     4       5          6
                rnd_param_string = self.RdRndParameters()
                #debug_tag wk_print("rnd file params: %s" % rnd_param_string)
                if rnd_param_string != ' ':
                    rndparamlist = self.findNumInByteList(rnd_param_string)
                    wk_print(rndparamlist)

                    self.maxFreq        = float(rndparamlist[0])   # maximum fit frequency
                    self.dbF_pk         = float(rndparamlist[1])   # decades below frequncy peak
                    self.deca_peak      = float(rndparamlist[2])
                    self.Peak_threshold = float(rndparamlist[3])   # minimum ohms drop for Im Z peak detection
                    self.Awguess        = float(rndparamlist[4])   # Aw guess fir fit
                    self.down_samprat   = float(rndparamlist[5])   # downsample ratio
                    self.maxiter        = float(rndparamlist[6])   # maximum iteration
                    # move to the next state
                    self.task_state = MeasStates.getVersion
                    wk_print("rdInParams -> getVersion")
                else:
                    wk_print("rdInParams -> failed")
                    self.task_state = MeasStates.error_params
            
            # Invoke the chimp, and get the version number
            ##############################################
            elif self.task_state == MeasStates.getVersion:
                #debug_tag wk_print("file contents: %s" % self.eis_param_string)
                #debug_tag wk_print("calling the chimp...")
                chimp_cmd = 'sudo /usr/bin/mono ' + os.getcwd() + '/PSoslerIS.exe ' + self.eis_param_string
                #debug_tag wk_print(chimp_cmd)
                child_retval = None
                self.start_time = datetime.datetime.utcnow()
                try:
                    #debug_tag wk_print("subprocess")
                    self.child = subprocess.Popen([chimp_cmd], 
                                            shell=True, 
                                            stdout=subprocess.PIPE, 
                                            stderr=subprocess.STDOUT,
                                            stdin=subprocess.PIPE,
                                            bufsize=1,
                                            universal_newlines=True)
                    #debug_tag wk_print("opened")
                    self.mono_done = False
                    while self.ChimpRunning():
                        if self.stop:
                            wk_print("stop on getVersion")
                            break
                        if self.monitor_cancel():
                            wk_print("getVersion -> cancel")
                            break
                        rx_data = self.child.stdout.readline()
                        child_retval = self.child.poll()
                        if child_retval is not None:
                            wk_print("exit on getVersion")
                            break
                        if rx_data != '\0':
                            #debug_tag wk_print(rx_data)
                            if rx_data.startswith("IS Measurement Version"):
                                self.chimpVersion = rx_data.rstrip()
                                #deprecated self.have_version = True
                                wk_print(self.chimpVersion)
                                self.task_state = MeasStates.gotVersion
                                wk_print("getVersion -> gotVersion")
                                break
                            if self.detectAssert(rx_data):
                                wk_print("getVersion -> assert")
                                break
                    #while

                except subprocess.CalledProcessError as err:
                    wk_print(err.output)
                    child_retval = ChimpReturns.MeasurementFailed
                except OSError:
                    wk_print("OS error")
                    child_retval = ChimpReturns.Bug
                except ValueError:
                    wk_print("Value error")
                    child_retval = ChimpReturns.Bug

                self.ParseChimpReply(child_retval)

            # Wait for OCP potential to start
            ##############################################
            elif self.task_state == MeasStates.gotVersion:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on gotVersion")
                        break
                    if self.monitor_cancel():
                        wk_print("gotVersion -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on gotVersion")
                        break
                    if rx_data != '\0':            # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("OCP Potential") :
                            # strip out the numeric values
                            ocp_list = self.findNumInByteList(rx_data)  #debug_tag wk_print(ocp_list)
                            self.Ocp = ocp_list[0]
                            self.Delta = ocp_list[1]
                            self.task_state = MeasStates.ms_Ocp
                            wk_print("gotVersion -> ms_Ocp")
                            break
                        if rx_data.startswith("Skipping OCP,"):
                            self.task_state = MeasStates.ms_EIS
                            wk_print("gotVersion -> ms_EIS")
                            break
                        if rx_data.startswith("EIS Measurement started"):
                            self.task_state = MeasStates.ms_EIS
                            wk_print("gotVersion -> ms_EIS")
                            break
                        if self.detectAssert(rx_data):
                            wk_print("gotVersion -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Update OCP potential and wait for stability
            ##############################################
            elif self.task_state == MeasStates.ms_Ocp:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on ms_Ocp")
                        break
                    if self.monitor_cancel():
                        wk_print("ms_Ocp -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on ms_Ocp")
                        break
                    if rx_data != '\0':            # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("OCP Potential") :
                            # strip out the numeric values
                            ocp_list = self.findNumInByteList(rx_data)
                            self.Ocp = ocp_list[0]
                            self.Delta = ocp_list[1]
                        elif rx_data.startswith("OCP too low") :
                            self.OcpState = OcpOutcome.low
                            self.task_state = MeasStates.unstableOcp
                            wk_print("ms_Ocp -> unstableOcp low")
                            break
                        elif rx_data.startswith("OCP too high") :
                            self.OcpState = OcpOutcome.high
                            self.task_state = MeasStates.unstableOcp
                            wk_print("ms_Ocp -> unstableOcp high")
                            break
                        elif rx_data.startswith("Would you like"):
                            self.task_state = MeasStates.askYesorNo
                            wk_print("ms_Ocp -> askYesorNo")
                            break
                        elif rx_data.startswith("EIS Measurement started"):
                            self.task_state = MeasStates.ms_EIS
                            wk_print("ms_Ocp -> ms_EIS")
                            break
                        if self.detectAssert(rx_data):
                            wk_print("ms_Ocp -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # It's gone pear shaped ask the user what's what
            ################################################
            elif self.task_state == MeasStates.unstableOcp:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on unstableOcp")
                        break
                    if self.monitor_cancel():
                        wk_print("unstableOcp -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on unstableOcp")
                        break
                    if rx_data != '\0':            # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("Would you like"):
                            self.task_state = MeasStates.askYesorNo
                            wk_print("unstableOcp -> askYesorNo")
                            break    
                        if self.detectAssert(rx_data):
                            wk_print("ms_OunstableOcpcp -> assert")
                            break
                        sys.stdout.write(rx_data)   
                # while
                self.ParseChimpReply(child_retval)

            # Ask Y/N and Wait for user input
            ################################################
            elif self.task_state == MeasStates.askYesorNo:
                if self.UserReply != UsrPromptReply.unknown:
                    self.task_state = MeasStates.gotReply
                    wk_print("askYesorNo -> gotReply")
                else:
                    time.sleep(1)


            # User reply arrived, process it
            ################################################
            elif self.task_state == MeasStates.gotReply:
                # Yes
                if self.UserReply == UsrPromptReply.yes:
                    wk_print("received touch yes")
                    ret_value = self.child.stdin.write('y\n')
                    self.child.stdin.flush()
                    wk_print(ret_value)
                    self.task_state = MeasStates.ms_EIS
                    wk_print("gotReply-> ms_EIS")
                # No    
                elif self.UserReply == UsrPromptReply.no:
                    wk_print("received touch no")
                    ret_value = self.child.stdin.write('n\n')
                    self.child.stdin.flush()
                    wk_print(ret_value)
                    wk_print("test1 - no")
                    os.system('pidof mono')
                    self.task_state = MeasStates.user_aborted
                    self.Error_active = True

                    # mop up and exit
                    while self.ChimpRunning():
                        if self.monitor_cancel():
                            wk_print("gotReply -> cancel")
                            break
                        rx_data = self.child.stdout.readline()
                        child_retval = self.child.poll()
                        if child_retval is not None:
                            wk_print("exit on gotReply")
                            break
                        if rx_data != '\0':            # null character
                            wk_print(rx_data)
                            if self.detectAssert(rx_data):
                                wk_print("gotReply -> assert")
                                break
                    # while
                    #self.stop = True
                    self.mono_done = True
                self.ParseChimpReply(child_retval)

                    
            # EIS measurement
            ################################################
            elif self.task_state == MeasStates.ms_EIS:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on ms_EIS")
                        break
                    if self.monitor_cancel():
                        wk_print("ms_EIS -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on ms_EIS")
                        break
                    if rx_data != '\0':           # null character
                        #debug_tag wk_print(rx_data)
                        if rx_data.startswith("index, Frequency Hz,"):
                            self.task_state = MeasStates.start_plotting
                            wk_print("ms_EIS -> start_plotting")
                            break
                        if self.detectAssert(rx_data):
                            wk_print("ms_EIS -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Plot
            ################################################
            elif self.task_state == MeasStates.start_plotting:
                some_data = []
                self.Freq = []
                self.negImZ = []
                self.ReZ = []
                data_is_sane = True
                round_mult = 100
                self.msCount += 1
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on start_plotting")
                        break
                    if self.monitor_cancel():
                        wk_print("start_plotting -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on start_plotting")
                        break
                    if rx_data != '\0':           # null character
                        #debug_tag wk_print(rx_data)
                        #debug_tag wk_print(':', end='')
                        if rx_data.startswith("EisData finished") :
                            self.task_state = MeasStates.wait_for_done
                            wk_print("start_plotting -> wait_for_done")
                            break
                        else:
                            pdata_list = self.findNumInByteList(rx_data)

                            if len(pdata_list) == 7:
                                self.please_wait = False
                                # we are expecting the data to arrive in this order
                                # index, Frequency Hz, ZReal Ohm, ZImaginary Ohm, ZAbs Ohm, -Phase deg, Idc uA
                                #  0          1           2           3             4           5        6
                                # save Frequency, Zreal and Zimaginary
                                self.Freq.append(float(pdata_list[1]))
                                self.ReZ.append(float(pdata_list[2]))
                                self.negImZ.append(float(pdata_list[3]))

                                self.Z_real = float(pdata_list[2])
                                self.Z_imag = float(pdata_list[3])
                                
                                # make sure the limits of our plot is within bounds of the arriving data
                                # so long as the data is sane
                                if data_is_sane:
                                    #debug_tag wk_print("zreal = %f" % self.Z_real)
                                    #debug_tag wk_print("zim = %f" % self.Z_imag)

                                    if self.enable_plot_mode:
    
                                        if self.Z_real > self.MaxX:
                                            self.MaxX = self.RoundUp( ( int(self.Z_real) ), round_mult)
                                        if self.Z_real < self.MinX:
                                            self.MinX = self.RoundDown( ( int(self.Z_real) ), round_mult)
                                        if self.Z_imag > self.MaxY:
                                            self.MaxY = self.RoundUp( ( int(self.Z_imag) ), round_mult)
                                        if self.Z_imag < self.MinY:
                                            self.MinY = self.RoundDown( ( int(self.Z_imag) ), round_mult)
                                        # now square off the axis
                                        if self.MinX < self.MinY:
                                            self.MinY = self.MinX
                                        else:
                                            self.MinX = self.MinY

                                        if self.MaxX > self.MaxY:
                                            self.MaxY = self.MaxX
                                        else:
                                            self.MaxX = self.MaxY

                                        some_data.append((self.Z_real,self.Z_imag))
                                        #stash it where everyone can access it
                                        self.msplot_coords=some_data
                                        self.msplot_Xmin = self.MinX
                                        self.msplot_Xmax = self.MaxX
                                        self.msplot_Ymin = self.MinY
                                        self.msplot_Ymax = self.MaxY

                                    self.have_data = True
                                else:
                                    self.have_data = False
                                    # at this point we may as well bail out there
                                    # is a bug in the system we need to fix
                                    self.task_state = MeasStates.error_Ps4
                                    wk_print("start_plotting -> error_Ps4")
                                    self.KillTheMonkeys(True)
                                    break
                        if self.detectAssert(rx_data):
                            wk_print("start_plotting -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)

            # Wait for done
            ################################################
            elif self.task_state == MeasStates.wait_for_done:
                while self.ChimpRunning():
                    if self.stop:
                        wk_print("stop on wait_for_done")
                        break
                    if self.monitor_cancel():
                        wk_print("wait_for_done -> cancel")
                        break
                    rx_data = self.child.stdout.readline()
                    child_retval = self.child.poll()
                    if child_retval is not None:
                        wk_print("exit on wait_for_done")
                        break
                    if rx_data != '\0':           # null character
                        wk_print(rx_data)
                        if self.detectAssert(rx_data):
                            wk_print("wait_for_done -> assert")
                            break
                # while
                self.ParseChimpReply(child_retval)


            # Do randles and curve fit
            #################################################
            elif self.task_state == MeasStates.do_randles:
                if self.have_data:
                    # parameters for FitRandles
                    # eis object
                    eis = EISData(
                        test_id = "test_id",  # test id string
                        meta_data = {},
                        E = None,
                        F = array(self.Freq),
                        ReZ = array(self.ReZ),
                        negImZ = array(self.negImZ)
                        )
                    if eis.success is True:
                        # FIXME: new parameters 
                        # decades above peak  default=2.0
                        # decades below peak  default=5.0
                        # downsample default= 1.0

                        self.cf = FitRandles(
                                eis, 
                                self.dbF_pk,       # decades_below_peak 
                                self.deca_peak,    # decades_above_peak
                                self.down_samprat, # downsample    
                                self.Awguess,     
                                self.maxiter,       # maxiter
                                0.01,               # negative theshold
                                0.002               # z_noise_threshold
                                )
                        #debug_tag wk_print(self.cf.success, self.cf.errors)
                        if self.cf.success is True:
                            self.fmt = ('Rs={:.3f} KOhm  Cdl={:.1f} nF  Rct={:.3f} KOhm  ' +
                                'Aw={:.2f} KOhm  phi={:.2f}')
                            self.rs = self.cf.fit_results['Rs']
                            self.cdl = self.cf.fit_results['Cdl']
                            self.rct = self.cf.fit_results['Rct']
                            self.aw = self.cf.fit_results['Aw']
                            self.phi = self.cf.fit_results['phi']
                            #debug_tag title = self.fmt.format(self.rs, self.cdl, self.rct, self.aw, self.phi)
                            wk_print("randles -> upload")
                            self.task_state = MeasStates.upload
                        else:
                            self.Results_to_Dropb(False)
                            if not self.is_result_ready(MeasStates.failed):
                                print("ERROR: randles -> failed", self.cf.errors)
                    else:
                        self.Results_to_Dropb(False)
                        if not self.is_result_ready(MeasStates.failed):
                            print("ERROR: randles -> eis failed", eis.errors)
                else:
                    if not self.is_result_ready(MeasStates.failed):
                        print("ERROR: randles -> eis failed", eis.errors)

            # save results to the cloud        
            ####################################################
            elif self.task_state == MeasStates.upload:
                self.sanity_chk_results()
                if not self.is_result_ready(MeasStates.done):
                    wk_print("upload -> done")

            # All done!
            ####################################################
            elif self.task_state == MeasStates.done:
                time.sleep(1)

            # Deal with the results
            ####################################################
            elif self.task_state == MeasStates.show_results:
                self.save_result()
                self.bHaveResults = True

                self.UserReply = UsrPromptReply.unknown
                self.task_state = MeasStates.da_results


            # Wait for the results to be read
            ####################################################
            elif self.task_state == MeasStates.da_results:
                if self.UserReply != UsrPromptReply.unknown:
                    self.task_state = MeasStates.done
                else:
                    time.sleep(5)


            # Attempt to recover from an error and start again
            ####################################################
            elif self.task_state == MeasStates.recover:
                prev_stop = self.stop
                self.State_Reset()  # reset our state
                # the reset has stomped on the stop value so restore it
                self.stop = prev_stop
                self.task_state = MeasStates.rdInParams # and go try again
                wk_print("recover -> rdInParams")

            # Error states
            ################################################
            elif self.task_state == MeasStates.failed:
                #if self.Error_active == False:
                #    self.task_state = MeasStates.recover
                #    wk_print("failed -> recover")
                #else:
                #    time.sleep(1)
                time.sleep(1)

            elif self.task_state == MeasStates.error_comms:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("comms error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_hw:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("hw error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_params:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("param error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.chimp_bug:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("chimp bug -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.user_aborted:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("user aborted -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.got_assert:
                # we don't budge from this state unless we are quiting
                self.monitor_cancel()

            elif self.task_state == MeasStates.error_EEPROM:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("EEPROM error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_Ps4:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("NaN error -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_cvUnstable:
                if not self.Error_active:
                    self.task_state = MeasStates.recover
                    wk_print("CV unstable -> recover")
                else:
                    time.sleep(1)

            elif self.task_state == MeasStates.error_ecable:
                wk_print("error_cable -> recover")
                for ix in range(50):
                    time.sleep(0.1)        # wait a wee bit before returning
                self.task_state = MeasStates.failed #FIXME
#                if self.Error_active == False:
#                    self.task_state = MeasStates.recover
#                    wk_print("error_cable -> recover")
            else:
                # check if the state has changed along the way 
                if self.task_state >= MeasStates.max_state:
                    wk_print("Bug not sure what error state we are in")
                    wk_print(self.task_state)
                    while True:
                        if self.stop == True or self.monitor_cancel() == True:
                            break

            self.monitor_cancel()

            # Exiting, tidy up
            #############################    
            if self.task_state == MeasStates.exiting:
                break

        #################################################################### 
        # End of while not cancelled loop
        ####################################################################

        if not self.mono_done:
            if self.child.poll() is None:
                ret_value = self.child.stdin.write('Q\n')
                self.child.stdin.flush()
                wk_print(ret_value)
                # schedule a clock that waits for the application to finish
                # if still live after 5 seconds then yank the plug
                killchimp_event = Clock.schedule_once(self.SchKillTheMonkeys, 5)
                while self.child.poll() is not None:
                    rx_data = self.child.stdout.readline()
                    sys.stdout.write(rx_data)
                #if the chimp went away then no need to kill it
                if self.child.poll is not None:
                    Clock.unschedule(killchimp_event)
        
        # wait here until we have tidied up
        if not self.mono_done:
            while self.child.poll() is not None:
                print('.', end='')

        self.shut_down = True
        sys.stdout.flush()
        return

    ################################################################
    # Helpers
    ################################################################
    # test whether we need to display results or wait for the next
    # measurement to be invoked, pass the state we should drop into
    # in case we are not ready
    def is_result_ready(self, i_next_state):
        show_result = False
        if self.msCount == self.TotalCount:
            self.task_state = MeasStates.show_results
            wk_print("all measurements completed -> show results")
            show_result = True
        else:
            self.task_state = i_next_state
        return show_result

    def RoundUp(self, in_number,  multiple):
        test_num = in_number % multiple
        return in_number if test_num == 0 else in_number + multiple - test_num

           
    def RoundDown(self, in_number, multiple):
        test_num = in_number % multiple
        return (in_number - multiple) if test_num == 0 else in_number - multiple - test_num

    #####################################################
    # Helper method, detects asserts and puts us in the 
    # correct state
    #####################################################
    def detectAssert(self, TestString):
        if TestString.startswith("Assert Failed"):
            wk_print(TestString)
            #extract the line number
            self.assert_LineNo = self.findNumInByteList(TestString)
            self.task_state = MeasStates.got_assert
            return True
        else:
            return False

    #####################################################
    # Helper method, strips out all the numbers in a
    # string represented by a list of bytes
    #####################################################
    def findNumInByteList(self, TestString):
        return re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?", (TestString) )

    #####################################################
    ## Kill any mono stray processes
    #####################################################
    def KillTheMonkeys(self, bforce):
        # check if there are any wild monkeys and kill them all
        if bforce:
            os.system("killall -9 mono")
            self.mono_done = True
        elif not self.mono_done:
            os.system("killall -9 mono")
            self.mono_done = True
        return

    #####################################################
    # Schedulable Forced Kill The Monkeys
    # Note this will only run once
    #####################################################
    def SchKillTheMonkeys(self, dt):
        self.KillTheMonkeys(True)
        return False


    #####################################################
    ## Stop any mono child process
    #####################################################
    def StopTheMonkey(self):
        wk_print("attempt to stop the chimp")
        if not self.mono_done:
            wk_print("chimp not done")
            if self.child.poll() is None:
                wk_print("child not None")
                ret_value = self.child.stdin.write('Q\n')
                self.child.stdin.flush()
                wk_print("Q signal sent")
                wk_print(ret_value)
                while self.child.poll() is not None:
                    rx_data = self.child.stdout.readline()
                    sys.stdout.write(rx_data)
                wk_print("chimp stopped")
                self.mono_done = True
        return


    #####################################################
    ## Test whether we still need to service the subprocess
    #####################################################
    def ChimpRunning(self):
        if not self.stop and not self.mono_done:
            return True
        else:
            return False

    #####################################################
    ## Update the state depending on what the chimp said
    #####################################################
    def ParseChimpReply(self, reply):
        # don't bother parsing when we are about to stop or 
        # when we are going to cancel
        if self.stop or self.cancelled:
            return
        print("ParseChimReply", reply)
        if reply is not None:
            if reply == ChimpReturns.Success:
                wk_print("chimp Success")
                if self.have_data:
                    self.task_state = MeasStates.do_randles
                    wk_print("-> do randles")
                else:
                    self.task_state = MeasStates.done
                    wk_print("-> done")
            elif reply == ChimpReturns.MeasurementFailed:
                wk_print("measurement failed -> failed")
                self.Error_active = True
                self.task_state = MeasStates.failed
            elif reply == ChimpReturns.FailedToConnect:
                self.gpio.get_ps4_on()
                wk_print("failed to connect -> error_comms")
                wk_print("going straight to Fail don't pass Go")
                wk_print("######################### ERROR: PS4")
                self.Error_active = True
                self.task_state = MeasStates.error_comms
            elif reply == ChimpReturns.DeviceNotFound:
                wk_print("device not found -> error_hw")
                wk_print("FATAL ERROR: is not connected or it has spannered its license")
                wk_print("going straight to Fail don't pass Go")
                self.Error_active = True
                # at this point we are pretty screwed so report a failed test
                wk_print("######################### ERROR: PS4")

                # we increment here because we never got to the plotting stage
                # where it would naturally increment
                self.msCount += 1
                self.is_result_ready(MeasStates.error_hw)

            elif reply == ChimpReturns.InvalidParameters:
                wk_print("Invalid parameters -> error_params")
                self.Error_active = True
                self.task_state = MeasStates.error_params
            elif reply == ChimpReturns.UserAborted:
                wk_print("user aborted -> initialising")
                self.task_state = MeasStates.initialising
            elif reply == ChimpReturns.EEPROM_lost:
                wk_print("PS4 brains corrupt -> error_EEPROM")
                self.Error_active = True
                self.task_state = MeasStates.error_EEPROM
            elif reply == ChimpReturns.CV_Unstable:
                wk_print("CV Unstable -- not sure how we got here -> error_cvUnstable")
                self.Error_active = True
                self.task_state = MeasStates.error_cvUnstable
            elif reply == ChimpReturns.Electrode_fault:
                wk_print("electrode cable fault -> error_ecable")
                #FIXME FIXME self.Error_active = True
                self.is_result_ready(MeasStates.error_ecable)
                wk_print("ERROR: Dodgy electrode -> show results")
            elif reply == ChimpReturns.HaveHitAssert:
                wk_print("have hit assert -> chimp_bug")
                self.Error_active = True
                self.task_state = MeasStates.chimp_bug
            elif reply == ChimpReturns.UnknownError: # 255
                wk_print("Unknown Error -> our_bug")
                self.Error_active = True
                self.task_state = MeasStates.our_bug
            else:
                # make sure we have a live process tot est against
                if not self.stop and self.child.poll() is not None:
                    wk_print("Unhandled Error! Bug! -> our_bug chimp reply[%d]" %reply)
                    self.Error_active = True
                    self.task_state = MeasStates.our_bug
            
            self.mono_done = True

            wk_print("mono done")
            wk_print(self.child.stdout.read())
            return
        else:
            wk_print("still going...")
            return


    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        return self.msplot_coords
    
    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return self.msplot_Xmin
    def get_PlotMaxX(self):
        return self.msplot_Xmax
    def get_PlotMinY(self):
        return self.msplot_Ymin
    def get_PlotMaxY(self):
        return self.msplot_Ymax

    ###############################
    # returns True if we have hit a 
    # measurement error
    ###############################
    def get_IsError(self):
        return self.Error_active

    ###############################
    # Returns True if we need the
    # user to select Yes or No option
    ###############################
    def get_PromptActive(self):
        if (self.task_state == MeasStates.askYesorNo or
                self.task_state == MeasStates.da_results):
            return True
        else:
            return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        assert ((self.task_state == MeasStates.askYesorNo) or (self.task_state == MeasStates.da_results)), "no active prompt here"
        return ui.index_gui_dict.ixgui_yesno

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    ###############################
    # Returns true if we are idle
    ###############################
    def get_IsPaused(self):
        if ( ( (self.task_state == MeasStates.idle) and (self.stop) ) or
                self.task_state == MeasStates.done or
                self.task_state == MeasStates.failed):
            return True
        else:
            return False

    ###############################
    # Returns a string reflecting
    # the current measurement state
    ###############################
    def get_StatusMsg(self):
        
        if   self.task_state == MeasStates.initialising :      
            return 'Initialising'
        elif self.task_state == MeasStates.idle         :      
            return 'Idle'
        elif self.task_state == MeasStates.rdInParams   :      
            return 'reading parameters'
        elif self.task_state == MeasStates.getVersion   : 
            return 'reading version'
        elif self.task_state == MeasStates.gotVersion   :
            return self.chimpVersion + '\n                 Connecting...'
        elif self.task_state == MeasStates.ms_Ocp       : #'{:.4f}'.format(x)
            return 'OCP = ' + '{:.4f}'.format(float(self.Ocp)) + ' V\nDelta = ' + '{:.4f}'.format(float(self.Delta)) + ' mV/s'
        elif self.task_state == MeasStates.ms_EIS       :
            return 'Starting EIS'
        elif self.task_state == MeasStates.failed       :
            return 'measurement failed'
        elif self.task_state == MeasStates.chimp_bug    :
            return 'have hit assert'
        elif self.task_state == MeasStates.done         :
            return self.fmt.format(self.rs, self.cdl, self.rct, self.aw, self.phi)
        elif self.task_state == MeasStates.show_results:
            return "Calculating..."
        elif self.task_state == MeasStates.da_results:
            if self.results == Results.negative:
                return 'NEGATIVE'
            elif self.results == Results.positive:
                return 'POSITIVE'
            else:
                return 'FAIL'   #self.results == Results.fail

        elif self.task_state == MeasStates.error_comms  :
            return 'failed to connect'
        elif self.task_state == MeasStates.error_hw     :
            return '     HW ERROR!:\nUSB-C PS4/PS4 PSU'
        elif self.task_state == MeasStates.error_params :
            return 'invalid parameters'
        elif self.task_state == MeasStates.our_bug      :
            return 'Unknown Error'
        elif self.task_state == MeasStates.askYesorNo   :
            if self.OcpState == OcpOutcome.low: 
                return '        Ocp is too low, ' + self.Ocp + ' V \nWould you like to continue with optional E DC?' 
            elif self.OcpState == OcpOutcome.high:
                return '        Ocp is too high, ' + self.Ocp + ' V\nWould you like to continue with optional E DC?' 
            else:
                return 'Corrected E DC = ' + self.Ocp +'\nWould you like to continue with optional E DC?' 
        elif self.task_state == MeasStates.unstableOcp  :
            return 'Unstable OCP'
        elif self.task_state == MeasStates.gotReply     :
            if self.UserReply == UsrPromptReply.no:
                return 'Cleaning up...'
            else:
                return 'Continuing...'
        elif self.task_state == MeasStates.user_aborted :
            return 'measurement aborted'
        elif self.task_state == MeasStates.exiting      :
            return 'shutting down'
        elif self.task_state == MeasStates.start_plotting:
            if self.please_wait:
                if self.pw_counter >= 40:
                    self.pw_counter = 0
                else:
                    self.pw_counter += 1
                if self.pw_counter < 10:
                    return 'EIS measurement starting\n         Please wait. |'
                elif 10 <= self.pw_counter < 20:
                    return 'EIS measurement starting\n         Please wait.  /'
                elif 20 <= self.pw_counter < 30:
                    return 'EIS measurement starting\n         Please wait. -- '
                elif 30 <= self.pw_counter <= 40:
                    return 'EIS measurement starting\n         Please wait.  \\'
            else:
                return 'Zr = ' + '{:.4f}'.format(self.Z_real) + ' ohm\nZi = ' + '{:4f}'.format(self.Z_imag) + ' ohm'
                
        elif self.task_state == MeasStates.wait_for_done:
            return 'EIS measurement complete'
        elif self.task_state == MeasStates.got_assert:
            return 'Internall error:\n' + self.assert_LineNo[0]
        elif self.task_state == MeasStates.do_randles:
            return 'Calculating...'
        elif self.task_state == MeasStates.upload:
            return self.fmt.format(self.rs, self.cdl, self.rct, self.aw, self.phi) + '\nsaving results to the cloud...'
        elif self.task_state == MeasStates.error_EEPROM:
            self.Error_active = True
            return 'PS EEPROM Error!'
        elif self.task_state == MeasStates.error_Ps4:
            self.Error_active = True
            return 'Error corrupt data!'
        elif self.task_state == MeasStates.error_ecable:
            #FIXME FIXME self.Error_active = True
            return      '      HW ERROR!:\nElectrode connection'
        elif self.task_state == MeasStates.error_cvUnstable:
            self.Error_active = True
            return 'Error CV Unstable!'
        elif self.task_state == MeasStates.recover:
            return 'Resseting'
        else:
            wk_print("Unhandled state %d" % self.task_state)
            return 'Bug! - get_StatusMsg -'

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        if bReply:
            self.UserReply = UsrPromptReply.yes
            wk_print("reply set to yes")
        else:
            self.UserReply = UsrPromptReply.no
            wk_print("reply set to no")
        return

    #############################
    # Clear the current error 
    # to get us back to retry
    #############################
    def set_ClearError(self):
        wk_print("clearing error")
        self.Error_active = False
        #self.stop = False
        self.task_state = MeasStates.idle
        self.stop = True
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        wk_print("continuing...")
        if self.task_state == MeasStates.idle or self.task_state == MeasStates.initialising:
            self.stop = False
        elif self.task_state == MeasStates.done or self.task_state == MeasStates.failed:
            self.State_Reset()
            self.task_state = MeasStates.idle
            self.stop = False
        else:
            wk_print("cannot continue in current state %d" %self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        wk_print("cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        wk_print("Stop called")
        if not self.stop:
            self.stop = True
            self.StopTheMonkey()
            self.task_state = MeasStates.initialising
            wk_print("stop -> initialising")
        else:
            wk_print("stop already in progress")
        return

    ################################################################
    # Find out how it all went
    ################################################################
    def save_result(self):
        if self.gpio.get_we() == 'ST':
            wk_print("WARNING: We are on the dummy cell")

        wk_print("getting results")
        # read our results
        kosher_list=[]
        wk_print("******************************* RESULTS ***************************")
        wk_print(self.rct_results)
        for ix in range(len(self.rct_results)):
            if self.rct_results[ix][0]:
                kosher_list.append(self.rct_results[ix][1])
        if len(kosher_list)!= 0:
            if max(kosher_list) > ResRct.positive_threshold:
                self.results = Results.positive
                wk_print("POSITIVE")
            else:
                self.results = Results.negative
                wk_print("NEGATIVE")
        else:
            wk_print("FAILED")
            self.results = Results.fail
        wk_print("******************************************************************")

        return

    ################################################################
    # Ensure result values are sane
    ################################################################
    def sanity_chk_results(self):
        self.result_ix += 1
        b_status = False
        # check if we are testing the dummy cell
        if self.gpio.get_we() == 'ST':
            wk_print("############################################ Rct = %f" % (self.rct * ResRct.kilo_ohm) )
            if ResRct.dummy_min < (self.rct * ResRct.kilo_ohm) < ResRct.dummy_max:
                wk_print("############################################ Dummy Cell Impedance Test [PASSED]")
            else:
                wk_print("############################################ Dummy Cell Impedance Test [FAILED]")

        # values arrive in kOhms
        # is Rct positive?
        if self.rct > 0:
            # if we are on the dummy cell then the value should be 10K
            # Rs should be between 0 and 10 Kohms
            if (self.rs > 0) and (self.rs < ResRct.rs_threshold):
                # OK all is well
                b_status = True
                self.rct_results.append((True, (self.rct * ResRct.kilo_ohm)))  # we have a good measurement
                wk_print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> GOOD measurement")
                wk_print(self.rct_results)
                if hold_on_eis:
                    input("press a key to continue...")
            else:
                self.rct_results.append((False, (self.rct * ResRct.kilo_ohm)))  # we have a bad measurement
                wk_print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BAD measurement Rs %f kOhms" % self.rs)
        else:
            self.rct_results.append((False, (self.rct * ResRct.kilo_ohm)))  # we have a bad measurement
            wk_print(self.rct_results)
            wk_print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> BAD measurement negative Rct")

        self.Results_to_Dropb(b_status)
        wk_print("count ====== %d/%d" % (self.msCount, self.TotalCount))

    ################################################################
    # Read EIS parameters
    # -check if we have a parameter file in our current location
    # -otherwise check for a default parameter file in our current 
    # location
    # - if neither of them found then leave the app to set the
    # parameters
    ################################################################
    def RdParameters(self):
        # check for new parameters
        try:
            if cms.DebugFlags.eis_debug_param_file:
                param_fname = ("%s%s" % (cms.CmsFilenames.eis_param_path, cms.CmsFilenames.eis_param_dbg))
                wk_print(param_fname)
                param_file = open(param_fname)
            else:
                param_fname = ("%s%s" % (cms.CmsFilenames.eis_param_path, cms.CmsFilenames.eis_param))
                wk_print(param_fname)
                param_file = open(param_fname)

        except IOError:
            wk_print("Parameter file not found")
            # no file found so check for a defaults file
            try:
                def_fname = ("%s%s" % (cms.CmsFilenames.eis_param_path, cms.CmsFilenames.eis_param_default) )
                wk_print(def_fname)
                param_file = open(def_fname)
            except IOError:
                # no defaults file either so let the chimp set the values
                wk_print("WARNING: EIS parameter file not found")
                return ' '  
            # let the chimp figure it out
            else:
                with param_file:
                    # read in the default parameter file
                    return param_file.read()    
        else:
            with param_file:
                wk_print("parameter file found")
                return param_file.read()    

    ################################################################
    # Read Randles parameters
    # -check if we have a parameter file in our current location
    # -otherwise check for a default parameter file in our current 
    # location
    # - if neither of them found then leave the caller set the
    # parameters
    ################################################################
    def RdRndParameters(self):
        # check for new parameters
        try:
            param_file = open ('./new-rnd-params.txt')
            # knock out above line and use this one instead when developing (goes faster)
            #debug_tag wk_print("FIXME FIXME FIXME HERE HERE HERE")
            #debug_tag param_file = open('./rnd-params.dummy.defaults.txt')
        except IOError:
            wk_print("Bug randles buggered up")
            self.Error_active = True
            self.task_state = MeasStates.our_bug
            # no file found so check for a defaults file
            try:
                param_file = open ('./rnd-params.defaults.txt')
            except IOError:
                # no defaults file either so let our caller set some defaults
                return ' '  
            # let the caller figure it out
            else:
                with param_file:
                    # read in the default parameter file
                    return param_file.read()
        else:
            with param_file:
                wk_print("randles parameter file found")
                return param_file.read()    

    #############################################################
    # Creates <measurement ID>_<hostname>_<timestamp> string
    # this gives us a unique filename
    #############################################################
    def Make_unique_str(self, i_mId, i_timestamp):
        res_ts = "{:%Y%m%d%H%M}".format(i_timestamp)  # grab a timestamp
        res_fts = res_ts.replace("/", '')   # remove the forward slashes from the string
        res_host = socket.gethostname()       #grab the hostname
        return i_mId + '_' + res_host + '_' + res_fts #create the unique string 


    #############################################################
    # method that deals with stashing the data to the cloud
    # grab the results file rename it with host and timestamp
    # then shove it up to dropbox
    #############################################################
    def Results_to_Dropb(self, b_have_results):
        # update this version number every time we mess with this file
        txtFileVersion = cms.get_vers_str()
        # rename the results file
        end_time = datetime.datetime.utcnow()
        # end_time_tag = self.Make_unique_str('EIS', end_time)
        end_time_tag = self.Make_unique_str(cms.EisDropBox.prefix, end_time)
        # newfilename = end_time_tag + '.txt'
        newfilename = cms.EisDropBox.path + end_time_tag + cms.EisDropBox.ext
        wk_print("%s -> %s" %(cms.EisDropBox.src_name, newfilename))
        # os.rename("isResults.txt", newfilename)
        os.rename(cms.EisDropBox.src_name, newfilename)
        # we will append the name, start time and version to the begining of the file
        # start_time_tag = self.Make_unique_str('EIS', self.start_time)
        start_time_tag = self.Make_unique_str(cms.EisDropBox.prefix, self.start_time)
        header_tag = cms.RdExpId() + 'Measurement ID: ' + txtFileVersion + ' ' + start_time_tag + '\n'
        
        # append the results if we have them
        with open(newfilename, "a") as resultfile:
            if b_have_results:
                resultfile.write( (self.fmt.format(self.rs, self.cdl, self.rct, self.aw, self.phi) ) )
            resultfile.write('\nMeasurement started: ' + "{:%Y%m%d%H%M}".format(self.start_time) )
            resultfile.write('\nMeasurement ended:   ' + "{:%Y%m%d%H%M}".format(end_time) )
            resultfile.flush()
            resultfile.close()
        # prepend the Measurement ID string
        with open(newfilename, "r+") as resultfile:
            contents = resultfile.read()
            resultfile.seek(0,0)
            resultfile.write(header_tag + contents)
            resultfile.flush()
            resultfile.close()

        return


###################################
# Debug print with worker id
###################################
def wk_print(dbg_string):
    print("EIS: %s" % dbg_string)


##
# EIS Rct thresholds
#
class ResRct():
    kilo_ohm = 1000.0
    rs_threshold = 10.0  # k Ohms
    dummy_min = 9900.0
    dummy_max = 10100.0
    positive_threshold = 500000.0


#########################################
# Background Task States
#########################################
class MeasStates():
    initialising     = 0     # initialising our variables
    idle             = 1     # waiting for a request to start measuring
    rdInParams       = 2     # we have been requested to start
    getVersion       = 3     # read in the version of the .Net subprocess
    gotVersion       = 4     # version obtained
    ms_Ocp           = 5     # measuring OCP
    ms_EIS           = 6     # measureing EIS
    failed           = 7     # measurement failed
    chimp_bug        = 8
    done             = 9     # task completed
    error_comms      = 10
    error_hw         = 11
    error_params     = 12
    our_bug          = 13
    askYesorNo       = 14    # ask the user what they want to do now
    unstableOcp      = 15
    gotReply         = 16    # user pressed a button, we have a reply
    user_aborted     = 17
    exiting          = 18    # start shuting down this thread
    start_plotting   = 19
    wait_for_done    = 20 
    got_assert       = 21
    do_randles       = 22    # do the dsp stuff
    upload           = 23
    error_EEPROM     = 24
    error_Ps4        = 25
    error_ecable     = 26     # dodgy electrode cable
    error_cvUnstable = 27
    recover          = 28    # recover from an error and try again
    show_results     = 29
    da_results       = 30

    max_state        = 31
###################################
# Error codes returned by the chimp
# Reference: debugDialogs.cs
# public enum ExitCode : int
###################################
class ChimpReturns:
    Success           = 0
    MeasurementFailed = 1
    FailedToConnect   = 2
    DeviceNotFound    = 3
    InvalidParameters = 4
    UserAborted       = 5
    EEPROM_lost       = 6
    CV_Unstable       = 7
    Electrode_fault   = 8
    HaveHitAssert     = 170   # 0xAA
    UnknownError      = 255   # 0xff
    Bug               = 300

###################################
# OCP measurement outcomes
###################################
class OcpOutcome():
    good    = 0
    low     = 1
    high    = 2
    unknown = 3

###################################
# User prompt reply
###################################
class UsrPromptReply():
    yes     = 0
    no      = 1
    unknown = 2


###################################
# User prompt reply
###################################
class Results():
    unknown  = 0
    positive = 1
    negative = 2
    fail     = 3


# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.gpio.bioc_init()     # initialise the hardware in stand alone mode
    worker.start()
    worker.gpio.set_ps4(True)    # switch the PS4 ON

    print("[p] to print status")
    print("[g] to start/continue")
    print("[s] to stop")
    print("[q] to quit")
    print("[e] to clear error")
    print("[y] yes")
    print("[n] no")
    print("[w] to switch electrodes")
    print("[?/h] for help")

    print("EIS: Initialising...")
    while not worker.get_IsPaused():
        time.sleep(0.25)

    if worker.get_IsPaused():
        print(worker.get_StatusMsg())
        print("selecting dummy electrode")
        worker.gpio.switch_we('ST')
        electrode = 11

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':                  # print status
            print(worker.get_StatusMsg())
            if electrode == hal.Mux.maximum:
                print("Working electrode [Dummy]")
            else:
                print("Working electrode [WE%d]" % electrode)
        elif cmd == 'g':                # go
            worker.set_Continue()
        elif cmd == 'e':                # clear the error
            worker.set_ClearError()
        elif cmd == 'q':                # quit
            worker.set_Cancel()
            worker.gpio.set_ps4(False)
            break
        elif cmd == 's':                # stop
            worker.Stop()
        elif cmd == 'y':                # yes
            worker.set_Reply(True)
        elif cmd == 'n':                # no
            worker.set_Reply(False)
        elif cmd == 'w':
            if electrode == 11:
                electrode = 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE1')

            elif electrode == 1:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE2')

            elif electrode == 2:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE3')

            elif electrode == 3:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE4')

            elif electrode == 4:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE5')

            elif electrode == 5:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE6')

            elif electrode == 6:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE7')

            elif electrode == 7:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE8')

            elif electrode == 8:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE9')

            elif electrode == 9:
                electrode += 1
                print("selecting electrode WE%d" % electrode)
                worker.gpio.switch_we('WE10')

            elif electrode == 10:
                electrode += 1
                print("selecting DUMMY electrode")
                worker.gpio.switch_we('ST')


        elif cmd == '?' or cmd == 'h':
            print("[p] to print status")
            print("[g] to start/continue")
            print("[s] to stop")
            print("[q] to quit")
            print("[e] to clear error")
            print("[y] yes")
            print("[n] no")
            print("[w] to switch electrodes")
            print("[?/h] for help")


if __name__ == "__main__":

    main()

    sys.exit()
