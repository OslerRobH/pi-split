################################################
# A class to define a standardised EIS data object.
# Author: Zahid Ansari
################################################
import numpy as np


class EISData:
    """
    A class to define a standardised EIS data object.

    Attributes:
    - test_id: A string identifier
    - meta_data: A dictionary of meta_data (Can be an empty dict)
    - E: The amplitude of the applied sinusoid (Can be None or a float)
    - F: A numpy array of the frequency values for each data point
    - ReZ: A numpy array with the real impedance values
    - Z: An array with the impedance ReZ - j*negImZ
    - negImZ: A numpy array with the imaginary impedance
    - min_length: The minimum number of points expected for R, ReZ and negImZ.
                  Is optional to allow for backward compatibility.
                  Default value is 5 to ensure that there are enough
                  points for fitting.
    - success: True if there are no errors otherwise False
    - errors: A dictionary with error message and exceptions (details below)
              {} if there were no errors

    Possible keys and values for errors dictionary:
    - test_id: If not a string. Info on class is returned in the message.
    - meta_data: If not a dict. Info on class is returned in the message.
    - E: If not None and cannot be converted to float. Exception im message.
    - F, ReZ, negImZ: the associated array is shorter than min_length
    - mismatch: the lengths of F, ReZ and negImZ are not identical
    - Exception: text of any thrown exceptions

    If an Exception is raised then Z is set to None to ensure that it exists

    A repr method is provided to allow printing of EISData
    """

    def __init__(self,
                 test_id: str,
                 meta_data: dict,
                 E: float,
                 F: np.ndarray,
                 ReZ: np.ndarray,
                 negImZ: np.ndarray,
                 min_length: int=5
                 ):
        """
        :param test_id: A string to identify the test
        :param meta_data: A dictionary with meta data
        :param E: The amplitude of the sinusoidal signal (Optional, Can be None)
        :param F: Measurement frequencies
        :param ReZ: Real part of impedance
        :param negImZ: Negative of the imaginary part of impedance

        The array parameters D, ReZ, Z are converted to numpy arrays of
        type float and tested to confirm that they are greater than or
        equal to min_length.
        min_length has a default value of 5 to ensure that fitting is possible.

        The dictionary attribute errors contains the status message for
        each validated attribute that fails the associate tests.
        If an Exception is raised, then the execution does not stop but
        a message is included in the errors with the key 'Exception'
        E is optional so no testing is performed on it.
        """

        array_args = ['F', 'ReZ', 'negImZ']

        try:
            self.errors = {}
            if not isinstance(test_id, str):
                msg = 'Expected string. Got {}'.format(test_id.__class__)
                self.errors['test_id'] = msg
            if not isinstance(meta_data, dict):
                msg = 'Expected dict. Got {}'.format(meta_data.__class__)
                self.errors['meta_data'] = msg
            if E is not None:
                try:
                    E = float(E)
                except Exception as e:
                    msg = ('{}'.format(e))
                    self.errors['E'] = msg
            for arg, argval in locals().items():
                if arg in array_args:
                    argval, status = self._validate_array(argval, min_length)
                    if status > 0:
                        msg = '{} less than minimum length of {}'.format(
                            arg, min_length
                        )
                        self.errors[arg] = msg
                self.__setattr__(arg, argval)
            if self._unequal_array_lengths():
                self.errors['mismatch'] = 'Unequal array lengths'
            self.Z = self.ReZ - 1j * self.negImZ

        except Exception as e:
            self.errors['Exception'] = str(e)
            self.Z = None

        self.success = self._test_errors()

    def _validate_array(self, arr, min_length):
        """
        A helper method for testing array objects.
        Converts iterables like lists and arrays to np.ndarray and
        confirms that the minimum length is 5 to ensure possibility to fit.
        Returns an np.narray and an enum as follows:
        0 = _test_errors
        1 = Fails minimum length
        """
        value = np.array(arr).astype(float)
        if value.shape[0] >= min_length:
            status = 0
        else:
            status = 1
        return (value, status)

    def _unequal_array_lengths(self):
        """
        A helper method for testing array lengths.
        """
        return not(self.F.shape == self.ReZ.shape == self.negImZ.shape)

    def _test_errors(self):
        """
        Returns True if the errors attribute is an empty dictionary
        """
        return self.errors == {}

    def __repr__(self):
        """
        Print the attributes
        """
        retstr = 'EIS Object:\n'
        for attr in ('test_id', 'meta_data', 'E', 'F',
                     'ReZ', 'negImZ', 'Z', 'min_length', 'errors'):
            try:
                retstr += ('{} => {}\n'.format(attr,
                                               self.__getattribute__(attr)))
            except:
                retstr += ('{} => {}\n'.format(attr,
                                               'ValueError'))
        return retstr
