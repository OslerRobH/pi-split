# README #

Demo code for prototype instruments.

## Running the demo ##

<pre>
./demo.sh
</pre>

## Running instrument self checks ##

<pre>
./test.sh
</pre>


To run the automated test press

<pre>
g
</pre>

then follow the instructions

## Running the manual hardware tests ##

<pre>
./test.sh
</pre>

To run the manual test press
<pre>
t
</pre>

To quit the manual test and go back to the automated version

<pre>
q
</pre>

## Running an Impedance test ##

<pre>
./impedance.sh
</pre>

## Powering down the Rpi ##

<pre>
./poff.sh
</pre>

