################################################
# Returns Z for a Randles equivalent circuit for 
# the range of frequencies
# Author: Zahid Ansari
################################################
import numpy as np

def impedance_model(F: np.array,
                    Rs: float,
                    Cdl: float,
                    phi: float,
                    Rct: float,
                    Aw: float) -> np.array:
    """
    Returns Z for a Randles equivalent circuit for thex
    range of frequencies omega
    :param F: Array of frequency values
    :param Rs: Series resistance
    :param Cdl: Double layer capacitance
    :param phi: angle?
    :param Rct: Charge transfer resistance
    :param Aw: Warburg coefficient
    :return: Impedance as a complex number array
    """
    omega = 2 * np.pi * F
    alpha = 0.50
    min_denom = 1e-12  # To avoid divide by zero
    # Impedance of Cdl
    ZCdl = 1 / (min_denom + ((1j * omega) ** phi) * Cdl * 1e-9)
    # Warburg Z = (Aw / (sqrt(omega)) - j * Aw / sqrt(jw))
    sqrt_omega = (min_denom + np.power(omega, alpha))
    ZW = (1 - 1j) * (Aw * 1.0e3) / sqrt_omega
    # Series combination of Rct and Warburg
    ZRctZW = (Rct * 1.0e3) + ZW
    # Parallel combination of Cdl and (Rct + Warburg) in ohms
    Z0 = 1 / ((1 / (min_denom + ZCdl)) +
              (1 / (min_denom + ZRctZW))
              )
    # Print error if numbers for Z0, ZCdl or ZRctZW have nan's
    if np.isnan(np.real(Z0)[0]):
        print('ERROR: Z0 has illegal values')
    if np.isnan(np.real(ZCdl)[0]):
        print('ERROR: ZCdl has illegal values')
    if np.isnan(np.real(ZRctZW)[0]):
        print('ERROR: ZRctZW has illegal values')

    Z = Rs * 1.0E3 + Z0  # Series combination of Rs and Z0

    return Z
