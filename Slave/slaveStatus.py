#!/usr/bin/env python
'''
This is a python script that configures a callback function for the I2C
When the Master (Compute) requests the status of the Slave (GUI), 
  the callback function will collect the request 
  and send back the collected data to the Master 
'''
import project_defaults
import time
import sys
sys.path.insert(0,'/home/pi/PIGPIO/pigpio')

import pigpio

I2C_ADDR = project_defaults.I2C_SLAVE_ADDRESS
LIMIT_COUNT = 30
cb1 = None
global pi

pi = pigpio.pi()       # pi accesses the local Pi's GPIO

if pi is None:
    print ("pi is set to None bye...")
    exit()

if not pi.connected:
    print ("pi is not connected bye...")
    exit()


def sendData(address, data):
    intsOfData = list(map(ord, data))
    try:
        # bus.write_i2c_block_data(address, intsOfData[0], intsOfData[1:])
        handle = pi.i2c_open(1, 0, 0)
        if handle >= 0:
            print("handle open")
            pi.i2c_write_byte(handle, 0x55)   # send byte   17 to device 1
            print("PASS")
            return True
        else:
            print("FAIL handle open")
            return False

    except:
        print("FAIL i2c_write_byte")
        return False

def i2c(id, tick):
    global pi

    s, b, d = pi.bsc_i2c(I2C_ADDR)
    if d[0] == project_defaults.STATUS_MESSAGE_TYPE:
        print ("STATUS_MESSAGE_TYPE received")
        value1 = 0x55
        values = "%c%c" % (project_defaults.STATUS_MESSAGE_TYPE, value1)
        result = sendData(project_defaults.I2C_MASTER_ADDRESS, values)
    else:
        print ("i2c callback called s=%.2X, b=%.2X, d=%.2X" 
            % (s, b, d[0]))


# Respond to BSC slave activity
e = pi.event_callback(pigpio.EVENT_BSC, i2c)

# Configure BSC as I2C slave
pi.bsc_i2c(I2C_ADDR) 

i = 0
while i < LIMIT_COUNT: 
    print ("waiting..")
    time.sleep(1)

# cb1.cancel()

pi.bsc_i2c(0) # Disable BSC peripheral

pi.stop()


