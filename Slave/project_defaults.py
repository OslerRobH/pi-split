'''
This is a project wide set of default values that are used in multiple places
'''

I2C_SLAVE_ADDRESS = 0x03

COMMAND_MESSAGE_TYPE = 0xE5
STATUS_MESSAGE_TYPE = 0xE6
STATE_MESSAGE_TYPE = 0xE7
