#!/usr/bin/env python3

# Microchip I2C 4-channel differential ADC

import common
fake_rpi_gpio = common.DebugFlags.on_dev_pc

import time
import cp2120   # SPI to I2C bridge
import bioc

# set to True to enable debug output, set to False to disable it
dbg_mcp3424 = False
dbg_rdmcp3424 = False

class mcp3424():

    # our I2C addresses
#    __U1_out__ = bioc.BiocI2cAddr.MCP23017_U1

    __U8__ = bioc.BiocI2cAddr.MCP3424_U8
    __U9__ = bioc.BiocI2cAddr.MCP3424_U9


    # Device default settings are:
    # *  Conversion bit resolution: 12 bits (240 sps)
    # *  Input channel: Channel 1
    # *  PGA gain setting: x1
    # *  Continuous conversion
    # *  -2048 to 2047
    # *  LSB = 1mV
    # Once the device is powered-up,
    # the user can reprogram the configuration
    # bits using I2C serial interface any time.
    # The configuration bits are stored in
    # volatile memory.
    #

    def __init__(self):
        self.i2c = cp2120.octet_i2c
        self.sample_rate = 240
        self.LSB = 0.001    # V
        self.PGA = 1.0


    def EnableDrv(self):
        if fake_rpi_gpio:
            return
        self.adc_set_channel(self.__U8__, 0)

        self.adc_set_channel(self.__U9__, 0)

    ####################################
    # Start a conversion from the desired
    # channel, valid values 0-3
    ####################################
    def adc_set_channel(self, i2c_addr, channel):
        """Start adc conversion on channel 0-4"""
        dbg_print("Entry: adc_set_channel(addr=0x%x, ch=%d)" % (i2c_addr, channel) )
        assert (0 < channel < 5), "adc channel out of bounds"
        assert (self.sample_rate == 240 or
                self.sample_rate == 60 or
                self.sample_rate == 15 or
                self.sample_rate == 3.75), "sample rate out of bounds"
        # bit
        # 7   RDY: Ready Bit. RD/WR
        #          RD: 0 - data is ready
        #          WR: 1 - start a new conversion
        #
        # 6-5 C1 - C0: Channel Selection Bits
        #       00: Channel 1 (Default)
        #       01: Channel 2
        #       10: Channel 3
        #       11: Channel 4
        #
        # 4     Conversion Mode Bit
        #       1: Continuous Mode
        #       0: One-Shot mode
        #
        # 3-2 S1 - S0: Sample Rate Selection Bit
        #       00: 240 SPS (12 bits)
        #       01:  60 SPS (14 bits)
        #       10:  15 SPS (16 bits)
        #       11: 3.75 SPS (18 bits)
        #
        # 1-0 G1-G0: PGA Gain Selection
        #       00: x1
        #       01: x2
        #       10: x4
        #       11: x8
        # to set the configuration byte send a packet [i2c addr, config byte]
        config_byte = 0x00
        config_byte |= 0x80 # start new conversion
        # select the channel
        if channel==1:
            config_byte |= 0x00
        elif channel==2:
            config_byte |= 0x20
        elif channel==3:
            config_byte |= 0x40
        else:
            config_byte |= 0x60
        # set the sample rate
        if self.sample_rate == 240:
            config_byte |= 0x00
        elif self.sample_rate == 60:
            config_byte |= 0x04
        elif self.sample_rate == 15:
            config_byte |= 0x08
        else:
            config_byte |= 0x0C
        packet = [config_byte]
        dbg_print("config value = 0x%x" % config_byte)
        self.i2c.WrI2C(i2c_addr, packet)
        dbg_print("Exit: adc_set_channel")
        return #config_byte

    ####################################
    # Read data from the ADC
    ####################################
    def adc_read_channel(self, i2c_addr, channel):
        """Read a raw value from the ADC. Return tuple of value and status."""
        dbg_rdprint("Entry: adc_read_channel(addr=0x%x ch=%d)" % (i2c_addr, channel))
        # start a conversion from the desired channel
        self.adc_set_channel(i2c_addr, channel)
        # now we need to wait for it to sample and convert
        # wait time is 1/(sample rate) + transaction time on the SPI-I2C bridge, lets say we add 20%
        time.sleep(1.2/self.sample_rate)
        attempts = 0
        volts = 0
        got_reading = False
        while attempts < 3:
            # attempt to read [ data high byte, low byte, config byte ]
            rd_bytes = self.i2c.RdI2C(i2c_addr, 3)
            dbg_print(rd_bytes)
            for ix in range(len(rd_bytes)):
                dbg_print("ADC<-:{0:b}".format(rd_bytes[ix]))
            # did we get 3 bytes?
            if len(rd_bytes) == 3:
                # check the ready flag, make sure our data is valid
                if (rd_bytes[2] & 0x80 == 0):
                    # data is ready so we are good to go
                    # The MSB (sign  bit) is always transmitted first through the I2C

                    # converting to voltage
                    #
                    # If MSB = 0 (Positive Output Code):
                    # V = OutputCode * (LSB/PGA)
                    #
                    # If MSB = 1 (Negative Output Code):
                    # V = (2's complement of Output Code) * (LSB/PGA)
                    #
                    # remember: 2's complement = 1's complement + 1
                    volts = ((rd_bytes[0] & 0x07) << 8 | rd_bytes[1]) & 0xFFF # 12 bits
                    if bool(rd_bytes[0] & 0x08):
                        volts = ~volts + 1
                    volts = volts * self.LSB * self.PGA
                    got_reading = True
                    break
            elif len(rd_bytes) == 1:
                # check the ready bit
                if rd_bytes[0] & 0x80:
                    dbg_print("ADC conversion not ready")
                # lets wait a wee bit before we request another read
                time.sleep(0.3/self.sample_rate)
            attempts += 1
        if not got_reading:
            dbg_rdprint("ERROR: ADC addr[0x%x] channel[%d]" % (i2c_addr, channel))
        dbg_rdprint("Exit adc_read_channel after %d attempts got reading %r volts=%d" %(attempts, got_reading, volts))
        return (volts, got_reading)



###################################
# Debug print
###################################
def dbg_print(dbg_string):
    if dbg_mcp3424:
        print(dbg_string)
    return

def dbg_rdprint(dbg_string):
    if dbg_rdmcp3424:
        print(dbg_string)
    return

# this is here so we only reference one instance of this module
hal_adc = mcp3424()
