/**
  * @file slaveReceive.cpp
  * @brief This dll will run as an I2C Slave and receive messages from the 
  *        Python I2C Master.
  *        Requirements
  *          1) Get the received i2C packet to be checked for length == 0x03
  *          2) Content all Command messages shall begin with 0xE5. 
  *          Refer to the Code Separation document Command - Response Table.
  *          3) If all is valid then extract the Command, Value1 and Value2.
  *          4) Python dll access uses get_Command(), get_Value1(), get_Value2()
  *
  *          At a higher level in Python the command sequence is decoded  
  */

#include <pigpio.h>


#include <stdio.h>
#include <unistd.h>

#define SLAVE_ADDRESS (0x03)
#define WAIT_PERIOD (0.25)
//#define PACKET_SIZE (0x03)
#define SLAVE_ADDRESS (0x03)

void runSlave();
void closeSlave();
int getControlBits(int, uint8_t);
uint8_t set_Status(uint16_t status);
uint8_t get_Status(uint8_t index);

bsc_xfer_t xfer; // Struct to control data flow

uint8_t Command = 0;
uint8_t Value1 = 0;
uint8_t Value2 = 0;
uint16_t Status = 0;

int main(){
    // Chose one of those two lines (comment the other out):
    runSlave();
    //closeSlave();

    return 0;
}

void reset_Command_Values(void)
{
    Command = Value1 = Value2 = 0;
}

void runSlave() {
    (void) gpioInitialise();
    // printf("Initialized GPIOs\n");
	
    // Close old device (if any)
    xfer.control = getControlBits(SLAVE_ADDRESS, 0 /*false*/); // To avoid conflicts when restarting

    bscXfer(&xfer);

    // Set I2C slave Address 
    xfer.control = getControlBits(SLAVE_ADDRESS, 1 /* true */);
    int status = bscXfer(&xfer); // Should now be visible in I2C-Scanners

    if (status >= 0)
    {
        int handle;
        int result;
        xfer.rxCnt = 0;
        uint8_t not_found = 0;
        uint16_t j = 0;

        while (not_found == 0)
        {
            status = bscXfer(&xfer);
            if (status >= 0)
            {
                if(xfer.rxCnt > 0) 
                {
                    // cout << "Received " << xfer.rxCnt << " bytes: ";
                    if (xfer.rxCnt == 0x03)
                    {
                        not_found = 1;
                        reset_Command_Values();
                        if (0xE5 == xfer.rxBuf[0] || 0xE6 == xfer.rxBuf[0] || 0xE7 == xfer.rxBuf[0]) 
                        {
                            Command = xfer.rxBuf[0];
                            Value1  = xfer.rxBuf[1];
                            Value2  = xfer.rxBuf[2];
                            // printf("VALID %.2X %.2X %.2X\n", Command, Value1, Value2);
                        }
                        else
                        {
                            // printf("INVALID %.2X %.2X %.2X\n", Command, Value1, Value2);
                        }
                    }
                    else
                    {
#if 0
                        printf("I2C received packet is not the size of a Command %.2X : %.2X\n", 0x03, xfer.rxCnt);
                        handle = i2cOpen(1, SLAVE_ADDRESS, 0);
                        if (handle >= 0)
                        {
                            result = i2cWriteByte(handle, 0x55);
                            if (result == 0)
                            {
                                not_found = 1;
                                printf("i2cWriteByte PASS");
                            }
                            else
                            {
                                printf("i2cWriteByte FAIL");
                            }
                        }
                        else
                        {
                            if (handle == PI_BAD_USER_GPIO)
                            {
                                printf("i2cOpen FAIL = PI_BAD_USER_GPIO");
                            }
                            else if (handle == PI_NOT_SERIAL_GPIO)
                            {
                                printf("i2cOpen FAIL = PI_NOT_SERIAL_GPIO");
                            }
                            else if (handle == PI_I2C_WRITE_FAILED)
                            {
                                printf("i2cOpen FAIL = PI_I2C_WRITE_FAILED");
                            }
                            else 
                            {
                                printf("i2cOpen FAIL = %.2X\n", handle);
                            }
                        }
#else
                        // xfer.txBuf[0] = 0x55;
                        // xfer.txCnt = 1;
                        xfer.txBuf[0] = get_Status(0);
                        xfer.txBuf[1] = get_Status(1);
                        xfer.txCnt = 2;
#endif
                    }
                }
                else
                {
                    sleep(WAIT_PERIOD);
                    j++;
                    // printf("%d Nothing Received by I2C\n", j);
                    fflush(stdout);
    
                    // exceeded retry count ?
                    if (j > 2000)
                    {
                        // printf("FAIL I2C exceeded retry count\n");
                        not_found = 1;    
                    }
                }
            }
            else
            {
                printf("Bad Status update from bscXfer()");
            }
        }
    }
    else
    {
        printf("FAIL Failed to open slave!!!\n");
    }
}

void closeSlave() {
    gpioInitialise();
    printf("Initialized GPIOs\n");

    xfer.control = getControlBits(SLAVE_ADDRESS, 0 /* false */);
    bscXfer(&xfer);
    printf("Closed slave.\n");

    gpioTerminate();
    printf("Terminated GPIOs.\n");
}


int getControlBits(int address /* max 127 */, uint8_t open) {
    /*
    Excerpt from http://abyz.me.uk/rpi/pigpio/cif.html#bscXfer regarding the control bits:

    22 21 20 19 18 17 16 15 14 13 12 11 10 09 08 07 06 05 04 03 02 01 00
    a  a  a  a  a  a  a  -  -  IT HC TF IR RE TE BK EC ES PL PH I2 SP EN

    Bits 0-13 are copied unchanged to the BSC CR register. See pages 163-165 of the Broadcom 
    peripherals document for full details. 

    aaaaaaa defines the I2C slave address (only relevant in I2C mode)
    IT  invert transmit status flags
    HC  enable host control
    TF  enable test FIFO
    IR  invert receive status flags
    RE  enable receive
    TE  enable transmit
    BK  abort operation and clear FIFOs
    EC  send control register as first I2C byte
    ES  send status register as first I2C byte
    PL  set SPI polarity high
    PH  set SPI phase high
    I2  enable I2C mode
    SP  enable SPI mode
    EN  enable BSC peripheral
    */

    // Flags like this: 0b/*IT:*/0/*HC:*/0/*TF:*/0/*IR:*/0/*RE:*/0/*TE:*/0/*BK:*/0/*EC:*/0/*ES:*/0/*PL:*/0/*PH:*/0/*I2:*/0/*SP:*/0/*EN:*/0;

    int flags;
    if(open)
        flags = /*RE:*/ (1 << 9) | /*TE:*/ (1 << 8) | /*I2:*/ (1 << 2) | /*EN:*/ (1 << 0);
    else // Close/Abort
        flags = /*BK:*/ (1 << 7) | /*I2:*/ (0 << 2) | /*EN:*/ (0 << 0);

    return (address << 16 /*= to the start of significant bits*/) | flags;
}

uint8_t get_Command(void)
{
    return Command;
}

uint8_t get_Value1(void)
{
    return Value1;
}

uint8_t get_Value2(void)
{
    uint8_t result = Value2;
    reset_Command_Values();
    return result;
}

uint8_t set_Status(uint16_t status)
{
    Status = status;
    return (0);
}

uint8_t get_Status(uint8_t index)
{
    if (index == 0)
    {
        return (uint8_t) Status;
    }
    else
    {
        return (uint8_t) (Status >> 8);
    }
}
