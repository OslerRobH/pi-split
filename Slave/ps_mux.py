################################################
# Multiplexer Worker
# Author: Muhammad Shakeel, 
# refactored: deya.sanchez@gmail.com
################################################
import sys        # flushing stdout
import time       # so we can sleep
import common as cms    # for on_dev_pc
import config_sequence as cfg    # determines which mux sequence to use


# Debugging flags
on_dev_pc = cms.DebugFlags.on_dev_pc
hold_on_mux = cms.DebugFlags.hold_on_mux

from threading import Thread

import hal


#########################################################
# This is the class to handle MUX worker operations
# This runs a thread that will switch to different MUX
# states as per mux_states.cfg in round robin fashion
#########################################################
class MainWorkerThread(Thread):

    def __init__(self):
        super(MainWorkerThread, self).__init__()
        self.daemon = True
        self.cancelled = False
        self.shut_down = False
        self.gpio = hal.octet_gpio
        print("hal ", self.gpio)
        self.cur_mux_state = 0
        self.switched_state = 0
        if cms.gui_geek_mode:
            self.our_cfg = cfg.geek_dict_mux_sequence
        else:
            self.our_cfg = cfg.dict_mux_sequence
        self.init_vars()
        print("%s MainWorkerThread(Thread) called" % __file__)

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        self.stop = True         # we start with everything quiet
        self.task_state = WorkerStates.initialising   # state machine tracker
        self.Error_active = False
        self.cur_mux_state = 0
        return

    ####################################
    # call this regularly when blocking
    # so we can bail out nicely when told to do so
    ####################################
    def monitor_cancel(self):
        if self.cancelled :
            wk_print("-> exiting")
            self.task_state = WorkerStates.exiting
            return True
        else:
            return False

    #########################################
    # state machine that deals with kicking
    # off the mux states
    #########################################
    def run(self):

        # keep running until the application exits
        while not self.cancelled:

            # Initialise this state machine
            #################################
            if self.task_state == WorkerStates.initialising:
                self.init_vars()
                self.task_state = WorkerStates.idle
                wk_print("rdInConfig -> idle")

            # Wait for a request to start the multiplexer
            #############################################
            elif self.task_state == WorkerStates.idle:
                if not self.stop:
                    self.task_state = WorkerStates.switch
                    wk_print("idle -> switch")
                else:
                    time.sleep(0.05)
                    self.monitor_cancel()

            # Switch the multiplexer
            ########################
            elif self.task_state == WorkerStates.switch:
                self.switched_state = self.cur_mux_state
                self.set_mux(self.cur_mux_state)    # do the business
                self.cur_mux_state += 1             # prepare the next switch
                if self.cur_mux_state == len(self.our_cfg): # check for overflow
                    self.cur_mux_state = 0
                if hold_on_mux :
                    input("press a key to continue...")
                self.task_state = WorkerStates.done
                wk_print("switch -> done")

            # switch done!
            ####################################################
            elif self.task_state == WorkerStates.done:
                time.sleep(5)

            # Error states
            ##############
            elif self.task_state == WorkerStates.failed:
                # our error is irrecoverable so we wait for death
                self.monitor_cancel()

            # bug!
            #######
            else:
                # check if the state has changed along the way 
                if self.task_state >= WorkerStates.max_state:
                    wk_print("Bug not sure what error state we are in")
                    wk_print(self.task_state)
                    while True:
                        pass

            self.monitor_cancel()            

            # Exiting, tidy up
            #############################    
            if self.task_state == WorkerStates.exiting:
                wk_print("thread exiting")
                self.shut_down = True
                break

        self.shut_down = True
        sys.stdout.flush()
        return

    ################################################################
    # Helpers
    ################################################################

    #################################
    # switch to the desired electrode
    #################################
    def set_mux(self, state):
        # check that the state is within bounds
        if state < len(self.our_cfg):
            # ok we are good
            wk_print("Switching to ----------> [%s]" % self.our_cfg[state])
            self.gpio.switch_we(self.our_cfg[state])
        else:
            wk_print("state out of bounds %d" % state)
            self.Error_active = True
            
        return


    ################################################################
    # Getters
    ################################################################

    ###############################
    # Plot data
    ###############################
    def get_PlotData(self):
        msplot_coords = []
        return msplot_coords

    ###############################
    # Plot coordinate values
    ###############################
    def get_PlotMinX(self):
        return 0
    def get_PlotMaxX(self):
        return 1
    def get_PlotMinY(self):
        return 0
    def get_PlotMaxY(self):
        return 1

    ###############################
    # returns True if there is an
    # mux error
    ###############################
    def get_IsError(self):
        return self.Error_active

    ###############################
    # Always return false, no user
    # interaction required by mux
    ###############################
    def get_PromptActive(self):
            return False

    ###############################
    # Returns the flavour of prompt
    # we require
    ###############################
    def get_PromptType(self):
        return 0

    ###############################
    # Has this thread been cancelled?
    ###############################
    def get_Cancelled(self):
        return self.cancelled

    ###############################
    # Returns true if we are idle
    ###############################
    def get_IsPaused(self):
        if ( (self.task_state == WorkerStates.idle) and self.stop ) or (self.task_state == WorkerStates.done):
            return True
        else:
            return False

    ###############################
    # Returns a string reflecting
    # the current mux state
    ###############################
    def get_StatusMsg(self):
        return self.our_cfg[self.switched_state]

    ################################################################
    # Setters
    ################################################################
    def set_Reply(self, bReply):
        return

    #############################
    # Clear the current error
    # to get us back to retry
    #############################
    def set_ClearError(self):
        wk_print("Cannot clear irrecoverable error")
        return

    #############################
    # We need to continue
    #############################
    def set_Continue(self):
        wk_print("continuing...")
        if self.task_state == WorkerStates.idle or self.task_state == WorkerStates.initialising:
            self.stop = False
        elif self.task_state == WorkerStates.done:
            self.task_state = WorkerStates.idle
            self.stop = False
        else:
            wk_print("cannot continue in current state %d" %self.task_state)
        return

    #############################
    # We need to die
    #############################
    def set_Cancel(self):
        wk_print("cancelling...")
        self.cancelled = True
        return

    #############################
    # Abort current operation
    # and re-initialise
    #############################
    def Stop(self):
        wk_print("Stop called")
        if not self.stop:
            self.stop = True
            self.task_state = WorkerStates.initialising
            wk_print("stop -> initialising")
        else:
            wk_print("stop already in progress")
        return

###################################
# Debug print with worker id
###################################
def wk_print(dbg_string):
    print("MUX: %s" % dbg_string)

#########################################
# Task States
#########################################
class WorkerStates():
    initialising     = 0     # initialising our variables
    idle             = 1     # waiting for a request to change mux state
    switch           = 2     # switch the mux
    done             = 3     # mux switched
    failed           = 4     # fudge, we have a bug
    exiting          = 5     # shuting down this API

    max_state        = 6


# Test code
####################################
def main():

    worker = MainWorkerThread()

    # Start the thread
    worker.gpio.bioc_init()     # initialise the hardware in stand alone mode
    worker.start()

    while True:
        # Set the new state only if already paused
        cmd = input("> ")
        if cmd == 'p':
            print(worker.get_StatusMsg())
        elif cmd == 'g':
            worker.set_Continue()
        elif cmd == 'e':
            worker.set_ClearError()
        elif cmd == 'q':
            worker.set_Cancel()
            break
        elif cmd == 's':
            worker.Stop()


if __name__ == "__main__":

    main()

    sys.exit()
