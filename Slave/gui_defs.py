################################################
# Dictionary of GUI prompts requiring user 
# intervention
# deya.sanchez@gmail.com
################################################

import common

# Label for Button 1 is for October Prototype
# Label for Button 2 is for Lab Development


class index_gui_dict():
    ixgui_continue = 0
    ixgui_ins_cart = 1
    ixgui_confirm  = 2
    ixgui_start    = 3
    ixgui_cancel   = 4
    ixgui_reset    = 5
    ixgui_yesno    = 6  # for Lab development
    ixgui_continue = 7  # for Lab development
    ixgui_bye      = 8  # for Lab development

if common.DebugFlags.flight_mode:

    # October Prototype GUI dictionary
    dict_gui_usr_prompt_op = {
        # LUT value       Label for Button,  
        0          :      (  'START'            ), # splash screen, initialising
        1          :      (  'INSERT CARTRIDGE' ), # after initialisation is complete
        2          :      (  'CONFIRM'          ), # marries up with status messages
        3          :      (  'TEST'             ), # once blood sample is ready
        4          :      (  'QUIT'             ), # to abort at any stage
        5          :      (  'RESET'            ), # in case of system error
        6          :      (  'bug 6'            ), # you shouldn't be calling these
        7          :      (  'bug 7'            ), # you shouldn't be calling these
        8          :      (  'bug 8'            ), # you shouldn't be calling these
    }
    
    
    # Lab Development GUI dictionary
    dict_gui_usr_prompt_lab = {
        # LUT value       Label for Button 1,  Label for Button 2      
        0          :      (  'START'           ,   'OFF'         ),
        1          :      (  'INSERT CARTRIDGE',   'OFF'         ),
        2          :      (  'CONFIRM'         ,   'OFF'         ),
        3          :      (  'TEST'            ,   'OFF'         ),
        4          :      (  'QUIT'            ,   'OFF'         ),
        5          :      (  'RESET'           ,   'OFF'         ),
        6          :      (  'YES'             ,   'NO'           ),
        7          :      (  'CONTINUE'        ,   'OFF'         ),
        8          :      (  'good'            ,   'bye'          ),
    }

else:
    # October Prototype GUI dictionary
    dict_gui_usr_prompt_op = {
        # LUT value       Label for Button,
        0          :      (  'START'            ), # splash screen, initialising
        1          :      (  'INSERT CARTRIDGE' ), # after initialisation is complete
        2          :      (  'CONFIRM'          ), # marries up with status messages
        3          :      (  'TEST'             ), # once blood sample is ready
        4          :      (  'CANCEL'           ), # to abort at any stage
        5          :      (  'RESET'            ), # in case of system error
        6          :      (  'bug 6'            ), # you shouldn't be calling these
        7          :      (  'bug 7'            ), # you shouldn't be calling these
        8          :      (  'bug 8'            ), # you shouldn't be calling these
    }


    # Lab Development GUI dictionary
    dict_gui_usr_prompt_lab = {
        # LUT value       Label for Button 1,  Label for Button 2
        0          :      (  'START'           ,   'QUIT'         ),
        1          :      (  'INSERT CARTRIDGE',   'QUIT'         ),
        2          :      (  'CONFIRM'         ,   'QUIT'         ),
        3          :      (  'TEST'            ,   'QUIT'         ),
        4          :      (  'CANCEL'          ,   'QUIT'         ),
        5          :      (  'RESET'           ,   'QUIT'         ),
        6          :      (  'YES'             ,   'NO'           ),
        7          :      (  'CONTINUE'        ,   'QUIT'         ),
        8          :      (  'good'            ,   'bye'          ),
    }
