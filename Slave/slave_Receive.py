import ctypes
import time
import project_defaults as project_defaults

class slave_Receive:

    def __init__(self):
        self.Command = 0
        self.Value1 = 0
        self.Value2 = 0

    def get_Message(self):

        # print("slave_Receive start 1")
     
        receiveLib = ctypes.cdll.LoadLibrary('/home/pi/pi-split/Slave/slaveReceive.so')
        if None == receiveLib:
            print("receiveLib is NULL")
        else:
            # print("slave_Receive start 2")
            receiveLib.main()
            if receiveLib.get_Command() != 0:
                self.Command = receiveLib.get_Command()
                self.Value1 = receiveLib.get_Value1() 
                self.value2 = receiveLib.get_Value2()
                    
                # print("Received [ %.2X %.2X %.2X ]" % (self.Command, self.Value1, self.Value2))
                if self.Command != project_defaults.COMMAND_MESSAGE_TYPE and self.Command != project_defaults.STATUS_MESSAGE_TYPE and self.Command != project_defaults.STATE_MESSAGE_TYPE:
                    return False

                return True
    
        return False # default response

    def get_Command(self):
        return self.Command
 
    def get_Value1(self):
        return self.Value1
 
    def get_Value2(self):
        return self.Value2

    def set_Status(self, newStatus):
        receiveLib = ctypes.cdll.LoadLibrary('/home/pi/pi-split/Slave/slaveReceive.so')
        if None == receiveLib:
            print("receiveLib is NULL")
            return (-1)
        else:
            receiveLib.set_Status(newStatus)
            print("newStatus = %.2X" % (newStatus))
            return (0)

 
# sr = slave_Receive()
# sr.get_Message()
