#!/bin/bash

# this the start up executable for the Slave

# empty the Python Cache
sudo rm -rf __pycache__

# set gui_geek_mode to False in common.py
sed -i 's/gui_geek_mode *= *True/gui_geek_mode = False/' common.py

# execute Python 3 big button start
sudo python3 slave.py

