# common flags so we only need to change in one place.
import socket                       # so we can identify the hostname
import subprocess

##
# set to True for plotting version, this is
# automatically set by the invoking shell script
# so don't change it here unless you are invoking
# the API directly
gui_geek_mode = False

##
# Set whether this is a debug or release version.
#
version_option_development = False  # True for development, False for release

##
# Set to true to calibrate fluidics
#
calibrate_fluidics = False
do_quick_test = False

def is_instrument():
    ##
    # Test whether this API currently running on a known instrument
    # otherwise we assume we are on a development PC or laptop
    #
    # @returns TRUE if we are on a known instrument

    if ( socket.gethostname() == 'athena' or
        socket.gethostname() == 'apollo' or
        socket.gethostname() == 'ares' or
        socket.gethostname() == 'delphi'):
        return True
    else:
        return False


##
# Returns the version string for our API
#
class OurVersion():

    def __init__(self):
        if version_option_development :
            print("Development", self.get_Api_version() )
        else:
            print("Release", self.get_Api_version() )

    def get_Api_version(self):
        ##
        # Returns the system wide version for this API
        # There are two versions this can return, the git hash number when we
        # are working with a debug version and a release version name for deployment
        #
        # @returns string in the format "Version hash number" or "Version name"
        #
        if version_option_development :

            global_version_string = "Version: " + (subprocess.check_output(['git',  'rev-parse', '--short', 'HEAD']).strip()).decode("utf-8")

        else:
            ##
            # For release, deployment
            # -----------------------
            # Single global version string gets updated when any component of this API changes this includes
            # all workers, C# modules, device drivers and libraries.
            # Ditching version numbers in favour of version name which should be more user friendly
            #
            # Theme: feathered birds
            # -----------------------
            # Previous versions
            # Buzzard - 070918 integration
            # Red Kite - 20181003 the big demo
            # Blackbird - 20181009 google demo
            # Blackap - 20181016 apple demo
            # Bluethroat - 20181018 demo candidate - declined
            # Bullfinch - demo
            # Chaffinch - 2018 Halloween
            # Brambling - 20181101 - flight mode
            # Chiffchaff - 20181102 - for Berlin demo
            # Bittern - pi-osler version
            # Capercaillie - 20181207 - pi-demo fluidics re-written
            # Snipe - 20190401 - Deya left the building
            global_version_string = 'Version: Snipe'
        return global_version_string


##
# Set our system wide debugging flags
#
class DebugFlags():

    ##
    # Debug Version
    # Developing on laptop or debugging on target hardware
    #
    if is_instrument():
        on_dev_pc = False
    else:
        on_dev_pc = True

    ##
    # Flags used for development, debugging and calibration
    #
    if version_option_development:
        if calibrate_fluidics:
            skip_fluidics = False
            ignore_pogo_fault = True
            eis_debug_param_file = False
            dud_mode = False    # for debugging the gui, don't care what the result is just display something
            auto_advance = True
        else:
            skip_fluidics = False
            ignore_pogo_fault = True
            eis_debug_param_file = False  # set to true if we are debugging the eis parameters
            dud_mode = False            # for debugging the gui, don't care what the result is just display something
            auto_advance = False

    ##
    # Flags used for Release versions
    # Ensures all flags are correct
    #
    else:
        calibrate_fluidics = False
        skip_fluidics = False
        ignore_pogo_fault = False
        eis_debug_param_file = False
        dud_mode = False
        auto_advance = False

    ##
    # Flags set when we are using the plotting version of this API
    #
    if gui_geek_mode:
        hold_on_mux = False # True if we want to wait for keyboard input to continue after a mux switch
        hold_on_eis = True  # as above but for the EIS worker
        hold_on_cv = True
    ##
    # Flags set when we are using the demo version of this API
    #
    else:
        hold_on_mux = False # True if we want to wait for keyboard input to continue after a mux switch
        hold_on_eis = False  # as above but for the EIS worker
        hold_on_cv = False

    ##
    # Flag set for an instrument going through airport security control
    #
    flight_mode = True

    ##
    # On the instrument apollo we may have two manifolds connected.
    # Decide which manifold we have. Set this to true if you are using the
    # one that is open, i.e. the one without the motor.
    #
    apollo_open = False  # True if using apollo test manifold


##
# Set the release flavour
# geek mode is the one that gives us plots
# if set to false then we get the demo version
# with the pretty faces
class ReleaseFlag():
    if gui_geek_mode:
        enable_plot = True     # set to true if we need graphs
    else:
        enable_plot = False


##
# Measurement related filenames
# CV and EIS
class CmsFilenames():
    id_tag_path = './'
    id_tag = 'exp_id.txt'
    eis_param_path = './'
    eis_param = 'eis-params.txt'
    eis_param_default = 'eis-params.defaults.txt'
    eis_param_dbg = 'eis-params.dummy.defaults.txt'
    cv_param_path = './'
    cv_param_peak = 'cv-params-for-peaks.txt'
    cv_param_stab = 'cv-params-for-stable.txt'


##
# CV related
#
class CvDefs():
    ##
    # True if we want to use the same parameter file for peak and for stability measurement
    #
    same_param_pk_stab = True   


##
# Dropbox file name definitions for CV measurements
#
class CvDropBox():
    src_name = 'cvResults.txt'          # name of the file as mono saves it
    prefix = 'CV'                    # prefix of the new filename
    path = '/home/pi/pi-dropbox/tmp/'   # path of where we dump the renamed file
    ext = '.octet.txt'                       # extension we attach to it


##
# Dropbox file name definitions for EIS measurements
#
class EisDropBox():
    src_name = 'isResults.txt'          # name of the file as mono saves it
    prefix = 'EIS'                    # prefix of the new filename
    path = '/home/pi/pi-dropbox/tmp/'   # path of where we dump the renamed file
    ext = '.octet.txt'                       # extension we attach to it


##
# GUI definitions for geek mode
# Dictionary of plot axis labels
dict_plotWorker_labels = {
    # worker ID     X    Y
    'HWinit'    : (' ' , ' '               )  , # no plot for this worker
    'Cartridge' : (' ' , ' '               )  , # no plot for this worker
    'Fluidics'  : (' ' , ' '               )  , # no plot for this worker
    'OCP'       : ('time'    , 'Voltage'   )  , # time, voltage
    'CV'        : ('Voltage' , 'Current'   )  , # current, voltage
    'EIS'       : ('Zr', 'Zim'             )  , # Zreal, Zim
    'MUX'       : (' ' , ' '               )  , # no plot for this worker
    'RESULTS'   : (' ' , ' '               )  , # no plot for this worker
    'PAUSE'     : (' ' , ' '               )  ,
    'PASSFAIL'  : (' ' , ' '               )  ,
}

##
# GUI definitions demo mode
# paths to images used in demo mode
class DemoImgDefs():
    path = './images/'
    ext = '.png'


###########################
# API wide helpers
###########################
class WkDbg(object):

    def __init__(self, worker_name, spam_enabled):
        WkDbg.wk = worker_name
        WkDbg.dbg_spam = spam_enabled

    def w_print(self, dbg_string):
        ##
        # Debugging method
        # Print debug output with an identifier prepended for this module.
        # But only print when variable dbg_spam is set to true
        # @param dbg_string: The debug string we want to print
        # @return: none
        #
        if WkDbg.dbg_spam:
            print("%s: %s" % (WkDbg.wk, dbg_string))


def get_vers_str():
    ##
    # Works out the version name or hash number for this build
    # @returns the system wide version string
    #
    version = OurVersion()
    return version.get_Api_version()


def RdExpId():
    ##
    # Helper that reads our Experiment ID, wee need this to
    # tack it to our measurement data
    # @returns experiment ID string with carriage return
    #
    b_status = True
    expid_fname = ("%s%s" % (CmsFilenames.id_tag_path, CmsFilenames.id_tag))
    try:
        id_file = open(expid_fname)
    except IOError:
        # throw a wobbly
        b_status = False
    else:
        experiment_id = id_file.read()

    assert(b_status), "Experiment ID tag BUG!"
    return (experiment_id + '\n')
