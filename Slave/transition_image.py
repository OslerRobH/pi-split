"""
Helper Class that handles the animation of the torso
filling up with blood and ending up with a pink box
"""
from kivy.app import App
from kivy.graphics import Color, Rectangle, Ellipse
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.uix.stencilview import StencilView
from functools import partial
from kivy.clock import Clock


##
# StencilView limits the drawing of child
# widgets to the specified bounding box.
class StencilWidget1(StencilView):
	pass

##
# Transition image class that handles the
# torso blood fill animation
class TransitionImage(App):

	w1 = 0
	h1 = 0
	x1 = 0
	y1 = 0
	wid1 = 0

	w2 = 0
	h2 = 0
	x2 = 0
	y2 = 0
	wid2 = 0

	timer = 0
	mask_number = 1

	def add_background(self, lwid, *largs):
		"""Set the background image, in
		this case the torso, full of blood with
		pink box"""
		with lwid.canvas:
			Rectangle(source = 'images/Asset 13a.png',
				pos=(0,0), 
				size=(480,800),
				allow_stretch=(True),
				keep_ratio=False)

	def build(self, cb, parent):
		"""Start with the image of the torso,
		no blood and a black box"""
		img = Image(source = 'images/Asset 12a.png', pos = (0,0), 
				size = (480,800), allow_stretch = (True), keep_ratio = False)

		TransitionImage.wid1 = StencilWidget1(size_hint = (None, None), 
				size = (TransitionImage.w1, TransitionImage.h1),
					pos = (TransitionImage.x1,TransitionImage.y1))
		self.add_background(TransitionImage.wid1)

		TransitionImage.wid2 = StencilWidget1(size_hint = (None, None), 
				size = (TransitionImage.w2, TransitionImage.h2),
					pos = (TransitionImage.x2,TransitionImage.y2))
		self.add_background(TransitionImage.wid2)

		root = BoxLayout(orientation='vertical', size=(480,800))
		rfl = FloatLayout()
		rfl.add_widget(img)
		rfl.add_widget(TransitionImage.wid1)
		rfl.add_widget(TransitionImage.wid2)
		root.add_widget(rfl)
	
		TransitionImage.timer = Clock.schedule_interval(partial(self.stepper_callback, cb, parent), 0.01)

		return root

	def run(self, cb, parent):
		TransitionImage.w1 = 330
		TransitionImage.h1 = 0
		TransitionImage.x1 = 75
		TransitionImage.y1 = 0

		TransitionImage.w2 = 0
		TransitionImage.h2 = 0
		TransitionImage.x2 = 0
		TransitionImage.y2 = 0
		TransitionImage.mask_number = 1

		return self.build(cb, parent)

	def stepper_callback(self, cb, parent, dt):

		if TransitionImage.mask_number == 1:
			TransitionImage.h1 += 4
			TransitionImage.wid1.size=(TransitionImage.w1, TransitionImage.h1)
			TransitionImage.wid1.pos = (TransitionImage.x1, TransitionImage.y1)
			if TransitionImage.h1 >= 625:
				TransitionImage.mask_number = 2

			TransitionImage.w2 = 100
			TransitionImage.h2 = 0
			TransitionImage.x2 = 190
			TransitionImage.y2 = 600
				
		else:
			TransitionImage.h2 += 3
			TransitionImage.w2 += 4
			TransitionImage.x2 -= 2
			TransitionImage.wid2.size = (TransitionImage.w2, TransitionImage.h2)
			TransitionImage.wid2.pos = (TransitionImage.x2, TransitionImage.y2)
			if TransitionImage.x2 <= 0:
				TransitionImage.timer.cancel()
				cb(parent)



