#!/usr/bin/env python3
"""
Hardware Abstraction Layer
"""
import common as cms
on_dev_pc = cms.DebugFlags.on_dev_pc
fake_rpi_gpio = on_dev_pc

if fake_rpi_gpio:
    import fake_RPi_GPIO as GPIO
    from random import randint
else:
    import RPi.GPIO as GPIO

import time
import sys
import bioc
import cp2120  # SPI to I2C bridge
import mcp23017  # I2C GPIO expanders
import mcp3424  # I2C ADCs


# set to True to enable debug output, set to False to disable it
dbg_hal = True
dbg_inputs = False
dbg_v_valve = False
dbg_outputs = False
dbg_self_test = False
dbg_pin_test = False
dbg_loopback_test = False
dbg_ps4 = True
dbg_mux = False

hal_i2c_addr = bioc.BiocI2cAddr
hal_pin_map  = bioc.dict_octet_bioc


##
# Hardware Abstraction Layer for the Octet
# prototype running on a Raspberry Pi Model 3B+
#
class OctetHardwareAbstractionLayer():
    """
    our chip I2C addresses
    """
    __U1_out__ = hal_i2c_addr.MCP23017_U1  #0x20
    __U2_out__ = hal_i2c_addr.MCP23017_U2  #0x21
    __U3_out__ = hal_i2c_addr.MCP23017_U3  #0x22
    __U4_in__  = hal_i2c_addr.MCP23017_U4  #0x23

    # Rpi BCM assignments
    __GEN_RESET__  = 26 # global reset
    """
    Raspberry Pi pins initialised by cp2120.py
    __MOSI__ = 10   Rpi --> CP2120 
    __MISO__ = 9    Rpi <-- CP2120 
    __SCLK__ = 11   Rpi --> CP2120 
    __CS__   = 8    Rpi --> CP2120 
    __INT__  = 16   Rpi <-- CP2120 
    """
    __VAR_VALVE_PWM__ = 12         # Pin to control the bastard variable valve
    __DC_MOTOR_PWM__  = 13         # Pin to control the DC motor
    __SHUTDOWN_OUT__  = 20         # Pin to shutdown

    # PWM frequencies
    __VAR_VAL_FREQ__  = 100        # Hz
    __DC_MOTOR_FREQ__ = 100        # Hz

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        self.ADC_FAIL_RETURN = -99  # ADCs will never read a voltage of -99, so use this as the failure value
        self.dc_motor_started = False      # set to true once the DC motor interface has been initialised
        self.prev_motor_dc = 100           # last value sent to the DC motor, this is our cached value
        self.var_valve_started = False     # set to true once the interface to the variable valve has been initialised.
        self.ps4_on = False                # set to true once the ps4 has been switched on
        self.Epd_state = False             # true when the charger is enabled
        self.Chg_full_state = False        # true when the battery has been fully charged
        self.v_supply_psi = 0              # voltage supplying our PSI sensors (feeds the ADCs)
        self.initialised = False           # set to true once this module has been initialised
        # These get initialise to correct values once the device drivers are run
        self.var_valve = None              # bastard variable valve interface
        self.dc_motor = None               # DC motor interface
        self.i2c = None                    # I2C bus
        self.gpio = None                   # GPIO expanders
        self.adc = None                    # ADCs
        self.we_selected = None            # Setting for the working electrode
        return

    def bioc_init(self):
        """
        Initialise the basic IO configuration for our hardware
        """
        self.initialised = True

        GPIO.setmode(GPIO.BCM)


        print("******************************************* %s" % cms.get_vers_str() )
        dbg_print("Rpi Pin configuration")
        # we do not configure the shutdown pin here as it has the undesirable effect of killing the power
        # papa hardware needs to fix this
        GPIO.setup(self.__GEN_RESET__, GPIO.OUT)    # initialise the global reset pin
        # System Reset
        dbg_print("Peripheral reset asserted")
        self.set_global_reset(True)

        time.sleep(3)    # reset hold time

        # initialise the PWM pins
        GPIO.setup(self.__VAR_VALVE_PWM__, GPIO.OUT)         # variable valve pin
        GPIO.output(self.__VAR_VALVE_PWM__, GPIO.LOW)        # set the pin off
        self.var_valve = GPIO.PWM(self.__VAR_VALVE_PWM__, self.__VAR_VAL_FREQ__) # set our working frequency
        self.var_valve.start(0)

        GPIO.setup(self.__DC_MOTOR_PWM__, GPIO.OUT)         # dc motor output pin
        GPIO.output(self.__DC_MOTOR_PWM__, GPIO.LOW)        # set the pin off
        self.dc_motor = GPIO.PWM(self.__DC_MOTOR_PWM__, self.__DC_MOTOR_FREQ__)   # set our working frequency
        self.dc_motor.start(0)

        # start with pwm outputs off
        dbg_print("Motor OFF")
        self.set_motor(0)
        dbg_print("Variable valve OFF")
        self.set_v_valve(0)

        self.set_global_reset(False)
        dbg_print("Peripheral reset released")

        dbg_print("Initialising peripherals")

        attempts = 0
        self.i2c = cp2120.octet_i2c              # initialise the SPI to I2C bridge
        i2c_status = self.i2c.EnableDrv()
        reset_length = 0.2
        start_delay = 0.1
        # Try to recover from any shit that could be on the comms from the boot
        while not i2c_status and attempts < 10:               # and enable it so we can start using it
            # that didn't go so well lets try and reset everything and try again
            print(" Attempting to recover.... [%d]" % attempts)
            self.set_global_reset(True)
            self.i2c.release_Bus()
            time.sleep(reset_length)
            self.set_global_reset(False)
            time.sleep(start_delay)
            i2c_status = self.i2c.EnableDrv()
            if i2c_status:
                break
            reset_length += 0.05
            start_delay += 0.05
            attempts += 1
        if not i2c_status:
            print("Failed to recover, please talk to papa hardware")
        else:
            if attempts != 0:
                print("Phew! we have recovered reset assert time [%f] start delay [%f]" % (reset_length, start_delay))

            time.sleep(0.1)
            dbg_print("Initialising the GPIO expanders")
            self.gpio = mcp23017.gpio_expander # initialise the GPIO expanders
            self.gpio.EnableDrv()              # and enable it so we can start using it
            time.sleep(0.1)
            dbg_print("Initialising the ADCs")
            self.adc = mcp3424.hal_adc
            time.sleep(0.1)

            if not fake_rpi_gpio:
                self.init_ps4()

                # write to the Output expander
                # Mux expander
                self.switch_we('ST')    # working electrode set to dummy cell
                self.Epd_state = self.get_pin('EPD')
                # reflect the state of the charger
                if self. Epd_state and not self.Chg_full_state:
                    self.set_pin(bioc.LEDs.green, 1)    # green for charger plugged in
                elif self. Epd_state and not self.Chg_full_state:
                    self.set_pin(bioc.LEDs.blue, 1)     # blue when the charger is plugged in and battery is full
                else:
                    self.set_pin(bioc.LEDs.red, 1)      # red for charger unplugged

                dbg_print("OPTOs OFF")
                self.set_pin('OPTLED', 0)
                # beep the buzzer
                self.buzz_beep(0.25)

                # Cartridge expander - pumps and valves
                dbg_print("Valves and Pumps OFF")
                self.set_port('valves',0)

                # Dump valve expander
                # everything off, dump valve and charger shutdown
                self.set_port('dump', 0)

                # read our rail so we can take a better pressure reading
                self.v_supply_psi = self.rd_5vQrail()

                # Purge the pressures
                # so we always start in a known state
                self.set_pin('DUMP', 1)  # open the dump valve
                self.set_v_valve(100)    # open the variable valve
                psi_neg = self.psi('PS-')
                psi_var = self.psi('PSv')

                #print("Purging the system ", end='')
                while (psi_var < bioc.PsiPurged.var or
                        psi_neg < bioc.PsiPurged.neg):
                    psi_neg = self.psi('PS-')
                    psi_pos = self.psi('PS+')
                    psi_var = self.psi('PSv')
                    #print("PURGING: (+) %2.2fpsi (v) %2.2fpsi (-) %2.2fpsi" %
                    #         (psi_pos, psi_neg, psi_var))
                    #print(".", end='')
                print("\nSystem purged")
                self.set_pin('DUMP', 0)
                self.set_v_valve(0)

                if not self.rd_port() & bioc.InputMask.opto_home:
                    print("cartridge is home")
                else:
                    print("cartridge not home")
                self.set_pin("OPTLED", 0)

        if not fake_rpi_gpio:
            # battery charger on
            self.set_pin('CHGSHDN', 1)
            print("Battery charger ON")

        dbg_print("TODO: manage the charger")
        dbg_print("TODO: check the PSU rails")

        return

    def dump_input_state(self):
        """
        Dump state of inputs, mainly used for hardware testing
        read all ports/ADCs and write to the expanders.
        """

        print(" ************* Inputs **********************")
        # read the input expander
        #print("Input port 0x%x" % self.rd_port())
        # now read the inputs pin by pin hopefully this flushes stuff out
        #print("Cartridge detected ", end = '')
        if self.get_pin('CARTDETIN'):
            print("[YES]")
        else:
            print("[NO]")

        #print('Loop detected ', end='')
        if self.get_pin('LOOPDETIN'):
            print("[GOODY]")
        else:
            print("[BADDY]")

        self.set_pin('OPTLED', 1)
        #print("Mech assembly is ", end='')
        if self.get_pin('OPTOPOS1'):
            print("[HOME]")
        elif self.get_pin('OPTOPOS2'):
            print("[POGO DOWN]")
        elif self.get_pin('OPTOPOS3'):
            print("[ENGAGED]")
        else:
            print("[UNKNOWN]")

        print('PS4 PSU is %s' % ("[ON]" if self.get_ps4_on() else "[OFF]"))

        self.set_pin('OPTLED', 0)
        #print('Button pressed detected [%d]' % self.get_pin('SWDET'))
        #print('External power detected ', end='')
        self.Epd_state = self.get_pin('EPD')
        if self.Epd_state:
            print("[YES]")
        else:
            print("[NO]")
        #print('Charge state ', end='')
        self.Chg_full_state = self.get_pin('CHGFULL')
        if self.Chg_full_state:
            print("[FULL]")
        else:
            print("[NOT FULL]")

        ##print('Charge fast detected ', end='' )
        if self.get_pin('CHGFAST'):
            print("[YES]")
        else:
            print("[NO]")

        #print('Charge ', end='')
        if self.get_chrg_error():
            print("[ERROR]")
            self.buzz_beep(0.5)
        else:
            print("[OK]")

        print('ALLRT [%r]' % self.get_pin('ALLRT'))
        #print('Loop detect 2 [%r]' % self.get_pin('LOOPDETIN2'))

        print(" ************* ADC **********************")
        # read from the ADCs

        print("Variable pressure sensor %f" % self.psi('PSv'))
        print("Positive pressure sensor %f" % self.psi('PS+'))
        print("Negative pressure sensor %f" % self.psi('PS-'))

        print("V Battery %f" % self.rd_vbatt() )
        print("5V quiet rail %f" % self.rd_5vQrail() )
        print("Papa Hw fix me: -3V3 rail %f" % self.rd_minus3V3rail() )
        print("5V rail %f" % self.rd_5vrail() )

        return

    ###################################
    # Call before exiting API
    ###################################
    def de_initialise(self):
        print("De-Initialising hardware")
        # switch off motors, valves and PS4
        print("motor off")
        self.set_motor(0)
        print("var valve off")
        self.set_v_valve(0)
        self.set_ps4(False)
        # set all valves and pumps off
        print("dump valve off")
        self.set_pin('DUMP', 0)
        time.sleep(1)
        print("(-) valve off")
        self.set_pin('PUMP_NEG',0)
        time.sleep(1)
        print("(+) valve off")
        self.set_pin('PUMP_POS',0)
        time.sleep(1)
        # opto LED off
        self.set_pin('OPTLED', 1) # active low
        self.set_global_reset(True)
        time.sleep(1)
        self.set_global_reset(False)
        GPIO.cleanup()
        self.initialised = False
        exit(1)

    #################################################################
    # RPi Pin Helpers
    #################################################################
    ###################################
    # Set the global reset
    ###################################
    def set_global_reset(self, b_enable):
        """set the global reset(b_enable), True for reset asserted"""
        assert self.initialised, "module not initialised"
        if b_enable:
            GPIO.output(self.__GEN_RESET__, GPIO.LOW) # assert the global reset
        else:
            GPIO.output(self.__GEN_RESET__, GPIO.HIGH)  # release the reset
            time.sleep(0.1)  # wait for all hardware to gather its brains
        return

    ###################################
    # Set the motor speed
    ###################################
    def set_motor(self, duty_cycle):
        """set the motor speed(PWM duty cycle value 0-100)"""
        dbg_print("Entry: set_motor(%d)" % duty_cycle)
        assert self.initialised, "module not initialised"
        assert (0 <= duty_cycle <= 100), "duty cycle out of range should be 0-100%"
        self.dc_motor.ChangeDutyCycle(duty_cycle)

# Leaving this as a reminder of broken code that leaks memory
#        if duty_cycle == 0:
#            self.dc_motor.stop()   <--------------- this method leaks memory
#            if self.dc_motor_started:
#                self.dc_motor_started = False
#            dbg_print("DC motor stopped")
#        elif self.dc_motor_started:
#            self.dc_motor.ChangeDutyCycle(duty_cycle)
#        else:
#            dbg_print("DC motor starting")
#            self.dc_motor.start(duty_cycle)
#            self.dc_motor_started = True
#        return

    ###################################
    # Set the variable valve duty
    ###################################
    def set_v_valve(self, duty_cycle):
        """set the variable valve duty(PWM duty cycle value 0-100)"""
        dbg_vvprint("Entry: set_varvalve(%d)" % duty_cycle)
        assert self.initialised, "module not initialised"
        assert (0 <= duty_cycle <= 100), "duty cycle out of range should be 0-100%"
        self.var_valve.ChangeDutyCycle(duty_cycle)

# Leaving this as a reminder of broken code that leaks memory
#        if duty_cycle == 0:
#            self.var_valve.stop()   <--------------- this method leaks memory
#            self.var_valve_started = False
#            #dbg_print("variable valve stopped")
#        elif self.var_valve_started:
#            self.var_valve.ChangeDutyCycle(duty_cycle)
#        else:
#            dbg_vvprint("variable valve starting")
#            self.var_valve.start(duty_cycle)
#            self.var_valve_started = True
#        self.prev_var_valve_dc = duty_cycle
        return

    #################################################################
    # GPIO Expander Helpers
    #################################################################
    ###################################
    # Beep the buzzer
    ###################################
    def buzz_beep(self, beep_time):
        """Read all the inputs in one"""
        assert self.initialised, "module not initialised"
        # beep the buzzer
        self.set_pin('BUZZER', 1)
        time.sleep(beep_time)
        self.set_pin('BUZZER', 0)

    ###################################
    # Read all the inputs in one go
    ###################################
    def rd_port(self):
        """Read all the inputs in one"""
        assert self.initialised, "module not initialised"
        if fake_rpi_gpio:
            return dbg_getvalue(0xFFFF)
        port_result = 0
        rx_bytes = self.gpio.RdPorts(self.__U4_in__)
        if len(rx_bytes) == 2:
            dbg_rdprint("<-:{0:b}".format(rx_bytes[0]) + "{0:b}".format(rx_bytes[1]))
            port_result = 0xFFFF & ( ((rx_bytes[0] << 8) & 0xFF00) | rx_bytes[1] )
            dbg_rdprint("port rd=0x%x" % port_result)
        else:
            print("ERROR reading port")
        # keep a copy of the lines we ar interested in
        if port_result & bioc.InputMask.epd:
            self.Epd_state = True
        else:
            self.Epd_state = False
        if port_result & bioc.InputMask.battery_full:
            self.Chg_full_state = True
        else:
            self.Chg_full_state = False
        if port_result & bioc.InputMask.ps4_off:
            self.ps4_on = False
        else:
            self.ps4_on = True

        return port_result

    ###################################
    # Set ports on an expander
    ###################################
    def set_port(self, expander_name, u16_value):
        """set all the ouptuts of an expander in one go(name, 16-bit value)"""
        dbg_wrprint("Entry:set_port(%s, 0x%x)" % (expander_name, u16_value))
        assert self.initialised, "module not initialised"
        port_value = u16_value
        exp_found = False
        i2c_addr = 0

        # Find the expander in our map
        for ix in range(len(bioc.dict_i2c_devs)):
            if (expander_name == bioc.dict_i2c_devs[ix][Devix.sw_name] and
                    bioc.dict_i2c_devs[ix][Devix.part] == bioc.ChipNum.gpio_exp and
                    bioc.dict_i2c_devs[ix][Devix.fun] == 'out'):
                exp_found = True
                i2c_addr = bioc.dict_i2c_devs[ix][Devix.addr]
                break
        assert exp_found, "expander not found"
        # Are we writing to the chip with the LED?
        if expander_name == bioc.LEDs.expander:
            # print("Led expander match")
            # take advantage of this call to update
            # the LEDs depending on what we last read
            # from the inputs was
            # clear out the LEDs first
            port_value = port_value & bioc.LEDsMask.all_of
            if self.Epd_state and not self.Chg_full_state:
                port_value = port_value | bioc.LEDsMask.green_on  # green for charger plugged in
            elif self.Epd_state and not self.Chg_full_state:
                port_value = port_value | bioc.LEDsMask.blue_on  # blue when the charger is plugged in and battery is full
            else:
                port_value = port_value | bioc.LEDsMask.red_on  # red for charger unplugged

        high_byte = (port_value >> 8) & 0xFF
        low_byte = (port_value & 0xFF)
        if fake_rpi_gpio:
            return

        dbg_wrprint("port write 0x%x 0x%x 0x%x" % (i2c_addr, high_byte, low_byte))
        # now write it to the hardware
        self.gpio.WrPorts(i2c_addr, high_byte, low_byte)
        # update the cached value
        for ix in range(len(gpio_cache)):
            if i2c_addr == gpio_cache[ix][Cix.addr]:
                gpio_cache[ix][Cix.value] = port_value
                break

        dbg_wrprint("Exit set_port 0x%x" % port_value)
        return

    ###################################
    # Read from an expander input pin
    # returns:
    # 1 for a high state, 0 for a low state
    ###################################
    def get_pin(self, pin_name):
        """read from an input pin(pin to read from)"""
        dbg_rdprint("Entry get_pin(%s)" % pin_name)
        assert self.initialised, "module not initialised"
        if fake_rpi_gpio:
            return 0
        stix = bioc.PIN_MAP_INPUTS_START_INDEX
        pin_index = 0
        pin_found = False

        # find the pin in our map, here we cheat and start looking where the inputs reside
        for ix in range(stix, len(hal_pin_map)):
            if (pin_name == hal_pin_map[ix][Pix.sw_name] or
                    pin_name == hal_pin_map[ix][Pix.sch_name]):
                pin_index = ix
                pin_found = True
                break
        assert pin_found, "pin is not a gpio expander input"
        port_data = self.rd_port()

        pin_value = port_data & hal_pin_map[pin_index][Pix.mask]
        result = 1
        if pin_value == 0:
            result = 0
        dbg_rdprint("Exit get_pin=%d" % result)
        return result

    ###################################
    # write to an expander output pin
    ###################################
    def set_pin(self, pin_name, value_01):
        """write to an output pin(pin to set, true/false value)"""
        dbg_wrprint("Entry set_pin(%s, %r)" %(pin_name, value_01))
        assert self.initialised, "module not initialised"
        i2c_addr = 0
        port_value = 0
        pin_index = 0
        port_index = 0
        pin_found = False
        port_val_found = False

        # figure out which expander this pin belongs to
        for ix in range(len(hal_pin_map)):
            # check pin name against sw reference and schematic name
            if (pin_name == hal_pin_map[ix][Pix.sw_name] or
                    pin_name == hal_pin_map[ix][Pix.sch_name]):
                # ok we have a match, grab the address
                i2c_addr = hal_pin_map[ix][Pix.addr]
                pin_index = ix
                pin_found = True
                break
        assert(i2c_addr != 0), ("pin %s unknown" % pin_name)
        # fetch the cached value
        for ix in range(len(gpio_cache)):
            if i2c_addr == gpio_cache[ix][Cix.addr]:
                port_value = gpio_cache[ix][Cix.value]
                port_index = ix
                port_val_found = True
                break
        assert port_val_found, "pin is not a gpio expander output"
        if port_val_found and pin_found:
            dbg_wrprint("index %d pin %s mask 0x%x" % (pin_index, pin_name, hal_pin_map[pin_index][Pix.mask] ))
            # ok mask in the value
            if value_01:
                port_value = (port_value | hal_pin_map[pin_index][Pix.mask]) & 0xFFFF
            else:
                port_value = (port_value & (~hal_pin_map[pin_index][Pix.mask])) & 0xFFFF
            # save it to cache
            gpio_cache[port_index][Cix.value] = port_value
            high_byte = (port_value >> 8) & 0xFF
            low_byte  = port_value & 0xFF

            dbg_wrprint("Writing to port @ address 0x%x [0x%x, 0x%x]" %(i2c_addr, high_byte, low_byte))
            if fake_rpi_gpio:
                return port_value
            # now write it to the hardware
            self.gpio.WrPorts(i2c_addr, high_byte, low_byte)
        dbg_wrprint("Exit set_pin 0x%x" % port_value)
        return port_value # return the cached value of the port

    #################################################################
    # API Target Hardware Helpers
    #################################################################

    # Pressure sensor 015PD
    # Pmin = -15 psi
    # Pmax = 15 psi
    # Vsupply = self.rd_5vQrail()
    # https://datasheet.octopart.com/SSCMDRN001PDAA5-Honeywell-datasheet-13028835.pdf
    # V = [(0.8 x Vsupply)/(Pmax - Pmin)] * (Papplied - Pmin) + (0.1 * Vsupply)
    # P = [Vadc - (0.1 * Vsupply)]
    def psi(self, pin_name):
        assert self.initialised, "module not initialised"
        pmin = -15.0
        pmax = 15.0

        # scaling fixed by resistive potential divider on ADC inputs
        scaling = { 'PSv': 2.0, 'PS+': 2.0, 'PS-': 2.0 }

        voltage = scaling[pin_name] * self.adc_pinread(pin_name)

        # Conversion: see Honeywell datasheet SSC series pressure sensors
        voltage = (7.5 * voltage) - 18.75

        return voltage

    #################################
    # read the Battery voltage
    #################################
    def rd_vbatt(self):
        assert self.initialised, "module not initialised"
        # Vbat --> r1 --> adc --> r2 --> GND
        r1 = 12.0
        r2 = 1.8
        # V = Vadc(r1+r2)/r2
        vadc = self.adc_pinread('VBATT')
        voltage = (vadc * (r1 + r2))/r2
        return voltage

    #################################
    # read the 5V quiet rail voltage
    #################################
    def rd_5vQrail(self):
        assert self.initialised, "module not initialised"
        # Vbat --> r1 --> adc --> r2 --> GND
        r1 = 20.0
        r2 = 10.0
        # V = Vadc(r1+r2)/r2
        vadc = self.adc_pinread('5VQ')
        voltage = (vadc * (r1 + r2))/r2
        return voltage

    #################################
    # read the -3V3 rail voltage
    #################################
    def rd_minus3V3rail(self):
        assert self.initialised, "module not initialised"
        # Vbat --> r1 --> adc --> r2 --> GND
        r1 = 5.0
        r2 = 10.0
        # V = Vadc(r1+r2)/r2
        vadc = self.adc_pinread('-3V3')
        print("vadc = %f" % vadc)
        voltage = (vadc * (r1 + r2))/r2
        return voltage

    #################################
    # read the 5V rail voltage
    #################################
    def rd_5vrail(self):
        assert self.initialised, "module not initialised"
        # Vbat --> r1 --> adc --> r2 --> GND
        r1 = 20.0
        r2 = 10.0
        # V = Vadc(r1+r2)/r2
        vadc = self.adc_pinread('5V')
        voltage = (vadc * (r1 + r2))/r2
        return voltage

    #################################
    # read the voltage from an ADC pin
    #################################
    def adc_pinread(self, pin_name):
        assert self.initialised, "module not initialised"

        voltage = self.ADC_FAIL_RETURN # backward compatibility ADC_FAIL_RETURN value
        i2c_addr = 0
        channel  = 0
        pin_found = False
        # look for the channel for the pin
        for ix in range(len(bioc.dict_adc_channel)):
            if pin_name == bioc.dict_adc_channel[ix][Adcix.name]:
                i2c_addr = bioc.dict_adc_channel[ix][Adcix.addr]
                channel = bioc.dict_adc_channel[ix][Adcix.channel]
                pin_found = True
                break

        assert pin_found, ("ADC pin unrecognised %s" %pin_found)
        if pin_found:
            # returns tuple (voltage, status of read)
            vstat = self.adc.adc_read_channel(i2c_addr, channel)
            # read the status part of the tuple
            if vstat[1]:
                # the reading is kosher
                # grab the voltage part of the tuple
                voltage = vstat[0]

        return voltage

    #################################
    # Returns the string setting for
    # the current selected working
    # electrode
    #################################
    def get_we(self):
        assert self.we_selected is not None, "WE has not been set"
        return dict_mux_address[self.we_selected][Muxix.name]

    #################################
    # Switch to desired working electrode
    #################################
    def switch_we(self, we_name):
        assert self.initialised, "module not initialised"
        #########################################
        # our mux switch configuration
        #                A4   A3   A2   A1   A0   decimal
        # WE1            0    0    0    0    0      0
        # WE2            0    0    0    0    1      1
        # WE3            0    0    0    1    0      2
        # WE4            0    0    0    1    1      3
        # WE5            0    0    1    0    0      4
        # WE6            0    0    1    0    1      5
        # WE7            0    0    1    1    0      6
        # WE8            0    0    1    1    1      7
        # WE9            0    1    0    0    0      8
        # WE10           0    1    0    0    1      9
        #
        # dummy          1    1    1    1    1     31
        #########################################
        # A1 -U1_GPA1, 0x0200
        # A2 -U1_GPA2, 0x0400
        # A3 -U1_GPA3, 0x0800
        # A4 -U1_GPA4, 0x1000
        # mask = 0x1F00
        electrode_found = False
        mux_mask = 0x1F00
        addr = 0
        mux_cache_index = 0 # FIXME make this a look up value
        for ix in range(len(dict_mux_address)):
            if we_name == dict_mux_address[ix][Muxix.name]:
                # we found a match, fetch the address
                addr = dict_mux_address[ix][Muxix.addr]
                electrode_found = True
                self.we_selected = ix
                break
        assert electrode_found, ("Unknown electrode %s" % we_name)
        # fetch the cache value of the port we are just about to write to
        port_val = gpio_cache[mux_cache_index][Cix.value]
        # clear the mux address bits
        port_val &= ~(mux_mask)
        # now OR in the value for the electrode
        dbg_muxprint("writing to mux 0x%x" % port_val)
        port_val |= addr

        dbg_muxprint("mux address 0x%x" % addr)
        dbg_muxprint("port value 0x%x" % port_val)

        # save it back to the cache
        gpio_cache[mux_cache_index][Cix.value] = port_val
        # and shove it out to hardware
        dbg_muxprint("calling set port %s 0x%x" % (bioc.ChipName.mux, port_val))
        self.set_port(bioc.ChipName.mux, port_val)

    #################################
    # Toggle the switch on the PS4
    #################################
    def set_ps4(self, i_bEnable):
        """Switch the PS4 on or off(True=on, False=off)"""
        ps4_boot_time = 8  # in seconds
        assert self.initialised, "module not initialised"

        dbg_ps4print("Asking PS4 to switch %s" % ('ON' if i_bEnable else 'OFF'))

        if i_bEnable != self.get_ps4_on():
            self.set_pin('PS4', 0)  # simulate a button press
            time.sleep(2)
            self.set_pin('PS4', 1)
        else:
            dbg_ps4print("PS4 already %s" % ('ON' if i_bEnable else 'OFF'))
            return

        if on_dev_pc:
            dbg_ps4print("PS4 status is fake")
            return
        
        # now make sure that actually worked
        # wait up to 10 seconds for rails to come up
        for i in range(5):
            time.sleep(5)
            self.get_ps4_on()
            if i_bEnable == self.ps4_on:
                # if we are switching on then wait for it to actually boot
                if i_bEnable:
                    dbg_ps4print("Waiting for PS4 to boot up")
                    time.sleep(ps4_boot_time)
                return
        # failed
        assert False, "PS4 power no worky worky"


    #################################
    # Toggle the switch on the PS4
    #################################
    def init_ps4(self):
        # prime the ps4 line regardless
        self.set_pin('PS4', 1)
        time.sleep(3)
        # start with the ps4 off
        self.set_ps4(False)

    #################################
    # Test whether the PS4 is on
    #################################
    def get_ps4_on(self):
        """True if the PS4 is ON, False if the PS4 is OFF"""
        assert self.initialised, "module not initialised"

        # active low signal
        if self.get_pin('IS_PS4_PWR') == 1:
            self.ps4_on = False
            dbg_ps4print("PS4 is OFF")
        else:
            self.ps4_on = True
            dbg_ps4print("PS4 is ON")
        return self.ps4_on

    ################################################################
    # Helper function that reboots the PS4, for debug
    ################################################################
    def RebootPs4(self):
        assert self.initialised, "module not initialised"

        # first switch it OFF
        dbg_ps4print("RebootPs: switching off")
        dbg_ps4print("RebootPs: stop = %r" % self.stop)
        # first check if it is actually ON
        if self.get_ps4_on():
            self.set_ps4(False)   # power off the PS4
        # now wait for rails to collapse
        escape_hatch = 0
        while self.get_ps4_on():
            time.sleep(0.25)
            escape_hatch += 1
            if escape_hatch > 40:
                break

        # Now switch it back on
        self.set_ps4(True)
        # now ensure rails have come up
        escape_hatch = 0
        while not self.get_ps4_on():
            time.sleep(0.25)
            escape_hatch += 1
            if escape_hatch > 40:
                break

        if not self.get_ps4_on():
                print("################################# HW ERROR PS4 not powering ON")
        return

    #################################
    # Power off this system
    #################################
    def pow_off(self):
        assert self.initialised, "module not initialised"
        # switch off the motor
        self.set_motor(0)
        self.dc_motor.stop()
        print("Motor Off")
        # switch off the variable valve
        self.var_valve(0)
        self.var_valve.stop()
        print("Var valve Off")
        # switch off valves and pumps
        self.set_port(bioc.ChipName.valves, 0)
        print("Valves & Pumps Off")
        # switch off dump valve
        self.set_pin('DUMP', 0)
        print("Dump valve Off")
        # switch off the PS4
        if self.ps4_on():
            self.set_ps4(False)
            print("PS4 Off")
        else:
            print("PS4 already off")
        # we are now going to die
        print("setting POFF high")
# disabling this feature as it requires a hardware mod to function correctly
#        GPIO.setup(self.__SHUTDOWN_OUT__, GPIO.HIGH)
        print("Waiting for death...")

        return

    def get_chrg_error(self):
        """
        Read in the state of the charger error pin
        """
        assert self.initialised, "module not initialised"

        if self.get_pin('CHGERR'):
            return False
        else:
            return True


    ######################################################################
    # Test Helpers
    ######################################################################

    ###################################
    # switch each pin on then switch each
    # pin off on this device
    ###################################
    def __pin_test(self):
        # go through the U2 pins setting the output
        for ix in range (20,35):
            self.set_pin(bioc.dict_octet_bioc[ix][Pix.sw_name], 1)
            #self.rd_port()
            time.sleep(0.5)
            cmd = input("continue? y/n")
            print(cmd)

            # go through the U4 pins reading the inputs
            #for ix in range(38,50):
            #    self.get_pin(bioc.dict_octet_bioc[ix][Pix.sw_name])

            # toggle the output low
            self.set_pin(bioc.dict_octet_bioc[ix][Pix.sw_name], 0)
            #self.rd_port()
            time.sleep(0.5)
            # go through the U4 pins reading the inputs
            #for ix in range(38,50):
            #    self.get_pin(bioc.dict_octet_bioc[ix][Pix.sw_name])
            cmd = input("continue? y/n")
            print(cmd)

        return

    ###################################
    # write to an expander output pin
    ###################################
    def __loop_back_test(self):
        # self test
        # in DS test hardware I've wired up the outputs of U1 to the inputs of U4
        out_val = 0x55
        out_val2 = 0xAA

        while dbg_self_test:
            if dbg_pin_test:
                self.__pin_test()
            if dbg_loopback_test:
                self.gpio.WrPorts(self.__U1_out__, out_val, out_val2)
                time.sleep(0.01)
                rx_byte = self.gpio.RdPorts(self.__U4_in__)
                dbg_print("input ports: 0x%x 0x%x" % (rx_byte[0], rx_byte[1]))
                dbg_print("->:{0:b}".format(out_val))
                dbg_print("<-:{0:b}".format(rx_byte[0]))
                if out_val != rx_byte[0]:
                    input("press a key to continue...")
                if out_val2 != rx_byte[1]:
                    input("press a key to continue...")
                if len(rx_byte) != 2:
                    dbg_print("FAILED")
                    dbg_print(rx_byte)
                time.sleep(1)
                out_val += 1
                out_val2 += 1
                if out_val == 0x100:
                    out_val = 0
                if out_val2 == 0x100:
                    out_val2 = 0
            print("---------------------------------------------")
        return # actually we never return

# P1
#########################################
# our mux switch configuration
#                A4   A3   A2   A1   A0   decimal
# WE1            0    0    0    0    0      0
# WE2            0    0    0    0    1      1
# WE3            0    0    0    1    0      2
# WE4            0    0    0    1    1      3
# WE5            0    0    1    0    0      4
# WE6            0    0    1    0    1      5 DW
# WE7            0    0    1    1    0      6 DW
# WE8            0    0    1    1    1      7 DW
# WE9            0    1    0    0    0      8
# WE10           0    1    0    0    1      9
#
# dummy          1    1    1    1    1     31
#########################################
# A0 -U1_GPA0, 0x0100
# A1 -U1_GPA1, 0x0200
# A2 -U1_GPA2, 0x0400
# A3 -U1_GPA3, 0x0800
# A4 -U1_GPA4, 0x1000

# P2
#########################################
# our mux switch configuration
#                A4   A3   A2   A1   A0   decimal
# WE1            0    0    0    0    0      0
# WE2            0    0    0    0    1      1
# WE3            0    0    0    1    0      2
# WE4            0    0    0    1    1      3
# WE5            0    0    1    0    0      4
# WE6            0    1    0    0    0      8
# WE7            0    1    0    0    1      9
# WE8            0    1    0    1    0      10
# WE9            0    1    0    1    1      11
# WE10           0    1    1    0    0      12
#
# dummy          1    1    1    1    1     31
#########################################
# A0 -U1_GPA0, 0x0100
# A1 -U1_GPA1, 0x0200
# A2 -U1_GPA2, 0x0400
# A3 -U1_GPA3, 0x0800
# A4 -U1_GPA4, 0x1000
dict_mux_address = {
    # P2 Hardware
# index     name      address
    0  :    ( 'WE1'  ,     0x0000    ),
    1  :    ( 'WE2'  ,     0x0100    ),
    2  :    ( 'WE3'  ,     0x0200    ),
    3  :    ( 'WE4'  ,     0x0300    ),
    4  :    ( 'WE5'  ,     0x0400    ),
    5  :    ( 'WE6'  ,     0x0800    ),
    6  :    ( 'WE7'  ,     0x0900    ),
    7  :    ( 'WE8'  ,     0x0A00    ),
    8  :    ( 'WE9'  ,     0x0B00    ),
    9  :    ( 'WE10' ,     0x0C00    ),
    10 :    ( 'ST'   ,     0x1F00    ),
}

##
# Multiplexer
#
class Mux():
    minimum = 1
    maximum = 11    # 10 electrodes on the sensor + dummy on board

##
# Helper MUX dictionary index
#
class Muxix(): # dict_mux_address
    """dict_mux_address"""
    name = 0
    addr = 1


##
# Helper that keeps a cache of the last written
# values to the io
#
gpio_cache = {
    # index   I2C addr              portAB
    0:      [hal_i2c_addr.MCP23017_U1, 0x0000],
    1:      [hal_i2c_addr.MCP23017_U2, 0x0000],
    2:      [hal_i2c_addr.MCP23017_U3, 0x0000],
}


##
# Helper GPIO cache look up index
#
class Cix(): # gpio_cache
    """gpio_cache"""
    addr  = 0
    value = 1


##
#  dict_octet_bioc pin map look up index
#
# index  :    sw name,    schematic ID, OnRpi?, BCM, Function   I2C addr,    AB Port mask
#                0           1           2       3      4          5             6
# -------------------------------------------------------------------------------------------
class Pix(): # dict_octet_bioc
    """dict_octet_bioc"""
    sw_name  = 0
    sch_name = 1
    on_rpi   = 2
    bcm_no   = 3
    fun      = 4
    addr     = 5
    mask     = 6


##
# dict_i2c_devs I2C devices map look up index
#
# index :    part num    Function, I2C addr,               sw reference
#              0           1            2
class Devix():
    """dict_i2c_devs"""
    part    = 0
    fun     = 1
    addr    = 2
    sw_name = 3


##
# ADC map look up indices
#
#  # index   channel      i2c address         sw name

class Adcix():
    """dict_adc_channel"""
    channel = 0
    addr    = 1
    name    = 2


##
# Helper for Debug print
#
def dbg_muxprint(dbg_string):
    if dbg_mux:
        print(dbg_print)


def dbg_print(dbg_string):
    if dbg_hal:
        print(dbg_string)
    return


def dbg_rdprint(dbg_string):
    if dbg_inputs:
        print(dbg_string)
    return


def dbg_wrprint(dbg_string):
    if dbg_outputs:
        print(dbg_string)
    return


def dbg_vvprint(dbg_string):
    if dbg_v_valve:
        print(dbg_string)
    return


def dbg_ps4print(dbg_string):
    if dbg_ps4:
        print(dbg_string)
    return

def dbg_getvalue(max_num):
    if fake_rpi_gpio:
        return randint(0, max_num)
    else:
        return 0


# this is here so we only reference one instance of this module
octet_gpio = OctetHardwareAbstractionLayer()

if __name__ == '__main__':
    try:
        OctetHardwareAbstractionLayer()

        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        GPIO.cleanup()
        sys.exit(0)

