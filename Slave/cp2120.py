#!/usr/bin/env python3
#

# This is the pipe everything has to go through to get to the hardware

import common
fake_rpi_gpio = common.DebugFlags.on_dev_pc

if fake_rpi_gpio:
    import fake_RPi_GPIO as GPIO
else:
    import RPi.GPIO as GPIO

import time
import sys

from threading import Lock

# set to True to enable debug output, set to False to disable it
dbg_cp2120 = False


class BitBashed_CP2120_SpiDriver():

    # Rpi BCM assignments
    __MOSI__ = 10   # Rpi --> CP2120
    __MISO__ = 9    # Rpi <-- CP2120
    __SCLK__ = 11   # Rpi --> CP2120
    __CS__   = 8    # Rpi --> CP2120
    __INT__  = 16   # Rpi <-- CP2120

    # time constants for our delays
    __MICRO_S__ = 1/1000000.0

    # proportion of time we wait for the I2C transaction to complete
    __I2CWAIT__ = 1.5

    # SPI timing constants
    __TSE__   = 0.1     #0.085  # uS time between CS low and SCK low
    __TCKHL__ = 0.25    #0.2    # uS time the clock needs to pulse for


    def __init__(self):
        print("Initialising BitBashed_CP2120_SpiDriver")
        GPIO.setmode(GPIO.BCM)
        self.setup_Spi_Pins()
        self.enabled = False
        self.hwMutex = Lock()
        return


    ###################################
    # Activate this driver
    ###################################
    def EnableDrv(self):
        return self.__config_cp2120()

    ###################################
    # Write Bytes to I2C
    ###################################
    def WrI2C(self, i2c_addr, payload):
        """I2C write(i2c address, bytes payload)"""
        # If the SPI Master attempts to transmit a command to the CP2120
        # while the CP2120 is acting as the Master on the I2C bus, the
        # CP2120  will  suspend  I2C  bus  activity until the SPI Master
        # has completed transmission of the command. For instance, if the
        # SPI Master calls the Read Internal Register command while the
        # CP2120 is in the middle of an I2C transaction, that I2C
        # transaction will stall until the CP2120 completely processes the
        # Read Internal Register command.
        assert(self.enabled), "driver not enabled"
        dbg_print("Entry:WrI2C @ 0x%x" % i2c_addr)
        self.hwMutex.acquire(blocking=True, timeout=1)
        retries = 0     # we resend the packet if we timed out
        status = False
        # [write command, num of bytes, slave addr + W, data byte 1...data byte N]
        packet = [Cmds.WR_I2C, len(payload), self.__get_wra(i2c_addr)] + payload
        dbg_print(packet)
        while retries < 3:
            self.__xfer_Packet(packet,0)
            # we approximate the time for I2C to complete:
            # (bits to transmit)/(I2C bus Speed) + 20% tolerance
            # 1.2(payload * 8)/speed
            wait_time = (self.__I2CWAIT__ * float(len(packet)) * 8.0)/400000.0
            time.sleep(wait_time)
            status = self.__rd_status()
            if status:
                break
            retries += 1
        self.hwMutex.release()
        if not status and not fake_rpi_gpio:
            print("FAILED: WrI2C for chip at address 0x%x with payload:" % i2c_addr)
            print(payload)

        return status


    ###################################
    # Read Bytes from I2C
    ###################################
    def RdI2C(self, i2c_addr, rd_size):
        """Read bytes from I2C(I2C address, number of bytes to read)"""
        # If the SPI Master attempts to transmit a command to the CP2120
        # while the CP2120 is acting as the Master on the I2C bus, the
        # CP2120  will  suspend  I2C  bus  activity until the SPI Master
        # has completed transmission of the command. For instance, if the
        # SPI Master calls the Read Internal Register command while the
        # CP2120 is in the middle of an I2C transaction, that I2C
        # transaction will stall until the CP2120 completely processes the
        # Read Internal Register command.
        assert(self.enabled), "driver not enabled"
        results = []
        dbg_print("Entry:RdI2C(0x%x, %d)" % (i2c_addr, rd_size))
        self.hwMutex.acquire(blocking=True, timeout=1)
        # [Read command, number of bytes, slave address]
        packet = [Cmds.RD_I2C, rd_size, self.__get_rda(i2c_addr)]
        dbg_print(packet)
        self.__xfer_Packet(packet, 0)
        # wait for the device to respond
        # we approximate the time for I2C to complete:
        # (bits to transmit)/(I2C bus Speed) + 20% tolerance
        # 1.2(payload * 8)/speed
        wait_time = (self.__I2CWAIT__ * float(len(packet)) * 8.0)/400000.0
        # now set a timer that will clear our flag once this time has elapsed
        time.sleep(wait_time)
        retries = 0
        done = False
        while retries < 10:
            done = self.__rd_status()
            if done:
                break
            else:
                time.sleep(0.005)
                retries += 1
        if done:
            results = self.__rd_rxbuff()
        self.hwMutex.release()
        dbg_print("RdI2C results")
        dbg_print(results)
        dbg_print("Exit RdI2C")
        return results

    ###################################
    # Read after write
    ###################################
    def RdAfterWr(self, i2c_wr_addr, wr_payload, i2c_rd_addr, rd_size):
        """Do a read from one I2C device after writing to another I2C device
        (address of device to write to, data to write, address of device to
        read from, number of bytes to read"""
        # If the SPI Master attempts to transmit a command to the CP2120
        # while the CP2120 is acting as the Master on the I2C bus, the
        # CP2120  will  suspend  I2C  bus  activity until the SPI Master
        # has completed transmission of the command. For instance, if the
        # SPI Master calls the Read Internal Register command while the
        # CP2120 is in the middle of an I2C transaction, that I2C
        # transaction will stall until the CP2120 completely processes the
        # Read Internal Register command.
        assert (self.enabled), "driver not enabled"
        self.hwMutex.acquire(blocking=True, timeout=1)
        # [RD after WR command, number wr bytes, number rd bytes, WR slave addr, wr byte 0...n, rd_addr]
        packet =[Cmds.RdAfterWrI2C, len(wr_payload), rd_size, self.__get_wra(i2c_wr_addr)]
        packet += wr_payload
        packet.append(self.__get_rda(i2c_rd_addr))
        dbg_print(packet)
        self.__xfer_Packet(packet, 0)
        # wait for the device to respond
        # we approximate the time for I2C to complete:
        # (bits to transmit)/(I2C bus Speed) + 20% tolerance
        # 1.2(payload * 8)/speed
        wait_time = (self.__I2CWAIT__ * float(len(packet)) * 8.0)/400000.0
        # now set a timer that will clear our flag once this time has elapsed
        time.sleep(wait_time)
        retries = 0
        done = False
        while retries < 3:
            done = self.__rd_status()
            if done:
                break
            else:
                time.sleep(0.005)
                retries += 1
        results = []
        if done:
            results = self.__rd_rxbuff()
        self.hwMutex.release()
        dbg_print("RdAfterWr results")
        dbg_print(results)
        dbg_print("Exit RdAfterWr")
        return results


    ###################################
    # WR I2C address
    ###################################
    def __get_wra(self, i2c_addr):
        """convert I2C address to I2C Write address"""
        return (i2c_addr << 1)

    ###################################
    # RD I2C address
    ###################################
    def __get_rda(self, i2c_addr):
        """covert I2C address to I2C read address"""
        return ((i2c_addr << 1) + 1)

    ###################################
    # Configure the CP2120
    ###################################
    def __config_cp2120(self):
        """initialise the CP2120 SPI to I2C bridge"""
        self.enabled = True
        if fake_rpi_gpio:
            return True

        i2cclock_val = 0x05
        i2cto_val = 0x1F
        i2cto2_val = 0xFF
        status = True  # hope for the best

        # Byte orientation to MSB
        self.__setByteOrientation(True)

        # I2C clock frequency to the maximum speed 400K
        self.__wr_register(RegAddr.I2CCLOCK, i2cclock_val)

        # The transaction time-out counter, which
        # terminates an I2C transaction after a
        # set period of time has passed
        self.__wr_register(RegAddr.I2CTO, i2cto_val)

        # Setting  the  I2C Bus Free Detect enables
        # the device to poll the SMBus lines and
        # determine when a transfer can begin. Setting
        # the SCL Low Time Out detect will cause an
        # SMBus transaction to abort if the SCL line
        # has been held low by a device for a period
        # of approximately 25 ms
        self.__wr_register(RegAddr.I2CTO2, i2cto2_val)

        # comms sanity check, read the revision
        # number of the CP2120
        self.__get_revision()
        sanity_cnt = 0
        # and print the contents of the registers
        result = self.__rd_register(RegAddr.I2CCLOCK)
        if result[0] != i2cclock_val:
            dbg_print("INIT ERROR: I2C CLOCK")
        else:
            sanity_cnt += 1
        result = self.__rd_register(RegAddr.I2CTO)
        if result[0] != i2cto_val:
            dbg_print("INIT ERROR: I2CTO")
        else:
            sanity_cnt += 1
        result = self.__rd_register(RegAddr.I2CTO2)
        if result[0] != i2cto2_val:
            dbg_print("INIT ERROR: I2CTO2")
        else:
            sanity_cnt += 1
        if sanity_cnt == 3:
            print("CP2120 is happy")
        else:
            print("ERROR: CP2120 is sulking")
            status = False

        return status

    ###################################
    # Configure and Initialise the pins
    # we will use for the SPI bus
    ###################################
    def setup_Spi_Pins(self):
        """ Set all pins as an output except MISO (Master Input, Slave Output)"""
        print("Initialising CP2120 pins")
        GPIO.setup(self.__MISO__, GPIO.IN)
        GPIO.setup(self.__MOSI__, GPIO.OUT)
        GPIO.setup(self.__SCLK__, GPIO.OUT)
        GPIO.setup(self.__CS__  , GPIO.OUT)
        GPIO.setup(self.__INT__ , GPIO.IN)


        # Datasheet section 5.
        # SCLK should be held high when idle
        self.release_Bus()
        return

    ###################################
    # send out a packet of bytes with an
    # optional read tacked on
    #####################################
    def __xfer_Packet(self, tx_bytes, num_rx_bytes):
        """send and receive (transmit payload, number of bytes to receive)"""
        assert(self.enabled), "driver not enabled"
        rd_packet = []

        self.__prime_Bus()

        # do we need to send?
        if len(tx_bytes) != 0:
            for send_byte in range(0, len(tx_bytes), 1):
                self.__tx_Bits(tx_bytes[send_byte])

        # do we need to read?
        if num_rx_bytes != 0:
            for rx_byte in range(0, num_rx_bytes, 1):
                rd_packet.append(self.__rx_Bits())

        self.release_Bus()

        return rd_packet

    ###################################
    # set our bus lines ready for a
    # transaction
    ###################################
    def __prime_Bus(self):
        """SCLK high (idle), CS low (asserted)"""
        GPIO.output(self.__SCLK__, GPIO.HIGH)   # SCLK high
        GPIO.output(self.__CS__  , GPIO.LOW )   # CS low, asserted
        # wait TSE time before driving the first clock cycle
        time.sleep(self.__TSE__ * self.__MICRO_S__)
        return

    ###################################
    # Release the bus, set the SCLK in
    # idle and de-assert the chip select
    ###################################
    def release_Bus(self):
        """SCLK high (idle) CS high (de-asserted)"""
        GPIO.output(self.__CS__  , GPIO.HIGH)
        GPIO.output(self.__SCLK__, GPIO.HIGH)
        return

    ###################################
    # Transmit bytes out the bus bit by
    # bit according to the timing diagram
    # for the CP2120
    ###################################
    def __tx_Bits(self, byte_to_send):
        """transfers a byte of data """
        assert(self.enabled), "driver not enabled"

        byte_to_send = byte_to_send & 0xFF     # ensure we have an 8-bit value
        bits_in_byte = 8

        for bit in range(bits_in_byte):
            GPIO.output(self.__SCLK__, GPIO.HIGH)  # we start with the clock idle
            # now we load the data bit
            if byte_to_send & 0x80:
                bit_state = GPIO.HIGH
            else:
                bit_state = GPIO.LOW
            GPIO.output(self.__MOSI__, bit_state)
            time.sleep(self.__TCKHL__ * self.__MICRO_S__)
            GPIO.output(self.__SCLK__, GPIO.LOW)    # clock that in
            time.sleep(self.__TCKHL__ * self.__MICRO_S__)
            byte_to_send <<= 1                      # grab the next bit

        # when we are done, tidy up. Place the clock and MOSI in idle
        GPIO.output(self.__SCLK__, GPIO.HIGH)
        GPIO.output(self.__MOSI__, GPIO.HIGH)
        return

    ###################################
    # Receive a byte from the CP2120
    ###################################
    def __rx_Bits(self):
        """Clocks in a byte of data"""
        assert(self.enabled), "driver not enabled"

        rx_byte = 0
        bits_in_byte = 8

        GPIO.output(self.__SCLK__, GPIO.HIGH)  # we start with the clock idle
        for bit in range(bits_in_byte):
            GPIO.output(self.__SCLK__, GPIO.LOW)    # clock the bit in

            # grab the bit
            if GPIO.input(self.__MISO__) == GPIO.HIGH:
                rx_byte |= 1
            time.sleep(self.__TCKHL__ * self.__MICRO_S__)
            # move to the next slot
            rx_byte <<= 1
            GPIO.output(self.__SCLK__, GPIO.HIGH)   # clock back to idle
            time.sleep(self.__TCKHL__ * self.__MICRO_S__)

        # at this point we have shifted one too many times so shift back
        rx_byte >>= 1
        return rx_byte

    ###################################
    # Write to register
    # [write command, register, data byte]
    ###################################
    def __wr_register(self, register, data_byte):
        """do a CP2120 register write"""
        assert(self.enabled), "driver not enabled"

        write_packet = [Cmds.WrToReg, register, data_byte]
        self.__prime_Bus()
        self.__xfer_Packet(write_packet, 0)


    ###################################
    # Read from register
    # -> CP2120 [read command, register, X]
    # <- CP2120 [                           data byte]
    ###################################
    def __rd_register(self, register):
        """do a CP2120 register read"""
        assert(self.enabled), "driver not enabled"

        result = 0
        write_packet = [Cmds.RdFromReg, register, 0x00]
        self.__prime_Bus()
        result = self.__xfer_Packet(write_packet, 1)
        dbg_print(result)
        return result

    ###################################
    # Read the status
    # The CP2120 updates I2CSTAT when an I2C
    # transaction begins, when an I2C
    # transaction completes (successfully
    # or unsuccessfully), and when a received
    # SPI command contains errors.
    #
    # It is not recommended that an SPI master
    # poll the CP2120's I2CSTAT Internal Register
    # to determine when an I2C transaction has
    # completed. The SPI master should instead
    # watch for the INT pin to drop low, and then
    # read  the I2CSTAT register to determine
    # the I2C transaction results
    ###################################
    def __rd_status(self):
        """read the status of the CP2120"""
        assert(self.enabled), "driver not enabled"
        retries = 0
        result = False

        # attempt retries if it moans about being busy
        while retries < 3:
            escape_hatch = 0
            # check if if the INT line is low
            while GPIO.input(self.__INT__) == GPIO.HIGH:
                time.sleep(0.01)
                escape_hatch += 1
                if escape_hatch > 500:
                    break

            if escape_hatch > 10:
                dbg_print("escaped after %d" % escape_hatch)

            if GPIO.input(self.__INT__) == GPIO.LOW:
                status = self.__rd_register(RegAddr.I2CSTAT)

                if status[0] == StatusReg.Done:
                    if retries != 0:
                        print("ACK")
                    dbg_print("ACK")
                    result = True
                    break
                elif status[0] == StatusReg.Busy:
                    print("Busy")
                    time.sleep(0.01)
                else:
                    if status[0] == StatusReg.Reset_Value:
                        dbg_print("Reset value")
                    elif status[0] == StatusReg.Addr_NACK:
                        print("Addr NACK")
                    elif status[0] == StatusReg.Data_NACK:
                        print("Data NACK")
                    elif status[0] == StatusReg.Buff_Error:
                        print("Buffer size mismatch")
                    elif status[0] == StatusReg.I2CTO_TO:
                        print("Timeout")
                    elif status[0] == StatusReg.SCL_TO:
                        print("SCL Low timeout")
                    elif status[0] == StatusReg.NOT_FREE:
                        print("bus is not free")
                    else:
                        # force an assert to track down the bug
                        assert (status[0] == StatusReg.Done), ("Unhandled status value 0x%x" % status[0])
                    break
            else:
                if dbg_cp2120:
                    print("INT line still high")
            retries += 1


        return result


    ###################################
    # RX Buffer read
    ###################################
    def __rd_rxbuff(self):
        """read the bytes in the RD_BUFF of the cp2120"""
        assert(self.enabled), "driver not enabled"
        dbg_print("Entry: __rd_rxbuff")
        rx_data = []
        bytes_rxed = self.__rd_register(RegAddr.RXBUFF)
        dbg_print("numb of bytes read")
        dbg_print(bytes_rxed)
        if bytes_rxed[0] != 0:
            wr_packet = [Cmds.RD_RXBUFF, 0x00]
            rx_data = self.__xfer_Packet(wr_packet, bytes_rxed[0])
            dbg_print("buffer data")
            dbg_print(rx_data)
        dbg_print("Exit: __rd_rxbuff")
        return rx_data


    ###################################
    # Set byte orientation
    ###################################
    def __setByteOrientation(self, b_msb_first):
        """Configure the endian on the cp2120"""
        assert(self.enabled), "driver not enabled"
        if b_msb_first:
            spi_cfg = ByteOrCfg.MSB_First
        else:
            spi_cfg = ByteOrCfg.LSB_First

        write_packet = [Cmds.ByteOrientation, spi_cfg]
        self.__prime_Bus()
        self.__xfer_Packet(write_packet,0)
        self.release_Bus()
        return

    ###################################
    # Read the chip's revision number
    ###################################
    def __get_revision(self):
        """read the revision of silicon of the cp2120"""
        assert(self.enabled), "driver not enabled"
        write_packet = [Cmds.Revision, 0x00]
        self.__prime_Bus()
        revision = self.__xfer_Packet(write_packet,2)
        self.release_Bus()
        dbg_print(revision)
        return

###################################
# Debug print
###################################
def dbg_print(dbg_string):
    if dbg_cp2120:
        print(dbg_string)
    return


class RegAddr():
    IOCONFIG = 0x00
    IOSTATE  = 0x01
    I2CCLOCK = 0x02
    I2CTO    = 0x03
    I2CSTAT  = 0x04
    I2CADR   = 0x05
    RXBUFF   = 0x06
    IOCONFIG2= 0x07
    EDGEINT  = 0x08
    I2CTO2   = 0x09

class Cmds():
    WR_I2C          = 0x00
    RD_I2C          = 0x01
    RdAfterWrI2C    = 0x02
    RD_RXBUFF       = 0x06
    WrToReg         = 0x20
    RdFromReg       = 0x21
    ByteOrientation = 0x18
    Revision        = 0x40

class ByteOrCfg():
    MSB_First = 0x81
    LSB_First = 0x42

class StatusReg():
    Reset_Value = 0x00
    Done        = 0xF0
    Addr_NACK   = 0xF1
    Data_NACK   = 0xF2
    Busy        = 0xF3
    I2CTO_TO    = 0xF8
    Buff_Error  = 0xF9
    SCL_TO      = 0xFA
    NOT_FREE    = 0xFB


# this is here so we only reference one instance of this module
octet_i2c = BitBashed_CP2120_SpiDriver()


if __name__ == '__main__':
    try:
        cp2120 = octet_i2c
        cp2120.EnableDrv()

        while True:
            time.sleep(5)
    except KeyboardInterrupt:
        GPIO.cleanup()
        sys.exit(0)
