################################################
# A class for applying curve fitting and parameter 
# extraction to an EISData object, assuming a 
# Randles equivalent circuit.
# Author: Zahid Ansari
################################################
import numpy as np
from collections import OrderedDict
from randles_impedance import impedance_model
from curve_fit import ComplexCurveFit
from scipy import interpolate


class FitRandles(ComplexCurveFit):
    """
    A class for applying curve fitting and parameter extraction to an EISData
    object, assuming a Randles equivalent circuit.

    Since it is designed for embedding, it will not abort in case of an error.
    Error messages and warnings are returned as dictionaries.

    Arguments required for initialisation are defined below.

    Attributes:
    - max_fit_freq: maximum frequency used for fitting
    - Z: The complex impedance data points from the input EIS object
    - F: The frequency data points from the input EIS object
    - fit_F: The subset of F used for fitting
    - fit_Z: The sunset of Z used for fitting
    - peak_F: the frequency of the detected peak in -ImZ
    - peak_Z: the complex impedance at the point the detected peak in -ImZ
    - fit_results: An ordered dictionary with fit input parameters,
        fit output values, and meta data from EIS if available.
        This is designed to be usable for output in a table on a GUI and
        for comparing results across fitting runs.
    - warnings: A dictionary with warnings
    - errors: A dictionary with error messages
    - success: A boolean - True if fitting worked
    """

    def __init__(self,
                 eis: object,
                 decades_below_peak: float,
                 decades_above_peak: float,
                 downsample: int,
                 Aw_guess: float = 5.0,
                 maxiter: int = 1000,
                 neg_threshold: float = 0.01,
                 z_noise_threshold: float=0.002,
                 max_allowed_pct_err=10     # was 20
                 ):
        """
        :param eis: An object of class EISData
        :param decades_below_peak: Maximum frequency to be used for fitting
                    in decades below the frequency of the peak in negative ImZ
        :param decades_above_peak: Maximum frequency to be used for fitting
                    in decades above frequency of the detected peak
        :param downsample: Downsample F and Z before fitting
                    integer N will result in using every Nth point
        :param Aw_guess: Guess of Aw to be passed to curve fit.
                    Default value of 5.0 works well.
        :param neg_threshold: Allow ReZ and ImZ to have small value with
                    the wrong sign. If values with larger than neg_threshold
                    times the mean have the wrong sign, then there is something
                    wrong with the Z data and curve fitting is not applied.
        :param z_noise_threshold: Values of ImZ less than z_noise_threshold
                    times the max on ImZ are treated as zero for the purpose
                    of peak detection to avoid tripping over noise.
                    This prevents getting false peaks when the point of
                    inflection gets very low.
        :param maxiter: Maximum number of iterations for the mimimisation
            method used in curve fitting
            This has a default value of 1000 to ensure backward compatibility
            with code that was written for earlier versions before maxiter
            was implemented
        :param max_allowed_pct_err: Max allowed %Err for fit to be a success
        """
        try:
            min_pts_needed = 5
            Z = eis.Z
            F = eis.F

            self.errors = {}
            self.warnings = {}
            self.max_allowed_pct_err = max_allowed_pct_err

            # Allow small positive values of ImZ and small negative values
            # of ReZ to allow for noise.
            re_z_limit = -(neg_threshold * np.abs(np.real(Z).mean()))
            if np.real(Z).min() < re_z_limit:
                msg = 'Negative real part of Z for {}'.format(eis.test_id)
                self.errors['Fit Error'] = msg

            im_z_limit = (neg_threshold * np.abs(np.imag(Z).mean()))
            if np.imag(Z).max() > im_z_limit:
                msg = 'Positive imaginary part of Z for {}'.format(eis.test_id)
                self.errors['Fit Error'] = msg

            self.Z = Z
            self.F = F
            self.decades_below_peak = decades_below_peak
            self.decades_above_peak = decades_above_peak

            # Get the subset of F and Z for fitting based on input parameters
            self._set_fit_F_and_Z_attr(
                F=F, Z=Z,
                decades_above_peak=decades_above_peak,
                decades_below_peak=decades_below_peak,
                downsample=downsample,
                min_pts_needed=min_pts_needed,
                z_noise_threshold=z_noise_threshold
            )

            if self.errors == {}:

                # Create the guess and bounds ordered dictionaries
                guess_dict = self._create_guess_dict(Aw_guess)
                bounds_dict = self._create_bounds_dict()

                ComplexCurveFit.__init__(self, impedance_model,
                                         self.fit_F,
                                         self.fit_Z,
                                         guess_dict,
                                         bounds_dict,
                                         maxiter)

                # Created arrays if fitted_Z for plotting
                self.fitted_Z = self._get_fitted_curve(self.F)

                # Created arrays if fitted_Z for plotting
                self._create_fit_results_dict(eis)

                # Create and interpolated curve for plotting a smoother curve
                self.interp_F = self._interp1d(self.F)
                self.interp_fitted_Z = self._get_fitted_curve(self.interp_F)

                self._check_for_curve_fit_errors()
                self.success = self._no_errors()
            else:
                self.success = False

        except Exception as e:
            self.errors = {'Fit Error': str(e)}
            self.success = False

    ###########################################################################
    # Private helper methods
    ###########################################################################


    def _check_for_curve_fit_errors(self):
        """
        Sets self.errors with messages if any of the tests fails
        """

        # Test if max pct error is acceptable
        if self._pct_err() > self.max_allowed_pct_err:
            error_string = '{} > {}'.format(self._pct_err(), self.max_allowed_pct_err)
            self.errors['Max Pct Error'] = error_string
        if self.fit_results['Rct'] < 0:
            error_string = '{:.2f}'.format(self.fit_results['Rct'])
            self.errors['Negative Rct Error'] = error_string

    def _no_errors(self):
        """
        Returns True if the error dictionary is empty
        """
        return self.errors == {}

    def _set_peak_attr(self, F, Z, z_noise_threshold,
                       Fmax_for_peak=1000.0):
        """
        Returns the index of the peak of the semicircle.
        A maximum in -ImZ should be defined by a rising zero crossing
        in the first derivative. In some cases there is not a peak in -ImZ.
        The lowest frequency point of inflection of the 1st derivative is
        found by locating the lowest frequency falling zero crossing in the
        2nd derivative.
        If there is not falling zero cross found, then the point of
        inflection is found using the peak in the second derivative.
        The frequency associated with the peak or point of inflection
        needs to be below Fmax_for_peak.
        Values less than z_noise_threshold * max(negImZ) are set to
        zero to avoid detecting peaks in low ImZ values.
        If not, it is assumed that the peak is at the end of the trace.
        """
        negImZ = -np.imag(Z).copy()
        # Set values below a percentage of the mean to zero to
        # avoid tripping over noise
        z_limit = negImZ.max() * z_noise_threshold
        negImZ[negImZ < z_limit] = z_limit

        d1_peak_index = last_zero_cross(differentiate(negImZ, 1),
                                        'rising')

        d2_peak_index = last_zero_cross(differentiate(negImZ, 2),
                                        'falling')

        if F[d1_peak_index] < Fmax_for_peak:
            peak_index = d1_peak_index
            peak_detect = 'Peak'
        elif F[d2_peak_index] < Fmax_for_peak:
            peak_index = d2_peak_index
            peak_detect = 'Inflection'
        else:  # Use the last point
            peak_index = F.size - 1
            peak_detect = 'End'

        self.peak_F = F[peak_index]
        self.peak_Z = Z[peak_index]
        self.peak_detect = peak_detect

    def _set_fit_F_and_Z_attr(self, F, Z,
                              decades_above_peak,
                              decades_below_peak,
                              downsample,
                              z_noise_threshold,
                              min_pts_needed):
        """
        Creates subsets of the F and Z arrays to be used for fitting.
        Frequencies above max_fit_freq are exlcuded.
        The peak is found in data for the subset of frequencies.
        max_fit_freq is calculated in decades above the detected peak.
        min_fit_freq is calculated in decades below the detected peak.
        The arrays fit_F and fit_Z cover the range from max_fit_freq to
        min_fit_freq downsampled by the downsample ratio.
        Sets the attributes peak_F, peak_Z, fit_F, fit_Z
        """
        try:
            # Downsample needs to be an int since it is used for indexing
            downsample = int(downsample)

            # Don't allow negative values for decades_below_peak
            decades_below_peak = np.max([0.0, decades_below_peak])

            # Don't allow negative values for decades_above_peak
            decades_above_peak = np.max([0.0, decades_above_peak])

            # Get the peak for the subset wit frequencies >= max_fit_freq
            self._set_peak_attr(F, Z, z_noise_threshold=z_noise_threshold)

            # Calculate the max_fit_frequency relative to the peak_F
            max_fit_freq = self.peak_F * (10 ** decades_above_peak)
            self.max_fit_freq = max_fit_freq

            # Calculate the min_fit_frequency relative to the peak_F
            min_fit_freq = self.peak_F / (10 ** decades_below_peak)

            # Create subsets that meet min and max frequency criteria
            self.fit_F = F[np.where((F >= min_fit_freq) &
                           (F <= max_fit_freq))][::downsample]
            self.fit_Z = Z[np.where((F >= min_fit_freq) &
                           (F <= max_fit_freq))][::downsample]
            if self.fit_F.size < min_pts_needed:
                msg = ('Not enough points in array')
                self.errors['fit_subset'] = msg

        except Exception as e:
            self.fit_F = F
            self.fit_Z = Z
            self.errors['fit_subset'] = '{}'.format(e)

    def _Rs_estimate(self, Z):
        """
        The value of Rs is estimated as the lowest value of ReZ
        :param Z: Complex impedance
        :return: Estimate of Rs
        """
        return np.real(Z).min() / 1.0e3

    def _Rct_estimate(self, Zpeak, Rs):
        """
        Rct is estimated as being twice the real part of Zpeak - Rs.
        :param Z:
        :return:
        """
        return 2.0 * (np.real(Zpeak) - Rs) / 1.0e3

    def _Cdl_estimate(self, Z, F, min_cdl=1.0e-8):
        """
        Estimates the value of Cdl by assuming that Warburg component is
        negligible at the point of maximum frequency.
        Limit frequency to 10KHz because very small ImZ at higher frequency
        results in significant error.
        This will slightly overestimate Cdl if on the curve but
        provides an estimate to use as a guess for curve fitting.
        :param Z: Impedance array
        :param F: Frequency array
        :return: Estimate of Cdl
        """
        max_F_ind = np.where(F <= 10000)[0][0]
        top_F = F[max_F_ind]
        Z1 = Z[max_F_ind]
        ImZ1 = -np.imag(Z1)
        estimate = 1 / (2 * np.pi * top_F * ImZ1)
        return np.max([estimate, min_cdl]) * 1.0e9

    def _pct_err(self):
        """
        Returns the mean percentage error of measured v. fitted points.
        """
        include = self.F <= self.max_fit_freq
        csq = (np.abs(self.fitted_Z[include] - self.Z[include]) /
               np.abs(self.Z[include]))
        errs = 100.0 * np.mean(csq)
        return errs

    def _pct_err_circ(self, Rct):
        """
        Returns the mean percentage error of fit measured v. fitted points
        for freqeunch less than the max fit frequency and
        with values of ReZ less than Rct.
        This is designed to give a measure of quality of fit for the
        semicircle in the Nyquist plot.
        """
        indices = np.where(np.logical_and(
            np.real(self.Z) < 1000.0 * Rct,
            self.F <= self.max_fit_freq)
        )[0]
        if len(indices) > 0:
            subset_fit = self.fitted_Z[indices]
            subset_z = self.Z[indices]
        else:
            subset_fit = self.fitted_Z
            subset_z = self.Z
        csq = np.abs(subset_fit - subset_z) / np.abs(subset_z)
        errs = 100.0 * np.mean(csq)
        return errs

    def _create_guess_dict(self, Aw_guess):
        """
        Returns a dictionary with starting guesses of Randles parmeters
        """
        Rs_guess = self._Rs_estimate(self.Z)
        Rct_guess = self._Rct_estimate(self.peak_Z, Rs_guess)
        Cdl_guess = self._Cdl_estimate(self.fit_Z, self.fit_F)
        phi_guess = 0.98
        guess_dict = OrderedDict((
            ('Rs', Rs_guess),
            ('Cdl', Cdl_guess),
            ('phi', phi_guess),
            ('Rct', Rct_guess),
            ('Aw', Aw_guess)
        ))
        return guess_dict

    def _create_bounds_dict(self):
        """
        Returns the bounds dictionary to constrain parameter values for fit
        """
        return OrderedDict((
            ('Rs', (0.01, 100)),
            ('Cdl', (1, 10000)),
            ('phi', (0.85, 1.1)),
            ('Rct', (0.1, 10000)),
            ('Aw', (1, 50))
        ))

    def _create_fit_results_dict(self, eis):
        """
        Create a fit_results ordered dictionary
        This stores input and fit results in a format that
        allows it to be output as a table with controlled
        ordering of the columns
        :param eis: EIS Object from which test_id and meta_data are used
        """
        self.fit_results['Dec Below'] = self.decades_below_peak
        self.fit_results['Dec Above'] = self.decades_above_peak
        self.fit_results['Fmin'] = self.fit_F.min()
        self.fit_results['Fpeak'] = self.peak_F
        self.fit_results['Fmax'] = self.fit_F.max()
        self.fit_results['Peak'] = self.peak_detect
        self.fit_results['%Err'] = self._pct_err()
        self.fit_results['%Err UptoRct'] = self._pct_err_circ(
            self.fit_results['Rct'])
        self.fit_results['ID'] = eis.test_id
        meta_keys = list(eis.meta_data.keys())
        if 'Date and time' in meta_keys:
            self.fit_results['Date'] = eis.meta_data['Date and time']
        else:
            self.fit_results['Date'] = None
        if 'Title' in meta_keys:
            self.fit_results['Title'] = eis.meta_data['Title']
        else:
            self.fit_results['Title'] = ''
        if 'Notes' in meta_keys:
            self.fit_results['Notes'] = eis.meta_data['Notes']
        else:
            self.fit_results['Notes'] = ''
        for k in ['%Err UptoRct', '%Err', 'Notes', 'Title', 'Date', 'ID']:
            self.fit_results.move_to_end(k, last=False)

    def _get_fitted_curve(self, F):
        """
        Calculated the fitted impedance for all frequencies
        :return:
        """
        argnames = ['Rs', 'Rct', 'Cdl', 'phi', 'Aw']
        # Interpolate the frequency to get a smooth curve
        kwargs = {'F': F}
        for arg in argnames:
            kwargs[arg] = self.fit_results[arg]
        Z = impedance_model(**kwargs)
        return Z

    def _interp1d(self, y, npoints=200):
        xmax = float(y.shape[0])
        x = np.arange(0, xmax, 1.0)
        f = interpolate.interp1d(x, y)
        step_size = xmax / npoints
        xstop = xmax - 1
        xnew = np.arange(0.0, xstop, step_size)  # A finer grain X
        ynew = f(xnew)  # Y interpolated
        return ynew


def differentiate(arr, n):
    """
    Returns nth discrete differentiation of arr, shifted right to have the same
    length as the input array.
    """
    diff = np.zeros_like(arr)
    diff[n:] = np.diff(arr, n=n)
    return diff


def last_zero_cross(arr, direction, noise_threshold=1.0):
    """
    Finds the index of the last zero cross in the direction in the array.
    If no zero_crossings are detected then 0 is returned.
    """
    if direction == 'rising':
        delta_sign = -2
    elif direction == 'falling':
        delta_sign = 2
    else:
        raise ValueError('zero_cross: direction must be "rising" or "falling"')
    sign_of_arr = np.sign(arr)
    change_in_sign = np.diff(sign_of_arr)
    # Find all zero crosses where slope goes from positive to negative
    zero_crosses = np.where(
        np.logical_and((np.abs(arr[:-1]) > noise_threshold),
                       (change_in_sign == delta_sign)))[0]
    if zero_crosses.size > 0:
        return zero_crosses[-1]
    else:
        return 0
