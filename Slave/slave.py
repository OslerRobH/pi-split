##
# @package slave
#
# Main entry to the API
#
# Sets up slave and manages the GUI, i2c used for commands/status reads by master.
# Relies on workers/modules implementing the agreed interfaces:
#
# https://oslerdiagnostics.planio.com/projects/platform/wiki/RPi_API_Interfaces
#
# Each worker is treated as an isolated module that carries out a specific
# unit of work.
#
# Examples of workers are:
#
# Motor control - cartridge_motor.py
#
# Fluidics - fluidics.py
#
# Multiplexer - ps_mux.py
#
# Impedance  - eis_measure.py
#
# StepX are classes for the various gui states
#
# Each class includes the following methods:
#
# initialise() - initialise the screen.           Called once when step starts
#
# process()    - any processing, animation etc    Called repeatedly in loop
#
# cleanup()    - any cleanup required             Called once when step finishes
#
# NOTES
#
# There is an 'Implicit' state machine here, defined by transitions and calls
# embedded in each stage.
#
# This module could be improved by refactoring as explicit state machine at future date
#
# Text for prompt buttons NOT obtained using API calls, since many of supplied
# graphics screens have appropriate text within the screen image.
# Where button text is needed it is hard coded.
#
# Our kivy specific ids for this GUI:
#
# fl - our overall float layout
#
# background_image - typically the static image
#
# Big_button - typically the entire screen
#
#
# deya.sanchez@gmail.com
# robert.heavey@gmail.com
#
import config_sequence as cfgseq    # workers and order of execution
import sys                          # reading stdout from subprocess calls
import os                           # so we can create an id tag file
import common as cms                # project-wide flags
import hal
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout

from kivy.clock import Clock

from kivy.uix.image import Image
import time
from transition_image import TransitionImage
import datetime
import project_defaults as project_defaults

from slave_Receive import slave_Receive

fake_rpi_gpio = cms.DebugFlags.on_dev_pc

if fake_rpi_gpio:
    import fake_RPi_GPIO as GPIO
else:
    import RPi.GPIO as GPIO

##
# Enables debug for this module
#
dbg_spam_enable = True             # Set to true to enable debug prints for this module

dud_mode = cms.DebugFlags.dud_mode



##
# Introduction face images
# Rotate through face images
#
# Entry: On program invocation
#
# Exit:  Once the hardware initialisation has finished
class Step1(object):

    def initialise(parent):
        ##
        # set the second, full image cycle and
        # image transition counters to zero
        #
        Step1.tick = 0  # tick - second counter
        Step1.loop = 0  # loop - number of times we cycle through all the images
        Step1.image_number = 0  # image_number - image transition counter
        return

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        pass
        return

    def process(parent):
        ##
        # Cycle through a number of images after a number of seconds
        # set by cycle_time_s, we are allowed to stop this process once
        # the hardware has been initialised
        #
        # Assets:
        # 5 - guy with beard
        # 6 - grey haired guy
        # 7 - lady
        # 8 - wee girl
        #
        img = {0: 'Asset 5', 1: 'Asset 6', 2: 'Asset 7', 3: 'Asset 8'}
        cycle_time_s = 2    # how often do we change the image
        snapshot_t = int(time.time())
        if Step1.tick != snapshot_t:
            Step1.tick = snapshot_t

            # Speed set here. Granularity is in seconds
            if Step1.tick % cycle_time_s == 0:
                # check if our worker has finished so we can move to the next state
                # otherwise keep cycling through the images
                # we know that our worker has finished because it will prompt us for
                # a yes or no answer
                if parent.current_state == PsCtrlState.AskYN:
                    # worker is done, so move to the next state
                    Step1.loop = 0
                    parent.step = GuiDemoStates.start_test
                else:
                    # keep cycling through the images
                    img_filename = img.get(Step1.image_number, 0) # fetch the filename
                    parent.ids.background_image.source = cms.DemoImgDefs.path + img_filename + cms.DemoImgDefs.ext
                    Step1.image_number += 1
                    if Step1.image_number == len(img):
                        Step1.image_number = 0
                        Step1.loop += 1
            else:
                time.sleep(0.25)    # don't hog the CPU
        time.sleep(0.25)    # don't hog the CPU
        return


##
# Start button
# PROMPT for Start
# Load Startup image.
#
# Entry: After cycling through faces
#
# Exit:  BUTTON pressed
class Step2(object):

    def initialise(parent):
        ##
        # Set the background image and the start button area
        #
        Step2.initialised = 0
        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 65' + cms.DemoImgDefs.ext
        parent.ids.Big_Button.color = (0, 1, 0, 0)
        parent.ids.Big_Button.font_size = 40
        parent.ButtonClear()  # flush out previous key press
        return

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        parent.ids.Big_Button.text = ''
        pass
        return

    def process(parent):
        ##
        # Wait for the user to press the button, this button has no
        # action or callback attached to it. We do not wait for a button
        # press if we have been told to automatically advance this state
        # then we don't wait for a press (typically set for debugging)
        ##

        if parent.ButtonPressed() or cms.DebugFlags.auto_advance:
            parent.TestPressed()
            parent.step = GuiDemoStates.start_test   # move to the next state
        else:
            time.sleep(0.25)    # don't hog the CPU
        return


##
# Cartridge animation and prompt
# for cartridge to be inserted. Animation is the cartridge
# with arrows pointing up
#
# Entry: After the static face image with the start button
#
# Exit:  BUTTON pressed.
class Step3(object):
    ##
    # Set the background image for this step
    #

    def initialise(parent):
        ##
        # Set up the torso image with the cartridge that will have
        # the blinking triangles/arrows
        #
        Step3.cartridgeImage = Image()
        Step3.icount = 1
        Step3.placeholder_count = 0

        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 20' + cms.DemoImgDefs.ext
        Step3.cartridgeImage.pos = (0, 0)
        Step3.cartridgeImage.size_hint = (1, 1)
        Step3.cartridgeImage.source = cms.DemoImgDefs.path + 'Asset 10-arrows-1' + cms.DemoImgDefs.ext
        Step3.cartridgeImage.allow_stretch = (True)
        Step3.cartridgeImage.keep_ratio = (False)
        parent.ids.fl.add_widget(Step3.cartridgeImage)

        Step3.clock = Clock.schedule_interval(Step3.arrows_callback, 0.4)
        parent.ButtonClear()  # flush out previous key press

    def arrows_callback(self):
        ##
        # Blink the arrows for the cartridge image
        #
        Step3.cartridgeImage.source = cms.DemoImgDefs.path + 'Asset 10-arrows-' + str(Step3.icount) + cms.DemoImgDefs.ext
        if Step3.icount == 4:
            Step3.icount = 1
        else:
            Step3.icount += 1

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        parent.ids.fl.remove_widget(Step3.cartridgeImage)
        parent.ids.Big_Button.text = ''
        Step3.icount = 1

    def process(parent):
        ##
        # Look out for a key press unless we are debugging and the
        # auto advance flag is set.
        # Feedback to our current worker and say that we may continue
        #
        if parent.ButtonPressed() or cms.DebugFlags.auto_advance:
            Step3.icount = 1
            # finally we can reply to the prompt of the worker that
            # dropped us into this state
            parent.HaveYesReply()
            parent.step = GuiDemoStates.blood_flow


##
# Blood flow animation
# Transition of blood flow into torso with pink box
#
# Entry after the cartridge animation
#
# Exit automatic, unless the pogo error check is enabled
class Step4(object):

    def initialise(parent):
        ##
        # Setup the image overlay
        #
        Step4.pogo_error = False
        Step4.finished = False
        if parent.current_step < 4:
            Step4.ti = TransitionImage().run(Step4.animate_callback, parent)
            parent.ids.fl.add_widget(Step4.ti)
        parent.ButtonClear()  # flush out previous key press

    def animate_callback(parent):
        parent.ids.fl.remove_widget(Step4.ti)
        Step4.finished = True

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        parent.ids.Big_Button.text = ''

    def process(parent):
        ##
        # During this animation our cartridge worker engages
        # the cartridge, bursts the capsules and tests whether we
        # have loop connection between the sensor and the pogo pins.
        # The cartridge worker lets us know if there are any errors,
        # for debug we can choose to ignore these and proceed to the
        # next stage
        #
        if Step4.finished:
            # check to see if we are handling pogo errors
            if cms.DebugFlags.ignore_pogo_fault:
                # wait for the cartridge to engage and check the loop detect
                if parent.current_state == PsCtrlState.AskYN:
                    parent.step = GuiDemoStates.anim_sample
            else:
                # look out for pogo errors
                if not Step4.pogo_error:
                    if parent.our_worker.get_IsError():
                        wk_print("    BADDY   Loop error")
                        Step4.pogo_error = True
                        # clear the error and get them to shove another cartridge in
                        parent.our_worker.set_ClearError()
                    elif parent.current_state == PsCtrlState.AskYN:
                        parent.step = GuiDemoStates.anim_sample
                else:
                    if parent.current_state == PsCtrlState.AskYN:
                        # clear the error before we exit
                        Step4.pogo_error = False
                        parent.step = GuiDemoStates.ins_cartridge
        else:
            time.sleep(0.01)   # slug this down a wee bit


##
# Add sample.
# Animation prompting user to add the blood sample
# Moves upwards from bottom.
#
# Entry: After the cartridge has been engaged.
#
# Exit: Once the animation has finished
class Step5(object):

    def initialise(parent):
        ##
        # Set up the background image and the image that will
        # be the animated
        Step5.tick_count = 0
        Step5.x = -400
        Step5.y = 0
        Step5.addBloodImage = Image()

        # torso with blood image
        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 21' + cms.DemoImgDefs.ext

        Step5.addBloodImage.pos = (0, Step5.x)
        Step5.addBloodImage.size_hint = (1, 1)
        # big pink box with the add sample dot
        Step5.addBloodImage.source = cms.DemoImgDefs.path + 'Asset 33' + cms.DemoImgDefs.ext
        Step5.addBloodImage.allow_stretch = (True)
        Step5.addBloodImage.keep_ratio = (False)

        parent.ids.fl.add_widget(Step5.addBloodImage)

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        parent.ids.fl.remove_widget(Step5.addBloodImage)

    def process(parent):
        ##
        # moves the pink box into position from bottom to centre
        #
        Step5.tick_count += 1
        if Step5.tick_count % 1 == 0:
            Step5.tick_count = 0

            if Step5.x <= 0:
                Step5.x += 4
                Step5.addBloodImage.pos = (0, Step5.x)
            else:
                if parent.current_state == PsCtrlState.AskYN:
                    parent.step = GuiDemoStates.add_sample
        else:
            time.sleep(0.25)    # don't hog the CPU


##
# Waits for the user to confirm that the sample
# has been added to the cartridge
#
# Enter: After the pink box prompt animation has
# finished and is in the centre of the screen
#
# Exit: Once the user taps on the screen
class Step6(object):
    def initialise(parent):
        ##
        # Sets the big pink box in the centre of the screen waiting
        # for the user to tap the screen
        #
        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 15' + cms.DemoImgDefs.ext
        parent.ButtonClear()  # flush out previous key press

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        pass

    def process(parent):
        ##
        # wait for the button (screen) to be pressed
        #
        if parent.ButtonPressed() or cms.DebugFlags.auto_advance:
            wk_print("Got button on step 6")
            if parent.current_state == PsCtrlState.AskYN:
                parent.HaveYesReply()  # send a reply to the worker with the prompt
                parent.step = GuiDemoStates.do_dots

        else:
            time.sleep(0.25)    # don't hog the CPU


##
# Dot animation, i.e. Please wait
#
# Enter: once the user has confirmed that the sample
# has been added
#
# Exit: once the test has completed or there is an
# error or the test is aborted (by tapping the screen).
class Step7(object):

    def initialise(parent):
        ##
        # background image is a pink box full of white dots
        #
        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 16' + cms.DemoImgDefs.ext
        Step7.rows = 13
        Step7.columns = 6
        Step7.total_dots = Step7.rows * Step7.columns
        Step7.time_per_dot = int(cfgseq.test_time / Step7.total_dots)  # was 1024
        wk_print("estimated total time %d time per dot %d" % (cfgseq.test_time, Step7.time_per_dot))

        Step7.result_check = 1  # how often do we check to see if the result is ready
        Step7.tick_count = 0
        Step7.x_start = 0
        Step7.y_start = 0
        Step7.x = 0
        Step7.y = 0
        Step7.direction = 1

        Step7.dot_counter = 0  # Set to the number of dots
        Step7.dots = []

        Step7.x_start = 605
        Step7.y_start = 107
        Step7.x = 0
        Step7.y = 0
        Step7.direction = 1
        Step7.dot_counter = 0
        Step7.x = Step7.x_start
        Step7.y = Step7.y_start
        Step7.tick_count = 0

        Step7.user_aborted = False
        parent.ButtonClear()  # flush out previous key press

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        pass

    def process(parent):
        ##
        # Monitor any presses so we can bail out early and
        # abort the measurement. In the meantime we delete
        # dots on the screen
        #
        if parent.ButtonPressed():
            wk_print("Got button on step 7")
            wk_print("go through the motions of stopping")
            parent.StopNreset() # tell all the workers to stop
            parent.cancel_pressed = True # flag to our parent that we are stopping
            Step7.user_aborted = True
            # Show the next screen to avoid 'flash' when the last dots are removed
            parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 66' + cms.DemoImgDefs.ext
            parent.step = GuiDemoStates.show_result

        elif not Step7.user_aborted:
            time_snapshot = int(time.time())

            if Step7.tick_count != time_snapshot:
                Step7.tick_count = time_snapshot

                # check if results are ready
                if parent.gui_api_result() or parent.FatalError:
                    Step7.clearDots(parent)
                    parent.step = GuiDemoStates.show_result
                    # Show the next screen to avoid 'flash' when the last dots are removed
                    if parent.passed == TestOutcome.high:  # POSITIVE
                        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Result High' + cms.DemoImgDefs.ext
                    elif parent.passed == TestOutcome.low:  # NEGATIVE
                        parent.ids.background_image.source = cms.DemoImgDefs.path + 'Result Low' + cms.DemoImgDefs.ext
                    elif parent.passed == TestOutcome.failed:  # FAIL
                       parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 66' + cms.DemoImgDefs.ext

                elif Step7.tick_count % Step7.time_per_dot == 0:
                    # progress the screen by one dot...

                    if Step7.dot_counter == Step7.total_dots:
                        # got to end of dot screen
                        Step7.clearDots(parent)
                        Step7.x_start = 605
                        Step7.y_start = 107
                        Step7.x = 0
                        Step7.y = 0
                        Step7.direction = 1
                        Step7.dot_counter = 0
                        Step7.x = Step7.x_start
                        Step7.y = Step7.y_start
                        Step7.tick_count = 0
                    else:
                        dot = Image()
                        Step7.dots.append(dot)
                        dot.source = cms.DemoImgDefs.path + 'dot' + cms.DemoImgDefs.ext
                        dot.pos = (Step7.y, Step7.x)

                        Step7.dot_counter += 1
                        if Step7.dot_counter % Step7.rows == 0:
                            Step7.y += 48
                            Step7.direction = 1 - Step7.direction
                        else:
                            if Step7.direction == 1:
                                Step7.x -= 41
                            else:
                                Step7.x += 41

                        dot.size_hint = (.06, .06)
                        parent.ids.fl.add_widget(dot)

        time.sleep(0.20)    # don't hog the CPU

    def clearDots(obj):
        for n in Step7.dots:
            obj.ids.fl.remove_widget(n)

        Step7.dots.clear()
        del Step7.dots[:]


#
# Display the results High, Low or Fail
#
# Entry: After the tests have completed
#
# Exit: Once the user presses the button
#
class Step8():
    def initialise(parent):
        parent.ButtonClear()
        if parent.passed == TestOutcome.high:  # POSITIVE
            parent.ids.background_image.source = cms.DemoImgDefs.path + 'Result High' + cms.DemoImgDefs.ext
        elif parent.passed == TestOutcome.low:  # NEGATIVE
            wk_print('Yay!')
            parent.ids.background_image.source = cms.DemoImgDefs.path + 'Result Low' + cms.DemoImgDefs.ext
        elif parent.passed == TestOutcome.failed:  # FAIL
            parent.ids.background_image.source = cms.DemoImgDefs.path + 'Asset 66' + cms.DemoImgDefs.ext
        parent.passed = TestOutcome.unknown # reset result for next run
        wk_print(("Start time: %s" % parent.test_time_start))
        wk_print(("End time  : %s" % parent.test_time_end))
        parent.FatalError = False

    def cleanup(parent):
        ##
        # deal with any house keeping here
        #
        pass

    def process(parent):
        # we have got to the end. Wait for a press to start again
        if parent.ButtonPressed():
            if parent.current_state == PsCtrlState.AskYN or (parent.current_state == PsCtrlState.Init and parent.FatalError == False) :
                wk_print("going back to the start")
                parent.step = GuiDemoStates.start_test
        else:
            time.sleep(0.25)    # don't hog the CPU


##
# Worker State machine and housekeeping
###############################################
class WorkerSm(BoxLayout):
    # NEWGUI states defining Classes step_x to cycle through
    pstep = {
        'step_1': Step1,  # faces
        'step_2': Step2,  # start button
        'step_3': Step3,  # torso cartridge animation and prompt
        'step_4': Step4,  # blood flow animation
        'step_5': Step5,  # add sample animation
        'step_6': Step6,  # add sample prompt
        'step_7': Step7,  # dots ... please wait a very long time
        'step_8': Step8,  # display high, low or fail screen
        }
    current_step = 0  # Track where gui is

    def __init__(self, ):
        super(WorkerSm, self).__init__()
        wk_print("WorkerSm init called")
        self.current_state = PsCtrlState.Init  # our current logic state as opposed to our gui state
        self.step = GuiDemoStates.faces      # step keeps track of gui state
        self.FatalError = False
        self.init_vars()
        self.passed = TestOutcome.unknown
        self.button_pressed = self.last_pressed = False
        return

    ##########################
    # initialise our variables
    ##########################
    def init_vars(self):
        wk_print("WorkerSm init_vars called")
        self.current_state = PsCtrlState.Init  # our current state
        self.passed = TestOutcome.unknown
        self.button_pressed = self.last_pressed = False
        self.sequence_started = False  # True once the Go/Test button gets pressed
        self.worker_to_run = 0  # the worker count
        self.prev_worker = 0    # previous worker, keeps track of when we change workers
        self.first_worker = 0   # first valid worker (i.e. we have code for it)
        self.replied = False
        self.HaveStartedOnce = False  # ensures we only initialise once
        self.shuttingDown = False     # ensures we only go through shut down motions once
        self.maxWorkers = 0
        self.WaitForStop = False
        self.ui_update_event = 0  # scheduled event to update the screen
        self.shutdown_event = 0   # scheduled event to start shutting down
        self.deadwait_event = 0   # scheduled event to wait for the workers to die for shutdown
        self.pulplug_event = 0    # scheduled event that yanks the plug
        self.cancel_pressed = False
        # initialise our system state machine sequence
        self.get_SysStateStats()

        # kick off main state machine
        # Moved. Used to be kicked off by a button press but we need to kick off
        # immediately
        # self.GoPressed()
        self.StartSequence()

        return

    ################################################################
    # check for I2C received commands
    ################################################################
    # this took 10 commands to happen 01/05/2019 RLH
    def CheckForI2CCommand(self):
        wk_print("CheckForI2CCommand start slave_Receive")
        changed = False
        SR = slave_Receive()
        result = SR.get_Message()
        command = SR.get_Command()
        Value1 = SR.get_Value1()
        Value2 = SR.get_Value2()
        if result == True:
            print ("command = [%.2X]" % (command))

            if command == project_defaults.COMMAND_MESSAGE_TYPE:
                print("COMMAND_MESSAGE_TYPE change state to %.2X" % (Value1))
                SR.set_Status(Value1) # reset status

                # if we are here the first byte is matched as 0xE5 (set state to new state)
                # decode the command Value bytes
                if Value1 == 0x00 and Value2 == 0x00:
                    # restart slave
                    print("Restart Slave")
                    self.__init__()
    
                elif Value1 == 0x01 and Value2 == 0x00:
                    # show faces - this is the outcome of restart slave
                    self.step = GuiDemoStates.faces
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x02 and Value2 == 0x00:
                    # show start
                    self.step = GuiDemoStates.start_test
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x03 and Value2 == 0x00:
                    # show cartridge
                    self.step = GuiDemoStates.ins_cartridge
                    self.GuiSwitchState()
                    changed = True
    
                elif Value1 == 0x04 and Value2 == 0x00:
                    # show Blood Flow
                    self.step = GuiDemoStates.blood_flow
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x05 and Value2 == 0x00:
                    # show Add Sample
                    self.step = GuiDemoStates.anim_sample
                    self.GuiSwitchState()
                    changed = True
    
                elif Value1 == 0x06 and Value2 == 0x00:
                    # show Confirm Sample
                    self.step = GuiDemoStates.add_sample
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x07 and Value2 == 0x00:
                    # show Dot Animation
                    self.step = GuiDemoStates.do_dots
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x08 and Value2 == 0x02:
                    # show Results High
                    self.step = GuiDemoStates.show_result
                    self.GuiSwitchState()
                    changed = True
    
                elif Value1 == 0x08 and Value2 == 0x01:
                    # show Results Low
                    self.step = GuiDemoStates.show_result
                    self.GuiSwitchState()
                    changed = True
     
                elif Value1 == 0x08 and Value2 == 0x00:
                    # show Results Fail
                    self.step = GuiDemoStates.show_result
                    self.GuiSwitchState()
                    changed = True
                
                else:
                    print("NOT processed Command = %d, Value1 = %d, Value2 = %d" 
                        % (Command, Value1, Value2))    
            
            elif command == project_defaults.STATUS_MESSAGE_TYPE: 
                print("STATUS_MESSAGE_TYPE self.step = %.2X" % (self.step))
                SR.set_Status(self.step) 
    
            elif command == project_defaults.STATE_MESSAGE_TYPE: 
                print ("STATE_MESSAGE_TYPE button_pressed = %.2X" % (self.button_pressed))
                SR.set_Status(self.button_pressed) 
 
        else:
            # print ("Nothing received from I2C")
        
            
            if self.button_pressed != self.last_pressed: 
                self.last_pressed = self.button_pressed
                print ("self.button_pressed = %.2X" % (self.button_pressed))

        # wk_print("CheckForI2CCommand end slave_Receive")

        return

    ################################################################
    # Update the GUI
    ################################################################
    def UpdateUiStatus(self, dt):

        self.CheckForI2CCommand()
        time.sleep(1)

        #wk_print("RLH not convinced we ever hit this state")

        try:
            self.GuiSwitchState()   # make sure the GUI can change state

            # check for the quit event
            if ( (self.current_state == PsCtrlState.EndGame) and
                    self.WaitForStop and self.Get_IsWorkerPaused(self.worker_to_run) ):
                self.WaitForStop = False
                self.BailOut()
                return True  # don't do anything until the user has pressed the Go button

            if self.Get_IsWorkerRunning(self.worker_to_run) and self.sequence_started:

                # look out for the abort state
                if ( self.Get_IsWorkerRunning(self.worker_to_run)) and (self.Get_AreAllCancelled() ):
                    wk_print("not convinced we ever hit this condition")
                    self.StartShutDown()
                    return True
                else:

                    # look out for the prompt state so we update our state accordingly
                    if self.our_worker.get_PromptActive():
                        if not self.replied:
                            self.StateUpdate(PsCtrlState.AskYN)
                    elif self.replied:
                        self.replied = False
                    # also look out for the error state
                    if self.our_worker.get_IsError():
                        if self.Get_WkStrId(self.worker_to_run) == 'CV' or self.Get_WkStrId(self.worker_to_run) == 'EIS':
                            # we are pretty screwed here, so bail out with a fail
                            self.HandleFatalError()
                        else:
                            self.StateUpdate(PsCtrlState.Error)

                    # Keep this state machine ticking
                    # --------------------------------
                    # check if our worker has finished, if it has then start the next one
                    if (self.shuttingDown == False) and (self.cancel_pressed == False):
                        self.Check_NextWorker()  
                    # --------------------------------


            if self.WaitForStop and self.Get_AreWorkersPaused():
                print("All Workers paused")
                self.WaitForStop = False
                # now set the current worker back to the first one
                self.worker_to_run = self.first_worker
            return True
        except KeyboardInterrupt:
            wk_print("Ctrl C pressed")
            wk_print("stopping workers...")
            self.StopAllWorkers()
            # no go ahead and cancel this event
            Clock.unschedule(self.ui_update_event)

            our_hardware = hal.octet_gpio
            our_hardware.de_initialise()
            GPIO.cleanup()
            sys.stdout.flush()
            exit(1)

    ################################################################
    #  Demo GUI - additional functions added
    ################################################################
    def GuiSwitchState(self):

        if self.step != WorkerSm.current_step:
            wk_print (("GuiSwitchState() Switch step %d -> %d" %(WorkerSm.current_step, self.step)))
            self.ButtonClear()
            if WorkerSm.current_step:
                # cleanup previous step
                self.pstep['step_' + str(WorkerSm.current_step)].cleanup(self)

            # grab the start and end time
            if WorkerSm.current_step == GuiDemoStates.add_sample:
                self.test_time_start = datetime.datetime.utcnow()
                self.MakeExpIdTag(self.test_time_start)
            if WorkerSm.current_step == GuiDemoStates.do_dots:
                self.test_time_end = datetime.datetime.utcnow()

            # initialise new step
            self.pstep['step_' + str(self.step)].initialise(self)

            WorkerSm.current_step = self.step

        # process current step
        self.pstep['step_' + str(self.step)].process(self)

    def ButtonClear(self):
        # Single button system. Flush button status
        self.button_pressed = False

    def ButtonPressed(self):
        # return button state
        if self.button_pressed:
            self.ButtonClear()
            return (True)
        return (False)

    def BigButtonPressedCallback(self):
        # Kivy callback function for button press or(on pc) mouse click
        wk_print("x")  # debug
        # print("self.button_pressed = %.2X" % (self.button_pressed))
        self.button_pressed = True
        # print("self.button_pressed = %.2X" % (self.button_pressed))

    ################################################################
    # Start the very first worker in the sequence
    # and set-up a scheduler for the screen update
    ################################################################
    def StartSequence(self):
        # don't do anything until the worker has actually stopped
        if self.WaitForStop:
            return
        self.sequence_started = True
        self.replied = False
        self.StateUpdate(PsCtrlState.Started)  # update our state
        # clear any previous cancel presses
        self.cancel_pressed = False

        # get the measurement going
        wk_print("StartSequence")
        self.Set_WkRunning(self.worker_to_run)
        # make sure we only set the scheduler once per boot
        if not self.HaveStartedOnce:
            self.ui_update_event = Clock.schedule_interval(self.UpdateUiStatus, 0.02)
            self.HaveStartedOnce = True
        return

    ########################################################################
    #
    #           GUI Helpers and housekeeping
    #
    ########################################################################

    ##
    # We are here because we can't
    # complete this test, so lets bail out
    # early and nicely typically this will
    # require some attention
    #####################################
    def HandleFatalError(self):
        # start by clearing the error
        self.our_worker.set_ClearError()
        # then ask everyone to stop
        self.cancel_pressed = True
        self.StopAllWorkers()
        # and tell the gui to display the failed state
        self.FatalError = True
        self.passed = TestOutcome.failed
        # go back to the start
        self.current_state = PsCtrlState.Init


    #####################################
    # We've started the test
    #####################################
    def TestPressed(self):
        if self.current_state == PsCtrlState.Init:  # Start
            self.StartSequence()
            wk_print("START pressed")


    #####################################
    # Try and clear the error and move on
    #####################################
    def ResetPressed(self):
        if self.current_state == PsCtrlState.Error:  # Reset
            self.cancel_pressed = False
            self.our_worker.set_ClearError()
            self.StateUpdate(PsCtrlState.Started)
            wk_print("Clearing the error")

    #####################################
    # They've hit the brakes
    #####################################
    def HaltPressed(self):
        if ((self.current_state == PsCtrlState.Started) or  # Stop
                (self.current_state == PsCtrlState.Stop)):  # Stop
            self.cancel_pressed = True
            self.StopNreset()
            wk_print("STOP pressed")

    #####################################
    # Handle the quit condition
    #####################################
    def QuitPressed(self):
        if ((self.current_state == PsCtrlState.Init) or  # Quit
                (self.current_state == PsCtrlState.Started) or  # Quit
                (self.current_state == PsCtrlState.Stop) or  # Quit
                (self.current_state == PsCtrlState.Error)):  # Quit
            wk_print("QUIT pressed")
            if not self.cancel_pressed:
                self.cancel_pressed = True
                self.StopNreset()
            self.StartShutDown()
        elif self.current_state == PsCtrlState.EndGame:  # 5 Bye
            wk_print("quit bye")

    #####################################
    # Handle the Reply from the Gui
    #####################################
    def HaveNoReply(self):
        if self.current_state == PsCtrlState.AskYN:  # 2 No
            wk_print("NO pressed")
            self.our_worker.set_Reply(False)
            self.replied = True
            self.StateUpdate(PsCtrlState.Init)

    #####################################
    # Handle the Reply from the Gui
    #####################################
    def HaveYesReply(self):
        if self.current_state == PsCtrlState.AskYN:
            self.our_worker.set_Reply(True)
            self.replied = True
            self.StateUpdate(PsCtrlState.Stop)

    ######################################
    # Test whether we have a result for
    # this test
    ######################################
    def gui_api_result(self):

        # API call to check if finished and high/low/fail results available
        # from current worker's status message
        if dud_mode:
            wk_print("TEST FINISHED")
            self.passed = TestOutcome.low   # lets be optimistic on demo mode
            return True
        else:
            statmsg = self.our_worker.get_StatusMsg()
            # debug_tag wk_print("Worker [%s] status %s " % (self.Get_WkStrId(self.worker_to_run), statmsg))

            if statmsg == 'POSITIVE':
                self.passed = TestOutcome.high
            elif statmsg == 'NEGATIVE':
                self.passed = TestOutcome.low
            elif statmsg == 'FAIL':
                self.passed = TestOutcome.failed

            if self.passed != TestOutcome.unknown:
                self.HaveYesReply()  # send a reply to the worker with the prompt
                return True
            else:
                return False

    ################################################################
    # Update GUI related stuff depending on our new state
    ################################################################
    def StateUpdate(self, new_state):
        if (new_state != self.current_state):
            wk_print("Gui [%s] ->[%s]" % (dict_strState[self.current_state], dict_strState[new_state]))
            self.current_state = new_state
        return

    #################################################################################
    # Shut Down Sequence Helpers
    #################################################################################

    #########################################
    # Call this first
    # Method to start shutting down
    #########################################
    def StartShutDown(self):
        if not self.shuttingDown:
            self.shuttingDown = True
            self.StateUpdate(PsCtrlState.EndGame)  # no more key presses allowed
            # if we never actually did anything then shortcut to exit
            if not self.HaveStartedOnce:
                self.PullThePlug(0)
            else:
                self.StopAllWorkers()
                # in case our current worker does not stop schedule switch
                # off regardless
                self.shutdown_event = Clock.schedule_once(self.ShuttingDown, 15)
        return

    #########################################
    # Scheduled event that executes in case
    # our current worker fails to stop.
    # This is scheduled by the StartShutDown
    # method
    #########################################
    def ShuttingDown(self, dt):
        self.BailOut()
        return False  # we should only need to do this once

    #########################################
    # Called once or worker has stopped.
    # May also be called by a shceduled
    # event ShuttingDown() if the worker
    # failed to stop
    # Method to kill all our workers
    # and schedule power off
    #########################################
    def BailOut(self):
        # if there is a schedule event to call us then cancel that now
        if (self.shutdown_event != 0):
            self.shutdown_event.cancel()
        self.Set_WkCancelAll()  # tell all our workers to Harakiri
        wk_print("cancelling screen updates")
        self.ui_update_event.cancel()
        # schedule a clock that shuts off regardless just in case
        # a worker hangs while trying to shutdown
        wk_print("Pulling the plug in 15 seconds..")
        self.pulplug_event = Clock.schedule_once(self.PullThePlug, 15)
        # now schedule shutting down nicely
        wk_print("wait for the death of workers")
        self.deadwait_event = Clock.schedule_interval(self.WaitForDeath, 0.5)
        return

    #########################################
    # Method that waits for all our workers
    # to die before yanking the plug
    #########################################
    def WaitForDeath(self, dt):
        workers_alive = 0
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.Get_WkExists(ix)):
                the_worker = self.Get_WkInstance(ix)
                if the_worker.is_alive():
                    workers_alive += 1
        if workers_alive == 0:
            wk_print("All workers are dead")
            # cancel the back up event
            self.pulplug_event.cancel()
            self.PullThePlug(0)
            return False  # this unschedules the event
        return True

    #########################################
    # Method that actually yanks the plug
    #########################################
    def PullThePlug(self, dt):
        wk_print("Pulling the plug")
        working_on_dev_pc = False
#        # so we don't accidentally switch off the PC while developing
#        if socket.gethostname() == 'obeeone':   # Deya's dev pc hostname
#        #if socket.gethostname() == 'ubuntu':  # Alan's dev pc VM hostname
#            wk_print("may the force be with you")
#            working_on_dev_pc = True
#        if working_on_dev_pc == False:  # anyone else will be powered off
#            bailoutcmd = "sudo /sbin/shutdown -h now"
#        if working_on_dev_pc == False:
#            subprocess.Popen(bailoutcmd.split())
#        # shouldn't get here
        GPIO.cleanup()
        quit()
        return False  # just to be tidy

    #################################################################################
    # Current Worker Helpers
    #################################################################################

    #########################################
    # Method to stop and reset all our workers
    #########################################
    def StopNreset(self):
        wk_print("StopNreset")
        self.StopAllWorkers()
        # we then go back to the start so we can re-start
        wk_print("go back to the start")
        self.StateUpdate(PsCtrlState.Init)
        self.step = GuiDemoStates.faces  # step keeps track of gui state

        # Alan: here you will need some way initialising your gui cleanly
        # and also a way to tell each individual step class that they need
        # to re-initialise

        return

    #################################################################################
    # System Control Helpers
    #################################################################################

    #####################################
    # Get our system state control stats
    # TODO: refactor so we don't have to
    # keep track of duplicates
    #####################################
    def get_SysStateStats(self):
        # determine the number of workers including duplicates
        self.maxWorkers = len(cfgseq.dict_sys_state_sequence)
        wk_print("num of workers in our sequence %d" % self.maxWorkers)

        # now create a matrix to keep track of live workers
        # worker index, instance, exists?, started?, running?
        self.WorkerLUT = {}
        # by default everything should be off until invoked
        # hence nothing is currently running
        wrunning = False
        # and nothing has been cancelled
        wcancelled = False
        # and everything is paused
        wpaused = True
        # scan through our System State Machine Worker Sequence to populate our LUT
        # entries will be populated as follows:
        #
        # index     = string ID of the worker
        # instance  = instantiation of this thread
        # exists    = true if it has been coded up
        # running   = true if it has been started
        # paused    = true if it has been started and currently paused
        # cancelled = true if it was started and then cancelled
        #
        #  ix   [index,       instance, wexists, wrunning, wpaused, wcancelled]
        # {0:   ['HwInit',    0,        False,   False,    True,    False     ],
        #  1:   ['Cartridge', 0,        False,   False,    True,    False     ],
        # etc...
        for ix in range(self.maxWorkers):
            index = cfgseq.dict_sys_state_sequence[ix]
            # debug_tag wk_print("index = %s\n" % index)
            # check if the worker has been cooked, otherwise don't attempt to run it
            if (cfgseq.dict_funcp_ssWorkers[index] == 'not_yet'):
                wexists = False
            else:
                wexists = True
            self.WorkerLUT[ix] = [index, 0, wexists, wrunning, wpaused, wcancelled]
        # now do a second pass to save instantiations
        for ix in range(self.maxWorkers):
            if (self.Get_WkExists(ix)) and (self.Get_WkInstance(ix) == 0):
                wk_instance = cfgseq.dict_funcp_ssWorkers[self.WorkerLUT[ix][Lix.id]].MainWorkerThread()
                self.Set_Instance(ix, wk_instance)
        # debug_tag wk_print(self.WorkerLUT)
        # Now set our first worker to the first existing one in our LUT
        for ix in range(self.maxWorkers):
            if (self.Get_WkExists(ix)):
                self.worker_to_run = ix
                self.first_worker = ix  # back up this value so we can restart
                wk_print("starting worker set to %s" % self.Get_WkStrId(ix))
                break
        # --- debug section
        # scan through each worker print the function pointer
        # debug_tag wk_print("here\n")
        # debug_tag wk_print( cfgseq.dict_funcp_ssWorkers[self.WorkerLUT[22][0]] )
        # debug_tag wk_print("\nhere\n")
        return

    #########################################
    # Method to stop the current worker
    #########################################
    def StopAllWorkers(self):
        wk_print("Stopping workers...")
        # this will take a while to trickle through the state machines
        # and reflect its state, this is no bad thing as the gui should
        # be able to show progress of stopping the active worker
        # until then block the user from pressing the start button again
        self.WaitForStop = True

        unique_wk_instances = []  # so we only stop each unique instance once

        # go through our list of workers and stop all the ones that
        # are running
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)
                if worker not in unique_wk_instances:
                    wk_print("Stopping %s" % self.Get_WkStrId(ix))
                    unique_wk_instances.append(worker)
                    worker.Stop()
        return

    ################################################################
    # Set the instance on all occurrences of this worker
    ################################################################
    def Set_Instance(self, i_workerId, i_instance):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker
        # as started and running
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # only do this once for each unique worker
                if self.Get_WkInstance(ix) == 0:
                    # we have a match so save the instance value
                    self.WorkerLUT[ix][Lix.instance] = i_instance
        return

    ################################################################
    # Start the worker thread running
    # if it has already started and is currently paused then
    # make it continue
    ################################################################
    def Set_WkRunning(self, i_workerId):
        # make sure the worker exists
        assert (self.Get_WkExists(i_workerId)), (
                    "this worker does not exist %d %s" % i_workerId % self.Get_WkStrId(i_workerId))
        started = True
        # check if this worker has already been started
        if not self.WorkerLUT[i_workerId][Lix.running]:
            # debug_tag wk_print("worker not running")
            # get it running
            self.our_worker = self.Get_WkInstance(i_workerId)  # invokes worker method MainWorkerThread
            try:
                self.our_worker.start()  # invokes worker method run()
            except RuntimeError:
                print("Could not start worker [%s]" % self.Get_WkStrId(i_workerId))
                print("")
                started = False
            except:
                print("Could not start worker %s"  % self.Get_WkStrId(i_workerId))
                started = False

            if started:
                # save the new state of this worker in our LUT
                self.Set_RunNFlagWorker(i_workerId, True)
                # now kick it and tell it to get on with it
                # give it a chance to initialise
                time.sleep(1)
                self.our_worker.set_Continue()
        elif ((self.Get_IsWorkerRunning(i_workerId)) and (self.Get_IsWorkerPaused(i_workerId))):
            # ensure our worker is up to date
            if self.our_worker != self.Get_WkInstance(i_workerId):
                self.our_worker = self.Get_WkInstance(i_workerId)
            # wake up this worker
            self.our_worker.set_Continue()
            self.Set_RunNFlagWorker(i_workerId, True)
        if started:
            # update the worker that has focus
            self.worker_to_run = i_workerId
            wk_print("worker running %s" % self.Get_WkStrId(i_workerId))
        return started

    ################################################################
    # Get all our workers to shutdown nicely because we are just
    # about to power off
    ################################################################
    def Set_WkCancelAll(self):
        # go through our list of workers and cancel all the ones that
        # are running

        unique_wk_instances = []  # so we only stop each unique instance once

        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)
                if worker not in unique_wk_instances:
                    unique_wk_instances.append(worker)
                    worker.set_Cancel()
                self.Set_CancelFlagWorker(ix)

    ################################################################
    # Set the cancel flag for all occurances of this worker
    # in our state machine
    ################################################################
    def Set_CancelFlagWorker(self, i_workerId):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker
        # as cancelled
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so set the flags
                self.WorkerLUT[ix][
                    Lix.cancelled] = True  # debug_tag wk_print("", self.Get_WkStrId(ix), " [", ix, "] cancelled")
        return

    ################################################################
    # Set/clear the running flag for all occurrences of this worker
    # in our state machine
    ################################################################
    def Set_RunNFlagWorker(self, i_workerId, i_bRunState):
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and set all appearances of this worker
        # as started and running also clear the paused flag
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so set the flags
                self.WorkerLUT[ix][Lix.running] = i_bRunState
                if i_bRunState:
                    self.WorkerLUT[ix][Lix.paused] = False
                else:
                    self.WorkerLUT[ix][Lix.paused] = True  
                    # debug_tag wk_print("", self.Get_WkStrId(ix), " [", ix, "] is running")
        return

    ################################################################
    # Check if our worker is running
    # returns true if the worker is running, false if it has not started
    # or if it does not exist
    #
    # ***Note*** that a worker may be running but may be paused.
    #
    ################################################################
    def Get_IsWorkerRunning(self, i_workerId):
        if not self.Get_WkExists(i_workerId):
            return False
        return self.WorkerLUT[i_workerId][Lix.running]

    ################################################################
    # Check if our worker is paused
    # returns true if the worer is paused (or does not exist),
    # false if active
    ################################################################
    def Get_IsWorkerPaused(self, i_workerId):
        # first check if it exists and is running
        if (self.Get_WkExists(i_workerId)) and (self.WorkerLUT[i_workerId][Lix.running]):
            # grab the instance
            the_worker = self.Get_WkInstance(i_workerId)
            current_state = the_worker.get_IsPaused()
        else:
            wk_print("worker not available [ %s ]" % self.Get_WkStrId(i_workerId))
            return True
        worker_Id = self.Get_WkStrId(i_workerId)
        # scan through our LUT and update the state of this worker
        for ix in range(self.maxWorkers):
            if worker_Id == self.Get_WkStrId(ix):
                # we have a match so check the flags
                self.WorkerLUT[ix][Lix.paused] = current_state
        return current_state

    ################################################################
    # Check if all our workers are paused
    ################################################################
    def Get_AreWorkersPaused(self):
        # hope for the best
        status = True
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.WorkerLUT[ix][Lix.cancelled] == False):
                worker = self.Get_WkInstance(ix)  # get the worker instance
                wkPauseState = worker.get_IsPaused()  # get the current state of pause
                self.WorkerLUT[ix][Lix.paused] = wkPauseState  # update our flag
                if not wkPauseState:
                    status = False
        return status

    ################################################################
    # Test whether a worker that was once running has now been
    # cancelled
    ################################################################
    def Get_IsWorkerCancelled(self, i_workerId):
        # first check whether it is or was once running
        if (self.WorkerLUT[i_workerId][Lix.running]) and (self.Get_WkExists(i_workerId)):
            the_worker = self.Get_WkInstance(i_workerId)
            current_state = the_worker.get_Cancelled()
        else:
            return False
        worker = self.Get_WkStrId(i_workerId)
        # scan through our LUT and update the state of this worker
        for ix in range(self.maxWorkers):
            if worker == self.Get_WkStrId(ix):
                # we have a match so check the flags
                self.WorkerLUT[ix][Lix.cancelled] = current_state
        return current_state

    ################################################################
    # Test whether all the workers have been told to cancel
    ################################################################
    def Get_AreAllCancelled(self):
        for ix in range(self.maxWorkers):
            if (self.WorkerLUT[ix][Lix.running]) and (self.Get_WkExists(ix)):
                the_worker = self.Get_WkInstance(ix)
                if not the_worker.get_Cancelled():
                    return False
        return True

    ################################################################
    # Get the worker instance
    ################################################################
    def Get_WkInstance(self, i_workerId):
        assert self.Get_WkExists(i_workerId), ("this worker does not exist %d %s" % i_workerId % self.Get_WkStrId(i_workerId))
        return self.WorkerLUT[i_workerId][Lix.instance]

    ################################################################
    # Get the worker string id
    ################################################################
    def Get_WkStrId(self, i_workerId):
        return self.WorkerLUT[i_workerId][Lix.id]

    ################################################################
    # Test if the worker exists
    ################################################################
    def Get_WkExists(self, i_workerId):
        return self.WorkerLUT[i_workerId][Lix.exists]

    ################################################################
    # Get the next worker
    ################################################################
    def Get_NextWorker(self, i_workerId):
        # debug_tag wk_print("Entry: Get_NextWorker(%d)" % i_workerId)
        # if we are at the last worker then go back to the start
        next_worker = i_workerId + 1

        if next_worker == self.maxWorkers:
            wk_print("max workers = %d" % self.maxWorkers)
            return self.first_worker

        for ix in range(next_worker, self.maxWorkers):
            if self.Get_WkExists(ix):
                # debug_tag wk_print("Next worker is %s" % self.Get_WkStrId(ix))
                return ix
            else:
                wk_print("skip [%s]" % self.Get_WkStrId(ix))
        # if we are here it means that we got to the end of the list and the
        # worker didn't exist so back to the start
        return self.first_worker

    ################################################################
    # Test whether our current worker has done its job and the next
    # one needs to be started. If so then kick that off.
    ################################################################
    def Check_NextWorker(self):
        # debug_tag wk_print("Entry: Check_NextWorker()")
        # don't run this method if we are about to shut down
        if not self.shuttingDown:
            if self.Get_IsWorkerPaused(self.worker_to_run):
                # wk_print("%s is done" % self.Get_WkStrId(self.worker_to_run) )
                # our current worker has nothing more to do so get the next one going
                next_worker = self.Get_NextWorker(self.worker_to_run)
                # debug_tag wk_print("%s next worker" % self.Get_WkStrId(next_worker))
                # if we've gone back round the loop then reset the gui state machine
                # but don't start the worker until the user presses the button
                if next_worker == self.first_worker:
                    self.sequence_started = False
                    wk_print("back round to %s worker" % self.first_worker)
                    self.worker_to_run = next_worker
                    # tell everyone to go back to the initial state
                    self.StopAllWorkers()
                    self.StateUpdate(PsCtrlState.Init)
                    return
                else:
                    WkStrId = self.Get_WkStrId(next_worker)
                    wk_print("kicking off next worker [%s]" % self.Get_WkStrId(next_worker))
                    # Update the worker to run and kick it off
                    if self.Set_WkRunning(next_worker):
                        print("Worker started successfully")

        return



    def MakeExpIdTag(self, i_tag):
        idtagfile = ("%s%s" % (cms.CmsFilenames.id_tag_path, cms.CmsFilenames.id_tag))

        # If file already exists, delete it
        if os.path.isfile(idtagfile):
            os.remove(idtagfile)

        # now save the file with the tag
        with open(idtagfile, "w") as idf:
            idf.write("Experiment ID: %s" % i_tag)
            idf.flush()
            idf.close()



##
# Helper method, Debug print with worker id
#
def wk_print(dbg_string):
    ##
    # Debugging method
    # Print debug output with an identifier prepended for this module.
    # But only print when variable dbg_spam_enable is set to true
    # :param dbg_string: The debug string we want to print
    # :return: none
    #
    if dbg_spam_enable:
        print("GUI: %s" % dbg_string)


##
# Helper class to supply meaningful
# names for the gui state machine
class GuiDemoStates():
    faces           = 1
    start_test      = 2
    ins_cartridge   = 3
    blood_flow      = 4
    anim_sample     = 5
    add_sample      = 6
    do_dots         = 7
    show_result     = 8


##
# Helper class to classify test result
# outcome
class TestOutcome():
    unknown = 0
    high    = 1
    low     = 2
    failed  = 3


##
# Helper class for our time constants
# for the sleep method call units are micro seconds
#
class cTime():
    milliS = 1000


##
# Helper class for the worker states.
# Look Up Table indeces
# self.WorkerLUT[ix]=[index, wexists, wrunning, wpaused]
#
class Lix():
    id        = 0   # string that identifies the worker
    instance  = 1   # instantiation pointer
    exists    = 2   # has this worker been coded
    running   = 3   # has this worker been started
    paused    = 4   # has this worker paused
    cancelled = 5   # has this worker died


##
# Helper class of our WorkerSm states
#
class PsCtrlState():
    Init   =  0 # nothing has been pressed
    Started=  1 # Start has been pressed
    AskYN  =  2 # waiting for user input
    Stop   =  3 # stop everythig
    Error  =  4 # something has gone wrong
    EndGame = 5 # buttons disabled, we are shutting down


##
# Helper class translates WorkerSm Api state
# to Debug string
dict_strState = {
    0 : ('Init   '),   # nothing has been pressed
    1 : ('Started'),   # Start has been pressed
    2 : ('AskYN  '),   # waiting for user input
    3 : ('Stop   '),   # stop everythig
    4 : ('Error  '),   # something has gone wrong
    5 : ('EndGame'),   # buttons disabled, we are shutting down
    
}


##
# Kivy workhorse, builds the screen elements based
# on what was specified in the kivy file
#
class GuiBuilder(App):
    dbg = cms.WkDbg(worker_name="GUI builder", spam_enabled=dbg_spam_enable)

    def build(self):
        dbg.w_print("Entry")
        return Builder.load_file("slave.kv")


##
# Main Entry to this module, loads the kivy GUI
# instantiates the workers, as and when they are
# needed
#
if __name__ == "__main__":

    dbg = cms.WkDbg(worker_name="GUI main", spam_enabled=dbg_spam_enable)
    dbg.w_print("API starting")

    # make sure we are not attempting to run this with
    # geek mode enabled
    if cms.gui_geek_mode:
        dbg.w_print("CRITICAL ERROR: please run ./demo.sh")
        quit()

    GuiBuilder().run()

# EOF
