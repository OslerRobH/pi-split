#!/usr/bin/env python3

# Microchip I2C GPIO expander

import common
fake_rpi_gpio = common.DebugFlags.on_dev_pc

import cp2120   # SPI to I2C bridge
import bioc     # target hardware map

# set to True to enable debug output, set to False to disable it
dbg_mcp23017 = False



class mcp23017():


    # our chip I2C addresses
    __U1_out__ = bioc.BiocI2cAddr.MCP23017_U1
    __U2_out__ = bioc.BiocI2cAddr.MCP23017_U2
    __U3_out__ = bioc.BiocI2cAddr.MCP23017_U3
    __U4_in__  = bioc.BiocI2cAddr.MCP23017_U4

    def __init__(self):
        self.i2c = cp2120.octet_i2c
        self.enabled = False
        return


    ###################################
    # Activate this driver
    ###################################
    def EnableDrv(self):
        self.__config_chips()

    ###################################
    # Read from ports
    ###################################
    def RdPorts(self, addr):
        """read ports A & B from a chip(address)"""
        assert(self.enabled), "driver not enabled"

        dbg_print("Entry RdPorts")
        # 3.2.1 Data Sheet A special mode (Byte mode with IOCON.BANK = 0)
        # causes   the   address   pointer   to   toggle   between
        # associated A/B register pairs. For example, if the BANK
        # bit is cleared and the Address Pointer is initially set to
        # address 12h (GPIOA) or 13h (GPIOB), the pointer will
        # toggle  between  GPIOA  and  GPIOB.
        packet = [Regs.IODIRA] # prevent the toggle bullocks

        results = self.i2c.RdAfterWr(addr, packet, addr, 22)
        abread = []
        if len(results) == 22:
            abread.append(results[0x13])
            abread.append(results[0x12])
        else:
            print("ERROR: Expander Read")
        dbg_print(results)
        dbg_print(abread)
        dbg_print("Exit RdPorts")
        return abread


    ###################################
    # Write to ports
    ###################################
    def WrPorts(self, addr, portA, portB):
        """write data to portA and portB"""
        assert(self.enabled), "driver not enabled"

        dbg_print(("Entry WrPorts(0x%x A=0x%x B=0x%x)" % (addr, portA, portB)))
        packet=[Regs.GPIOA, portA, portB]

        self.i2c.WrI2C(addr, packet)

        dbg_print("Exit WrPorts")


    ###################################
    # configure the chips hanging off
    # the I2C bus, set the addressing
    # mode and port direction
    ###################################
    def __config_chips(self):
        """Configure all the expanders on this hardware"""
        self.enabled = True
        if fake_rpi_gpio:
            return

        dbg_print("Configuring MCP23017 chips")

        #todo do this automagically from a LUT

        dbg_print("Configuring U1 as output @ address 0x%x" % self.__U1_out__)
        self.__config_addr(self.__U1_out__)
        self.__set_port_dir(self.__U1_out__, False)
        dbg_print("Configuring U2 as output @ address 0x%x" % self.__U2_out__)
        self.__config_addr(self.__U2_out__)
        self.__set_port_dir(self.__U2_out__, False)
        dbg_print("Configuring U3 as output @ address 0x%x" % self.__U3_out__)
        self.__config_addr(self.__U3_out__)
        self.__set_port_dir(self.__U3_out__, False)
        dbg_print("Configuring U4 as input @ address 0x%x" % self.__U4_in__)
        self.__config_addr(self.__U4_in__)
        self.__set_port_dir(self.__U4_in__, True)
        return

    ###################################
    # configure how we are addressing
    # this chip
    ###################################
    def __config_addr(self, addr):
        """configure the addressing mode for the expanders"""
        assert(self.enabled), "driver not enabled"
        iocon_cfg  = 0x00   # default value
        iocon_cfg += 0x10   # enable slew rate
        packet_a = [Regs.IOCONA, iocon_cfg]
        packet_b = [Regs.IOCONB, iocon_cfg]
        self.i2c.WrI2C(addr,packet_a)
        self.i2c.WrI2C(addr,packet_b)

    ###################################
    # set port direction of the chip
    # Note: currently both ports on the
    # chip will be configured to the
    # same direction
    ###################################
    def __set_port_dir(self, addr, b_input):
        """configure whether the expander is to function as input or output"""
        assert(self.enabled), "driver not enabled"
        direction = IODIR.OUTPUT
        if b_input:
            direction = IODIR.INPUT
        packet_a = [Regs.IODIRA, direction]
        packet_b = [Regs.IODIRB, direction]
        self.i2c.WrI2C(addr, packet_a)
        self.i2c.WrI2C(addr, packet_b)


###################################
# Debug print
###################################
def dbg_print(dbg_string):
    if dbg_mcp23017:
        print(dbg_string)
    return


# The  GPIO  module is a general purpose, 16-bit wide,
# bidirectional port that is functionally split into two
# 8-bit wide ports.
# The GPIO module contains the data ports (GPIOn),
# internal pull-up resistors and the output latches
# (OLATn).
#
# Reading the GPIOn register reads the value on the
# port. Reading the OLATn register only reads the
# latches, not the actual value on the port.
# Writing to the GPIOn register actually causes a write to
# the latches (OLATn). Writing to the OLATn register
# forces the associated output drivers to drive to the level
# in OLATn.


class Regs():
    IODIRA   = 0x00
    IPOLA    = 0x02
    GPINTENA = 0x04
    DEFVALA  = 0x06
    INTCONA  = 0x08
    IOCONA   = 0x0A
    GPPUA    = 0x0C
    INTFA    = 0x0E
    INTCAPA  = 0x10
    GPIOA    = 0x12
    OLATA    = 0x14

    IODIRB   = 0x01
    IPOLB    = 0x03
    GPINTENB = 0x05
    DEFVALB  = 0x07
    INTCONB  = 0x09
    IOCONB   = 0x0B
    GPPUB    = 0x0D
    INTFB    = 0x0F
    INTCAPB  = 0x11
    GPIOB    = 0x13
    OLATB    = 0x15


class IODIR():
    OUTPUT = 0x00
    INPUT  = 0xFF



# this is here so we only reference one instance of this module
gpio_expander = mcp23017()
