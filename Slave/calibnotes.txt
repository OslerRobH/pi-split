Apollo:
Version: c69a1b3
============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 703 out of 1820 control entries [0.39%]
Over pressure warnings 0 [0.00%]
Under pressure warnings 703 [0.39%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.00%]
Over pressure warnings 0 [0.00%]
Under pressure warnings 0 [0.00%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.00%]
Over Pressure warnings 0 [0.00%]
Under Pressure warnings 0 [0.00%]
PID settings: udv = True Kp= 10.0 Ki=  0.0 sv=  5.5 min=  0.0 max=100.0 wg= 25.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 196 out of 1820 control entries [10.8%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 196 [10.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp= 10.0 Ki=  0.0 sv=  5.5 min=  0.0 max=100.0 wg= 25.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 220 out of 1820 control entries [12.1%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 220 [12.1%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp= 10.0 Ki=  0.0 sv=  5.5 min=  0.0 max=100.0 wg= 25.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 149 out of 1820 control entries [8.2%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 149 [8.2%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp= 13.0 Ki=  0.0 sv=  5.4 min=  0.0 max=100.0 wg= 25.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 1633 out of 1820 control entries [89.7%]
Over pressure warnings 1619 [89.0%]
Under pressure warnings 14 [0.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=  0.0 sv=  5.3 min=  0.0 max=  6.0 wg= 20.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 113 out of 1820 control entries [6.2%]
Over pressure warnings 26 [1.4%]
Under pressure warnings 87 [4.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=  0.0 sv=  5.3 min=  0.1 max=  6.0 wg= 20.0 s_dump=  0.1 t_dump= -2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 641 out of 1820 control entries [35.2%]
Over pressure warnings 606 [33.3%]
Under pressure warnings 35 [1.9%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=  0.0 sv=  5.3 min=  0.1 max=  7.0 wg= 20.0 s_dump=  0.1 t_dump= -2.2
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 409 out of 1820 control entries [22.5%]
Over pressure warnings 348 [19.1%]
Under pressure warnings 61 [3.4%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=0.0 sv=5.3 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.2
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 76 out of 1820 control entries [4.2%]
Over pressure warnings 12 [0.7%]
Under pressure warnings 64 [3.5%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=0.0 sv=5.3 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 67 out of 1820 control entries [3.7%]
Over pressure warnings 21 [1.2%]
Under pressure warnings 46 [2.5%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=-1.0 sv=5.3 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 82 out of 1820 control entries [4.5%]
Over pressure warnings 24 [1.3%]
Under pressure warnings 58 [3.2%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=-2.0 sv=5.3 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 4.80 t_pos_cap= 4.80 t_neg=-4.80
============================================


Delphi


Ares

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 194 out of 1820 control entries [10.7%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 194 [10.7%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-1.0 Ki=0.0 sv=5.0 min=0.1 max=10.0 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 78 out of 1820 control entries [4.3%]
Over pressure warnings 40 [2.2%]
Under pressure warnings 38 [2.1%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=0.0 sv=5.0 min=0.1 max=10.0 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 122 out of 1820 control entries [6.7%]
Over pressure warnings 94 [5.2%]
Under pressure warnings 28 [1.5%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-13.0 Ki=-1.0 sv=5.0 min=0.1 max=10.0 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 82 out of 1820 control entries [4.5%]
Over pressure warnings 48 [2.6%]
Under pressure warnings 34 [1.9%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.0 sv=5.0 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 114 out of 1820 control entries [6.3%]
Over pressure warnings 82 [4.5%]
Under pressure warnings 32 [1.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-11.0 Ki=-1.0 sv=5.0 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 100 out of 1820 control entries [5.5%]
Over pressure warnings 68 [3.7%]
Under pressure warnings 32 [1.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.0 sv=5.0 min=0.1 max=6.5 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 91 out of 1820 control entries [5.0%]
Over pressure warnings 59 [3.2%]
Under pressure warnings 32 [1.8%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.0 sv=5.0 min=0.1 max=6.5 wg=20.0 s_dump=0.2 t_dump=-2.0
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 38 out of 1820 control entries [2.1%]
Over pressure warnings 2 [0.1%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.0 sv=5.0 min=0.1 max=5.0 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 72 out of 1820 control entries [4.0%]
Over pressure warnings 36 [2.0%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.0 sv=5.0 min=0.1 max=5.5 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 70 out of 1820 control entries [3.8%]
Over pressure warnings 34 [1.9%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.5 sv=5.0 min=0.1 max=5.5 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 80 out of 1820 control entries [4.4%]
Over pressure warnings 44 [2.4%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.5 Ki=-1.5 sv=5.0 min=0.1 max=5.0 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 120 out of 1820 control entries [6.6%]
Over pressure warnings 84 [4.6%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.5 sv=5.0 min=0.1 max=5.5 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================

============================================
           FLUIDIC CONTROL STATS
Warning tolerance at 20%
Var Pressure Warnings 112 out of 1820 control entries [6.2%]
Over pressure warnings 76 [4.2%]
Under pressure warnings 36 [2.0%]
- - - - - - - - - - - - - - - - - - - - - -
(+) Pressure Warnings 0 out of 1820 control entries [0.0%]
Over pressure warnings 0 [0.0%]
Under pressure warnings 0 [0.0%]
- - - - - - - - - - - - - - - - - - - - - -
(-) Warnings 0 out of 1820 control entries [0.0%]
Over Pressure warnings 0 [0.0%]
Under Pressure warnings 0 [0.0%]
PID settings: udv = True Kp=-12.0 Ki=-1.5 sv=5.0 min=0.1 max=5.5 wg=20.0 s_dump=0.1 t_dump=-1.9
t_pos= 3.50 t_pos_cap= 3.00 t_neg=-4.80
============================================
